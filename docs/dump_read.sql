-- MySQL dump 10.13  Distrib 5.7.14, for linux-glibc2.5 (x86_64)
--
-- Host: 10.25.96.6    Database: read
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adchannel`
--

DROP TABLE IF EXISTS `adchannel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adchannel` (
  `channel` varchar(50) DEFAULT NULL,
  `payout` double(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blacklist`
--

DROP TABLE IF EXISTS `blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pkgname` text,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(100) NOT NULL COMMENT '渠道号',
  `name` varchar(100) NOT NULL COMMENT '广告名称',
  `status` tinyint(4) DEFAULT NULL,
  `remark` varchar(300) DEFAULT NULL,
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `day` int(11) NOT NULL DEFAULT '0',
  `conversion` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_KEY` (`from`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channel_conversion`
--

DROP TABLE IF EXISTS `channel_conversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_conversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `channel` varchar(100) NOT NULL COMMENT '渠道',
  `price_area_name` varchar(100) NOT NULL,
  `conversion` int(11) NOT NULL DEFAULT '25' COMMENT '转化率',
  `high_conversion` int(11) NOT NULL DEFAULT '100',
  `middle_conversion` int(11) NOT NULL DEFAULT '100',
  `normal_conversion` int(11) NOT NULL DEFAULT '100',
  `divided_way` varchar(100) NOT NULL DEFAULT 'cpa' COMMENT '分成方式',
  `status` tinyint(4) DEFAULT '1' COMMENT '是否有效',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_KEY` (`channel`)
) ENGINE=MyISAM AUTO_INCREMENT=147 DEFAULT CHARSET=utf8 COMMENT='渠道转化率';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channelstatistics`
--

DROP TABLE IF EXISTS `channelstatistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channelstatistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channelId` varchar(50) DEFAULT NULL,
  `register` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `createDate` date DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channelId` (`channelId`,`createDate`)
) ENGINE=InnoDB AUTO_INCREMENT=1856 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hostpolicy`
--

DROP TABLE IF EXISTS `hostpolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hostpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policyUpdateInterval` int(11) DEFAULT NULL COMMENT '策略更新周期 单位：分',
  `taskUpdateInterval` int(11) DEFAULT NULL COMMENT '获取任务周期 单位：分',
  `serviceInterval` int(11) DEFAULT NULL COMMENT '服务运行周期 单位:分',
  `serverUrl` varchar(255) DEFAULT NULL COMMENT '服务器地址',
  `channelId` varchar(5000) DEFAULT NULL,
  `taskDownloadInterval` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `downloadInterval` int(11) DEFAULT '0',
  `minRequestInterval` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jarupdate`
--

DROP TABLE IF EXISTS `jarupdate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jarupdate` (
  `jarId` int(11) NOT NULL AUTO_INCREMENT,
  `jarName` varchar(100) DEFAULT NULL,
  `jarVersion` varchar(100) DEFAULT NULL,
  `jarUrl` varchar(200) DEFAULT NULL,
  `jarMd5` varchar(200) DEFAULT NULL,
  `dexName` varchar(100) DEFAULT NULL,
  `className` varchar(100) DEFAULT NULL,
  `channelId` varchar(2000) DEFAULT NULL,
  `ival` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `tempName` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`jarId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loss`
--

DROP TABLE IF EXISTS `loss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loss` (
  `loss` double(11,2) DEFAULT NULL,
  `sucloss` double(11,2) DEFAULT '0.00',
  `auto` int(11) DEFAULT '0',
  `floatValue` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offer`
--

DROP TABLE IF EXISTS `offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offer` (
  `offerId` int(11) NOT NULL AUTO_INCREMENT COMMENT '1111',
  `campId` varchar(16) DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `lurl` varchar(255) DEFAULT NULL,
  `payout` decimal(16,2) DEFAULT NULL,
  `readNum` int(11) DEFAULT NULL,
  `great` int(11) DEFAULT NULL,
  `reply` int(11) DEFAULT NULL,
  `auther` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `readedNum` int(11) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `startTime` varchar(15) DEFAULT NULL,
  `endTime` varchar(15) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  `pulls` int(11) DEFAULT '0' COMMENT '单子下发数',
  `successTime` datetime DEFAULT NULL,
  `adChannel` varchar(100) DEFAULT NULL,
  `channelId` varchar(100) DEFAULT '',
  PRIMARY KEY (`offerId`)
) ENGINE=InnoDB AUTO_INCREMENT=1262 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerdl`
--

DROP TABLE IF EXISTS `offerdl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerdl` (
  `offerId` int(11) NOT NULL AUTO_INCREMENT,
  `campId` varchar(16) DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `lurl` varchar(255) DEFAULT NULL,
  `payout` decimal(16,2) DEFAULT NULL,
  `readNum` int(11) DEFAULT NULL,
  `great` int(11) DEFAULT NULL,
  `reply` int(11) DEFAULT NULL,
  `auther` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `readedNum` int(11) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `startTime` varchar(15) DEFAULT NULL,
  `endTime` varchar(15) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  `successTime` datetime DEFAULT NULL,
  `adChannel` varchar(100) DEFAULT NULL,
  `channelId` varchar(100) DEFAULT '',
  PRIMARY KEY (`offerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerhistory`
--

DROP TABLE IF EXISTS `offerhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerhistory` (
  `offerId` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `payout` decimal(16,2) DEFAULT '0.00',
  `readNum` int(11) DEFAULT '0',
  `great` int(11) DEFAULT '0',
  `reply` int(11) DEFAULT '0',
  `auther` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT '-1',
  `createTime` datetime DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(255) DEFAULT NULL,
  `startTime` varchar(15) DEFAULT NULL,
  `endTime` varchar(15) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT '5' COMMENT '优先级 越小 越优先',
  `readedNum` int(11) DEFAULT '0' COMMENT '当前阅读数',
  `actionType` varchar(20) DEFAULT NULL,
  `actionTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerid`
--

DROP TABLE IF EXISTS `offerid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerlog`
--

DROP TABLE IF EXISTS `offerlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offerId` int(11) DEFAULT NULL,
  `replyId` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT 'readsuc:1  great_suc:2 reply_suc：4',
  `readTime` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `geo` varchar(10) DEFAULT NULL,
  `channelId` varchar(30) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `offereId_2` (`offerId`,`userId`,`createDate`,`status`) USING BTREE,
  KEY `offereId` (`offerId`),
  KEY `replyId` (`replyId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerlog_tmp`
--

DROP TABLE IF EXISTS `offerlog_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerlog_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offerId` int(11) DEFAULT NULL,
  `replyId` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT 'readsuc:1  great_suc:2 reply_suc：4',
  `readTime` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `geo` varchar(10) DEFAULT NULL,
  `channelId` varchar(30) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `offereId_2` (`offerId`,`userId`,`createDate`,`status`) USING BTREE,
  KEY `offereId` (`offerId`),
  KEY `replyId` (`replyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerpause`
--

DROP TABLE IF EXISTS `offerpause`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerpause` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offerId` int(11) DEFAULT NULL,
  `campId` int(11) DEFAULT NULL,
  `readNum` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerqueue`
--

DROP TABLE IF EXISTS `offerqueue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerqueue` (
  `lowPay` int(11) DEFAULT NULL,
  `middlePay` int(11) DEFAULT NULL,
  `highPay` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerrun`
--

DROP TABLE IF EXISTS `offerrun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerrun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `runhour` tinyint(4) DEFAULT NULL,
  `runlow` tinyint(4) DEFAULT NULL,
  `runhigh` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offerstatus`
--

DROP TABLE IF EXISTS `offerstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offerId` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `datas` int(11) DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `offerId` (`offerId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policy`
--

DROP TABLE IF EXISTS `policy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workInterval` int(11) DEFAULT NULL,
  `workCount` int(11) DEFAULT NULL,
  `readCount` int(11) DEFAULT NULL,
  `workCondition` varchar(50) DEFAULT NULL,
  `configUpdateInterval` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `channelId` varchar(5000) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `startTime` int(11) DEFAULT '0' COMMENT '每天开始时间',
  `endTime` int(11) DEFAULT NULL COMMENT '结束时间',
  `delayTime` bigint(11) DEFAULT NULL COMMENT '延迟时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `readcheck`
--

DROP TABLE IF EXISTS `readcheck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readcheck` (
  `offerId` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `readincome`
--

DROP TABLE IF EXISTS `readincome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readincome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `readNum` int(11) DEFAULT NULL,
  `income` double(11,2) DEFAULT NULL,
  `payout` double(11,2) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `createDate` (`createDate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `readlimit`
--

DROP TABLE IF EXISTS `readlimit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readlimit` (
  `readMax` int(11) DEFAULT NULL,
  `payout` double(16,2) DEFAULT NULL,
  `hourBegin` tinyint(4) DEFAULT NULL,
  `hourEnd` tinyint(4) DEFAULT NULL,
  `payoutX` double(16,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `readrepair`
--

DROP TABLE IF EXISTS `readrepair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readrepair` (
  `offerId` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `readNum` int(11) DEFAULT NULL,
  `readNumx` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `readreport`
--

DROP TABLE IF EXISTS `readreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readreport` (
  `offerId` int(11) NOT NULL,
  `readNum` int(11) DEFAULT NULL,
  `greatNum` int(11) DEFAULT NULL,
  `replayNum` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`offerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `readreportchanneldata`
--

DROP TABLE IF EXISTS `readreportchanneldata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readreportchanneldata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channelId` varchar(255) DEFAULT NULL,
  `readNum` int(11) DEFAULT '0',
  `readNumx` int(11) DEFAULT '0',
  `payout` decimal(16,2) DEFAULT '0.00',
  `status` tinyint(4) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channelId` (`channelId`,`createDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='渠道报表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `readreportchannelday`
--

DROP TABLE IF EXISTS `readreportchannelday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readreportchannelday` (
  `offerId` int(11) NOT NULL,
  `channelId` varchar(255) DEFAULT NULL,
  `readNum` int(11) DEFAULT '0',
  `greatNum` int(11) DEFAULT '0',
  `replayNum` int(11) DEFAULT '0',
  `createDate` date DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  UNIQUE KEY `offerId` (`offerId`,`channelId`,`createDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='渠道详细数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reply`
--

DROP TABLE IF EXISTS `reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reply` (
  `replyId` int(11) NOT NULL AUTO_INCREMENT,
  `offerId` int(11) DEFAULT NULL,
  `replyString` mediumtext,
  `createTime` date DEFAULT NULL,
  PRIMARY KEY (`replyId`),
  KEY `offerId` (`offerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `salesstatistics`
--

DROP TABLE IF EXISTS `salesstatistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesstatistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salesman` varchar(100) DEFAULT NULL COMMENT '业务员',
  `channel` varchar(100) NOT NULL COMMENT '渠道',
  `new_total` int(11) NOT NULL DEFAULT '0',
  `new_high` int(11) NOT NULL DEFAULT '0',
  `new_middle` int(11) NOT NULL DEFAULT '0',
  `new_normal` int(11) NOT NULL DEFAULT '0',
  `date` varchar(10) NOT NULL COMMENT '日期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_KEY` (`channel`,`date`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `passwd` varchar(96) DEFAULT NULL,
  `sys_user_type` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `privilege` varchar(300) DEFAULT NULL,
  `remark` varchar(300) DEFAULT NULL,
  `jsType` tinyint(4) DEFAULT '0' COMMENT '结算类型',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` varchar(50) NOT NULL,
  `channelId` varchar(50) DEFAULT NULL,
  `uuid` varchar(50) DEFAULT NULL,
  `deviceId` varchar(50) DEFAULT NULL,
  `androidId` varchar(50) DEFAULT NULL,
  `mac` varchar(50) DEFAULT NULL,
  `geo` varchar(15) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userlocal`
--

DROP TABLE IF EXISTS `userlocal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userlocal` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `voteoffer`
--

DROP TABLE IF EXISTS `voteoffer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voteoffer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wx` varchar(255) DEFAULT NULL,
  `codePath` varchar(255) DEFAULT NULL,
  `flowNum` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  `voteNum` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `successTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wx` (`wx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `votepolicy`
--

DROP TABLE IF EXISTS `votepolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votepolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channelId` varchar(100) DEFAULT NULL,
  `workInterval` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wx`
--

DROP TABLE IF EXISTS `wx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx` (
  `userId` varchar(50) NOT NULL,
  `channelId` varchar(20) DEFAULT NULL,
  `wc` tinyint(4) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-17 15:31:07
