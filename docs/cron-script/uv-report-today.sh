#!/bin/bash
#定时任务统计uv数据，每15分钟执行一次，生成数据到user_report表中
#crontab */15 * * * * uv-report-today.sh 1

function init()
{
  theDay2=`date +%Y-%m-%d`
}
function executeSqlPush()
{
  if [[ "$1" = "" ]]
  then
    cat | mysql -h'127.0.0.1' -P'23456' -u'root' -p'ySwOHv0pK2kc6qGz' pushwebservice -N --local-infile
  else
    echo "$1" | mysql -h'127.0.0.1' -P'23456' -u'root' -p'ySwOHv0pK2kc6qGz' pushwebservice -N --local-infile
  fi
}
function executeSqlReading()
{
  sql="$1"

  if [[ "$sql" = "" ]]
  then
    cat | mysql -h'127.0.0.1' -P'23456' -u'root' -p'ySwOHv0pK2kc6qGz' reading -N --local-infile
  else
    echo "$sql" | mysql -h'127.0.0.1' -P'23456' -u'root' -p'ySwOHv0pK2kc6qGz' reading -N --local-infile
  fi
}
function getChannel()
{
    sql="select \`from\` from \`user\` where DATE(lock_begin)='${theDay2}' GROUP BY \`from\`;"
    executeSqlPush "$sql"
}
function getRegister()
{
    channel="$1"
    sql="SELECT count(*) FROM \`user\` WHERE DATE(gmt_create)='${theDay2}' and \`from\`='${channel}';"
    executeSqlPush "$sql"
}
# 统计前N台注册数
function getNRegister() {
    channel="$1"
    sql="SELECT COUNT(*) FROM \`user\` WHERE lock_status=3 and DATE(lock_begin)='${theDay2}' and \`from\`='${channel}'"
    executeSqlPush "$sql"
}
# 统计激活数
function getActive() {
    channel="$1"
    sql="SELECT COUNT(*) FROM \`user\` WHERE lock_status=2 and DATE(lock_begin)='${theDay2}' and \`from\`='${channel}'"
    executeSqlPush "$sql"
}
# 统计DAU
function getDAU() {
    channel="$1"
    sql="SELECT count(*) FROM \`user\` WHERE DATE(gmt_modified)='${theDay2}' and \`from\`='${channel}';"
    executeSqlPush "$sql"
}
# 统计业务包注册数
function getBizRegister() {
    channel="$1"
    sql="select count(*) from \`user\` where channelId='$channel' and DATE(createTime)='$theDay2';"
    executeSqlReading "$sql"
}
#统计业务包日活数
function getBizDAU() {
    channel="$1"
    sql="select count(*) from \`user\` where channelId='$channel' and DATE(updateTime)='$theDay2';"
    executeSqlReading "$sql"
}

function main()
{
    init $1
    getChannel | while read channel; do
      if [[ ${channel} = "NULL" ]]; then
        continue;
      fi
      #salesman=`getSalesman "$channel"`
      register=`getRegister "$channel"`
      nRegister=`getNRegister "$channel"`
      active=`getActive "$channel"`
      DAU=`getDAU "$channel"`
      bizRegister=`getBizRegister "$channel"`
      bizDAU=`getBizDAU "$channel"`

      #入库
      row=$(executeSqlReading "select count(*) from user_report WHERE channel='$channel' AND date='$theDay2';")
      if [[ ${row} -eq 0 ]]; then
        sql="insert into user_report(channel,salesman,register,nRegister,active,DAU,bizRegister,bizDAU,date,status,gmt_create) \
        values('$channel','',$register,$nRegister,$active,$DAU,$bizRegister,$bizDAU,'$theDay2',0,now());"
      else
        sql="update user_report set register=$register,nRegister=$nRegister,active=$active,DAU=$DAU, \
        bizRegister=$bizRegister,bizDAU=$bizDAU,gmt_modified=now() WHERE channel='$channel' AND date='$theDay2';"
      fi
      executeSqlReading "$sql"

      #生成log
      time=`date "+%Y-%m-%d %H:%M:%S"`
      echo "time:"${time} "channel:"${channel} "register:"${register} "nRegister:"${nRegister} "active:"${active} \
      "DAU:"${DAU} "bizRegister:"${bizRegister} "bizDAU:"${bizDAU}>>/home/log/statics/uv-${theDay2}.log
    done
}

main $1