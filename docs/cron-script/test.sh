#!/bin/bash

function init()
{
  theDay="$1"
  if [[ "$theDay" = "" ]]
  then
    theDay=`date +%Y%m%d -d "1 days ago "`
  fi
  theDay2=`date +%Y-%m-%d -d "$theDay"`
}

function main()
{
  init "$1"
  echo 'theDay2='${theDay2}
}

main "$1"