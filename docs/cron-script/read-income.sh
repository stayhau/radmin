#!/bin/bash
#定时任务统计昨天收益，次日0:30执行
#crontab 30 0 * * * read-income.sh

#定时任务统计今日收益，每5分钟执行一次
#crontab */5 * * * * read-income.sh 1

function init()
{
  theDay="$1"
  if [[ "$theDay" = "" ]]
  then
    theDay=`date +%Y%m%d -d "1 days ago "`
  fi
  theDay2=`date +%Y-%m-%d -d "$theDay"`
}

function executeSql()
{
  sql="$1"
  if [[ "$sql" = "" ]]
  then
    cat | mysql -h'127.0.0.1' -P'23456' -u'root' -p'ySwOHv0pK2kc6qGz' reading -N --local-infile
  else
    echo "$sql" | mysql -h'127.0.0.1' -P'23456' -u'root' -p'ySwOHv0pK2kc6qGz' reading -N --local-infile
  fi
}

function main()
{
    init "$1"

    sql="select IFNULL(TRUNCATE(sum(readNum/1000*payout),2),0) totalIncome,IFNULL(SUM(readNum),0) totalRedNum, \
        IFNULL(TRUNCATE(avg(payout),2),0) avgPayout from offer where date(successTime)='$theDay2' and status=5 AND auther='root';"

    executeSql "$sql" | while read income readNum payout; do
        sql="select count(*) from readincome where createDate='$theDay2';"
        row=$(executeSql "$sql")
        if [[ ${row} -eq 0 ]] && [[ ${readNum} -gt 0 ]]; then
            sql="insert into readincome(readNum,income,createDate,payout)values($readNum,$income,'$theDay2',$payout);"
        else
            sql="update readincome set readNum=$readNum,income=$income,payout=$payout where createDate='$theDay2';"
        fi
        executeSql "$sql"
    done

    sql="select IFNULL(SUM(readNum),0) totalRedNum2, IFNULL(TRUNCATE(sum(readNum/1000*payout2),2),0) totalIncome2, \
        IFNULL(TRUNCATE(avg(payout2),2),0) avgPayout2 from offer where date(successTime)='$theDay2' and status=5;"

    executeSql "$sql" | while read readNum2 income2 payout2; do
        sql="select count(*) from readincome where createDate='$theDay2';"
        row=$(executeSql "$sql")
        if [[ ${row} -eq 0 ]] && [[ ${readNum} -gt 0 ]]; then
            sql="insert into readincome(readNum2,createDate,income2,payout2)values($readNum2,'$theDay2',$income2,$payout2);"
        else
            sql="update readincome set readNum2=$readNum2,income2=$income2,payout2=$payout2 where createDate='$theDay2';"
        fi
        executeSql "$sql"
    done
}

main "$1"