/*
SQLyog Ultimate v12.3.1 (64 bit)
MySQL - 5.7.27-0ubuntu0.16.04.1 : Database - user_local
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`user_local` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `user_local`;

/*Table structure for table `userlocal_01` */

DROP TABLE IF EXISTS `userlocal_01`;

CREATE TABLE `userlocal_01` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_02` */

DROP TABLE IF EXISTS `userlocal_02`;

CREATE TABLE `userlocal_02` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_03` */

DROP TABLE IF EXISTS `userlocal_03`;

CREATE TABLE `userlocal_03` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_04` */

DROP TABLE IF EXISTS `userlocal_04`;

CREATE TABLE `userlocal_04` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_05` */

DROP TABLE IF EXISTS `userlocal_05`;

CREATE TABLE `userlocal_05` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_06` */

DROP TABLE IF EXISTS `userlocal_06`;

CREATE TABLE `userlocal_06` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_07` */

DROP TABLE IF EXISTS `userlocal_07`;

CREATE TABLE `userlocal_07` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_08` */

DROP TABLE IF EXISTS `userlocal_08`;

CREATE TABLE `userlocal_08` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_09` */

DROP TABLE IF EXISTS `userlocal_09`;

CREATE TABLE `userlocal_09` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_10` */

DROP TABLE IF EXISTS `userlocal_10`;

CREATE TABLE `userlocal_10` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_11` */

DROP TABLE IF EXISTS `userlocal_11`;

CREATE TABLE `userlocal_11` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_12` */

DROP TABLE IF EXISTS `userlocal_12`;

CREATE TABLE `userlocal_12` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_13` */

DROP TABLE IF EXISTS `userlocal_13`;

CREATE TABLE `userlocal_13` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_14` */

DROP TABLE IF EXISTS `userlocal_14`;

CREATE TABLE `userlocal_14` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_15` */

DROP TABLE IF EXISTS `userlocal_15`;

CREATE TABLE `userlocal_15` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_16` */

DROP TABLE IF EXISTS `userlocal_16`;

CREATE TABLE `userlocal_16` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_17` */

DROP TABLE IF EXISTS `userlocal_17`;

CREATE TABLE `userlocal_17` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_18` */

DROP TABLE IF EXISTS `userlocal_18`;

CREATE TABLE `userlocal_18` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_19` */

DROP TABLE IF EXISTS `userlocal_19`;

CREATE TABLE `userlocal_19` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_20` */

DROP TABLE IF EXISTS `userlocal_20`;

CREATE TABLE `userlocal_20` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_21` */

DROP TABLE IF EXISTS `userlocal_21`;

CREATE TABLE `userlocal_21` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_22` */

DROP TABLE IF EXISTS `userlocal_22`;

CREATE TABLE `userlocal_22` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_23` */

DROP TABLE IF EXISTS `userlocal_23`;

CREATE TABLE `userlocal_23` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_24` */

DROP TABLE IF EXISTS `userlocal_24`;

CREATE TABLE `userlocal_24` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_25` */

DROP TABLE IF EXISTS `userlocal_25`;

CREATE TABLE `userlocal_25` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_26` */

DROP TABLE IF EXISTS `userlocal_26`;

CREATE TABLE `userlocal_26` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_27` */

DROP TABLE IF EXISTS `userlocal_27`;

CREATE TABLE `userlocal_27` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_28` */

DROP TABLE IF EXISTS `userlocal_28`;

CREATE TABLE `userlocal_28` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_29` */

DROP TABLE IF EXISTS `userlocal_29`;

CREATE TABLE `userlocal_29` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_30` */

DROP TABLE IF EXISTS `userlocal_30`;

CREATE TABLE `userlocal_30` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_31` */

DROP TABLE IF EXISTS `userlocal_31`;

CREATE TABLE `userlocal_31` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_32` */

DROP TABLE IF EXISTS `userlocal_32`;

CREATE TABLE `userlocal_32` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_33` */

DROP TABLE IF EXISTS `userlocal_33`;

CREATE TABLE `userlocal_33` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_34` */

DROP TABLE IF EXISTS `userlocal_34`;

CREATE TABLE `userlocal_34` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_35` */

DROP TABLE IF EXISTS `userlocal_35`;

CREATE TABLE `userlocal_35` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_36` */

DROP TABLE IF EXISTS `userlocal_36`;

CREATE TABLE `userlocal_36` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_37` */

DROP TABLE IF EXISTS `userlocal_37`;

CREATE TABLE `userlocal_37` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_38` */

DROP TABLE IF EXISTS `userlocal_38`;

CREATE TABLE `userlocal_38` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_39` */

DROP TABLE IF EXISTS `userlocal_39`;

CREATE TABLE `userlocal_39` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_40` */

DROP TABLE IF EXISTS `userlocal_40`;

CREATE TABLE `userlocal_40` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_41` */

DROP TABLE IF EXISTS `userlocal_41`;

CREATE TABLE `userlocal_41` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_42` */

DROP TABLE IF EXISTS `userlocal_42`;

CREATE TABLE `userlocal_42` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_43` */

DROP TABLE IF EXISTS `userlocal_43`;

CREATE TABLE `userlocal_43` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_44` */

DROP TABLE IF EXISTS `userlocal_44`;

CREATE TABLE `userlocal_44` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_45` */

DROP TABLE IF EXISTS `userlocal_45`;

CREATE TABLE `userlocal_45` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_46` */

DROP TABLE IF EXISTS `userlocal_46`;

CREATE TABLE `userlocal_46` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_47` */

DROP TABLE IF EXISTS `userlocal_47`;

CREATE TABLE `userlocal_47` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_48` */

DROP TABLE IF EXISTS `userlocal_48`;

CREATE TABLE `userlocal_48` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_49` */

DROP TABLE IF EXISTS `userlocal_49`;

CREATE TABLE `userlocal_49` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_50` */

DROP TABLE IF EXISTS `userlocal_50`;

CREATE TABLE `userlocal_50` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_51` */

DROP TABLE IF EXISTS `userlocal_51`;

CREATE TABLE `userlocal_51` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_52` */

DROP TABLE IF EXISTS `userlocal_52`;

CREATE TABLE `userlocal_52` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_53` */

DROP TABLE IF EXISTS `userlocal_53`;

CREATE TABLE `userlocal_53` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_54` */

DROP TABLE IF EXISTS `userlocal_54`;

CREATE TABLE `userlocal_54` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_55` */

DROP TABLE IF EXISTS `userlocal_55`;

CREATE TABLE `userlocal_55` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_56` */

DROP TABLE IF EXISTS `userlocal_56`;

CREATE TABLE `userlocal_56` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_57` */

DROP TABLE IF EXISTS `userlocal_57`;

CREATE TABLE `userlocal_57` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_58` */

DROP TABLE IF EXISTS `userlocal_58`;

CREATE TABLE `userlocal_58` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_59` */

DROP TABLE IF EXISTS `userlocal_59`;

CREATE TABLE `userlocal_59` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_60` */

DROP TABLE IF EXISTS `userlocal_60`;

CREATE TABLE `userlocal_60` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_61` */

DROP TABLE IF EXISTS `userlocal_61`;

CREATE TABLE `userlocal_61` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_62` */

DROP TABLE IF EXISTS `userlocal_62`;

CREATE TABLE `userlocal_62` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_63` */

DROP TABLE IF EXISTS `userlocal_63`;

CREATE TABLE `userlocal_63` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_64` */

DROP TABLE IF EXISTS `userlocal_64`;

CREATE TABLE `userlocal_64` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_65` */

DROP TABLE IF EXISTS `userlocal_65`;

CREATE TABLE `userlocal_65` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_66` */

DROP TABLE IF EXISTS `userlocal_66`;

CREATE TABLE `userlocal_66` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_67` */

DROP TABLE IF EXISTS `userlocal_67`;

CREATE TABLE `userlocal_67` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_68` */

DROP TABLE IF EXISTS `userlocal_68`;

CREATE TABLE `userlocal_68` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_69` */

DROP TABLE IF EXISTS `userlocal_69`;

CREATE TABLE `userlocal_69` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_70` */

DROP TABLE IF EXISTS `userlocal_70`;

CREATE TABLE `userlocal_70` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_71` */

DROP TABLE IF EXISTS `userlocal_71`;

CREATE TABLE `userlocal_71` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_72` */

DROP TABLE IF EXISTS `userlocal_72`;

CREATE TABLE `userlocal_72` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_73` */

DROP TABLE IF EXISTS `userlocal_73`;

CREATE TABLE `userlocal_73` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_74` */

DROP TABLE IF EXISTS `userlocal_74`;

CREATE TABLE `userlocal_74` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_75` */

DROP TABLE IF EXISTS `userlocal_75`;

CREATE TABLE `userlocal_75` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_76` */

DROP TABLE IF EXISTS `userlocal_76`;

CREATE TABLE `userlocal_76` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_77` */

DROP TABLE IF EXISTS `userlocal_77`;

CREATE TABLE `userlocal_77` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_78` */

DROP TABLE IF EXISTS `userlocal_78`;

CREATE TABLE `userlocal_78` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_79` */

DROP TABLE IF EXISTS `userlocal_79`;

CREATE TABLE `userlocal_79` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_80` */

DROP TABLE IF EXISTS `userlocal_80`;

CREATE TABLE `userlocal_80` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_81` */

DROP TABLE IF EXISTS `userlocal_81`;

CREATE TABLE `userlocal_81` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_82` */

DROP TABLE IF EXISTS `userlocal_82`;

CREATE TABLE `userlocal_82` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_83` */

DROP TABLE IF EXISTS `userlocal_83`;

CREATE TABLE `userlocal_83` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_84` */

DROP TABLE IF EXISTS `userlocal_84`;

CREATE TABLE `userlocal_84` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_85` */

DROP TABLE IF EXISTS `userlocal_85`;

CREATE TABLE `userlocal_85` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_86` */

DROP TABLE IF EXISTS `userlocal_86`;

CREATE TABLE `userlocal_86` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_87` */

DROP TABLE IF EXISTS `userlocal_87`;

CREATE TABLE `userlocal_87` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_88` */

DROP TABLE IF EXISTS `userlocal_88`;

CREATE TABLE `userlocal_88` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_89` */

DROP TABLE IF EXISTS `userlocal_89`;

CREATE TABLE `userlocal_89` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_90` */

DROP TABLE IF EXISTS `userlocal_90`;

CREATE TABLE `userlocal_90` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_91` */

DROP TABLE IF EXISTS `userlocal_91`;

CREATE TABLE `userlocal_91` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_92` */

DROP TABLE IF EXISTS `userlocal_92`;

CREATE TABLE `userlocal_92` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_93` */

DROP TABLE IF EXISTS `userlocal_93`;

CREATE TABLE `userlocal_93` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_94` */

DROP TABLE IF EXISTS `userlocal_94`;

CREATE TABLE `userlocal_94` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_95` */

DROP TABLE IF EXISTS `userlocal_95`;

CREATE TABLE `userlocal_95` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_96` */

DROP TABLE IF EXISTS `userlocal_96`;

CREATE TABLE `userlocal_96` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_97` */

DROP TABLE IF EXISTS `userlocal_97`;

CREATE TABLE `userlocal_97` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_98` */

DROP TABLE IF EXISTS `userlocal_98`;

CREATE TABLE `userlocal_98` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlocal_99` */

DROP TABLE IF EXISTS `userlocal_99`;

CREATE TABLE `userlocal_99` (
  `userId` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
