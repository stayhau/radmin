package com.scavenger.business.service;

import com.scavenger.business.domain.AdownerStatistics;
import com.scavenger.business.domain.ChannelStatistics;
import com.scavenger.business.persistence.AdownerStatisticsMapper;
import com.scavenger.business.persistence.ChannelStatisticsMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdownerStatisticsService extends BaseService<AdownerStatistics> {
    private AdownerStatisticsMapper adownerStatisticsMapper;

	@Autowired
	public void setAdownerStatisticsMapper(AdownerStatisticsMapper adownerStatisticsMapper) {
		this.mapper = adownerStatisticsMapper;
        this.adownerStatisticsMapper = adownerStatisticsMapper;
    }
    public List<String> getChannels() {
        return adownerStatisticsMapper.getChannels();
    }

    public AdownerStatistics getSum(Paginator paginator) {
        return adownerStatisticsMapper.getSum(paginator);
    }
}
