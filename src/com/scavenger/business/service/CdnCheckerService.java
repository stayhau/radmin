package com.scavenger.business.service;

import com.scavenger.business.domain.CdnChecker;
import com.scavenger.business.persistence.CdnCheckerMapper;
import com.tuziilm.web.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CdnCheckerService extends BaseService<CdnChecker> {
    private CdnCheckerMapper cdnCheckerMapper;

    @Resource
    private MyBatisBatchItemWriter writerCdnChecker;

	@Autowired
	public void setCdnCheckerMapper(CdnCheckerMapper cdnCheckerMapper) {
		this.mapper = cdnCheckerMapper;
        this.cdnCheckerMapper = cdnCheckerMapper;
    }

    public List<CdnChecker> getLastest(){
        return cdnCheckerMapper.getLastest();
    }

    public void insertAllCdnChecker(List<CdnChecker> cdnCheckers) {
        writerCdnChecker.write(cdnCheckers);
    }


}
