package com.scavenger.business.service;

import com.scavenger.business.domain.LogChannel;
import com.scavenger.business.persistence.LogChannelMapper;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LogChannelService extends ObjectBasedGroupCacheSupportService<LogChannel> {
    private LogChannelMapper logChannelMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_KEY="map_all_key";
	@Autowired
	public void setLogChannelMapper(LogChannelMapper logChannelMapper) {
		this.mapper = logChannelMapper;
        this.logChannelMapper = logChannelMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, LogChannel logChannel) {
        ((List<LogChannel>)update.get(LIST_ALL_KEY)).add(logChannel);
        ((Map<String, LogChannel>)update.get(MAP_ALL_KEY)).put(logChannel.getPkgName(), logChannel);
    }

    public List<LogChannel> getAllLogChannelCache(){
        return (List<LogChannel>)getCache(LIST_ALL_KEY);
    }

    public Map<String, LogChannel> getAllChannel2SalesmanMapCache(){
        return (Map<String, LogChannel>)getCache(MAP_ALL_KEY);
    }

}
