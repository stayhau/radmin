package com.scavenger.business.service;

import com.scavenger.business.domain.ChannelStatistics;
import com.scavenger.business.persistence.ChannelStatisticsMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChannelStatisticsService extends BaseService<ChannelStatistics> {
    private ChannelStatisticsMapper channelStatisticsMapper;

	@Autowired
	public void setChannelStatisticsMapper(ChannelStatisticsMapper channelStatisticsMapper) {
		this.mapper = channelStatisticsMapper;
        this.channelStatisticsMapper = channelStatisticsMapper;
    }
    public List<String> getChannels() {
        return channelStatisticsMapper.getChannels();
    }

    public ChannelStatistics getSum(Paginator paginator) {
        return channelStatisticsMapper.getSum(paginator);
    }
    public List<ChannelStatistics> select(Paginator paginator) {
        return channelStatisticsMapper.select(paginator);
    }
}
