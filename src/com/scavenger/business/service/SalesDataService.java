package com.scavenger.business.service;

import com.scavenger.business.domain.IncomeData;
import com.scavenger.business.onlinebusiness.domain.SalesData;
import com.scavenger.business.onlinebusiness.persistence.SalesDataMapper;
import com.tuziilm.web.common.Tuple;
import com.tuziilm.web.service.BaseService;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 */
@Service
public class SalesDataService extends BaseService<SalesData> {

    @Resource
    private SalesDataMapper salesDataMapper;
    @Resource
    private IncomeDataService incomeDataService;

    @Resource
    private MyBatisBatchItemWriter writerSalesData;

    @Autowired
    public void setSalesDataMapper(SalesDataMapper salesDataMapper) {
        this.mapper = salesDataMapper;
        this.salesDataMapper=salesDataMapper;
    }

    public void saveSalesDataAndUpdateStatus(Tuple<?,?> tuple){
        writerSalesData.write((List<SalesData>)tuple.getFirst());
        incomeDataService.updateBatch((List<IncomeData>)tuple.getSecond());
    }
    public List<String> getChannels() {
        return salesDataMapper.getChannels();
    }
}
