package com.scavenger.business.service;

import com.scavenger.business.domain.Salesman;
import com.scavenger.business.persistence.SalesmanMapper;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SalesmanService extends ObjectBasedGroupCacheSupportService<Salesman> {
    private SalesmanMapper salesmanMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_SALE_KEY = "map_all_sale_key";

    @Autowired
	public void setSalesmanMapper(SalesmanMapper salesmanMapper) {
		this.mapper = salesmanMapper;
        this.salesmanMapper = salesmanMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_SALE_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, Salesman salesman) {
        ((List<Salesman>)update.get(LIST_ALL_KEY)).add(salesman);
        ((Map<Integer, Salesman>)update.get(MAP_ALL_SALE_KEY)).put(salesman.getId(), salesman);
    }

    public List<Salesman> getAllSalesmanCache(){
        return (List<Salesman>)getCache(LIST_ALL_KEY);
    }

    public Map<Integer, Salesman> getAllSalesmanMapCache(){
        return (Map<Integer, Salesman>)getCache(MAP_ALL_SALE_KEY);
    }

    public Integer getSalesmanId(String name) {
        return this.salesmanMapper.getSalemanId(name);
    }
}
