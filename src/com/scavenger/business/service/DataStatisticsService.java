package com.scavenger.business.service;

import com.scavenger.business.domain.DataStatistics;
import com.scavenger.business.persistence.DataStatisticsMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/3
 * Time: ${Time}
 */
@Service
public class DataStatisticsService extends BaseService<DataStatistics> {
    private DataStatisticsMapper dataStatisticsMapper;

    @Resource
    private MyBatisBatchItemWriter writerStatusUpdate;
    @Autowired
    public void setDataStatisticsMapper(DataStatisticsMapper dataStatisticsMapper) {
        this.mapper = dataStatisticsMapper;
        this.dataStatisticsMapper =dataStatisticsMapper;
    }

    public List<String> getProducts() {
        return dataStatisticsMapper.getProducts();
    }

    public List<String> getChannels() {
        return dataStatisticsMapper.getChannels();
    }

    public List<DataStatistics> getDataStatisticsByIds(int[] ids) {
        return dataStatisticsMapper.getDataStatisticsByIds(ids);
    }

    public List<DataStatistics> getSumByProduct(Paginator paginator) {
        return this.dataStatisticsMapper.getSumByProduct(paginator);
    }

    public void updateBatch(@Param("apps")List<DataStatistics> dataStatisticses) {
        writerStatusUpdate.write(dataStatisticses);
    }

}
