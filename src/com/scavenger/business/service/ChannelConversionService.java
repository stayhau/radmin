package com.scavenger.business.service;

import com.scavenger.business.domain.ChannelConversion;
import com.scavenger.business.domain.PriceArea;
import com.scavenger.business.persistence.ChannelConversionMapper;
import com.scavenger.business.persistence.PriceAreaMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChannelConversionService extends ObjectBasedGroupCacheSupportService<ChannelConversion> {
    private ChannelConversionMapper channelConversionMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_KEY="map_all_key";
	@Autowired
	public void setChannelConversionMapper(ChannelConversionMapper channelConversionMapper) {
		this.mapper = channelConversionMapper;
        this.channelConversionMapper = channelConversionMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, ChannelConversion channelConversion) {
        ((List<ChannelConversion>)update.get(LIST_ALL_KEY)).add(channelConversion);
        ((Map<String, ChannelConversion>)update.get(MAP_ALL_KEY)).put(channelConversion.getChannel(), channelConversion);
    }

    public List<ChannelConversion> getAllChannelConversionCache(){
        return (List<ChannelConversion>)getCache(LIST_ALL_KEY);
    }

    public Map<String, ChannelConversion> getAllChannelConversionMapCache(){
        return (Map<String, ChannelConversion>)getCache(MAP_ALL_KEY);
    }
    public int updateByChannel(ChannelConversion channelConversion) {
        int ret = this.channelConversionMapper.updateByChannel(channelConversion);
        resetCache();
        return ret;
    }

    public List<String> getChannelsByDividedWay(Paginator paginator) {
        return this.channelConversionMapper.getChannelsByDividedWay(paginator);
    }
    public List<String> getChannelsByPriceAreaName(Paginator paginator) {
        return this.channelConversionMapper.getChannelsByPriceAreaName(paginator);
    }
    public List<String> getAllDividedWay() {
        return this.channelConversionMapper.getAllDividedWay();
    }
    public List<String> getAllPriceAreaName() {
        return this.channelConversionMapper.getAllPriceAreaName();
    }

}
