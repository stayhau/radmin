package com.scavenger.business.service;

import com.scavenger.business.domain.EventLogStatistics;
import com.scavenger.business.persistence.EventLogStatisticsMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/25
 * Time: 9:25
 */
@Service
public class EventLogStatisticsService extends BaseService<EventLogStatistics> {
    private EventLogStatisticsMapper eventLogStatisticsMapper;

    @Autowired
    private void setEventLogStatisticsMapper(EventLogStatisticsMapper eventLogStatisticsMapper) {
        this.mapper = eventLogStatisticsMapper;
        this.eventLogStatisticsMapper = eventLogStatisticsMapper;
    }

    public List<String> getChannelsByApp(String app) {
        return this.eventLogStatisticsMapper.getChannelsByApp(app);
    }

    public List<String> getEvents() {
        return this.eventLogStatisticsMapper.getEvents();
    }
    public List<String> getApps() {
        return this.eventLogStatisticsMapper.getApps();
    }

    public EventLogStatistics getSum(Paginator paginator) {
        return this.eventLogStatisticsMapper.getSum(paginator);
    }
}
