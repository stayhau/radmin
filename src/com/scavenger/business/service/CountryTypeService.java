package com.scavenger.business.service;

import com.scavenger.business.domain.CountryType;
import com.scavenger.business.persistence.CountryTypeMapper;
import com.tuziilm.web.service.ListBasedCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/11
 * Time: 9:45
 */
@Service
public class CountryTypeService extends ListBasedCacheSupportService<CountryType> {
    private CountryTypeMapper countryTypeMapper;
    @Autowired
    public void setCountryTypeMapper(CountryTypeMapper countryTypeMapper) {
        this.mapper = countryTypeMapper;
        this.countryTypeMapper = countryTypeMapper;
    }
}
