package com.scavenger.business.service;

import com.scavenger.business.domain.IncomeData;
import com.scavenger.business.domain.SalesStatistics;
import com.scavenger.business.persistence.SalesStatisticsMapper;
import com.tuziilm.web.common.Tuple;
import com.tuziilm.web.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/12
 * Time: ${Time}
 */
@Service
public class SalesStatisticsService extends BaseService<SalesStatistics> {

    @Resource
    private SalesStatisticsMapper salesStatisticsMapper;
    @Resource
    private IncomeDataService incomeDataService;

    @Resource
    private MyBatisBatchItemWriter writer1;

    @Autowired
    public void setSalesStatisticsMapper(SalesStatisticsMapper salesStatisticsMapper) {
        this.mapper = salesStatisticsMapper;
        this.salesStatisticsMapper=salesStatisticsMapper;
    }

    public void insertAllSalesStatistics(@Param("apps")List<SalesStatistics> salesStatisticses) {
        writer1.write(salesStatisticses);
    }
    public void saveSalesStatisticsAndUpdateStatus(Tuple<?,?> tuple){
        writer1.write((List<SalesStatistics>)tuple.getFirst());
        incomeDataService.updateBatch((List<IncomeData>)tuple.getSecond());
    }

    public List<String> getChannels() {
        return salesStatisticsMapper.getChannels();
    }
}
