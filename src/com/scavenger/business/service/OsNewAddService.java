package com.scavenger.business.service;

import com.scavenger.business.domain.OsNewAdd;
import com.scavenger.business.persistence.OsNewAddMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/3
 * Time: ${Time}
 */
@Service
public class OsNewAddService extends BaseService<OsNewAdd> {
    private OsNewAddMapper osNewAddMapper;
    @Resource
    private MyBatisBatchItemWriter writerOsNewAdd;

    @Autowired
    public void setNewAddDataMapper(OsNewAddMapper osNewAddMapper) {
        this.mapper = osNewAddMapper;
        this.osNewAddMapper =osNewAddMapper;
    }
    public void insertAllOsNewAdd(@Param("apps")List<OsNewAdd> osNewAdds) {
        writerOsNewAdd.write(osNewAdds);
    }
    public List<String> getProducts() {
        return osNewAddMapper.getProducts();
    }

    public List<String> getChannels() {
        return osNewAddMapper.getChannels();
    }

    public int sumByQueryItem(Paginator paginator) {
        return osNewAddMapper.sumByQueryItem(paginator);
    }
}
