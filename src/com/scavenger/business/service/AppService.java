package com.scavenger.business.service;

import com.scavenger.business.domain.App;
import com.scavenger.business.domain.Channel2Salesman;
import com.scavenger.business.persistence.AppMapper;
import com.scavenger.business.persistence.Channel2SalesmanMapper;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AppService extends ObjectBasedGroupCacheSupportService<App> {
    private AppMapper appMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_KEY="map_all_key";
	@Autowired
	public void setAppMapper(AppMapper appMapper) {
		this.mapper = appMapper;
        this.appMapper = appMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, App app) {
        ((List<App>)update.get(LIST_ALL_KEY)).add(app);
        ((Map<String, String>)update.get(MAP_ALL_KEY)).put(app.getPkgName(), app.getApp());
    }

    public List<App> getAllAppCache(){
        return (List<App>)getCache(LIST_ALL_KEY);
    }

    public Map<String, String> getAllAppMapCache(){
        return (Map<String, String>)getCache(MAP_ALL_KEY);
    }
}
