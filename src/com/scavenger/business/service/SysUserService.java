package com.scavenger.business.service;

import com.scavenger.business.domain.SysUser;
import com.scavenger.business.persistence.SysUserMapper;

import com.tuziilm.web.service.SimpleCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统用户数据操作服务类
 *
 */
@Service
public class SysUserService  extends SimpleCacheSupportService<SysUser> {
	private SysUserMapper sysUserMapper;
	@Autowired
	public void setSysUserMapper(SysUserMapper sysUserMapper) {
		this.mapper = sysUserMapper;
		this.sysUserMapper=sysUserMapper;
	}

	public SysUser getByUsername(String username) {
		return sysUserMapper.getByUsername(username);
	}
	
	public Integer insertcps(SysUser sysUser){
		 return sysUserMapper.insertcps(sysUser);
	}
	
	public Integer updateCpsByChannelIdSelective(SysUser sysUser){
		return sysUserMapper.updateCpsByChannelIdSelective(sysUser);
	}
}
