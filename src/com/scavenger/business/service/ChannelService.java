package com.scavenger.business.service;

import com.scavenger.business.domain.Channel;
import com.scavenger.business.persistence.ChannelMapper;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChannelService extends ObjectBasedGroupCacheSupportService<Channel> {
    private ChannelMapper channelMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_KEY="map_all_key";
	@Autowired
    public void setChannelMapper(ChannelMapper channelMapper) {
        this.mapper = channelMapper;
        this.channelMapper = channelMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, Channel channel) {
        ((List<Channel>)update.get(LIST_ALL_KEY)).add(channel);
        ((Map<String, Channel>)update.get(MAP_ALL_KEY)).put(channel.getFrom(), channel);
    }

    public List<Channel> getAllChannelCache(){
    	
        return channelMapper.selectAll();
    }

    public Map<String, Channel> getAllChannelMapCache(){
        return (Map<String, Channel>)getCache(MAP_ALL_KEY);
    }

    public Channel getChannelByFrom(String from) {
        return this.channelMapper.getChannelByFrom(from);
    }

}
