package com.scavenger.business.service;

import com.scavenger.business.gp.domain.GpData;
import com.scavenger.business.gp.persistence.GpDataMapper;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 */
@Service
public class GpDataService extends BaseService<GpData> {

    @Resource
    private GpDataMapper gpDataMapper;

    @Autowired
    public void setGpDataMapper(GpDataMapper gpDataMapper) {
        this.mapper = gpDataMapper;
        this.gpDataMapper=gpDataMapper;
    }


    public List<String> getChannels() {
        return gpDataMapper.getChannels();
    }
}
