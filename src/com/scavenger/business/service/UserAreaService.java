package com.scavenger.business.service;

import com.scavenger.business.domain.UserArea;
import com.scavenger.business.persistence.UserAreaMapper;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/12
 * Time: ${Time}
 */
@Service
public class UserAreaService extends BaseService<UserArea> {
    @Resource
    private UserAreaMapper userAreaMapper;

    @Autowired
    public void setUserAreaMapper(UserAreaMapper userAreaMapper) {
        this.mapper = userAreaMapper;
        this.userAreaMapper = userAreaMapper;
    }

    public List<String> getChannels() {
        return userAreaMapper.getChannels();
    }
}
