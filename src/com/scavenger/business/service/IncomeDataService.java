package com.scavenger.business.service;

import com.scavenger.business.domain.ChannelConversion;
import com.scavenger.business.domain.IncomeData;
import com.scavenger.business.persistence.IncomeDataMapper;
import com.tuziilm.web.common.Tuple;
import com.tuziilm.web.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/3
 * Time: ${Time}
 */
@Service
public class IncomeDataService extends BaseService<IncomeData> {
    private IncomeDataMapper incomeDataMapper;
    @Resource
    private MyBatisBatchItemWriter writer;
    @Resource
    private MyBatisBatchItemWriter writerUpdate;
    @Resource
    private ChannelConversionService channelConversionService;

    @Autowired
    public void setIncomeDataMapper(IncomeDataMapper incomeDataMapper) {
        this.mapper = incomeDataMapper;
        this.incomeDataMapper =incomeDataMapper;
    }
    public void insertAllIncomeData(@Param("apps")List<IncomeData> incomeDatas) {
        writer.write(incomeDatas);
    }

    public List<String> getProducts() {
        return incomeDataMapper.getProducts();
    }

    public List<String> getChannels() {
        return incomeDataMapper.getChannels();
    }

    public List<IncomeData> getIncomeDataByIds(int[] ids) {
        return incomeDataMapper.getIncomeDataByIds(ids);
    }

    public void updateBatch(@Param("apps")List<IncomeData> incomeDatas) {
        writerUpdate.write(incomeDatas);
    }

    public void updateIncomeDataAndConversion(Tuple<?,?> tuple) {
        saveOrUpdate((IncomeData) tuple.first);
        channelConversionService.updateByChannel((ChannelConversion) tuple.second);
    }
}
