package com.scavenger.business.service;

import com.scavenger.business.domain.Channel2Salesman;
import com.scavenger.business.persistence.Channel2SalesmanMapper;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Channel2SalesmanService extends ObjectBasedGroupCacheSupportService<Channel2Salesman> {
    private Channel2SalesmanMapper channel2SalesmanMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_KEY="map_all_key";
	@Autowired
	public void setChannel2SalesmanMapper(Channel2SalesmanMapper channel2SalesmanMapper) {
		this.mapper = channel2SalesmanMapper;
        this.channel2SalesmanMapper = channel2SalesmanMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, Channel2Salesman channel2Salesman) {
        ((List<Channel2Salesman>)update.get(LIST_ALL_KEY)).add(channel2Salesman);
        ((Map<String, Channel2Salesman>)update.get(MAP_ALL_KEY)).put(channel2Salesman.getChannel(), channel2Salesman);
    }

    public List<Channel2Salesman> getAllChannel2SalesmanCache(){
        return (List<Channel2Salesman>)getCache(LIST_ALL_KEY);
    }

    public Map<String, Channel2Salesman> getAllChannelMapCache(){
        return (Map<String, Channel2Salesman>)getCache(MAP_ALL_KEY);
    }

    public List<Channel2Salesman> getChannelBySalesmanId(Integer id) {
        return this.channel2SalesmanMapper.getChannelBySalesmanId(id);
    }
}
