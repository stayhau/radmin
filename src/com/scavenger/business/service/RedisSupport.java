package com.scavenger.business.service;

import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.util.Pool;

import javax.annotation.Resource;

@Component
public class RedisSupport extends com.tuziilm.web.service.RedisSupport{
    @Resource
    private JedisPool jedisPool;

    @Override
    public void init() {
        jedisPools=new Pool[]{jedisPool};
    }
}
