package com.scavenger.business.service;

import com.scavenger.business.domain.EventData;
import com.scavenger.business.domain.IncomeData;
import com.scavenger.business.persistence.EventDataMapper;
import com.scavenger.business.persistence.IncomeDataMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/29
 */
@Service
public class EventDataService extends BaseService<EventData> {
    private EventDataMapper eventDataMapper;
    @Resource
    private MyBatisBatchItemWriter writerEventData;

    @Autowired
    public void setEventDataMapper(EventDataMapper eventDataMapper) {
        this.mapper = eventDataMapper;
        this.eventDataMapper =eventDataMapper;
    }
    public void insertAllEventData(@Param("apps")List<EventData> eventDatas) {
        writerEventData.write(eventDatas);
    }

    public List<String> getProducts() {
        return eventDataMapper.getProducts();
    }

    public List<String> getChannels() {
        return eventDataMapper.getChannels();
    }
    public List<String> getEvents() {
        return eventDataMapper.getEvents();
    }
    public List<String> getParams() {
        return eventDataMapper.getParams();
    }

    public List<EventData> getByQuery(Paginator paginator){
      return eventDataMapper.select(paginator);
    }



}
