package com.scavenger.business.service;

import com.scavenger.business.domain.BasicData;
import com.scavenger.business.domain.RevisitData;
import com.scavenger.business.persistence.BasicDataMapper;
import com.tuziilm.web.service.BaseService;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/10/8
 * Time: 10:29
 */
@Service
public class BasicDataService extends BaseService<BasicData> {
    private BasicDataMapper basicDataMapper;
    @Resource
    private MyBatisBatchItemWriter writerBasicData;
    @Resource
    private RevisitDataService revisitDataService;

    @Autowired
    public void setBasicDataMapper(BasicDataMapper basicDataMapper) {
        this.mapper = basicDataMapper;
        this.basicDataMapper =basicDataMapper;
    }

    public void saveAllDatas(List<BasicData> basicDatas,List<RevisitData> revisitDatas) {
        writerBasicData.write(basicDatas);
        revisitDataService.insertAllRevisitData(revisitDatas);
    }
}
