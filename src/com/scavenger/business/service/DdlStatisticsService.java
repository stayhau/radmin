package com.scavenger.business.service;

import com.scavenger.business.domain.DdlStatistics;
import com.scavenger.business.persistence.DdlStatisticsMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DdlStatisticsService extends BaseService<DdlStatistics> {
    private DdlStatisticsMapper ddlStatisticsMapper;

	@Autowired
	public void setDdlStatisticsMapper(DdlStatisticsMapper ddlStatisticsMapper) {
		this.mapper = ddlStatisticsMapper;
        this.ddlStatisticsMapper = ddlStatisticsMapper;
    }

    public DdlStatistics getSum(Paginator paginator) {
        return ddlStatisticsMapper.getSum(paginator);
    }
}
