package com.scavenger.business.service;

import com.scavenger.business.domain.PriceArea;
import com.scavenger.business.persistence.PriceAreaMapper;
import com.tuziilm.web.common.Config;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PriceAreaService extends ObjectBasedGroupCacheSupportService<PriceArea> {
    private PriceAreaMapper priceAreaMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_KEY="map_all_key";
	@Autowired
	public void setPriceAreaMapper(PriceAreaMapper priceAreaMapper) {
		this.mapper = priceAreaMapper;
        this.priceAreaMapper = priceAreaMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, PriceArea priceArea) {
        ((List<PriceArea>)update.get(LIST_ALL_KEY)).add(priceArea);
        ((Map<String, PriceArea>)update.get(MAP_ALL_KEY)).put(priceArea.getTypeName()+ Config.SEP+priceArea.getType(), priceArea);
    }

    public List<PriceArea> getAllPriceAreaCache(){
        return (List<PriceArea>)getCache(LIST_ALL_KEY);
    }

    public Map<String, PriceArea> getAllPriceAreaMapCache(){
        return (Map<String, PriceArea>)getCache(MAP_ALL_KEY);
    }

    public List<String> getAllPriceAreaName() {
        return this.priceAreaMapper.getAllPriceAreaName();
    }
}
