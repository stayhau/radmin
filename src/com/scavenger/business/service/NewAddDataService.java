package com.scavenger.business.service;

import com.scavenger.business.domain.NewAddData;
import com.scavenger.business.domain.SystemCeroa;
import com.scavenger.business.persistence.NewAddDataMapper;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/3
 * Time: ${Time}
 */
@Service
public class NewAddDataService extends BaseService<NewAddData> {
    private NewAddDataMapper newAddDataMapper;
    @Resource
    private MyBatisBatchItemWriter writerNewAddData;

    @Autowired
    public void setNewAddDataMapper(NewAddDataMapper newAddDataMapper) {
        this.mapper = newAddDataMapper;
        this.newAddDataMapper =newAddDataMapper;
    }
    public void insertAllNewAddData(@Param("apps")List<NewAddData> newAddDatas) {
        writerNewAddData.write(newAddDatas);
    }

    public List<String> getChannels() {
        return newAddDataMapper.getChannels();
    }

    public List<NewAddData> getAllNewAdd(Paginator paginator) {
        return newAddDataMapper.getAllNewAdd(paginator);
    }

    public List<SystemCeroa> getAllSystemCeroa(Paginator paginator) {
        return newAddDataMapper.getAllSystemCeroa(paginator);
    }

    public List<NewAddData> select(Paginator paginator) {
        return newAddDataMapper.select(paginator);
    }

    public <T> void rePaginator(int page,Paginator paginator,List<T> results,Model model){
        model.addAttribute("paginator", paginator);
        model.addAttribute("hasDatas",!results.isEmpty());
        if(results.isEmpty()){
            return;
        }else{
            paginator.setHasData(true);
        }
        paginator.setTotal(results.size());

        int startIdx = paginator.getStart();
        int endIdx = startIdx + paginator.getSize();

        if(results.size()>startIdx){
            model.addAttribute("datas", results.subList(startIdx, Math.min(endIdx,results.size())));
            paginator.setHasNextPage(endIdx<results.size());
        }
        paginator.setNeedTotal(true);
    }
}
