package com.scavenger.business.service;

import com.scavenger.business.model.UserReport;
import com.scavenger.business.persistence.UserReportMapper;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserReportService extends BaseService<UserReport> {
    private UserReportMapper userReportMapper;

    @Autowired
    public void setUserReportMapper(UserReportMapper userReportMapper) {
        this.mapper = userReportMapper;
        this.userReportMapper = userReportMapper;
    }

    public List<UserReport> list() {
        return this.userReportMapper.list();
    }

    public Map<String, Integer> listTotal() {
        return this.userReportMapper.listTotal();
    }
}
