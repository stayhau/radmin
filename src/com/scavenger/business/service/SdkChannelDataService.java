package com.scavenger.business.service;

import com.scavenger.business.domain.DataStatistics;
import com.scavenger.business.domain.SdkChannelData;
import com.scavenger.business.persistence.SdkChannelDataMapper;
import com.tuziilm.web.common.Tuple;
import com.tuziilm.web.service.BaseService;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 */
@Service
public class SdkChannelDataService extends BaseService<SdkChannelData> {

    @Resource
    private SdkChannelDataMapper sdkChannelDataMapper;
    @Resource
    private DataStatisticsService dataStatisticsService;

    @Resource
    private MyBatisBatchItemWriter writerSdkData;

    @Autowired
    public void setSdkChannelDataMapper(SdkChannelDataMapper sdkChannelDataMapper) {
        this.mapper = sdkChannelDataMapper;
        this.sdkChannelDataMapper=sdkChannelDataMapper;
    }

    public void saveSdkChannelDataAndUpdateStatus(Tuple<?,?> tuple){
        writerSdkData.write((List<SdkChannelData>)tuple.getFirst());
        dataStatisticsService.updateBatch((List<DataStatistics>)tuple.getSecond());
    }
    public List<String> getChannels() {
        return sdkChannelDataMapper.getChannels();
    }
}
