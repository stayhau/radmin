package com.scavenger.business.service;

import com.scavenger.business.domain.ChannelEffective;
import com.scavenger.business.persistence.ChannelEffectiveMapper;
import com.tuziilm.web.service.ObjectBasedGroupCacheSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChannelEffectiveService extends ObjectBasedGroupCacheSupportService<ChannelEffective> {
    private ChannelEffectiveMapper channelEffectiveMapper;
    private final static String LIST_ALL_KEY="list_all_key";

    private final static String MAP_ALL_KEY="map_all_key";

    private final static String MAP_CHANNEL_KEY = "map_channel_key";

    @Autowired
	public void setChannelEffectiveMapper(ChannelEffectiveMapper channelEffectiveMapper) {
		this.mapper = channelEffectiveMapper;
        this.channelEffectiveMapper = channelEffectiveMapper;
    }

    @Override
    public String[] cacheGroupKeys() {
        return new String[]{LIST_ALL_KEY, MAP_ALL_KEY,MAP_CHANNEL_KEY};
    }

    @Override
    public Object newObject(String cacheGroupKey) {
        if(cacheGroupKey.startsWith("map")){
            return new HashMap();
        }else{
            return new ArrayList();
        }
    }

    @Override
    public void updateCacheList(Map<String, Object> update, ChannelEffective channelEffective) {
        ((List<ChannelEffective>)update.get(LIST_ALL_KEY)).add(channelEffective);
        ((Map<String, ChannelEffective>)update.get(MAP_ALL_KEY)).put(channelEffective.getChannel()+channelEffective.getDate(), channelEffective);
        if(((Map<String, List<ChannelEffective>>)update.get(MAP_CHANNEL_KEY)).get(channelEffective.getChannel())==null) {
            List<ChannelEffective> effectives = new ArrayList<>();
            effectives.add(channelEffective);
            ((Map<String, List<ChannelEffective>>) update.get(MAP_CHANNEL_KEY)).put(channelEffective.getChannel(), effectives);
        }else {
            ((Map<String, List<ChannelEffective>>) update.get(MAP_CHANNEL_KEY)).get(channelEffective.getChannel()).add(channelEffective);
        }
    }

    public List<ChannelEffective> getAllChannelEffectiveCache(){
        return (List<ChannelEffective>)getCache(LIST_ALL_KEY);
    }

    public Map<String, ChannelEffective> getAllChannelEffectiveMapCache(){
        return (Map<String, ChannelEffective>)getCache(MAP_ALL_KEY);
    }
    public Map<String, List<ChannelEffective>> getChannelEffectiveMapCache(){
        return (Map<String, List<ChannelEffective>>)getCache(MAP_CHANNEL_KEY);
    }
}
