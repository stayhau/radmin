package com.scavenger.business.service;

import com.scavenger.business.domain.UserLoss;
import com.scavenger.business.persistence.UserLossMapper;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/12
 * Time: ${Time}
 */
@Service
public class UserLossService extends BaseService<UserLoss> {
    @Resource
    private UserLossMapper userLossMapper;

    @Autowired
    public void setUserLossMapper(UserLossMapper userLossMapper) {
        this.mapper = userLossMapper;
        this.userLossMapper = userLossMapper;
    }

    public List<String> getChannels() {
        return userLossMapper.getChannels();
    }
}
