package com.scavenger.business.service;

import com.scavenger.business.domain.UserSaturation;
import com.scavenger.business.persistence.UserSaturationMapper;
import com.tuziilm.web.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/12
 * Time: ${Time}
 */
@Service
public class UserSaturationService extends BaseService<UserSaturation> {
    @Resource
    private UserSaturationMapper userSaturationMapper;

    @Autowired
    public void setUserSaturationMapper(UserSaturationMapper userSaturationMapper) {
        this.mapper = userSaturationMapper;
        this.userSaturationMapper = userSaturationMapper;
    }

    public List<String> getChannels() {
        return userSaturationMapper.getChannels();
    }
}
