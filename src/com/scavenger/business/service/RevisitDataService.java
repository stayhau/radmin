package com.scavenger.business.service;

import com.scavenger.business.domain.RevisitData;
import com.scavenger.business.persistence.RevisitDataMapper;
import com.tuziilm.web.service.BaseService;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/10/8
 * Time: 10:29
 */
@Service
public class RevisitDataService extends BaseService<RevisitData> {
    private RevisitDataMapper revisitDataMapper;
    @Resource
    private MyBatisBatchItemWriter writerRevisitData;

    @Autowired
    public void setRevisitDataMapper(RevisitDataMapper revisitDataMapper) {
        this.mapper = revisitDataMapper;
        this.revisitDataMapper =revisitDataMapper;
    }

    public void insertAllRevisitData(List<RevisitData> revisitDatas) {
        writerRevisitData.write(revisitDatas);
    }
}
