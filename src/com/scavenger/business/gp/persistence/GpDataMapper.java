package com.scavenger.business.gp.persistence;

import com.scavenger.business.gp.domain.GpData;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface GpDataMapper extends BaseMapper<GpData> {
    int insertBatch(List<GpData> gpDatas);
    List<String> getChannels();
}
