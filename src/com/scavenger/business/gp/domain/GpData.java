package com.scavenger.business.gp.domain;

import com.tuziilm.web.domain.Id;

import java.util.Date;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 */
public class GpData extends Id {
    private String channel;
    private Date date;
    private Integer intercepted;
    private double price;
    private double interceptPrice;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getIntercepted() {
        return intercepted;
    }

    public void setIntercepted(Integer intercepted) {
        this.intercepted = intercepted;
    }

    public double getInterceptPrice() {
        return interceptPrice;
    }

    public void setInterceptPrice(double interceptPrice) {
        this.interceptPrice = interceptPrice;
    }
}
