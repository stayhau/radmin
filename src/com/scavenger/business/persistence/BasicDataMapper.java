package com.scavenger.business.persistence;

import com.scavenger.business.domain.BasicData;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

/**
 * lianzi basic data
 * Date: 2016/10/8 10:01
 */
public interface BasicDataMapper extends BaseMapper<BasicData> {
    int insertBatch(List<BasicData> basicDataList);
}
