package com.scavenger.business.persistence;

import com.scavenger.business.domain.App;
import com.tuziilm.web.persistence.BaseMapper;

public interface AppMapper extends BaseMapper<App> {
}
