package com.scavenger.business.persistence;

import com.scavenger.business.domain.SysUser;
import com.tuziilm.web.persistence.BaseMapper;

/**
 * ibatis操作系统用户表的Mapper接口
 *
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

	SysUser getByUsername(String username);
	
	Integer insertcps(SysUser sysUser); 
	
	Integer updateCpsByChannelIdSelective(SysUser sysUser);
}