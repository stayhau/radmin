package com.scavenger.business.persistence;


import com.scavenger.business.domain.ChannelEffective;
import com.tuziilm.web.persistence.BaseMapper;

public interface ChannelEffectiveMapper extends BaseMapper<ChannelEffective> {
}
