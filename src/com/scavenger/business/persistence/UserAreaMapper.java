package com.scavenger.business.persistence;

import com.scavenger.business.domain.UserArea;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface UserAreaMapper extends BaseMapper<UserArea> {
    public List<String> getChannels();
}
