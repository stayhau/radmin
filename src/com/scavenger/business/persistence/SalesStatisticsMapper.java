package com.scavenger.business.persistence;

import com.scavenger.business.domain.SalesStatistics;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface SalesStatisticsMapper extends BaseMapper<SalesStatistics> {
    int insertBatch(List<SalesStatistics> salesStatisticses);

    List<String> getChannels();
}
