package com.scavenger.business.persistence;

import com.scavenger.business.domain.EventLogStatistics;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/24
 * Time: 20:40
 */
public interface EventLogStatisticsMapper extends BaseMapper<EventLogStatistics> {
    List<String> getChannelsByApp(String app);
    List<String> getEvents();
    List<String> getApps();
    EventLogStatistics getSum(Paginator paginator);
}
