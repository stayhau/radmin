package com.scavenger.business.persistence;

import com.scavenger.business.domain.IncomeData;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface IncomeDataMapper extends BaseMapper<IncomeData> {
    int insertBatch(List<IncomeData> incomeDatas);

    List<String> getProducts();

    List<String> getChannels();

    List<IncomeData> getIncomeDataByIds(int[] ids);

    int updateBatch(List<IncomeData> incomeDatas);
}
