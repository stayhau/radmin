package com.scavenger.business.persistence;

import com.scavenger.business.domain.PriceArea;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface PriceAreaMapper extends BaseMapper<PriceArea> {
    List<String> getAllPriceAreaName();
}
