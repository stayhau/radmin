package com.scavenger.business.persistence;

import com.scavenger.business.domain.NewAddData;
import com.scavenger.business.domain.SystemCeroa;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface NewAddDataMapper extends BaseMapper<NewAddData> {

    int insertBatch(List<NewAddData> newAddDatas);

    List<String> getChannels();

    List<NewAddData> getAllNewAdd(Paginator paginator);

    List<SystemCeroa> getAllSystemCeroa(Paginator paginator);
}
