package com.scavenger.business.persistence;


import com.scavenger.business.domain.ChannelConversion;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface ChannelConversionMapper extends BaseMapper<ChannelConversion> {
    public int updateByChannel(ChannelConversion channelConversion);

    public List<String> getChannelsByDividedWay(Paginator paginator);

    public List<String> getChannelsByPriceAreaName(Paginator paginator);

    public List<String> getAllDividedWay();

    public List<String> getAllPriceAreaName();
}
