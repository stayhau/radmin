package com.scavenger.business.persistence;

import com.scavenger.business.domain.DataStatistics;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/19
 * Time: 16:09
 */
public interface DataStatisticsMapper extends BaseMapper<DataStatistics> {

    List<String> getProducts();

    List<String> getChannels();

    List<DataStatistics> getDataStatisticsByIds(int[] ids);

    List<DataStatistics> getSumByProduct(Paginator paginator);

    int updateBatch(List<DataStatistics> dataStatisticses);

}
