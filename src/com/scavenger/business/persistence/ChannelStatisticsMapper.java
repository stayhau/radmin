package com.scavenger.business.persistence;

import com.scavenger.business.domain.ChannelStatistics;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface ChannelStatisticsMapper extends BaseMapper<ChannelStatistics> {
    List<String> getChannels();

    ChannelStatistics getSum(Paginator paginator);
}
