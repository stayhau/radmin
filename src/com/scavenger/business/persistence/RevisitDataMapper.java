package com.scavenger.business.persistence;

import com.scavenger.business.domain.RevisitData;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

/**
 * lianzi basic data
 * Date: 2016/10/8 10:01
 */
public interface RevisitDataMapper extends BaseMapper<RevisitData> {
    int insertBatch(List<RevisitData> revisitDatas);
}
