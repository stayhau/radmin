package com.scavenger.business.persistence;

import com.scavenger.business.domain.LogChannel;
import com.tuziilm.web.persistence.BaseMapper;

public interface LogChannelMapper extends BaseMapper<LogChannel> {
}
