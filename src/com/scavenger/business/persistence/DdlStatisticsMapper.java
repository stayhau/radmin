package com.scavenger.business.persistence;

import com.scavenger.business.domain.DdlStatistics;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

public interface DdlStatisticsMapper extends BaseMapper<DdlStatistics> {

    DdlStatistics getSum(Paginator paginator);
}
