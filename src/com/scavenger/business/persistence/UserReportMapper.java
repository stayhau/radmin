package com.scavenger.business.persistence;

import com.scavenger.business.model.UserReport;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;
import java.util.Map;

public interface UserReportMapper extends BaseMapper<UserReport> {
    List<UserReport> list();

    Map<String, Integer> listTotal();
}
