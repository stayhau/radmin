package com.scavenger.business.persistence;

import com.scavenger.business.domain.CountryType;
import com.tuziilm.web.persistence.BaseMapper;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/11
 * Time: 9:29
 */
public interface CountryTypeMapper extends BaseMapper<CountryType> {
}
