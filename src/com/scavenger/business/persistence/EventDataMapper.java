package com.scavenger.business.persistence;


import com.scavenger.business.domain.EventData;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface EventDataMapper extends BaseMapper<EventData> {
    int insertBatch(List<EventData> eventDatas);

    List<String> getProducts();

    List<String> getChannels();

    List<String> getEvents();

    List<String> getParams();

}
