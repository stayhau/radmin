package com.scavenger.business.persistence;

import com.scavenger.business.domain.CdnChecker;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface CdnCheckerMapper extends BaseMapper<CdnChecker> {
    int insertBatch(List<CdnChecker> cdnCheckers);

    List<CdnChecker> getLastest();
}
