package com.scavenger.business.persistence;

import com.scavenger.business.domain.SdkChannelData;
import com.scavenger.business.onlinebusiness.domain.SalesData;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/19
 * Time: 16:09
 */
public interface SdkChannelDataMapper extends BaseMapper<SdkChannelData> {

    List<String> getChannels();
    int insertBatch(List<SdkChannelData> sdkChannelDatas);
}
