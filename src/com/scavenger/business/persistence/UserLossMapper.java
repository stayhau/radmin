package com.scavenger.business.persistence;

import com.scavenger.business.domain.UserLoss;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface UserLossMapper extends BaseMapper<UserLoss> {
    public List<String> getChannels();
}
