package com.scavenger.business.persistence;

import com.scavenger.business.domain.Channel;
import com.tuziilm.web.persistence.BaseMapper;

public interface ChannelMapper extends BaseMapper<Channel> {
    public Channel getChannelByFrom(String from);
}
