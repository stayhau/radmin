package com.scavenger.business.persistence;

import com.scavenger.business.domain.AdownerStatistics;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface AdownerStatisticsMapper extends BaseMapper<AdownerStatistics> {
    List<String> getChannels();

    AdownerStatistics getSum(Paginator paginator);
}
