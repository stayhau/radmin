package com.scavenger.business.persistence;

import com.scavenger.business.domain.UserSaturation;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface UserSaturationMapper extends BaseMapper<UserSaturation> {
    public List<String> getChannels();
}
