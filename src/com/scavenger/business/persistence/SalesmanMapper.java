package com.scavenger.business.persistence;

import com.scavenger.business.domain.Salesman;
import com.tuziilm.web.persistence.BaseMapper;

public interface SalesmanMapper extends BaseMapper<Salesman> {

    Integer getSalemanId(String name);
}
