package com.scavenger.business.persistence;

import com.scavenger.business.domain.OsNewAdd;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface OsNewAddMapper extends BaseMapper<OsNewAdd> {

    int insertBatch(List<OsNewAdd> OsNewAdds);

    List<String> getProducts();

    List<String> getChannels();

    int sumByQueryItem(Paginator paginator);
}
