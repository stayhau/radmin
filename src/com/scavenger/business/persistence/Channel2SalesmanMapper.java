package com.scavenger.business.persistence;

import com.scavenger.business.domain.Channel2Salesman;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface Channel2SalesmanMapper extends BaseMapper<Channel2Salesman> {
    List<Channel2Salesman> getChannelBySalesmanId(Integer id);
}
