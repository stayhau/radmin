package com.scavenger.business.domain;

public class ReadView {
    private int viewCount;
    private int starCount;

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getStarCount() {
        return starCount;
    }

    public void setStarCount(int starCount) {
        this.starCount = starCount;
    }

    @Override
    public String toString() {
        return "ReadView{" +
                "viewCount=" + viewCount +
                ", starCount=" + starCount +
                '}';
    }
}
