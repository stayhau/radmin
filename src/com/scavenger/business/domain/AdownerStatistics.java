package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/3
 * Time: 15:02
 */
public class AdownerStatistics extends Id {
    private String channelId;
    private String country;
    private Integer request;
    private Integer click;
    private Integer install;
    private Integer callback;
    private Double payout;
    private Double interpayout;
    private String date;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getClick() {
        return click;
    }

    public void setClick(Integer click) {
        this.click = click;
    }

    public Integer getInstall() {
        return install;
    }

    public void setInstall(Integer install) {
        this.install = install;
    }

    public Integer getCallback() {
        return callback;
    }

    public void setCallback(Integer callback) {
        this.callback = callback;
    }

    public Double getPayout() {
        return payout;
    }

    public void setPayout(Double payout) {
        this.payout = payout;
    }

    public Double getInterpayout() {
        return interpayout;
    }

    public void setInterpayout(Double interpayout) {
        this.interpayout = interpayout;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
