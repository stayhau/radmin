package com.scavenger.business.domain;

import com.tuziilm.web.domain.RemarkStatusId;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/12
 * Time: ${Time}
 */
public class ChannelEffective extends RemarkStatusId {
    private String date;
    private String channel;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
