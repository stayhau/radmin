package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/12
 * Time: ${Time}
 */
public class SalesStatistics extends Id {
    private String channel;
    private String salesman;
    private Integer newTotal;
    private Integer newHigh;
    private Integer newMiddle;
    private Integer newNormal;
    private String date;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public Integer getNewTotal() {
        return newTotal;
    }

    public void setNewTotal(Integer newTotal) {
        this.newTotal = newTotal;
    }

    public Integer getNewHigh() {
        return newHigh;
    }

    public void setNewHigh(Integer newHigh) {
        this.newHigh = newHigh;
    }

    public Integer getNewMiddle() {
        return newMiddle;
    }

    public void setNewMiddle(Integer newMiddle) {
        this.newMiddle = newMiddle;
    }

    public Integer getNewNormal() {
        return newNormal;
    }

    public void setNewNormal(Integer newNormal) {
        this.newNormal = newNormal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
