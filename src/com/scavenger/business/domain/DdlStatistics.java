package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/5
 * Time: 16:12
 */
public class DdlStatistics extends Id {
    private Integer request;
    private Integer install;
    private String date;

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getInstall() {
        return install;
    }

    public void setInstall(Integer install) {
        this.install = install;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
