package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/19
 * Time: 15:17
 */
public class SdkChannelData extends Id {
    private String date;
    private String product;
    private String channel;
    private Integer highPrice;
    private Integer middlePrice;
    private Integer normalPrice;
    private Integer tabletActive;
    private Integer phoneActive;
    private Integer tabletAdd;
    private Integer phoneAdd;
    private Integer totalRegister;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(Integer highPrice) {
        this.highPrice = highPrice;
    }

    public Integer getMiddlePrice() {
        return middlePrice;
    }

    public void setMiddlePrice(Integer middlePrice) {
        this.middlePrice = middlePrice;
    }

    public Integer getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(Integer normalPrice) {
        this.normalPrice = normalPrice;
    }

    public Integer getTabletActive() {
        return tabletActive;
    }

    public void setTabletActive(Integer tabletActive) {
        this.tabletActive = tabletActive;
    }

    public Integer getPhoneActive() {
        return phoneActive;
    }

    public void setPhoneActive(Integer phoneActive) {
        this.phoneActive = phoneActive;
    }

    public Integer getTabletAdd() {
        return tabletAdd;
    }

    public void setTabletAdd(Integer tabletAdd) {
        this.tabletAdd = tabletAdd;
    }

    public Integer getPhoneAdd() {
        return phoneAdd;
    }

    public void setPhoneAdd(Integer phoneAdd) {
        this.phoneAdd = phoneAdd;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getTotalRegister() {
        return totalRegister;
    }

    public void setTotalRegister(Integer totalRegister) {
        this.totalRegister = totalRegister;
    }
}

