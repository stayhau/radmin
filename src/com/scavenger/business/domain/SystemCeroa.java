package com.scavenger.business.domain;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/29
 * Time: 15:28
 */
public class SystemCeroa {
    private Integer systemAdd;
    private Integer cerorAdd;
    private String date;
    private double rate;

    public Integer getSystemAdd() {
        return systemAdd;
    }

    public void setSystemAdd(Integer systemAdd) {
        this.systemAdd = systemAdd;
    }

    public Integer getCerorAdd() {
        return cerorAdd;
    }

    public void setCerorAdd(Integer cerorAdd) {
        this.cerorAdd = cerorAdd;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
