package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/12/28
 * Time: 14:26
 */
public class UserSaturation extends Id {
    private String channel;
    private Integer sat0;
    private Integer sat1;
    private Integer sat2;
    private String date;
    private Integer active;
    private Integer count;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getSat0() {
        return sat0;
    }

    public void setSat0(Integer sat0) {
        this.sat0 = sat0;
    }

    public Integer getSat1() {
        return sat1;
    }

    public void setSat1(Integer sat1) {
        this.sat1 = sat1;
    }

    public Integer getSat2() {
        return sat2;
    }

    public void setSat2(Integer sat2) {
        this.sat2 = sat2;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
