package com.scavenger.business.domain;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.tuziilm.web.domain.RemarkStatusId;

import java.util.Set;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/3
 * Time: ${Time}
 */
public class PriceArea extends RemarkStatusId {
    private Set<String> countries;
    private Integer type;
    private Integer price;

    private String typeName;

    public String getCountries() {
        if(countries==null || countries.isEmpty()){
            return "";
        }
        return Joiner.on(",").skipNulls().join(countries);
    }

    public void setCountries(String countries) {
        if(Strings.isNullOrEmpty(countries)){
            return;
        }
        this.countries = Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(countries));
    }

    public Set<String> getCountriesObject(){
        return this.countries;
    }

    public void setCountriesObject(Set<String> countries){
        this.countries = countries;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
