package com.scavenger.business.domain;

import com.tuziilm.web.common.AbstractJsonObject;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/24
 * Time: 11:25
 */
public class Event extends AbstractJsonObject {
    private String eventName;
    private String extData;
    private String ename;
    private Integer eventType;

    @JsonIgnore
    public String getJsonString(){
        return toJsonWithNoException(this);
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getExtData() {
        return extData;
    }

    public void setExtData(String extData) {
        this.extData = extData;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }
}
