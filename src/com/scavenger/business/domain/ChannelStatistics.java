package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/1
 * Time: 17:29
 */
public class ChannelStatistics extends Id {
    private String channel;
    private Integer reg;
    private Integer active;
    private Integer remain;
    private Double payout;
    private Double interpayout;
    private String date;
    private Boolean warning;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getReg() {
        return reg;
    }

    public void setReg(Integer reg) {
        this.reg = reg;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getRemain() {
        return remain;
    }

    public void setRemain(Integer remain) {
        this.remain = remain;
    }

    public Double getPayout() {
        return payout;
    }

    public void setPayout(Double payout) {
        this.payout = payout;
    }

    public Double getInterpayout() {
        return interpayout;
    }

    public void setInterpayout(Double interpayout) {
        this.interpayout = interpayout;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getWarning() {
        return warning;
    }

    public void setWarning(Boolean warning) {
        this.warning = warning;
    }

    @Override
    public String toString() {
        return  "{日期：'" + date + '\'' +
                ", 注册数：'" + reg + '\'' +
                ", 活跃数：'" + active + '\'' +
                ", 留存数：'" + remain + '\'' +
                ", 刷金额：'" + payout + '\'' +
                ", 拦截金额：'" + interpayout + '\'' +
                ", 渠道：'" + channel + '\'' +
                '}';
    }
}
