package com.scavenger.business.domain;

import com.tuziilm.web.common.AbstractJsonObject;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/12/2
 * Time: 14:50
 */
public class InnerEvent extends AbstractJsonObject {
    private Event event;

    public InnerEvent(Event event) {
        this.event = event;
    }

    @JsonIgnore
    public String getJsonString() {
        return toJsonWithNoException(this);
    }

    public String getEventName() {
        return event.getEventName();
    }

    public String getEname() {
        return event.getEname();
    }

    public Integer getEventType() {
        return event.getEventType();
    }

}
