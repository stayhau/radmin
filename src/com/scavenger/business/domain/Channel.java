package com.scavenger.business.domain;

import com.tuziilm.web.domain.RemarkStatusId;

public class Channel extends RemarkStatusId {
    private String name;
    private String from;
    private Integer day;//注册后第N天下发
    private double conversion;//转化率
    
    

    public double getConversion() {
		return conversion;
	}

	public void setConversion(double conversion) {
		this.conversion = conversion;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}
