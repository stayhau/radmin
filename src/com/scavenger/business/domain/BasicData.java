package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * 对接莲子数据
 * Date: 2016/9/22
 * Time: 17:23
 */
public class BasicData extends Id {
    private String product;
    private String channel;
    private Integer active;
    private Integer startTimes;
    private Integer countUsers;
    private Integer updateUsers;
    private Integer countStartTimes;
    private Integer newAdd;
    private String date;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getStartTimes() {
        return startTimes;
    }

    public void setStartTimes(Integer startTimes) {
        this.startTimes = startTimes;
    }

    public Integer getCountUsers() {
        return countUsers;
    }

    public void setCountUsers(Integer countUsers) {
        this.countUsers = countUsers;
    }

    public Integer getUpdateUsers() {
        return updateUsers;
    }

    public void setUpdateUsers(Integer updateUsers) {
        this.updateUsers = updateUsers;
    }

    public Integer getCountStartTimes() {
        return countStartTimes;
    }

    public void setCountStartTimes(Integer countStartTimes) {
        this.countStartTimes = countStartTimes;
    }

    public Integer getNewAdd() {
        return newAdd;
    }

    public void setNewAdd(Integer newAdd) {
        this.newAdd = newAdd;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
