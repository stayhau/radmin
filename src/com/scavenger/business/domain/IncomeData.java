package com.scavenger.business.domain;

import com.tuziilm.web.domain.RemarkStatusId;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/3
 * Time: ${Time}
 */
public class IncomeData extends RemarkStatusId {
    private String product;
    private String channel;
    private Integer newTotal;
    private Integer newHigh;
    private Integer newMiddle;
    private Integer newNormal;
    private double actualConversion;
    private Integer oldTotal;
    private Integer oldHigh;
    private Integer oldMiddle;
    private Integer oldNormal;
    private Integer oldNA;
    private double defaultConversion;
    private Integer income;
    private String date;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getNewTotal() {
        return newTotal;
    }

    public void setNewTotal(Integer newTotal) {
        this.newTotal = newTotal;
    }

    public Integer getNewHigh() {
        return newHigh;
    }

    public void setNewHigh(Integer newHigh) {
        this.newHigh = newHigh;
    }

    public Integer getNewMiddle() {
        return newMiddle;
    }

    public void setNewMiddle(Integer newMiddle) {
        this.newMiddle = newMiddle;
    }

    public Integer getNewNormal() {
        return newNormal;
    }

    public void setNewNormal(Integer newNormal) {
        this.newNormal = newNormal;
    }

    public double getActualConversion() {
        return actualConversion;
    }

    public void setActualConversion(double actualConversion) {
        this.actualConversion = actualConversion;
    }

    public Integer getOldTotal() {
        return oldTotal;
    }

    public void setOldTotal(Integer oldTotal) {
        this.oldTotal = oldTotal;
    }

    public Integer getOldHigh() {
        return oldHigh;
    }

    public void setOldHigh(Integer oldHigh) {
        this.oldHigh = oldHigh;
    }

    public Integer getOldMiddle() {
        return oldMiddle;
    }

    public void setOldMiddle(Integer oldMiddle) {
        this.oldMiddle = oldMiddle;
    }

    public Integer getOldNormal() {
        return oldNormal;
    }

    public void setOldNormal(Integer oldNormal) {
        this.oldNormal = oldNormal;
    }

    public Integer getOldNA() {
        return oldNA;
    }

    public void setOldNA(Integer oldNA) {
        this.oldNA = oldNA;
    }

    public double getDefaultConversion() {
        return defaultConversion;
    }

    public void setDefaultConversion(double defaultConversion) {
        this.defaultConversion = defaultConversion;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
