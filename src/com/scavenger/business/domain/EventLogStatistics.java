package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/24
 * Time: 20:34
 */
public class EventLogStatistics extends Id {
    private String app;
    private String channel;
    private String eventName;
    private String geo;
    private Integer active;
    private Integer newAdd;
    private Integer ipCount;
    private String date;
    private Integer tablet;
    private Integer phone;
    private Integer distinctAdd;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getNewAdd() {
        return newAdd;
    }

    public void setNewAdd(Integer newAdd) {
        this.newAdd = newAdd;
    }

    public Integer getIpCount() {
        return ipCount;
    }

    public void setIpCount(Integer ipCount) {
        this.ipCount = ipCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTablet() {
        return tablet;
    }

    public void setTablet(Integer tablet) {
        this.tablet = tablet;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Integer getDistinctAdd() {
        return distinctAdd;
    }

    public void setDistinctAdd(Integer distinctAdd) {
        this.distinctAdd = distinctAdd;
    }
}
