package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/12/28
 * Time: 14:26
 */
public class UserLoss extends Id {
    private String channel;
    private Integer lotuseedNewAdd;
    private Integer lotuseedActive;
    private Integer register;
    private Integer operateNewAdd;
    private Integer operateActive;
    private double activeRate;
    private Integer noimei;
    private Integer imeiRepeat;
    private Integer macRepeat;
    private double income;
    private Integer invaildArea;
    private String date;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getLotuseedNewAdd() {
        return lotuseedNewAdd;
    }

    public void setLotuseedNewAdd(Integer lotuseedNewAdd) {
        this.lotuseedNewAdd = lotuseedNewAdd;
    }

    public Integer getLotuseedActive() {
        return lotuseedActive;
    }

    public void setLotuseedActive(Integer lotuseedActive) {
        this.lotuseedActive = lotuseedActive;
    }

    public Integer getRegister() {
        return register;
    }

    public void setRegister(Integer register) {
        this.register = register;
    }

    public Integer getOperateNewAdd() {
        return operateNewAdd;
    }

    public void setOperateNewAdd(Integer operateNewAdd) {
        this.operateNewAdd = operateNewAdd;
    }

    public Integer getOperateActive() {
        return operateActive;
    }

    public void setOperateActive(Integer operateActive) {
        this.operateActive = operateActive;
    }

    public double getActiveRate() {
        return activeRate;
    }

    public void setActiveRate(double activeRate) {
        this.activeRate = activeRate;
    }

    public Integer getNoimei() {
        return noimei;
    }

    public void setNoimei(Integer noimei) {
        this.noimei = noimei;
    }

    public Integer getImeiRepeat() {
        return imeiRepeat;
    }

    public void setImeiRepeat(Integer imeiRepeat) {
        this.imeiRepeat = imeiRepeat;
    }

    public Integer getMacRepeat() {
        return macRepeat;
    }

    public void setMacRepeat(Integer macRepeat) {
        this.macRepeat = macRepeat;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public Integer getInvaildArea() {
        return invaildArea;
    }

    public void setInvaildArea(Integer invaildArea) {
        this.invaildArea = invaildArea;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
