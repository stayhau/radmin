package com.scavenger.business.domain;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.tuziilm.web.domain.Id;

import java.util.Set;

/**
 *
 * 隐藏日志渠道
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/30
 * Time: 16:45
 */
public class LogChannel extends Id {
    private String pkgName;//应用ID
    private Set<String> froms;

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getFroms() {
        if(froms==null || froms.isEmpty()){
            return "";
        }
        return Joiner.on(",").skipNulls().join(froms);
    }
    public void setFroms(String froms) {
        if(Strings.isNullOrEmpty(froms)){
            return;
        }
        this.froms= Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(froms));
    }

    public Set<String> getFromsObject(){
        return this.froms;
    }

    public void setFromsObject(Set<String> froms){
        this.froms= froms;
    }
}
