package com.scavenger.business.domain;

import com.tuziilm.web.domain.RemarkStatusId;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/6
 * Time: ${Time}
 */
public class ChannelConversion extends RemarkStatusId {
    private String priceAreaName;

    private String channel;

    private Integer conversion;

    private Integer highConversion;

    private Integer middleConversion;

    private Integer normalConversion;

    private String dividedWay;

    public String getPriceAreaName() {
        return priceAreaName;
    }

    public void setPriceAreaName(String priceAreaName) {
        this.priceAreaName = priceAreaName;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getConversion() {
        return conversion;
    }

    public void setConversion(Integer conversion) {
        this.conversion = conversion;
    }

    public Integer getHighConversion() {
        return highConversion;
    }

    public void setHighConversion(Integer highConversion) {
        this.highConversion = highConversion;
    }

    public Integer getMiddleConversion() {
        return middleConversion;
    }

    public void setMiddleConversion(Integer middleConversion) {
        this.middleConversion = middleConversion;
    }

    public Integer getNormalConversion() {
        return normalConversion;
    }

    public void setNormalConversion(Integer normalConversion) {
        this.normalConversion = normalConversion;
    }

    public ChannelConversion(String channel, Integer conversion) {
        this.channel = channel;
        this.conversion = conversion;
    }

    public ChannelConversion() {
    }

    public String getDividedWay() {
        return dividedWay;
    }

    public void setDividedWay(String dividedWay) {
        this.dividedWay = dividedWay;
    }
}
