package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * 获取指定产品用户留存数据
 * Date: 2016/9/22
 * Time: 17:23
 */
public class RevisitData extends Id {
    private String product;
    private String channel;
    private Integer oneDay;
    private Integer twoDays;
    private Integer threeDays;
    private Integer fourDays;
    private Integer fiveDays;
    private Integer sixDays;
    private Integer sevenDays;
    private Integer eightDays;
    private Integer nineDays;
    private Integer tenDays;
    private Integer fourteenDays;
    private Integer thirtyDays;
    private Integer ninetyDays;
    private String date;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getOneDay() {
        return oneDay;
    }

    public void setOneDay(Integer oneDay) {
        this.oneDay = oneDay;
    }

    public Integer getTwoDays() {
        return twoDays;
    }

    public void setTwoDays(Integer twoDays) {
        this.twoDays = twoDays;
    }

    public Integer getThreeDays() {
        return threeDays;
    }

    public void setThreeDays(Integer threeDays) {
        this.threeDays = threeDays;
    }

    public Integer getFourDays() {
        return fourDays;
    }

    public void setFourDays(Integer fourDays) {
        this.fourDays = fourDays;
    }

    public Integer getFiveDays() {
        return fiveDays;
    }

    public void setFiveDays(Integer fiveDays) {
        this.fiveDays = fiveDays;
    }

    public Integer getSixDays() {
        return sixDays;
    }

    public void setSixDays(Integer sixDays) {
        this.sixDays = sixDays;
    }

    public Integer getSevenDays() {
        return sevenDays;
    }

    public void setSevenDays(Integer sevenDays) {
        this.sevenDays = sevenDays;
    }

    public Integer getEightDays() {
        return eightDays;
    }

    public void setEightDays(Integer eightDays) {
        this.eightDays = eightDays;
    }

    public Integer getNineDays() {
        return nineDays;
    }

    public void setNineDays(Integer nineDays) {
        this.nineDays = nineDays;
    }

    public Integer getTenDays() {
        return tenDays;
    }

    public void setTenDays(Integer tenDays) {
        this.tenDays = tenDays;
    }

    public Integer getFourteenDays() {
        return fourteenDays;
    }

    public void setFourteenDays(Integer fourteenDays) {
        this.fourteenDays = fourteenDays;
    }

    public Integer getThirtyDays() {
        return thirtyDays;
    }

    public void setThirtyDays(Integer thirtyDays) {
        this.thirtyDays = thirtyDays;
    }

    public Integer getNinetyDays() {
        return ninetyDays;
    }

    public void setNinetyDays(Integer ninetyDays) {
        this.ninetyDays = ninetyDays;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
