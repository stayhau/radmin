package com.scavenger.business.domain;

import com.tuziilm.web.domain.RemarkStatusId;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/19
 * Time: 15:17
 */
public class DataStatistics extends RemarkStatusId {
    private String date;
    private String product;
    private String channel;
    private Integer highPrice;
    private Integer middlePrice;
    private Integer normalPrice;
    private Integer tabletActive;
    private Integer phoneActive;
    private Integer tabletAdd;
    private Integer phoneAdd;
    private Integer totalRegister;
    //界面展示数据
    private Integer newHighPrice;
    private Integer newMiddlePrice;
    private Integer newNormalPrice;

    private Integer highConversion;
    private Integer middleConversion;
    private Integer normalConversion;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(Integer highPrice) {
        this.highPrice = highPrice;
    }

    public Integer getMiddlePrice() {
        return middlePrice;
    }

    public void setMiddlePrice(Integer middlePrice) {
        this.middlePrice = middlePrice;
    }

    public Integer getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(Integer normalPrice) {
        this.normalPrice = normalPrice;
    }

    public Integer getTabletActive() {
        return tabletActive;
    }

    public void setTabletActive(Integer tabletActive) {
        this.tabletActive = tabletActive;
    }

    public Integer getPhoneActive() {
        return phoneActive;
    }

    public void setPhoneActive(Integer phoneActive) {
        this.phoneActive = phoneActive;
    }

    public Integer getTabletAdd() {
        return tabletAdd;
    }

    public void setTabletAdd(Integer tabletAdd) {
        this.tabletAdd = tabletAdd;
    }

    public Integer getPhoneAdd() {
        return phoneAdd;
    }

    public void setPhoneAdd(Integer phoneAdd) {
        this.phoneAdd = phoneAdd;
    }

    public Integer getNewHighPrice() {
        return newHighPrice;
    }

    public void setNewHighPrice(Integer newHighPrice) {
        this.newHighPrice = newHighPrice;
    }

    public Integer getNewMiddlePrice() {
        return newMiddlePrice;
    }

    public void setNewMiddlePrice(Integer newMiddlePrice) {
        this.newMiddlePrice = newMiddlePrice;
    }

    public Integer getNewNormalPrice() {
        return newNormalPrice;
    }

    public void setNewNormalPrice(Integer newNormalPrice) {
        this.newNormalPrice = newNormalPrice;
    }

    public Integer getHighConversion() {
        return highConversion;
    }

    public void setHighConversion(Integer highConversion) {
        this.highConversion = highConversion;
    }

    public Integer getMiddleConversion() {
        return middleConversion;
    }

    public void setMiddleConversion(Integer middleConversion) {
        this.middleConversion = middleConversion;
    }

    public Integer getNormalConversion() {
        return normalConversion;
    }

    public void setNormalConversion(Integer normalConversion) {
        this.normalConversion = normalConversion;
    }

    public Integer getTotalRegister() {
        return totalRegister;
    }

    public void setTotalRegister(Integer totalRegister) {
        this.totalRegister = totalRegister;
    }
}

