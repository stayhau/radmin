package com.scavenger.business.domain;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.tuziilm.web.domain.RemarkStatusId;

import java.util.Set;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/11
 * Time: 9:25
 */
public class CountryType extends RemarkStatusId {
    private String countryTypeName;
    private Set<String> countries;

    public String getCountries() {
        if(countries==null || countries.isEmpty()){
            return "";
        }
        return Joiner.on(",").skipNulls().join(countries);
    }

    public void setCountries(String countries) {
        if(Strings.isNullOrEmpty(countries)){
            return;
        }
        this.countries = Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(countries));
    }

    public Set<String> getCountriesObject(){
        return this.countries;
    }

    public void setCountriesObject(Set<String> countries){
        this.countries = countries;
    }

    public String getCountryTypeName() {
        return countryTypeName;
    }

    public void setCountryTypeName(String countryTypeName) {
        this.countryTypeName = countryTypeName;
    }
}
