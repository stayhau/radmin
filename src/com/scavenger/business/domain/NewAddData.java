package com.scavenger.business.domain;

import com.tuziilm.web.domain.RemarkStatusId;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/22
 * Time: 16:16
 */
public class NewAddData extends RemarkStatusId {
    private String date;
    private String product;
    private String channel;
    private Integer newAdd;
    private Integer gdAdd;
    private Integer suAdd;
    private Integer gmdAdd;
    private Integer ccAdd;
    private double gdRate;
    private double suRate;
    private double gmdRate;
    private double addRate;
    private Integer qrActive;
    private Integer suActive;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getNewAdd() {
        return newAdd;
    }

    public void setNewAdd(Integer newAdd) {
        this.newAdd = newAdd;
    }

    public Integer getGdAdd() {
        return gdAdd;
    }

    public void setGdAdd(Integer gdAdd) {
        this.gdAdd = gdAdd;
    }

    public Integer getSuAdd() {
        return suAdd;
    }

    public void setSuAdd(Integer suAdd) {
        this.suAdd = suAdd;
    }

    public double getGdRate() {
        return gdRate;
    }

    public void setGdRate(double gdRate) {
        this.gdRate = gdRate;
    }

    public double getSuRate() {
        return suRate;
    }

    public void setSuRate(double suRate) {
        this.suRate = suRate;
    }

    public Integer getGmdAdd() {
        return gmdAdd;
    }

    public void setGmdAdd(Integer gmdAdd) {
        this.gmdAdd = gmdAdd;
    }

    public Integer getCcAdd() {
        return ccAdd;
    }

    public void setCcAdd(Integer ccAdd) {
        this.ccAdd = ccAdd;
    }

    public double getAddRate() {
        return addRate;
    }

    public void setAddRate(double addRate) {
        this.addRate = addRate;
    }

    public double getGmdRate() {
        return gmdRate;
    }

    public void setGmdRate(double gmdRate) {
        this.gmdRate = gmdRate;
    }

    public Integer getQrActive() {
        return qrActive;
    }

    public void setQrActive(Integer qrActive) {
        this.qrActive = qrActive;
    }

    public Integer getSuActive() {
        return suActive;
    }

    public void setSuActive(Integer suActive) {
        this.suActive = suActive;
    }
}
