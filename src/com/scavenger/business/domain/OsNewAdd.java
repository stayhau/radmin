package com.scavenger.business.domain;

import com.tuziilm.web.domain.RemarkStatusId;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/6/24
 * Time: 9:56
 */
public class OsNewAdd extends RemarkStatusId {
    private String date;
    private String product;
    private String channel;
    private Integer newAdd;
    private String os;
    private double rate;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getNewAdd() {
        return newAdd;
    }

    public void setNewAdd(Integer newAdd) {
        this.newAdd = newAdd;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
