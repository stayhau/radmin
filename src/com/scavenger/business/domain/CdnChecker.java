package com.scavenger.business.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/10/11
 * Time: 16:00
 */
public class CdnChecker extends Id {
    private String name;
    private String isp;
    private String view;
    private String ip;
    private String ipFrom;
    private String totalTime;
    private String nsLookup;
    private String connectTime;
    private String downtime;
    private String fileSize;
    private String realSize;
    private String speed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIpFrom() {
        return ipFrom;
    }

    public void setIpFrom(String ipFrom) {
        this.ipFrom = ipFrom;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getNsLookup() {
        return nsLookup;
    }

    public void setNsLookup(String nsLookup) {
        this.nsLookup = nsLookup;
    }

    public String getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(String connectTime) {
        this.connectTime = connectTime;
    }

    public String getDowntime() {
        return downtime;
    }

    public void setDowntime(String downtime) {
        this.downtime = downtime;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getRealSize() {
        return realSize;
    }

    public void setRealSize(String realSize) {
        this.realSize = realSize;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return  "{监测点：'" + name + '\'' +
                ", ISP：'" + isp + '\'' +
                ", 省份：'" + view + '\'' +
                ", 解析IP：'" + ip + '\'' +
                ", 解析IP所在地：'" + ipFrom + '\'' +
                ", 总时间：'" + totalTime + '\'' +
                ", 解析时间：'" + nsLookup + '\'' +
                ", 连接时间：'" + connectTime + '\'' +
                ", 下载时间：'" + downtime + '\'' +
                ", 下载大小：'" + fileSize + '\'' +
                ", 文件大小：'" + realSize + '\'' +
                ", 下载速度：'" + speed + '\'' +
                '}';
    }
}
