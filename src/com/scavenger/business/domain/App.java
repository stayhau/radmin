package com.scavenger.business.domain;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.tuziilm.web.domain.Id;

import java.util.Set;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/24
 * Time: 11:24
 */
public class App extends Id {
    private String app;
    private String pkgName;
    private Set<String> froms;
    private Event[] events;
    private InnerEvent[] innerEvents;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getFroms() {
        if(froms==null || froms.isEmpty()){
            return "";
        }
        return Joiner.on(",").skipNulls().join(froms);
    }
    public void setFroms(String froms) {
        if(Strings.isNullOrEmpty(froms)){
            return;
        }
        this.froms= Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(froms));
    }

    public Set<String> getFromsObject(){
        return this.froms;
    }

    public void setFromsObject(Set<String> froms){
        this.froms= froms;
    }

    public Event[] getEventsObject() {
        return events;
    }
    public void setEventsObject(Event[] events) {
        this.events = events;
        if(events==null || events.length<1){
            return;
        }
        innerEvents = new InnerEvent[events.length];
        for(int i=0;i<events.length;i++){
            innerEvents[i]=new InnerEvent(events[i]);
        }
    }

    public InnerEvent[] getInnerEventsObject() {
        return innerEvents;
    }

    public String getInnerEvents() {
        return InnerEvent.toJsonWithNoException(innerEvents);
    }

    public String getEvents() {
        return Event.toJsonWithNoException(events);
    }

    public void setEvents(String pageRuleJson) {
        setEventsObject(Event.nullOnExceptionValueOf(pageRuleJson, Event[].class));
    }

    public void setInnerEvents(String pageRuleJson) {
        setEvents(pageRuleJson);
    }


}
