package com.scavenger.business.onlinebusiness.domain;

import com.tuziilm.web.domain.Id;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 */
public class SalesData extends Id {
    private String channel;
    private String salesman;
    private Integer newTotal;
    private Integer newHigh;
    private Integer newMiddle;
    private Integer newNormal;
    private String date;
    private String isShow;
    private double price;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public Integer getNewTotal() {
        return newTotal;
    }

    public void setNewTotal(Integer newTotal) {
        this.newTotal = newTotal;
    }

    public Integer getNewHigh() {
        return newHigh;
    }

    public void setNewHigh(Integer newHigh) {
        this.newHigh = newHigh;
    }

    public Integer getNewMiddle() {
        return newMiddle;
    }

    public void setNewMiddle(Integer newMiddle) {
        this.newMiddle = newMiddle;
    }

    public Integer getNewNormal() {
        return newNormal;
    }

    public void setNewNormal(Integer newNormal) {
        this.newNormal = newNormal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
