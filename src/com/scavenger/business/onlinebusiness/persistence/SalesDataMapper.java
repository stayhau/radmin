package com.scavenger.business.onlinebusiness.persistence;

import com.scavenger.business.onlinebusiness.domain.SalesData;
import com.tuziilm.web.persistence.BaseMapper;

import java.util.List;

public interface SalesDataMapper extends BaseMapper<SalesData> {
    int insertBatch(List<SalesData> salesDatas);
    List<String> getChannels();
}
