package com.scavenger.business.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.Properties;

public class EncryptablePropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
	protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
		throws BeansException {
			try {
				super.processProperties(beanFactory, PropertiesUtils.loadMetaProps(props));
			} catch (Exception e) {
				e.printStackTrace();
				throw new BeanInitializationException(e.getMessage());
			}
		}
}