package com.scavenger.business.common;

/**
 * @author Lorry
 * @since 2020-03-24 0:19
 **/
public interface RedisKey {
    String OFFER_LIST_KEY = "offerList:offer%d";
    String ISSUE_COUNT_KEY = "issueOfferCount:offer%d";
    String FILL_AMOUNT_KEY = "fillAmount:offer%d";
    String USER_REPORTED_KEY = "userReported-1:offer%d";
}
