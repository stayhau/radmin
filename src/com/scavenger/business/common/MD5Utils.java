package com.scavenger.business.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Utils {

	private static final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
			'e', 'f' };
	private static final int HEX_RIGHT_SHIFT_COEFFICIENT = 4;
	private static final int HEX_HIGH_BITS_BITWISE_FLAG = 0x0f;

	/**
	 * Takes the raw bytes from the digest and formats them correct.
	 *
	 * @param bytes
	 *            the raw bytes from the digest.
	 * @return the formatted bytes.
	 */
	public static String encode(final byte[] bytes) {

			try {
				MessageDigest md5 = MessageDigest.getInstance("MD5");
				byte[] encode = md5.digest(bytes);
				return getFormattedText(encode);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return bytes.toString();
	}
	

	public static String encode(final String password, final String salt) throws UnsupportedEncodingException {
		String beforeEncode = password + salt;
		return encode(beforeEncode.getBytes("utf-8"));
	}

	public static String encode(final String passwordAndSalt) throws UnsupportedEncodingException {
		return encode(passwordAndSalt.getBytes("utf-8"));
	}

	
	
	
    /**
     * Takes the raw bytes from the digest and formats them correct.
     *
     * @param bytes the raw bytes from the digest.
     * @return the formatted bytes.
     */
    private static String getFormattedText(final byte[] bytes) {
        final StringBuilder buf = new StringBuilder(bytes.length * 2);

        for (int j = 0; j < bytes.length; j++) {
            buf.append(HEX_DIGITS[(bytes[j] >> HEX_RIGHT_SHIFT_COEFFICIENT) & HEX_HIGH_BITS_BITWISE_FLAG]);
            buf.append(HEX_DIGITS[bytes[j] & HEX_HIGH_BITS_BITWISE_FLAG]);
        }
        return buf.toString();
    }

	
	
	
	public static void main(String[] args) throws IOException {
		if (args != null && args.length > 0) {

			String beforeEncode  = args[0];
			System.out.println(beforeEncode);

			System.out.println(MD5Utils.encode(beforeEncode));
		} else {
			System.out.println("������");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String str = br.readLine();
			
			System.out.println(MD5Utils.encode(str));
		}

	}
	
	
	
	

}
