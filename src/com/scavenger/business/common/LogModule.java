package com.scavenger.business.common;


/**
 * 日志统计模块
 *
 */
public enum LogModule {
    SHORTCUT("快捷方式", "get/shortcut"),
    AD_SCREEN("插屏广告", "get/ad_screen"),
    POLICY("策略", "get/policy");

	private String title;
    private String link;

	private LogModule(String title,String link) {
		this.title = title;
        this.link = link;
	}

    public String getLink() {
        return link;
    }

    public String getTitle() {
		return title;
	}
}
