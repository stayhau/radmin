package com.scavenger.business.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * 日期方法类
 */
public final class DateUtil {
	// ~ Static fields/initializers
	static Locale locale = Locale.US;

	/** 秒数 */
	public static final int SECOND = 1000;

	/** 分数 */
	public static final int MINUTE = SECOND * 60;

	/** 时数 */
	public static final int HOUR = MINUTE * 60;

	/** 天数 */
	public static final int DAY = HOUR * 24;

	/** 周数 */
	public static final int WEEK = DAY * 7;

	/** 年数 */
	public static final int YEAR = DAY * 365; // or 366 ???

	/** DOCUMENT ME! */
	public static SimpleDateFormat dateFormatYMD = new SimpleDateFormat(
			"yyyy-MM-dd");
	
	/** DOCUMENT ME! */
	public static SimpleDateFormat dateFormatYMD1 = new SimpleDateFormat(
			"yyyy/MM/dd");

	/** DOCUMENT ME! */
	public static SimpleDateFormat dateFormatYMDHMS = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	/** DOCUMENT ME! */
	public static SimpleDateFormat dateFormatHMS = new SimpleDateFormat(
			"HH:mm:ss");

	public static SimpleDateFormat dateFormatHMS1 = new SimpleDateFormat(
			"HHmmss");

	public static DecimalFormat decimalValue = new DecimalFormat("##.00");

	/** DOCUMENT ME! */
	public static SimpleDateFormat weekFormat = new SimpleDateFormat(
			"yyyy年M月d日");

	public static SimpleDateFormat weekFormatYMDcn = new SimpleDateFormat(
			"yyyy年M月d日");


	public static SimpleDateFormat YYYYYMMDDHHSSFormat = new SimpleDateFormat(
			"yyyy年MM月dd日HH时mm分");

	/** DOCUMENT ME! */
	public static SimpleDateFormat DateFormatYYYYMMDD = new SimpleDateFormat(
			"yyyyMMdd");

	/** DOCUMENT ME! */
	private static final String[] WEEKS = { "星期日", "星期一", "星期二", "星期三", "星期四",
			"星期五", "星期六" };

	/** **生肖数组 ***** */
	public final static String[] ANIMALS = new String[] { "", "鼠", "牛", "虎",
			"免", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" };

	public final static String[] CONSTELLATIONS = new String[] { "", "牡羊座",
			"金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座",
			"水瓶座", "双鱼座" };

	/** 时间部分（用于表示要取一个时间的哪个部分）－年 */
	public static final int DATE_PART_YEAR = 0;

	/** 时间部分（用于表示要取一个时间的哪个部分）－月 */
	public static final int DATE_PART_MONTH = 1;

	/** 时间部分（用于表示要取一个时间的哪个部分）－日 */
	public static final int DATE_PART_DAY = 2;

	/** 时间部分（用于表示要取一个时间的哪个部分）－年，不做18年前限制 */
	public static final int DATE_PART_YEAR2 = 3;
	
	public static final String PATTEM_DATE="yyyy-MM-dd";
	public static final String PATTEM_DATE_YYYYMMDD="yyyyMMdd";
	public static final String PATTEM_TIME="HH:mm:ss";
	public static final String PATTEM_DATE_TIME="yyyy-MM-dd HH:mm:ss";
	
	public static final String PATTEM_TIMESTAMP="yyyy-MM-dd HH:mm:ss.SSS";
	
	public static final String PATTEM_DAY_BEGIN="yyyy-MM-dd 00:00:00";
	public static final String PATTEM_DAY_END="yyyy-MM-dd 23:59:59";
	public static final long ONEDAY_MILLISECONDS=24*60*60*1000;

	// ~ Constructors

	/**
	 * private constructor
	 */

	// ~ Methods
	/**
	 * 格式化日期
	 * 
	 * @param date
	 *            Date
	 * 
	 * @return String
	 */
	public static String formatDateTimeWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.setFirstDayOfWeek(Calendar.SUNDAY);

		int weeknum = cal.get(Calendar.DAY_OF_WEEK) - 1;
        StringBuffer dateStr = new StringBuffer(26);

		dateStr.append(weekFormat.format(date)).append(" ").append(WEEKS[weeknum]);
        return dateStr.toString();
	}
	
	
	public static String now(){
		return dateFormatYMDHMS.format(new Date());
	}
	
	
	public static boolean isNew(Date date) {
        long hour = getSubtractHour(date);
        if(hour <= 24) {
        	return true;
        } else {
        	return false;
        }
	}
	/**
	 * 根据用户的生日计算年龄
	 * 
	 * @param memberBirthday
	 * @return String
	 */
	public static String getMemberAge(Date memberBirthday) {
		String retVal = "";

		try {
			Calendar birthday = Calendar.getInstance();
			birthday.setTimeInMillis(memberBirthday.getTime());

			Calendar today = Calendar.getInstance();
			int age = today.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);
			birthday.add(Calendar.YEAR, age);
			retVal = Integer.toString(age);
		} catch (Exception ex) {
			;
		}
		return retVal;
	}

	/**
	 * 当前日期
	 * 
	 * @return Date
	 */
	public static Date getCurrentDateTime() {
		return new Date(System.currentTimeMillis());
	}
	
	public static Date getCurrentDate()
	{
		Calendar cal = new GregorianCalendar();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH),0,0,0);
		return cal.getTime();
	}
	
	public static void main(String[] args) {
		System.out.println(getCurrentDate());
		System.out.println(getCurrentDate().getTime());
	}
	
	public static java.sql.Date getCurrentSQLDate() {
		return new java.sql.Date(System.currentTimeMillis());
	}

	public static java.sql.Date getMonthFirstDate(Date time, int month) {
		Date date = new Date(time.getTime());
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);

		int Month2 = Month + month;

		cal.set(Calendar.YEAR, Year);
		cal.set(Calendar.MONTH, Month2);
		cal.set(Calendar.DAY_OF_MONTH, 1);

		return new java.sql.Date(cal.getTimeInMillis());
	}
	/**
	 * 生日的年份
	 * 
	 * @param birth
	 *            Timestamp
	 * 
	 * @return int
	 */
	public static int getBirthYear(Timestamp birth) {
		int birth_year = 0;
		Calendar birthday = Calendar.getInstance();
		birthday.setTimeInMillis(birth.getTime());
		birth_year = birthday.get(Calendar.YEAR);

		return birth_year;
	}

	/**
	 * 生日的月份
	 * 
	 * @param birth
	 *            Timestamp
	 * 
	 * @return int
	 */
	public static int getBirthMonth(Timestamp birth) {
		int birth_month = 0;
		Calendar birthday = Calendar.getInstance();
		birthday.setTimeInMillis(birth.getTime());
		birth_month = birthday.get(Calendar.MONTH) + 1;

		return birth_month;
	}

	/**
	 * 生日的日期
	 * 
	 * @param birth
	 *            Timestamp
	 * 
	 * @return int
	 */
	public static int getBirthDay(Timestamp birth) {
		int birth_day = 0;
		Calendar birthday = Calendar.getInstance();
		birthday.setTimeInMillis(birth.getTime());
		birth_day = birthday.get(Calendar.DATE);

		return birth_day;
	}

	/**
	 * 图片路径
	 * 
	 * @return String
	 */
	public static String getPicPath() {
		StringBuffer picpath = new StringBuffer(50);
		Calendar date = Calendar.getInstance();
		picpath.append("/");
		picpath.append(date.get(Calendar.DAY_OF_MONTH) % 8);
		picpath.append("/");
		picpath.append(date.get(Calendar.MINUTE));
		picpath.append("/");
		picpath.append(date.get(Calendar.SECOND));
		picpath.append("/");

		return picpath.toString();
	}

	/**
	 * 当前的Timestamp
	 * 
	 * @return Timestamp
	 */
	public static Timestamp getCurrentTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	/**
	 * 将 Timestamp 转为 String
	 * 
	 * @param timestamp
	 *            Timestamp
	 * @param displayTime
	 *            是否显示时间部分
	 * 
	 * @return String
	 */
	public static String timestampToString(Timestamp timestamp,
			boolean displayTime) {
		if (timestamp == null) {
			return "";
		}

		if (displayTime) {
			return timestamp.toString().substring(0, 16);
		}

		return timestamp.toString().substring(0, 10);
	}

	/**
	 * 将 Timestamp 转为 String (包括秒)
	 * 
	 * @param timestamp
	 *            Timestamp
	 * @param displayTime
	 *            是否显示时间部分(包括秒)
	 * 
	 * @return String
	 */
	public static String timestampToStringSecond(Timestamp timestamp,
			boolean displayTime) {
		if (timestamp == null) {
			return "";
		}

		if (displayTime) {
			return timestamp.toString().substring(0, 19);
		} else {
			return timestamp.toString().substring(0, 10);
		}
	}

	/**
	 * 将 Timestamp 转为 String，不显示时间部分
	 * 
	 * @param timestamp
	 *            Timestamp
	 * 
	 * @return String
	 */
	public static String timestampToString(Timestamp timestamp) {
		return timestampToString(timestamp, false);
	}

	/**
	 * 截取时间的年月日
	 * 
	 * @param datetime
	 * 
	 * @return
	 */
	public static String getYMD(Timestamp datetime) {
		return dateFormatYMD.format(datetime);
	}
	
	
	/**
	 * 截取时间的年月日
	 * 
	 * @param datetime
	 * 
	 * @return
	 */
	public static String getYMD(Date datetime) {
		if(datetime==null)return null;
		return dateFormatYMD.format(datetime);
	}
	
	public static String getFormatDate(Date... date){
		return dateFormatYMD.format(date.length == 1 ? date[0] : new Date());
	}

	/**
	 * 截取时间小时
	 * 
	 * @param datetime
	 * 
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static int getHour(String datetime) {
		if ((datetime == null) || datetime.equals("")) {
			return 0;
		} else {
			Timestamp datavalue = Timestamp.valueOf(datetime);
			return datavalue.getHours();
		}
	}

	/**
	 * 截取时间的年月日小时
	 * 
	 * @return
	 */
	public static String getYMDHSMFormat(Timestamp date) {

		return dateFormatYMDHMS.format(date);
	}

	/**
	 * 截取时间的年月日小时
	 * 
	 * @return
	 */
	public static String getYMDHSMFormat(Date date) {

		return dateFormatYMDHMS.format(date);
	}
	
	public static String getDateFormatYYYYMMDD(Timestamp date) {
		return DateFormatYYYYMMDD.format(date);
	}

	public static String getDateFormatYYYYMMDDHHSS(java.sql.Timestamp date) {
		return YYYYYMMDDHHSSFormat.format(date);
	}
	
	/**
	 * 根据时间字符串 返回时间 yyyy-MM-dd HH:mm:ss
	 * @param timeStr
	 * @return
	 */
	public static Date getDateDateFormatYMDHMS(String timeStr){
		try {
			return dateFormatYMDHMS.parse(timeStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date();
	}
	

	/**
	 * 得到当前时间一个日期运算天数后的结果，加（5）或减（-5）
	 * 
	 * @param days
	 *            （正数DAY 是当前时间的将来DAY天后日期，负数是当前时间过去DAY天的日期）
	 * @return
	 */
	public static Timestamp getAddDayDate(int days) {
		Calendar cal = Calendar.getInstance();

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);
		int Day = cal.get(Calendar.DAY_OF_MONTH);

		int Day2 = Day + days;

		cal.set(Calendar.YEAR, Year);
		cal.set(Calendar.MONTH, Month);
		cal.set(Calendar.DAY_OF_MONTH, Day2);

		return new Timestamp(cal.getTimeInMillis());
	}

	/**
	 * 得到当前时间一个日期运算天数后的结果，加（5）或减（-5）
	 * 
	 * @param days
	 *            （正数DAY 是当前时间的将来DAY天后日期，负数是当前时间过去DAY天的日期）
	 * @return
	 */
	public static Timestamp getAddDayDateReset(int days, int hour, int minute) {
		Calendar cal = Calendar.getInstance();

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);
		int Day = cal.get(Calendar.DAY_OF_MONTH);

		int Day2 = Day + days;

		cal.set(Calendar.YEAR, Year);
		cal.set(Calendar.MONTH, Month);
		cal.set(Calendar.DAY_OF_MONTH, Day2);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new Timestamp(cal.getTimeInMillis());
	}

	/**
	 * 得到两个时间相差多少天
	 * 
	 * @param beginDate
	 * @param endTime
	 * @return
	 */
	public static int getDateMargin(Timestamp beginDate, Timestamp endDate) {
		int day = 0;
		final int mOfDay = 1000 * 60 * 60 * 24;
		final long divtime = (endDate.getTime() - beginDate.getTime());
		final long lday = divtime % mOfDay > 0 ? divtime / mOfDay + 1 : divtime
				/ mOfDay;
		day = Long.valueOf(lday).intValue();
		return day;
	}
	
	
	/**
	 * 得到两个时间相差多少天
	 * 
	 * @param beginDate
	 * @param endTime
	 * @return
	 */
	public static int getDateMargin(Date beginDate, Date endDate) {
		int day = 0;
		final int mOfDay = 1000 * 60 * 60 * 24;
		final long divtime = (endDate.getTime() - beginDate.getTime());
		final long lday = divtime % mOfDay > 0 ? divtime / mOfDay + 1 : divtime
				/ mOfDay;
		day = Long.valueOf(lday).intValue();
		return day;
	}

	/**
	 * 获取距离今天结束时间还有多少分钟
	 * @return
	 */
	public static long getMinuteEndlong(){
		Calendar cal = Calendar.getInstance();
		int h = cal.get(Calendar.HOUR_OF_DAY);
		int m = cal.get(Calendar.MINUTE);
		int t = (24-h)*60-m;
		return t;
	}
	
	/**
	 * 得到两个时间相差多少分钟
	 * 
	 * @param beginDate
	 * @param endTime
	 * @return
	 */
	public static long getMinuteMargin(Date beginDate, Date endDate) {
		long nd = 1000 * 24 * 60 * 60;
	    long nh = 1000 * 60 * 60;
	    long nm = 1000 * 60;
	    // 获得两个时间的毫秒时间差异
	    long diff =beginDate.getTime() - endDate.getTime();
	    long min = diff % nd % nh / nm;
		return min;
	}
	
	
	public static String getDateLong(Timestamp beginDate, Timestamp endDate) {
		long divtime = (endDate.getTime() - beginDate.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(divtime);
		String result = "";
		if (divtime > 0) {
			long hour = divtime / (60000 * 60);
			divtime = divtime - hour * 60000 * 60;
			long min = divtime / 60000;
			divtime = divtime - min * 60000;
			long sec = divtime / 1000;
			result = hour + ":" + min + ":" + sec;
		} else {
			result = "00:00:00.0";
		}
		return result;
	}

	public static String getDateLong(Long divtime) {
		String result = "";
		if (divtime > 0) {
			long hour = divtime / (60000 * 60);
			divtime = divtime - hour * 60000 * 60;
			long min = divtime / 60000;
			divtime = divtime - min * 60000;
			long sec = divtime / 1000;
			result = hour + ":" + min + ":" + sec;
		} else {
			result = "00:00:00.0";
		}
		return result;
	}

	/**
	 * 得到一个日期运算月数后的结果，加（5）或减（-5）
	 * 
	 * @param month
	 *            （正数month 是当前时间的将来month月后日期，负数是当前时间过去month月的日期）
	 * @return
	 */
	public static Timestamp getAddMonthDate(int month) {
		Calendar cal = Calendar.getInstance();

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);
		int Day = cal.get(Calendar.DAY_OF_MONTH);

		int Month2 = Month + month;

		cal.set(Calendar.YEAR, Year);
		cal.set(Calendar.MONTH, Month2);
		cal.set(Calendar.DAY_OF_MONTH, Day);

		return new Timestamp(cal.getTimeInMillis());
	}

	/**
	 * 
	 * 将指定的日期加上指定的月数并返回，加（5）或减（-5）
	 * 
	 * @param time
	 *            指定的时间
	 * @param month
	 *            （正数month 是当前时间的将来month月后日期，负数是当前时间过去month月的日期）
	 * @return
	 */
	public static java.sql.Date getAddMonthDate(Date time, int month) {
		Date date = new Date(time.getTime());
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);
		int Day = cal.get(Calendar.DAY_OF_MONTH);

		int Month2 = Month + month;

		cal.set(Calendar.YEAR, Year);
		cal.set(Calendar.MONTH, Month2);
		cal.set(Calendar.DAY_OF_MONTH, Day);

		return new java.sql.Date(cal.getTimeInMillis());
	}

	/**
	 * 
	 * 将指定的日期加上指定的天数并返回，加（5）或减（-5）
	 * 
	 * @param time
	 *            指定的时间
	 * @param month
	 *            （正数dy 是当前时间的将来day天后日期，负数是当前时间过去day天的日期）
	 * @return
	 */
	public static java.sql.Date getAddDayDate(Date time, int day) {
		Date date = new Date(time.getTime());
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);
		int Day = cal.get(Calendar.DAY_OF_MONTH);

		int Day2 = Day + day;

		cal.set(Calendar.YEAR, Year);
		cal.set(Calendar.MONTH, Month);
		cal.set(Calendar.DAY_OF_MONTH, Day2);

		return new java.sql.Date(cal.getTimeInMillis());
	}

	/**
	 * date日期运算了addNum个时间单位后的日期
	 * 
	 * @param date
	 *            日期
	 * @param addNum
	 *            要的加单位量
	 * @param ymdhmsType
	 *            1 年 2 月 3 日 4 时 5分 6秒
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Timestamp getAddMonthDate(java.sql.Date date, int addNum,
			int ymdhmsType) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);
		int Day = cal.get(Calendar.DAY_OF_MONTH);
		int Hour = cal.get(Calendar.HOUR_OF_DAY);
		int Minute = cal.get(Calendar.MINUTE);
		int Second = cal.get(Calendar.SECOND);
		int tempAddnum = 0;
		switch (ymdhmsType) {
		case 1:
			tempAddnum = Year + addNum;
			cal.set(Calendar.YEAR, tempAddnum);
			break;
		case 2:
			tempAddnum = Month + addNum;
			cal.set(Calendar.MONTH, tempAddnum);
			break;
		case 3:
			tempAddnum = Day + addNum;
			cal.set(Calendar.DAY_OF_MONTH, tempAddnum);
			break;
		case 4:
			tempAddnum = Hour + addNum;
			cal.set(Calendar.HOUR_OF_DAY, tempAddnum);
			break;
		case 5:
			tempAddnum = Minute + addNum;
			cal.set(Calendar.MINUTE, tempAddnum);
			break;
		case 6:
			tempAddnum = Second + addNum;
			cal.set(Calendar.SECOND, tempAddnum);
			break;
		}
		
		
//		日期往前计算时，遇到29，30，31这几天，又刚好跨2月的29--31日时候处理
		if( addNum < 0 && (Day == 1 || Day == 2 || Day==3) && date.getMonth()==2){
				//Calendar newCal = new GregorianCalendar();
				//newCal.setTime(returnTimestamp);
				switch(Day){				
				case 1:
					if(cal.get(Calendar.YEAR)/4 != 0){
						cal.set(Calendar.DAY_OF_MONTH, Day-1);						
					}
					break;
				case 2:
					cal.set(Calendar.DAY_OF_MONTH, Day-2);					
					break;
				case 3:					
					cal.set(Calendar.DAY_OF_MONTH, Day-3);					
					break;
				}				
		}
		return  new Timestamp(cal.getTimeInMillis());
	}

	/**
	 * date日期运算了addNum个时间单位后的日期
	 * 
	 * @param date
	 *            日期
	 * @param addNum
	 *            要的加单位量
	 * @param ymdhmsType
	 *            1 年 2 月 3 日 4 时 5分 6秒
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Timestamp getAddMonthDate(Timestamp date, int addNum,
			int ymdhmsType) {
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);
		int Day = cal.get(Calendar.DAY_OF_MONTH);
		int Hour = cal.get(Calendar.HOUR_OF_DAY);
		int Minute = cal.get(Calendar.MINUTE);
		int Second = cal.get(Calendar.SECOND);
		int tempAddnum = 0;
		switch (ymdhmsType) {
		case 1:
			tempAddnum = Year + addNum;
			cal.set(Calendar.YEAR, tempAddnum);
			break;
		case 2:
			tempAddnum = Month + addNum;
			cal.set(Calendar.MONTH, tempAddnum);
			break;
		case 3:
			tempAddnum = Day + addNum;
			cal.set(Calendar.DAY_OF_MONTH, tempAddnum);
			break;
		case 4:
			tempAddnum = Hour + addNum;
			cal.set(Calendar.HOUR_OF_DAY, tempAddnum);
			break;
		case 5:
			tempAddnum = Minute + addNum;
			cal.set(Calendar.MINUTE, tempAddnum);
			break;
		case 6:
			tempAddnum = Second + addNum;
			cal.set(Calendar.SECOND, tempAddnum);
			break;
		}

//		日期往前计算时，遇到29，30，31这几天，又刚好跨2月的29--31日时候处理
		if( addNum < 0 && (Day == 1 || Day == 2 || Day==3) && date.getMonth()==2){
				//Calendar newCal = new GregorianCalendar();
				//newCal.setTime(returnTimestamp);
				switch(Day){				
				case 1:
					if(cal.get(Calendar.YEAR) % 4 != 0){
						cal.set(Calendar.DAY_OF_MONTH, -1);						
					}
					break;
				case 2:
					cal.set(Calendar.DAY_OF_MONTH, Day-2);					
					break;
				case 3:					
					cal.set(Calendar.DAY_OF_MONTH, Day-3);					
					break;
				}				
		}
		return  new Timestamp(cal.getTimeInMillis());
	}

	/**
	 * 得到一个日期运算年数后的结果，加（5）或减（-5）
	 * 
	 * @param years
	 *            （正数years 是当前时间的将来years天后日期，负数是当前时间过去years天的日期）
	 * @param timestamp
	 *            用来控制是否需要显示时分秒
	 * @return
	 */
	public static Timestamp getAddYearDate(int years, boolean timestamp) {
		Calendar cal = Calendar.getInstance();

		int Year = cal.get(Calendar.YEAR);
		int Month = cal.get(Calendar.MONTH);

		cal.set(Calendar.YEAR, Year + years);
		cal.set(Calendar.MONTH, Month);
		if (!timestamp) {
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		}

		return new Timestamp(cal.getTimeInMillis());
	}

	/*private static Date addDate(Date d, int days) {
		int Year, Month, Month2, Month_r, Day, Day2, Day_r, i;
		boolean rYear;
		int DayOfMonths[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		Calendar cal;
		cal = Calendar.getInstance();
		cal.setTime(d);
		Year = cal.get(Calendar.YEAR);
		Month = cal.get(Calendar.MONTH);
		Day = cal.get(Calendar.DAY_OF_MONTH);

		Day2 = Day + days;

		// 判断是否为闰年
		if ((Year % 4 == 0 && Year % 100 != 0) || Year % 400 == 0) {
			rYear = true;
		} else {
			rYear = false;
		}

		// 如果是闰年,2月份就是29天否则是28天
		if (rYear) {
			DayOfMonths[1] = 29;
		} else {
			DayOfMonths[1] = 28;
		}

		// 跨度为几个月，请求出结果月
		i = 0;
		while (true) {
			if ((Month + i) > 11) {
				Month = 1;
				i = 0;
				Year++;
			}
			Day_r = Day2;
			Day2 = Day2 - DayOfMonths[Month + i];
			if (Day2 < 0) {
				Month_r = Month + i + 1;
				break;
			} else {
				i++;
			}
		}

		cal.set(Calendar.YEAR, Year);
		cal.set(Calendar.MONTH, Month);
		cal.set(Calendar.DAY_OF_MONTH, Day_r);

		return cal.getTime();
	}*/

	/**
	 * 通过出生日期计算出一个人的年龄
	 * 
	 * @param date
	 * @return
	 */
	public static int getAge(Date memberBirthday) {
		if (memberBirthday == null)
			return -1;
		Calendar birthday = Calendar.getInstance();
		birthday.setTimeInMillis(memberBirthday.getTime());

		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);
		birthday.add(Calendar.YEAR, age);

		if (today.before(birthday)) {
			age--;
		}
		return age;
	}

	/**
	 * 通过出生日期计算出一个人的年龄
	 * 
	 * @param date
	 * @return
	 */
	public static String getAgeStr(Date date) {
		if (date == null)
			return "未知";
		return String.valueOf(getMemberAge(date));
	}

	/**
	 * 是否有效的日期
	 * 
	 * @param strDate
	 * @param pattern
	 * @return
	 */
	public static boolean isValidDate(String strDate, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);

		try {
			df.setLenient(false);
			df.parse(strDate);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	/**
	 * 返回格式化为XXXX年X月X日的日期格式字符串
	 * 
	 * @param date
	 * @return
	 */
	public static String getYMDcn(Timestamp date) {
		if (date == null) {
			return "";
		}
		return weekFormatYMDcn.format(date);
	}
	public static String getYMDcndate(Date date) {
		if (date == null) {
			return "";
		}
		return weekFormatYMDcn.format(date);
	}

	/**
	 * 返回格式化为XXXX年X月X日的日期格式字符串
	 * 
	 * @param date
	 * @return
	 */
	public static String getYMDcnUtil(Date date) {
		if (date == null) {
			return "";
		}
		return weekFormatYMDcn.format(date);
	}
	
	/**
	 * 
	 * @param dateStr
	 *            dateStr like new Date().toString()
	 * @param formatStr
	 *            formatStr like "yyyy-MM-dd hh:mm:ss"
	 * @return
	 */
	public static String dateParser2String(String dateStr, String formatStr) {

		// Object locale = null;
		SimpleDateFormat bartDateFormat = new SimpleDateFormat(formatStr,
				locale);
		Format format = null;
		try {
			Date date = bartDateFormat.parse(dateStr);
			format = new SimpleDateFormat(formatStr);
			return format.format(date);
		} catch (ParseException pe) {
			System.err.println(pe.getMessage());
		}
		return null;
	}

	/**
	 * 
	 * @param dateStr
	 * @param formatStr
	 * @return
	 */
	public static String date2String(Date date) {
		return date2String(date, "yyyy-MM-dd hh:mm:ss");
	}

	public static String date2String(Date date, String formatStr) {

		// Object locale = null;
		Format format = null;
		try {
			// Date date = bartDateFormat.parse(dateStr);
			format = new SimpleDateFormat(formatStr);
			return format.format(date);
		} catch (Exception pe) {
			System.err.println(pe.getMessage());
		}
		return "";
	}

	public static String getYMDDate(java.sql.Date date) {
		if (date == null) {
			return "";
		}
		return weekFormatYMDcn.format(date);
	}

	/**
	 * 一个时间和当前时间的比较 如果date是在当前时间之前，返回F，否则返回T
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isAfterDate(java.sql.Date date) {
		if (date == null) {
			return false;
		}
		return date.after(new java.sql.Date(System.currentTimeMillis()));
	}

	/**
	 * 计算1900年以后的年份对应的生肖，只能大致计算，因为生肖是按农历数
	 * 
	 * @param date
	 *            1900年以后的日期
	 * @return short 1鼠,2牛,3虎,4免,5龙,6蛇,7马,8羊,9猴,10鸡,11狗,12猪
	 */
	public static short getAnimal(Calendar date) {
		int year = date.get(Calendar.YEAR);
		// １900年是鼠年
		year = year - 1899;
		year = year % 12;
		return Integer.valueOf(year).shortValue();
	}

	/**
	 * 计算1900年以后的年份对应的生肖，只能大致计算，因为生肖是按农历计数
	 * 
	 * @param date
	 *            1900年以后的日期
	 * @return short 1鼠,2牛,3虎,4免,5龙,6蛇,7马,8羊,9猴,10鸡,11狗,12猪
	 */
	public static short getAnimal(Date date) {
		if (date == null) {
			return -1;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return getAnimal(cal);
	}

	/**
	 * 计算1900年以后的年份对应的生肖，并返回生肖名称，只能大致计算，因为生肖是按农历计数
	 * 
	 * @param year
	 * @return String 1鼠,2牛,3虎,4免,5龙,6蛇,7马,8羊,9猴,10鸡,11狗,12猪
	 */
	public static String getAnimalName(Calendar date) {
		return ANIMALS[getAnimal(date)];
	}

	/**
	 * 计算1900年以后的年份对应的生肖，并返回生肖名称，只能大致计算，因为生肖是按农历数
	 * 
	 * @param year
	 * @return String 1鼠,2牛,3虎,4免,5龙,6蛇,7马,8羊,9猴,10鸡,11狗,12猪
	 */
	public static String getAnimalName(Date date) {
		return ANIMALS[getAnimal(date)];
	}

	/**
	 * 根据指定日期，计算星座
	 * 
	 * @param date
	 * @return short 1牡羊座 2金牛座 3双子座 4巨蟹座 5狮子座 6处女座 7天秤座 8天蝎座 9射手座 10魔羯座 11水瓶座
	 *         12双鱼座
	 */
	public static short getConstellation(Calendar date) {
		if (date == null) {
			return -1;
		}
		Calendar startCal = Calendar.getInstance();
		Calendar endCal = Calendar.getInstance();
		int year = date.get(Calendar.YEAR);
		int day = date.get(Calendar.DAY_OF_YEAR);

		// 牡羊座(03.21-04.20)
		startCal.set(year, 2, 21);
		endCal.set(year, 3, 20);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 1;
		}
		// 金牛座(04.21-05.20)
		startCal.set(year, 3, 21);
		endCal.set(year, 4, 20);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 2;
		}
		// 双子座(05.21-06.21)
		startCal.set(year, 4, 21);
		endCal.set(year, 5, 21);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 3;
		}
		// 巨蟹座(06.22-07.22)
		startCal.set(year, 5, 22);
		endCal.set(year, 6, 22);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 4;
		}
		// 狮子座(07.23-08.22)
		startCal.set(year, 6, 23);
		endCal.set(year, 7, 22);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 5;
		}
		// 处女座(08.23-09.22)
		startCal.set(year, 7, 23);
		endCal.set(year, 8, 22);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 6;
		}
		// 天秤座(09.23-10.22)
		startCal.set(year, 8, 23);
		endCal.set(year, 9, 22);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 7;
		}
		// 天蝎座(10.23-11.21)
		startCal.set(year, 9, 23);
		endCal.set(year, 10, 21);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 8;
		}
		// 射手座(11.22-12.21)
		startCal.set(year, 10, 22);
		endCal.set(year, 11, 21);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 9;
		}
		// 魔羯座(12.22-01.19)
		startCal.set(year, 11, 22);
		endCal.set(year, 0, 19);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				|| day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 10;
		}
		// 水瓶座(01.20-02.19)
		startCal.set(year, 0, 20);
		endCal.set(year, 1, 19);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 11;
		}
		// 双鱼座(02.20-03.20)
		startCal.set(year, 1, 20);
		endCal.set(year, 2, 20);
		if (day >= startCal.get(Calendar.DAY_OF_YEAR)
				&& day <= endCal.get(Calendar.DAY_OF_YEAR)) {
			return 12;
		}
		return 0;
	}

	/**
	 * 根据指定日期，计算星座
	 * 
	 * @param date
	 * @return short 1牡羊座 2金牛座 3双子座 4巨蟹座 5狮子座 6处女座 7天秤座 8天蝎座 9射手座 10魔羯座 11水瓶座
	 *         12双鱼座
	 */
	public static short getConstellation(Date date) {
		if (date == null) {
			return -1;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return getConstellation(cal);
	}

	/**
	 * 根据指定日期，计算星座名称
	 * 
	 * @param date
	 * @return String
	 */
	public static String getConstellationName(Calendar date) {
		return CONSTELLATIONS[getConstellation(date)];
	}

	/**
	 * 根据指定日期，计算星座名称
	 * 
	 * @param date
	 * @return String
	 */
	public static String getConstellationName(Date date) {
		short constellation = getConstellation(date);
		if (constellation == -1) {
			return "";
		}
		return CONSTELLATIONS[constellation];
	}

	/**
	 * 得到当前时间和指定时间的小时差
	 * 
	 * @param date
	 * @return
	 */
	public static long getSubtractHour(Date date) {
		if (date == null) {
			return 0;
		}
		long currTime = System.currentTimeMillis();
		long hour = currTime - date.getTime();
		hour = hour / (60 * 60 * 1000);
		hour = hour % (60 * 60 * 1000) > 0 ? hour + 1 : hour;
		return hour;
	}

	/**
	 * 得到当前时间和指定时间的分钟差
	 * 
	 * @param date
	 * @return
	 */
	public static long getSubtractMinute(Date date) {
		if (date == null) {
			return 0;
		}
		long currTime = System.currentTimeMillis();
		long minute = currTime - date.getTime();
		minute = minute / (60 * 1000);
		return minute;
	}

	/**
	 * 格式化日期为yyyyMMdd
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDateYyyymmdd(Date date) {
		return DateFormatYYYYMMDD.format(date);
	}
	
	/**
	 * 格式化日期为yyyy-MM-dd
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDateYMD(Date date) {
		return dateFormatYMD.format(date);
	}
	/**
	 * 格式化日期为yyyy/MM/dd
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDateYMD1(Date date) {
		return dateFormatYMD1.format(date);
	}
	

	public static String formatNumber(double value) {
		return decimalValue.format(value);
	}

    /**
     * 取得时间的中文描述
     * 
     * @param date
     * @return String
     */
    public static String getDescFromTimestamp(Timestamp date) {
        Date newDate = new Date(date.getTime());
        return getDescFromUtilDate(newDate);
    }
	/**
	 * 取得时间的中文描述
	 * 
	 * @param date
	 * @return String
	 */
	public static String getDescFromUtilDate(Date date) {
        if(date==null)return "";
		long hour = getSubtractHour(date);
		String timeDesc = null;
		if (hour <= 1) {
			long minute = getSubtractMinute(date);
			if (minute <= 10) {
				timeDesc = "10分钟内";
			} else if (minute > 10 && minute <= 30) {
                timeDesc = "半小时内";
            } else {
				timeDesc = "1小时内";
			}
		} else if (hour <= 24) {
			timeDesc = hour + "小时内";
		} else if (hour <= (24 * 60)) {
			long day = hour / 24;
			day = hour % 24 > 0 ? day + 1 : day; // 如果除24有余数就加1
			timeDesc = day + "天内";
		} else {
			timeDesc = "两个月前";
		}
		return timeDesc;
	}

	/**
	 * 返回一个时间ArrayList
	 * 
	 * @param datePart
	 *            DATE_PART_YEAR、DATE_PART_MONTH、DATE_PART_DAY
	 * @return ArrayList
	 */
	public static ArrayList<Integer> getDateList(int datePart) {
		ArrayList<Integer> dateList = null;
		Calendar cal = null;
		switch (datePart) {
		case DATE_PART_YEAR:
			cal = Calendar.getInstance();
			int defaultYear = cal.get(Calendar.YEAR);
			dateList = new ArrayList<Integer>();
			for (int i = defaultYear - 18; i >= defaultYear - 86; i--) {
				dateList.add(i);
			}
			break;
		case DATE_PART_YEAR2:
			cal = Calendar.getInstance();
			int defaultYear2 = cal.get(Calendar.YEAR);
			dateList = new ArrayList<Integer>();
			for (int i = defaultYear2; i >= defaultYear2 - 86; i--) {
				dateList.add(i);
			}
			break;
		case DATE_PART_MONTH:
			cal = Calendar.getInstance();
			dateList = new ArrayList<Integer>();
			for (int i = 1; i <= 12; i++) {
				dateList.add(i);
			}
			break;
		case DATE_PART_DAY:
			cal = Calendar.getInstance();
			int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			dateList = new ArrayList<Integer>();
			for (int i = 1; i <= days; i++) {
				dateList.add(i);
			}
			break;
		}
		return dateList;
		// actionForm.set("birthdayYear", new Integer(defaultYear - 25));
		// actionForm.set("birthdayMonth", new Integer(6));
		// actionForm.set("birthdayDay", new Integer(15));
	}

	/**
	 * 
	 * 方法说明：把日期变成 2009-01-02这样的格式
	  * 创建日期：2009-2-23,下午02:24:03,hyc
	 * @param date
	 * @return
	 */
	public static String formateDateToStr(Date date){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		return df.format(date);
	}
	
	// 判断时间date1是否在时间date2之前
	// 时间格式 2005-4-21 16:16:34
	public static boolean isDateBefore(String date1, String date2) {
		try {
			DateFormat df = DateFormat.getDateTimeInstance();
			return df.parse(date1).before(df.parse(date2));
		} catch (Exception e) {
			System.out.print("[SYS] " + e.getMessage());
			return false;
		}
	}
	
	/**
	 * 得到当前年
	 * 
	 * @return
	 */
	public static int getYear() {
		return Calendar.YEAR;
	}

	public static String dateFormatHMS1(Timestamp d) {
		return dateFormatHMS1.format(d);
	}

	@SuppressWarnings("deprecation")
	public static int getYear(java.sql.Date d) {

		return d.getYear() + 1900;
	}
	@SuppressWarnings("deprecation")
	public static int getUtilYear(java.util.Date d) {

		return d.getYear() + 1900;
	}

	@SuppressWarnings("deprecation")
	public static int getMonth(java.sql.Date d) {

		return d.getMonth() + 1;
	}
	@SuppressWarnings("deprecation")
	public static int getUtilMonth(java.util.Date d) {

		return d.getMonth() + 1;
	}

	@SuppressWarnings("deprecation")
	public static int getDay(java.sql.Date d) {

		return d.getDate();
	}
	
	@SuppressWarnings("deprecation")
	public static int getUtilDay(java.util.Date d) {
		return d.getDate();
	}
	
	/**
	 * 计算一个日期相隔几天的日期
	 * 之前的用负数，之后的用正数
	 * @param date
	 * @param incr
	 * @return
	 */
	public static Date getNextDay(Date date, int incr) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, incr);
		return new Timestamp(calendar.getTimeInMillis());
	}

	/**
	 * 计算当前时间距离一个指定时期若干天后剩余的天数
	 * 格式为d天h时m分
	 * @param beginDate
	 * @param days
	 * @return
	 */
	public static String getDHM(Date beginDate, int incr) {
		Date endDate = getNextDay(beginDate, incr);
		long msec = endDate.getTime() - System.currentTimeMillis();
		long days = msec / DAY;
		long hours = msec % DAY / HOUR;
		long minutes = msec % DAY % HOUR / MINUTE;
		return days + "天" + hours + "时" + minutes + "分";
	}
	/**
	 * 计算当前时间距离一个指定时期若干天后剩余的天数
	 * 格式为d天h时m分
	 * @param beginDate
	 * @param days
	 * @return
	 */
	public static String getDHM1(Date beginDate, int incr) {
		Date endDate = getNextDay(beginDate, incr);
		long msec = endDate.getTime() - System.currentTimeMillis();
		long days = msec / DAY;
		long hours = msec % DAY / HOUR;
		long minutes = msec % DAY % HOUR / MINUTE;
		return days + "-" + hours + "-" + minutes;
	}
	/**
	 * 根据一个日期字符串和给定格式得到一个日期对象
	 * @param datestr
	 * @param format
	 * @return
	 */
	public static Date getDate(String datestr,String format) {  
        SimpleDateFormat formatter=new SimpleDateFormat(format);  
        try {
			return formatter.parse(datestr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
        return null;
    } 
	
	/**
	 * 计算一个日期和当前时间相隔的天数，忽略时分秒
	 * @param date
	 * @return
	 */
	public static int intervalDays(Date date){
		String now = getYMD(new Timestamp(System.currentTimeMillis()));
		String datestr = getYMD(new Timestamp(date.getTime()));
		String format = "yyyy-MM-dd";
		long msec = getDate(now, format).getTime()-getDate(datestr, format).getTime();
		return (int) (msec/DAY);
	}

	/**
	 * 计算距离明天还有多少分钟
	 * 
	 * @return
	 */
	public static int leftMinutes() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		long msec = calendar.getTimeInMillis() - System.currentTimeMillis();
		return (int) msec / MINUTE;
	}
	
	public static String getAndroidTime(String time) {
		String timeStr = "";
		try{
			Calendar calendar = Calendar.getInstance();

			Calendar today = Calendar.getInstance(); // 今天
			today.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			today.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			today.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
			today.set(Calendar.HOUR_OF_DAY, 0);
			today.set(Calendar.MINUTE, 0);
			today.set(Calendar.SECOND, 0);
			today.clear(Calendar.MILLISECOND);

			Calendar yesterday = Calendar.getInstance(); // 昨天
			yesterday.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			yesterday.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			yesterday.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
			yesterday.set(Calendar.HOUR_OF_DAY, 0);
			yesterday.set(Calendar.MINUTE, 0);
			yesterday.set(Calendar.SECOND, 0);
			yesterday.clear(Calendar.MILLISECOND);

			Calendar beforeYesterday = Calendar.getInstance(); // 前天
			beforeYesterday.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			beforeYesterday.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			beforeYesterday.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 2);
			beforeYesterday.set(Calendar.HOUR_OF_DAY, 0);
			beforeYesterday.set(Calendar.MINUTE, 0);
			beforeYesterday.set(Calendar.SECOND, 0);
			beforeYesterday.clear(Calendar.MILLISECOND);
			
			int day_of_week = 0;
			Calendar monday = Calendar.getInstance();  //周一
			day_of_week = monday.get(Calendar.DAY_OF_WEEK) - 1;
			if (day_of_week == 0) {
				day_of_week = 7;
			}
			monday.add(Calendar.DATE, -day_of_week + 1);
			monday.set(Calendar.HOUR_OF_DAY, 0);
			monday.set(Calendar.MINUTE, 0);
			monday.set(Calendar.SECOND, 0);
			monday.clear(Calendar.MILLISECOND);

			Calendar sunday = Calendar.getInstance();  //周日
			sunday.add(Calendar.DATE, -day_of_week + 7);
			sunday.set(Calendar.HOUR_OF_DAY, 0);
			sunday.set(Calendar.MINUTE, 0);
			sunday.set(Calendar.SECOND, 0);
			sunday.clear(Calendar.MILLISECOND);

			Calendar year = Calendar.getInstance();  //今年的1月1日
			year.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			year.set(Calendar.MONTH, 0);
			year.set(Calendar.DAY_OF_MONTH, 1);
			year.set(Calendar.HOUR_OF_DAY, 0);
			year.set(Calendar.MINUTE, 0);
			year.set(Calendar.SECOND, 0);
			year.clear(Calendar.MILLISECOND);
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = format.parse(time);
			calendar.setTime(date);
			String hourZoneCH = getHourZoneCH(date);
			if(calendar.equals(today) || calendar.after(today)){
				SimpleDateFormat todayFormat = new SimpleDateFormat("HH:mm");
				timeStr = hourZoneCH + " " + todayFormat.format(date);
			}else if(calendar.after(yesterday)){
				timeStr = "昨天" + hourZoneCH;
			}else if(calendar.after(beforeYesterday)){
				timeStr = "前天" + hourZoneCH;
			}else if(calendar.after(monday) && calendar.before(sunday)){
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
				String dayOfWeekCH = "";
				if(dayOfWeek==1){
					dayOfWeekCH = "一";
				}else if(dayOfWeek==2){
					dayOfWeekCH = "二";
				}else if(dayOfWeek==3){
					dayOfWeekCH = "三";
				}else if(dayOfWeek==4){
					dayOfWeekCH = "四";
				}else if(dayOfWeek==5){
					dayOfWeekCH = "五";
				}else if(dayOfWeek==6){
					dayOfWeekCH = "六";
				}else{
					dayOfWeekCH = "日";
				}
				timeStr = "周" + dayOfWeekCH + hourZoneCH;
			}else if(calendar.after(year)){
				SimpleDateFormat formatYear = new SimpleDateFormat("M月d日 HH:mm");
				timeStr = formatYear.format(date);
			}else{
				SimpleDateFormat formatNotYear = new SimpleDateFormat("yyyy年M月d日 HH:mm");
				timeStr = formatNotYear.format(date);
			}

		} catch (Exception e) {
			timeStr = "时间格式不正确";
			e.printStackTrace();
		}
		return timeStr;
	}
	
	public static String getAndroidTimeRuleOne(String time) {
		String timeStr = "";
		try{
			Calendar calendar = Calendar.getInstance();

			Calendar today = Calendar.getInstance(); // 今天
			today.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			today.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			today.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
			today.set(Calendar.HOUR_OF_DAY, 0);
			today.set(Calendar.MINUTE, 0);
			today.set(Calendar.SECOND, 0);
			today.clear(Calendar.MILLISECOND);

			Calendar yesterday = Calendar.getInstance(); // 昨天
			yesterday.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			yesterday.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			yesterday.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
			yesterday.set(Calendar.HOUR_OF_DAY, 0);
			yesterday.set(Calendar.MINUTE, 0);
			yesterday.set(Calendar.SECOND, 0);
			yesterday.clear(Calendar.MILLISECOND);

			Calendar beforeYesterday = Calendar.getInstance(); // 前天
			beforeYesterday.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			beforeYesterday.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			beforeYesterday.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 2);
			beforeYesterday.set(Calendar.HOUR_OF_DAY, 0);
			beforeYesterday.set(Calendar.MINUTE, 0);
			beforeYesterday.set(Calendar.SECOND, 0);
			beforeYesterday.clear(Calendar.MILLISECOND);
			
			int day_of_week = 0;
			Calendar monday = Calendar.getInstance();  //周一
			day_of_week = monday.get(Calendar.DAY_OF_WEEK) - 1;
			if (day_of_week == 0) {
				day_of_week = 7;
			}
			monday.add(Calendar.DATE, -day_of_week + 1);
			monday.set(Calendar.HOUR_OF_DAY, 0);
			monday.set(Calendar.MINUTE, 0);
			monday.set(Calendar.SECOND, 0);
			monday.clear(Calendar.MILLISECOND);

			Calendar sunday = Calendar.getInstance();  //周日
			sunday.add(Calendar.DATE, -day_of_week + 7);
			sunday.set(Calendar.HOUR_OF_DAY, 0);
			sunday.set(Calendar.MINUTE, 0);
			sunday.set(Calendar.SECOND, 0);
			sunday.clear(Calendar.MILLISECOND);

			Calendar year = Calendar.getInstance();  //今年的1月1日
			year.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			year.set(Calendar.MONTH, 0);
			year.set(Calendar.DAY_OF_MONTH, 1);
			year.set(Calendar.HOUR_OF_DAY, 0);
			year.set(Calendar.MINUTE, 0);
			year.set(Calendar.SECOND, 0);
			year.clear(Calendar.MILLISECOND);
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = format.parse(time);
			calendar.setTime(date);
			String hourZoneCH = getHourZoneCH(date);
			String hourStr = "";
			String minuteStr = "";
			int hour = calendar.get(Calendar.HOUR_OF_DAY);
			if(hour>12){
				hour = hour - 12;
			}
			if(hour<10){
				hourStr = "0"+hour;
			}else{
				hourStr = hour+"";
			}
			int minute = calendar.get(Calendar.MINUTE);
			if(minute<10){
				minuteStr = "0"+minute;
			}else{
				minuteStr = minute+"";
			}
			if(calendar.equals(today) || calendar.after(today)){
				SimpleDateFormat todayFormat = new SimpleDateFormat("HH:mm");
				timeStr = hourZoneCH + " " + todayFormat.format(date);
			}else if(calendar.after(yesterday)){
				timeStr = "昨天" + hourZoneCH + " " + hourStr + ":" + minuteStr;
			}else if(calendar.after(beforeYesterday)){
				timeStr = "前天" + hourZoneCH + " " + hourStr + ":" + minuteStr;
			}else if(calendar.after(monday) && calendar.before(sunday)){
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
				String dayOfWeekCH = "";
				if(dayOfWeek==1){
					dayOfWeekCH = "一";
				}else if(dayOfWeek==2){
					dayOfWeekCH = "二";
				}else if(dayOfWeek==3){
					dayOfWeekCH = "三";
				}else if(dayOfWeek==4){
					dayOfWeekCH = "四";
				}else if(dayOfWeek==5){
					dayOfWeekCH = "五";
				}else if(dayOfWeek==6){
					dayOfWeekCH = "六";
				}else{
					dayOfWeekCH = "日";
				}
				timeStr = "周" + dayOfWeekCH + hourZoneCH + " " + hour + ":" + calendar.get(Calendar.MINUTE);
			}else if(calendar.after(year)){
				SimpleDateFormat formatYear = new SimpleDateFormat("M月d日 HH:mm");
				timeStr = formatYear.format(date);
			}else{
				SimpleDateFormat formatNotYear = new SimpleDateFormat("yyyy年M月d日 HH:mm");
				timeStr = formatNotYear.format(date);
			}

		} catch (Exception e) {
			timeStr = "时间格式不正确";
			e.printStackTrace();
		}
		return timeStr;
	}

	public static String getHourZoneCH(Date date) {
		if (date == null) {
			return "时间为空";
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		Calendar oneHour = Calendar.getInstance();
		oneHour.setTime(date);
		oneHour.set(Calendar.HOUR_OF_DAY, 1);
		oneHour.set(Calendar.MINUTE, 0);
		oneHour.set(Calendar.SECOND, 0);

		Calendar sixHour = Calendar.getInstance();
		sixHour.setTime(date);
		sixHour.set(Calendar.HOUR_OF_DAY, 6);
		sixHour.set(Calendar.MINUTE, 0);
		sixHour.set(Calendar.SECOND, 0);

		Calendar elevenHour = Calendar.getInstance();
		elevenHour.setTime(date);
		elevenHour.set(Calendar.HOUR_OF_DAY, 11);
		elevenHour.set(Calendar.MINUTE, 0);
		elevenHour.set(Calendar.SECOND, 0);

		Calendar thirteenHour = Calendar.getInstance();
		thirteenHour.setTime(date);
		thirteenHour.set(Calendar.HOUR_OF_DAY, 13);
		thirteenHour.set(Calendar.MINUTE, 0);
		thirteenHour.set(Calendar.SECOND, 0);

		Calendar eighteenHour = Calendar.getInstance();
		eighteenHour.setTime(date);
		eighteenHour.set(Calendar.HOUR_OF_DAY, 18);
		eighteenHour.set(Calendar.MINUTE, 0);
		eighteenHour.set(Calendar.SECOND, 0);

		Calendar nineteenHour = Calendar.getInstance();
		nineteenHour.setTime(date);
		nineteenHour.set(Calendar.HOUR_OF_DAY, 19);
		nineteenHour.set(Calendar.MINUTE, 0);
		nineteenHour.set(Calendar.SECOND, 0);

		String hour = "傍晚";
		if(calendar.before(oneHour)){
			hour = "深夜";
		}else if((calendar.before(sixHour) && calendar.after(oneHour)) || calendar.equals(oneHour)){
			hour = "凌晨";
		}else if((calendar.before(elevenHour) && calendar.after(sixHour)) || calendar.equals(sixHour)){
			hour = "上午";
		}else if(calendar.before(thirteenHour) && calendar.after(elevenHour) || calendar.equals(elevenHour)){
			hour = "中午";
		}else if(calendar.before(eighteenHour) && calendar.after(thirteenHour) || calendar.equals(thirteenHour)){
			hour = "下午";
		}else if(calendar.before(nineteenHour) && calendar.after(eighteenHour) || calendar.equals(eighteenHour)){
			hour = "傍晚";
		}else{
			hour = "晚上";
		}
		return hour;
	}


}
