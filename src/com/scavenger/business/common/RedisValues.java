package com.scavenger.business.common;

/**
 * Redis Values
 */
public final class RedisValues {
    public static String getAppKey(String appName){
        return String.format("uploadlog:event:%s", appName);
    }
}
