package com.scavenger.business.common;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * @ClassName: PropertiesUtils
 * @Description: 常用配置文件操作
 * @author hpboys
 * @date 2015年7月8日 下午5:12:38
 * @version V1.0
 */
public class PropertiesUtils {

	private static Map<String, String> properties = new HashMap<String, String>();

	public static Properties initConfigProperties(Properties props) {
		for (Object key : props.keySet()) {
			String keyTemp = String.valueOf(key);
			String valueTemp = props.getProperty(keyTemp);
			properties.put(keyTemp, valueTemp);
		}
		return props;
	}

	/**
	 * @Description: 得到经过SecurityUtils加密后的原数据
	 * @param @param props
	 * @return Properties 返回类型
	 */
	public static Properties loadMetaProps(Properties props) {
		String db_encrypt_key = "";
		if(StringUtils.isNotEmpty(props.getProperty("pool.db_encrypt_key"))){
			db_encrypt_key  = props.getProperty("pool.db_encrypt_key");
		}
		
		for (Object key : props.keySet()) {
			String keyTemp = String.valueOf(key);
			if(keyTemp.equals("pool.db_encrypt_key")){
				continue;
			}
			props.setProperty(keyTemp, SecurityUtils.decrypt(props.getProperty(keyTemp), db_encrypt_key));
			properties.put(keyTemp,props.get(keyTemp).toString());
		}
		return props;
	}

	/**
	 * @Description: 设置配置到系统变量中
	 * @param @param props
	 * @param @return 参数说明
	 * @return Properties 返回类型
	 */
	public static Properties loadMetaPropsToSystem(String rootpath,Properties props) {
		for (Object key : props.keySet()) {
			String keyTemp = String.valueOf(key);
			properties.put(keyTemp, rootpath+File.separator+SecurityUtils.decrypt(props.getProperty(keyTemp)));
		}
		return props;
	}

	public static Map<String, String> getProperties() {
		return properties;
	}

	public static String getProperty(String key) {
		return properties.get(key);
	}

	public static String get(String key) {
		return properties.get(key);
	}
}
