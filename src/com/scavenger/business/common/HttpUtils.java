package com.scavenger.business.common;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

/**
 * @description HTTP连接工具类
 * @author denglitao
 *
 */
public class HttpUtils {
	private static final String ENCODING = "UTF-8";
	
	private static int connectTimeout = 15*1000;
	private static int readTimeout = 15*1000;
	
	public static void main(String[] args) throws Exception {
//		String result = postRequest(new URL("http://www.renren.com"), null);
//		System.out.println(result);
	}
	
	public static String getRequest(URL url) throws Exception {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs,
					String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs,
					String authType) {
			}
		} };
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setRequestMethod("GET");
		conn.connect();

		InputStream in = conn.getInputStream();
		return IOUtils.toString(in,"utf-8");
	}
	
	/**
	 * @description 发送HTTP请求
	 * @param url
	 * @param parameters
	 * @return
	 * @throws java.io.IOException
	 */
	public static String postRequest(URL url, Map<String, String> parameters) throws IOException {
		HttpURLConnection conn = null;
		OutputStream out = null;
		InputStream in = null;
		try {
			conn = (HttpURLConnection)url.openConnection();
			if(connectTimeout != -1) {
				conn.setConnectTimeout(connectTimeout);
			}
			if (readTimeout != -1) {
				conn.setReadTimeout(readTimeout);
			}
			conn.setRequestMethod("POST");
			//conn.setRequestProperty( "Content-type", "application/x-www-form-urlencoded" );
			conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			conn.setRequestProperty("Content-Type",	"application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length","286");
			conn.setRequestProperty("Accept-Language","zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
			conn.setRequestProperty("Accept-Encoding","gzip, deflate");
			conn.setRequestProperty("Referer","http://www.17ce.com/site/cdn.html");
			conn.setRequestProperty("Host","www.17ce.com");
			conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0");
			conn.setDoOutput(true);
			conn.connect();
			
			// 写入POST数据
			out = conn.getOutputStream();
			byte[] params = generatorParamString(parameters).getBytes();
			out.write(params, 0, params.length);
			out.flush();
			
			// 读取响应数据
			in = conn.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, ENCODING));
			StringBuilder buffer = new StringBuilder();
			char[] buf = new char[1000];
			int len = 0;
			while (len >= 0) {
				buffer.append(buf, 0, len);
				len = reader.read(buf);
			}
			return buffer.toString();
		} catch(IOException e) {
			throw e;
		} finally {
			close(in);
			close(out);
			if(conn != null){
				conn.disconnect();
			}
		}
	}
	
	
	
	/**
	 * @description 发送HTTP请求
	 * @param url
	 * @param parameters
	 * @return
	 * @throws java.io.IOException
	 */
	public static String postRequestXML(URL url, String xml) throws IOException {
		HttpURLConnection conn = null;
		OutputStream out = null;
		InputStream in = null;
		try {
			conn = (HttpURLConnection)url.openConnection();
			if(connectTimeout != -1) {
				conn.setConnectTimeout(connectTimeout);
			}
			if (readTimeout != -1) {
				conn.setReadTimeout(readTimeout);
			}
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "text/xml");  
			 conn.setRequestProperty("Accept-Charset", "UTF-8");
			//conn.setRequestProperty( "Content-type", "application/x-www-form-urlencoded" );
			conn.setDoOutput(true);
			conn.connect();
			
			// 写入POST数据
			out = conn.getOutputStream();
			byte[] params = xml.getBytes();
			out.write(params, 0, params.length);
			out.flush();
			
			// 读取响应数据
			in = conn.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, ENCODING));
			StringBuilder buffer = new StringBuilder();
			char[] buf = new char[1000];
			int len = 0;
			while (len >= 0) {
				buffer.append(buf, 0, len);
				len = reader.read(buf);
			}
			return buffer.toString();
		} catch(IOException e) {
			throw e;
		} finally {
			close(in);
			close(out);
			if(conn != null){
				conn.disconnect();
			}
		}
	}
	
	/**
	 * @description 生成请求参数字符串
	 * @param parameters
	 * @return
	 */
	public static String generatorParamString(Map<String, String> parameters) {
        StringBuffer params = new StringBuffer();
        if(parameters != null) {
        	for(Iterator<String> iter = parameters.keySet().iterator(); iter.hasNext(); ) {
        		String name = iter.next();
        		String value = parameters.get(name);
        		params.append(name + "=");
                try {
                    params.append(URLEncoder.encode(value, ENCODING));
                } catch (UnsupportedEncodingException e) {
                	throw new RuntimeException(e.getMessage(), e);
                } catch (Exception e) {
                	throw new RuntimeException(String.format("'%s'='%s'", name, value), e);
                }
                if(iter.hasNext())
                	params.append("&");
            }
        }
        return params.toString();
    }
	
	/**
	 * @description 关闭对象
	 * @param f
	 */
	public static void close(Closeable f) {
		if(f != null){
			try{
				f.close();
			}catch(IOException e){
				Logger.getAnonymousLogger().info(e.toString());
			}
		}
	}
	
	public static String inputStream2String(InputStream is) {
		boolean i = true;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			int i1;
			while ((i1 = is.read()) != -1) {
				baos.write(i1);
			}

			String e = baos.toString();
			baos.flush();
			baos.close();
			return e;
		} catch (IOException arg3) {
			arg3.printStackTrace();
			return null;
		}
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream(), Charset.forName("utf-8")));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！"+e);
			e.printStackTrace();
		}
		//使用finally块来关闭输出流、输入流
		finally{
			try{
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}
		return result;
	}

}
