package com.scavenger.business.common;

import java.math.BigDecimal;

/**
 * @author Lorry
 * @since 2020-03-27 23:05
 **/
public class CoolUtil {
    public static double formatNumber(double f) {
        BigDecimal bg = new BigDecimal(f);
        return bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static int parseInt(String value) {
        int i = 0;
        try {
            i = Integer.parseInt(value);
        } catch (Exception ignore) {
        }
        return i;
    }

    public static void random() {
        double rate = 0.8;
        int payout = 19;

        if (payout >= 10) {
            if (payout > 18 && payout < 30) {
                rate = 0.65;
            } else if (payout >= 30 && payout < 50) {
                rate = 0.6;
            } else if (payout >= 50) {
                rate = 0.45;
            }
            int payoutNew = new Double(Math.floor(payout * rate)).intValue();
            System.out.println(String.format("%s * %s = %s", payout, rate, payoutNew));
        }
    }

    public static void main(String[] args) {
        random();
    }
}
