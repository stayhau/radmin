package com.scavenger.business.common;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.OperationTimeoutException;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MemcachedCache
 * @author kevin.lee
 *
 */
public class MemcachedCache implements Cache {
	
	private MemcachedClient memcachedClient;
	
	@Override
	public void set(String key, Object value, int expiry) {
		if (StringUtils.isEmpty(key) || value == null) {
			return;
		}
		memcachedClient.set(key, expiry * 60, value);
	}
	
	@Override
	public Object get(String key) {
		if (StringUtils.isEmpty(key)) {
			return null;
		}
		Object o = null;
		try{
			o = memcachedClient.get(key);
		}catch(OperationTimeoutException e){}
		
		return o;
	}
	
	@Override
	public Map<String, Object> getMulti(List<String> keys) {
		if (keys == null || keys.size() == 00) {
			return new HashMap<String, Object>(0);
		}
		String[] strKeys = new String[keys.size()];
		strKeys = keys.toArray(strKeys);
		return memcachedClient.getBulk(strKeys);
	}

	@Override
	public Object[] getMulti(String[] keys) {
		if (keys == null || keys.length == 00) {
			return new Object[0];
		}
		Map<String, Object> map = memcachedClient.getBulk(keys);
		return map.values().toArray();
	}
	
	@Override
	public void delete(String key) {
		if (StringUtils.isEmpty(key)) {
			return;
		}
		memcachedClient.delete(key);
	}

	@Override
	public boolean exists(String key) {
		if (StringUtils.isEmpty(key)) {
			return false;
		}
		return memcachedClient.get(key) != null;
	}
	
	public void setMemcachedClient(MemcachedClient memcachedClient) {
		this.memcachedClient = memcachedClient;
	}
	
}
