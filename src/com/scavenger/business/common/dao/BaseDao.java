package com.scavenger.business.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.scavenger.business.common.DBUtil;
import com.scavenger.business.common.PageHolder;


public class BaseDao  {
	
	private static final int DEFAULT_ROW_INDEX = 0;
	private static final int DEFAULT_ROW_SIZE = 100;
	
	
	public <T> int insert( NamedParameterJdbcOperations hnaJdbc, T t) {
		String table =  getTableName(t.getClass());
		String sql = DBUtil.generateInsertSQL(table, t);
		return hnaJdbc.update(sql, new BeanPropertySqlParameterSource(t));
	}
	public <T> int insertFetchId(NamedParameterJdbcOperations hnaJdbc,T t) {
		String table = getTableName(t.getClass());
		String sql = DBUtil.generateInsertSQL(table, t);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		hnaJdbc.update(sql, new BeanPropertySqlParameterSource(t), keyHolder);
		return keyHolder.getKey().intValue();
	}

	public <T> int update(NamedParameterJdbcOperations hnaJdbc,T t,Map<String,Object> conditions) {
		Map<String, Object> fields = DBUtil.comparePojo(null, t);
		fields.remove("updateTime");
		if (fields == null || fields.size() == 0) {
			return 0;
		}
		
		String table  = getTableName(t.getClass());
		String sql = DBUtil.generateUpdateSQL(table, fields, conditions);
		return hnaJdbc.update(sql, new BeanPropertySqlParameterSource(t));
	}

	
	public <T> int update(NamedParameterJdbcOperations hnaJdbc,T t, Map<String, Object> fileds,
			Map<String, Object> conditions) {
		String conditionsExt = " limit 100 ";
		String table = getTableName(t.getClass());
		String sql = DBUtil.generateUpdateSQL(table, fileds, conditions, conditionsExt);
		
		//��֧�ָ��������ֶ�
		fileds.putAll(conditions);
		DBUtil.format(fileds);
		
		return hnaJdbc.update(sql, fileds);
	}


	public <T> int delete(NamedParameterJdbcOperations hnaJdbc,T t,Map<String,Object> conditions) {
		Map<String, Object> fields = DBUtil.comparePojo(null, t);
		fields.remove("updateTime");
		if (fields == null || fields.size() == 0) {
			return 0;
		}
		
		String table  = getTableName(t.getClass());
		String sql = DBUtil.generateDeleteSQL(table, conditions);

		return hnaJdbc.getJdbcOperations().update(sql);
	}

	public <T> int replace(NamedParameterJdbcOperations hnaJdbc,T t) {
		String table =  getTableName(t.getClass());
		String sql = DBUtil.generateReplaceSQL(table, t);
		return hnaJdbc.update(sql, new BeanPropertySqlParameterSource(t));
	}

	
	
	public <T> List<T> listByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz,
			String contionExt) {
		contionExt+=" limit :rowIndex, :rowSize";
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, contionExt);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex",DEFAULT_ROW_INDEX );
		params.put("rowSize", DEFAULT_ROW_SIZE);
		return hnaJdbc.query(DBUtil.restrictSelectSQL(sql, clazz), params, rm);
	}
	
	public <T> List<T> listByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz, int rowIndex, int rowSize) {
		String conditionsExt = " limit :rowIndex, :rowSize";
		
		String table = getTableName(clazz);
		if(table.equals("issuedstrategy")){
			table="upload_log.issuedstrategy";
		}
		String sql = DBUtil.generateSelectSQL(table, fields, conditionsExt);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex", rowIndex);
		params.put("rowSize", rowSize);
		
		return hnaJdbc.query(DBUtil.restrictSelectSQL(sql, clazz), params, rm);
	}
	
	
	public <T> List<T> listByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz,String contionExt, int rowIndex, int rowSize) {
		String conditionsExt = "  "+contionExt+" limit :rowIndex, :rowSize";
		
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, conditionsExt);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex", rowIndex);
		params.put("rowSize", rowSize);
		
		return hnaJdbc.query(DBUtil.restrictSelectSQL(sql, clazz), params, rm);
	}

	public <T> T getByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz){
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		return hnaJdbc.queryForObject(sql, fields,rm);
	}
	
	public <T> List<T> listByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz) {
		return listByFields(hnaJdbc,fields, clazz, DEFAULT_ROW_INDEX, DEFAULT_ROW_SIZE);
	}

	public <T> List<Map<String, Object>> listMapByFields(NamedParameterJdbcOperations hnaJdbc,
			Map<String, Object> fields, Class<T> clazz, int rowIndex,
			int rowSize,String orderBy) {
		String conditionsExt = "limit :rowIndex, :rowSize";
		if(StringUtils.isNotBlank(orderBy)){
			conditionsExt = orderBy +" "+ conditionsExt ;
		}
		
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, conditionsExt);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex", rowIndex);
		params.put("rowSize", rowSize);
		return hnaJdbc.queryForList(sql, params);
	}
	
	public <T> List<Map<String, Object>> listMapByFields(NamedParameterJdbcOperations hnaJdbc,
			Map<String, Object> fields, Class<T> clazz, String orderBy) {
		
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, orderBy);
		
		return hnaJdbc.queryForList(sql, fields);
	}
	
	public <T> int countByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz) {
		String table = getTableName(clazz);
		String sql = DBUtil.generateCountSQL(table, fields);
		
		return hnaJdbc.queryForInt(sql, fields);
	}
	
	public <T> int countByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz ,String conditionsExt) {
		String table = getTableName(clazz);
		String sql = DBUtil.generateCountSQL(table, fields,conditionsExt);
		
		int count = 0;
		try {
			count = hnaJdbc.queryForInt(sql, fields);
		} catch (Exception e) {
			e.printStackTrace();
			count = 0;
		}
		return count;
	}
	
	
	
	
	public <T> void page(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields,Class<T> clazz,PageHolder<Map<String, Object>> pageHolder){
		int count = countByFields(hnaJdbc,fields, clazz);
		if(count>0){
			List<Map<String, Object>> list = listMapByFields(hnaJdbc,fields, clazz, pageHolder.getRowOffset(), pageHolder.getPageSize(),pageHolder.getOrderBy());
			pageHolder.setRowCount(count);
			pageHolder.setList(list);
			
		}
	}
	
	
	

	public <T> int sumByFields(NamedParameterJdbcOperations hnaJdbc,Map<String, Object> fields, Class<T> clazz) {
		String table = getTableName(clazz);
		String sql = DBUtil.generateSumSQL(table, fields);
		
		return hnaJdbc.queryForInt(sql, fields);
	}
	
	 protected String getTableName(Object obj)
	  {
	    if (obj == null) return null;
	    String table = "";
	    if (StringUtils.isEmpty(table)) {
	      if (obj.getClass().equals(String.class))
	        table = obj.toString();
	      else if (Class.class.equals(obj.getClass()))
	        table = ((Class)obj).getSimpleName();
	      else {
	    	  table=obj.getClass().getSimpleName();
	    	  table=(new StringBuilder()).append(table.toLowerCase()).toString();
	    	  return table;
	      }
	    }
	    table= (new StringBuilder()).append(table.toLowerCase()).toString();
	    return table;
	  }
	
}
