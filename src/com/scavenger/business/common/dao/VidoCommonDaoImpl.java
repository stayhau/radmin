package com.scavenger.business.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.scavenger.business.common.DBUtil;
import com.scavenger.business.common.PageHolder;
@Repository
public class VidoCommonDaoImpl extends BaseDao implements VidoCommonDao {

	@Autowired
	private NamedParameterJdbcOperations vidoJdbc;
	private static final int DEFAULT_ROW_INDEX = 0;
	private static final int DEFAULT_ROW_SIZE = 1000;
	
    @Override
	public <T> int insert( T t) {
		String table =  getTableName(t.getClass());
		String sql = DBUtil.generateInsertSQL(table, t);
		return vidoJdbc.update(sql, new BeanPropertySqlParameterSource(t));
	}
    @Override
	public <T> int insertFetchId(T t) {
		String table = getTableName(t.getClass());
		String sql = DBUtil.generateInsertSQL(table, t);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		vidoJdbc.update(sql, new BeanPropertySqlParameterSource(t), keyHolder);
		return keyHolder.getKey().intValue();
	}
    @Override
	public <T> int update(T t,Map<String,Object> conditions) {
		Map<String, Object> fields = DBUtil.comparePojo(null, t);
		fields.remove("updateTime");
		if (fields == null || fields.size() == 0) {
			return 0;
		}
		
		String table  = getTableName(t.getClass());
		String sql = DBUtil.generateUpdateSQL(table, fields, conditions);
		return vidoJdbc.update(sql, new BeanPropertySqlParameterSource(t));
	}

    @Override
	public <T> int update(T t, Map<String, Object> fileds,
			Map<String, Object> conditions) {
		String conditionsExt = " limit 100 ";
		String table = getTableName(t.getClass());
		String sql = DBUtil.generateUpdateSQL(table, fileds, conditions, conditionsExt);
		
		//��֧�ָ��������ֶ�
		fileds.putAll(conditions);
		DBUtil.format(fileds);
		
		return vidoJdbc.update(sql, fileds);
	}

    @Override
	public <T> int delete(T t,Map<String,Object> conditions) {
		Map<String, Object> fields = DBUtil.comparePojo(null, t);
		fields.remove("updateTime");
		if (fields == null || fields.size() == 0) {
			return 0;
		}
		
		String table  = getTableName(t.getClass());
		String sql = DBUtil.generateDeleteSQL(table, conditions);

		return vidoJdbc.getJdbcOperations().update(sql);
	}
    @Override
	public <T> int replace(T t) {
		String table =  getTableName(t.getClass());
		String sql = DBUtil.generateReplaceSQL(table, t);
		return vidoJdbc.update(sql, new BeanPropertySqlParameterSource(t));
	}

	
	
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz,
			String contionExt) {
		contionExt+=" limit :rowIndex, :rowSize";
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, contionExt);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex",DEFAULT_ROW_INDEX );
		params.put("rowSize", DEFAULT_ROW_SIZE);
		return vidoJdbc.query(DBUtil.restrictSelectSQL(sql, clazz), params, rm);
	}
	
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz, int rowIndex, int rowSize) {
		String conditionsExt = " limit :rowIndex, :rowSize";
		
		String table = getTableName(clazz);
		if(table.equals("issuedstrategy")){
			table="upload_log.issuedstrategy";
		}
		String sql = DBUtil.generateSelectSQL(table, fields, conditionsExt);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex", rowIndex);
		params.put("rowSize", rowSize);
		
		return vidoJdbc.query(DBUtil.restrictSelectSQL(sql, clazz), params, rm);
	}
	
	 @Override
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz,String contionExt, int rowIndex, int rowSize) {
		String conditionsExt = "  "+contionExt+" limit :rowIndex, :rowSize";
		
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, conditionsExt);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex", rowIndex);
		params.put("rowSize", rowSize);
		
		return vidoJdbc.query(DBUtil.restrictSelectSQL(sql, clazz), params, rm);
	}
	 @Override
	public <T> T getByFields(Map<String, Object> fields, Class<T> clazz){
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields);
		RowMapper<T> rm = ParameterizedBeanPropertyRowMapper.newInstance(clazz);
		return vidoJdbc.queryForObject(sql, fields,rm);
	}
	 @Override
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz) {
		return listByFields(vidoJdbc,fields, clazz, DEFAULT_ROW_INDEX, DEFAULT_ROW_SIZE);
	}
	 @Override
	public <T> List<Map<String, Object>> listMapByFields(
			Map<String, Object> fields, Class<T> clazz, int rowIndex,
			int rowSize,String orderBy) {
		String conditionsExt = "limit :rowIndex, :rowSize";
		if(StringUtils.isNotBlank(orderBy)){
			conditionsExt = orderBy +" "+ conditionsExt ;
		}
		
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, conditionsExt);
		
		Map<String, Object> params = new HashMap<String, Object>(fields);
		params.put("rowIndex", rowIndex);
		params.put("rowSize", rowSize);
		return vidoJdbc.queryForList(sql, params);
	}
	 @Override
	public <T> List<Map<String, Object>> listMapByFields(
			Map<String, Object> fields, Class<T> clazz, String orderBy) {
		
		String table = getTableName(clazz);
		String sql = DBUtil.generateSelectSQL(table, fields, orderBy);
		
		return vidoJdbc.queryForList(sql, fields);
	}
	 @Override
	public <T> int countByFields(Map<String, Object> fields, Class<T> clazz) {
		String table = getTableName(clazz);
		String sql = DBUtil.generateCountSQL(table, fields);
		
		return vidoJdbc.queryForInt(sql, fields);
	}
	 @Override
	public <T> int countByFields(Map<String, Object> fields, Class<T> clazz ,String conditionsExt) {
		String table = getTableName(clazz);
		String sql = DBUtil.generateCountSQL(table, fields,conditionsExt);
		
		int count = 0;
		try {
			count = vidoJdbc.queryForInt(sql, fields);
		} catch (Exception e) {
			e.printStackTrace();
			count = 0;
		}
		return count;
	}
	
	
	
	 @Override
	public <T> void page(Map<String, Object> fields,Class<T> clazz,PageHolder<Map<String, Object>> pageHolder){
		int count = countByFields(vidoJdbc,fields, clazz);
		if(count>0){
			List<Map<String, Object>> list = listMapByFields(vidoJdbc,fields, clazz, pageHolder.getRowOffset(), pageHolder.getPageSize(),pageHolder.getOrderBy());
			pageHolder.setRowCount(count);
			pageHolder.setList(list);
			
		}
	}
	
	
	
	 @Override
	public <T> int sumByFields(Map<String, Object> fields, Class<T> clazz) {
		String table = getTableName(clazz);
		String sql = DBUtil.generateSumSQL(table, fields);
		
		return vidoJdbc.queryForInt(sql, fields);
	}
	
	
	
	

	
}
