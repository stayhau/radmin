package com.scavenger.business.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.scavenger.business.domain.ReadView;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadUtil {
    private static final String ENCODING = "UTF-8";
    private static final String TIAN_URL = "http://api.tianapi.com/txapi/wxread/index";
    private static final String TIAN_APIKEY = "8c7fd688a3d72363383b09afb5a45bf6";
    static Logger logger = Logger.getLogger(com.scavenger.business.common.ReadUtil.class);

    public static ReadView getData(String url) {
        if (StringUtils.isEmpty(url)) {
            return null;
        }
        String httpArg = "key=" + TIAN_APIKEY + "&url=" + url;
        String rs = request(TIAN_URL, httpArg);
        logger.info("tianxing url: " + url + ", rs = " + rs);

        Newslist newslist = JSONObject.parseObject(rs, Newslist.class);
        if (newslist.getCode() == 200) {
            if (!CollectionUtils.isEmpty(newslist.getNewslist())) {
                Newslist.NewslistDTO dto = newslist.getNewslist().get(0);
                int viewCount = dto.getReadnum();
                int starCount = dto.getLikenum();
                ReadView rv = new ReadView();
                rv.setStarCount(starCount);
                rv.setViewCount(viewCount);
                return rv;
            }
        }

        return null;
    }

    public static ReadView getDataNew(String url) {
        if (StringUtils.isEmpty(url)) {
            return null;
        }

        String rs = post(url);

        Map<String, Object> map = (Map<String, Object>) JSON.parse(rs);
        String errorCode = map.get("msg").toString();
        logger.info("wxApi url: " + url + ", rs = " + rs);

        if (StringUtils.equals("succ", errorCode)) {
            Map<String, Object> data = (Map<String, Object>) map.get("data");
            int viewCount = Integer.parseInt(data.get("readnums").toString());
            int starCount = Integer.parseInt(data.get("zannums").toString());
            int state = Integer.parseInt(data.get("state").toString());
            if (state == 0) {
                ReadView rv = new ReadView();
                rv.setStarCount(starCount);
                rv.setViewCount(viewCount);
                return rv;
            }
        } else {
            return getData(url);
        }
        return null;
    }

    public static void main(String[] args) {
        String url = "https://mp.weixin.qq.com/s/GMSvVjwOkHJPYgK6fXkJtQ";

        ReadView rd = getDataNew(url);
        if (rd != null) {
            System.out.println(rd.toString());
        }
    }

    public static int getMy() {
        try {
            String rs = get(Config.getParam("wxapi_my_url"));
            Map<String, Object> parse = (Map<String, Object>) JSON.parse(rs);
            return Integer.parseInt(parse.get("剩余次数").toString());
        } catch (Exception e) {
        }
        return 0;
    }

    public static String getShotUrl(String url) {
        String rs = get(url);
        Pattern p = Pattern.compile("msg_link = \"(.*?)#rd");
        Matcher m = p.matcher(rs);
        while (m.find()) {
            return (m.group(1) + "#wechat_redirect").replaceAll("\\\\x26amp;", "&");
        }
        return "";
    }

    public static String get(String url) {
        try {
            HttpClient httpclient = new HttpClient();
            GetMethod method = new GetMethod(url);
            httpclient.executeMethod(method);
            return new String(method.getResponseBody(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get url, exception: " + e.getMessage());
        }
        return "";
    }

    public static String post(String url) {
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(Config.getParam("wxapi_url"));
        try {
            post.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
            NameValuePair[] param = {new NameValuePair("url", URLEncoder.encode(url))};
            post.setRequestBody(param);
            client.executeMethod(post);
            return post.getResponseBodyAsString();
        } catch (HttpException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param parameters
     * @return
     * @description 生成请求参数字符串
     */
    public static String generatorParamString(Map<String, String> parameters) {
        StringBuffer params = new StringBuffer();
        if (parameters != null) {
            for (Iterator<String> iter = parameters.keySet().iterator(); iter.hasNext(); ) {
                String name = iter.next();
                String value = parameters.get(name);
                params.append(name + "=");
                try {
                    params.append(URLEncoder.encode(value, ENCODING));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e.getMessage(), e);
                } catch (Exception e) {
                    throw new RuntimeException(String.format("'%s'='%s'", name, value), e);
                }
                if (iter.hasNext())
                    params.append("&");
            }
        }
        return params.toString();
    }

    /**
     * @param url
     * @param parameters
     * @return
     * @throws IOException
     * @description 发送HTTP请求
     */
    public static String postRequest(URL url, Map<String, String> parameters) throws IOException {
        HttpURLConnection conn = null;
        OutputStream out = null;
        InputStream in = null;
        int connectTimeout = -1;
        int readTimeout = -1;
        try {
            conn = (HttpURLConnection) url.openConnection();
            if (connectTimeout != -1) {
                conn.setConnectTimeout(connectTimeout);
            }
            if (readTimeout != -1) {
                conn.setReadTimeout(readTimeout);
            }
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            conn.connect();

            // 写入POST数据
            out = conn.getOutputStream();
            byte[] params = generatorParamString(parameters).getBytes();
            out.write(params, 0, params.length);
            out.flush();

            // 读取响应数据
            in = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, ENCODING));
            StringBuilder buffer = new StringBuilder();
            char[] buf = new char[1000];
            int len = 0;
            while (len >= 0) {
                buffer.append(buf, 0, len);
                len = reader.read(buf);
            }
            return buffer.toString();
        } catch (IOException e) {
            throw e;
        } finally {
            close(in);
            close(out);
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public static void close(Closeable f) {
        if (f != null) {
            try {
                f.close();
            } catch (IOException e) {
                java.util.logging.Logger.getAnonymousLogger().info(e.toString());
            }
        }
    }

    public static String request(String urlx) {
        String result = null;
        try {
            String urls = URLEncoder.encode(urlx, "UTF-8");
            String appid = "8c7fd688a3d72363383b09afb5a45bf6";
            String httpUrl = "http://api.tianapi.com/txapi/wxread/index";
            String httpArg = "key=" + appid + "&url=" + urls;
            BufferedReader reader = null;
            StringBuffer sbf = new StringBuffer();
            httpUrl = httpUrl + "?" + httpArg;
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            InputStream is = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sbf.append(strRead);
                sbf.append("\r\n");
            }
            reader.close();
            result = sbf.toString();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return result;
    }

    /**
     * @param httpUrl 请求接口
     * @param httpArg 参数
     * @return 返回结果
     */
    public static String request(String httpUrl, String httpArg) {
        BufferedReader reader = null;
        String result = null;
        StringBuffer sbf = new StringBuffer();
        httpUrl = httpUrl + "?" + httpArg;

        try {
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream is = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sbf.append(strRead);
                sbf.append("\r\n");
            }
            reader.close();
            result = sbf.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

class Newslist {
    private Integer code;
    private String msg;
    private List<NewslistDTO> newslist;

    public static class NewslistDTO {
        private String title;
        private Integer readnum;
        private Integer likenum;
        private Integer givenum;
        private Integer commentnum;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getReadnum() {
            return readnum;
        }

        public void setReadnum(Integer readnum) {
            this.readnum = readnum;
        }

        public Integer getLikenum() {
            return likenum;
        }

        public void setLikenum(Integer likenum) {
            this.likenum = likenum;
        }

        public Integer getGivenum() {
            return givenum;
        }

        public void setGivenum(Integer givenum) {
            this.givenum = givenum;
        }

        public Integer getCommentnum() {
            return commentnum;
        }

        public void setCommentnum(Integer commentnum) {
            this.commentnum = commentnum;
        }
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<NewslistDTO> getNewslist() {
        return newslist;
    }

    public void setNewslist(List<NewslistDTO> newslist) {
        this.newslist = newslist;
    }
}