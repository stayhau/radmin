package com.scavenger.business.common;

public class OfferUtil {

	
	public static String OffereStatusStr(Integer status){
//		status 说明
//		 -3 暂停
//		 -4  下架
//		 1 等待开始跑
//		 -2 审核中 
//		 3 正在跑
//		 5 完成 
//		 6 已跑完等待核查真实阅读
		String result ="";
		 switch (status+"") {
		case "1":
			result = "排队中";
			break;
		case "-2":
			result = "审核中";
			break;
		case "3":
			result = "运行中";
			break;
		case "4":
			break;
		case "5":
			result = "已完成";
			break;
		case "6":
			result = "待检查";
			break;
		case "10":
			result = "转化异常";
			break;
		case "-1":
			result = "撤单";
			break;
		case "-3":
			result = "暂停";
			break;
		case "-4":
			result = "下架";
			break;
		default:
			break;
		}
		return result;
	}
}
