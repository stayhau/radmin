package com.scavenger.business.common;

import com.google.common.io.Resources;

import java.io.IOException;
import java.util.Properties;

/**
 * 集中定义一些常量
 */
public final class Config {
    public final static Properties props = new Properties();

    static {
        try {
            props.load(Resources.getResource("config.properties").openStream());
        } catch (IOException e) {
            System.exit(1);
        }
    }

    /**
     * 获得配置文件中某个参数的值
     *
     * @param paramName
     * @return 2015年7月27日
     * @author tuziilm
     */
    public static String getParam(String paramName) {
        return props.getProperty(paramName);
    }
}
