package com.scavenger.business.common;

import java.util.List;
import java.util.Map;

public interface Cache {

	/**
	 * @param key		要缓存的对象的key
	 * @param value		要缓存的对象
	 * @param expireMinutes	超时时间
	 */
	public void set(String key, Object value, int expireMinutes);

	/**
	 * 获取缓存
	 * 
	 * @param key	要获取的缓存对象的key
	 * @return		缓存的对象
	 */
	public Object get(String key);
	
	/**
	 * 批量获取缓存
	 * 
	 * @param keys	缓存的key的list
	 * @return		获取到的缓存map
	 */
	public Map<String, Object> getMulti(List<String> keys);

	/**
	 * 批量获取缓存
	 * 
	 * @param keys	缓存的key数组
	 * @return		获取到的缓存数组
	 */
	public Object[] getMulti(String[] keys);

	/**
	 * 判断缓存对象是否存在
	 * 
	 * @param key	缓存对象的key
	 * @return		是否存在
	 */
	public boolean exists(String key);

	/**
	 * 删除指定的缓存对象
	 * 
	 * @param key	缓存对象的key
	 */
	public void delete(String key);

}
