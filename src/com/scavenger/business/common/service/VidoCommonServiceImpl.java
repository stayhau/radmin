package com.scavenger.business.common.service;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scavenger.business.common.PageHolder;
import com.scavenger.business.common.dao.VidoCommonDao;

@Service
public class VidoCommonServiceImpl implements VidoCommonService {

	@Autowired
	private VidoCommonDao vidoCommonDao;
	
	@Override
	public <T> int insert(T t) {
		return vidoCommonDao.insert(t);
	}

	@Override
	public <T> int insertFetchId(T t) {
		return vidoCommonDao.insertFetchId(t);
	}

	@Override
	public <T> int update(T t, Map<String, Object> conditions) {
		return vidoCommonDao.update(t, conditions);
	}

	@Override
	public <T> int update(T t, Map<String, Object> fileds, Map<String, Object> conditions) {
		return vidoCommonDao.update(t, fileds, conditions);
	}

	@Override
	public <T> int delete(T t, Map<String, Object> conditions) {
		return vidoCommonDao.delete(t, conditions);
	}

	@Override
	public <T> int replace(T t) {
		return vidoCommonDao.replace(t);
	}

	@Override
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz, String contionExt) {
		return vidoCommonDao.listByFields(fields, clazz, contionExt);
	}

	@Override
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz, int rowIndex, int rowSize) {
		return vidoCommonDao.listByFields(fields, clazz, rowIndex, rowSize);
	}

	@Override
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz, String contionExt, int rowIndex, int rowSize) {
		return vidoCommonDao.listByFields(fields, clazz, contionExt, rowIndex, rowSize);
	}

	@Override
	public <T> T getByFields(Map<String, Object> fields, Class<T> clazz) {
		return vidoCommonDao.getByFields(fields, clazz);
	}

	@Override
	public <T> List<T> listByFields(Map<String, Object> fields, Class<T> clazz) {
		return vidoCommonDao.listByFields(fields, clazz);
	}

	@Override
	public <T> List<Map<String, Object>> listMapByFields(Map<String, Object> fields, Class<T> clazz, int rowIndex, int rowSize, String orderBy) {
		return null;
	}

	@Override
	public <T> List<Map<String, Object>> listMapByFields(Map<String, Object> fields, Class<T> clazz, String orderBy) {
		return null;
	}

	@Override
	public <T> int countByFields(Map<String, Object> fields, Class<T> clazz) {
		return vidoCommonDao.countByFields(fields, clazz);
	}

	@Override
	public <T> int countByFields(Map<String, Object> fields, Class<T> clazz, String conditionsExt) {
		return 0;
	}

	@Override
	public <T> void page(Map<String, Object> fields, Class<T> clazz, PageHolder<Map<String, Object>> pageHolder) {
		vidoCommonDao.page(fields, clazz, pageHolder);
	}

	@Override
	public <T> int sumByFields(Map<String, Object> fields, Class<T> clazz) {
		return 0;
	}

	
}
