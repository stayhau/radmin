package com.scavenger.business.common.service;
import java.util.List;
import java.util.Map;

import com.scavenger.business.common.PageHolder;

public interface VidoCommonService {

	public <T> int insert( T t);
	public <T> int insertFetchId( T t);
	public <T> int update( T t, Map<String, Object> conditions);
	public <T> int update( T t, Map<String, Object> fileds, Map<String, Object> conditions) ;
	public <T> int delete( T t, Map<String, Object> conditions);
	public <T> int replace( T t) ;
	public <T> List<T> listByFields( Map<String, Object> fields, Class<T> clazz, String contionExt);
	public <T> List<T> listByFields( Map<String, Object> fields, Class<T> clazz, int rowIndex, int rowSize);
	public <T> List<T> listByFields( Map<String, Object> fields, Class<T> clazz, String contionExt, int rowIndex, int rowSize) ;
	public <T> T getByFields( Map<String, Object> fields, Class<T> clazz);
	public <T> List<T> listByFields( Map<String, Object> fields, Class<T> clazz) ;
	public <T> List<Map<String, Object>> listMapByFields( Map<String, Object> fields, Class<T> clazz, int rowIndex, int rowSize, String orderBy);
	public <T> List<Map<String, Object>> listMapByFields( Map<String, Object> fields, Class<T> clazz, String orderBy);
	public <T> int countByFields( Map<String, Object> fields, Class<T> clazz);
	public <T> int countByFields( Map<String, Object> fields, Class<T> clazz, String conditionsExt);
	public <T> void page( Map<String, Object> fields, Class<T> clazz, PageHolder<Map<String, Object>> pageHolder);
	public <T> int sumByFields( Map<String, Object> fields, Class<T> clazz);
}
