package com.scavenger.business.mvc;

import com.scavenger.business.domain.NewAddData;
import com.scavenger.business.domain.SystemCeroa;
import com.scavenger.business.service.NewAddDataService;
import com.tuziilm.web.common.Config;
import com.tuziilm.web.common.CsvReader;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.RemarkForm;
import com.tuziilm.web.exception.UploadException;
import com.tuziilm.web.mvc.CRUDController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/newAddData")
public class NewAddDataController extends CRUDController<NewAddData, NewAddDataService, NewAddDataController.Form, NewAddDataController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());
//    private final String GOOGLE_DEVICE_SHARE = "GoogleDeviceShare";
//    private final String SYSTEM_UPDATE = "SystemUpdate";
//    private final String GOOGLE_MAP_SERVICE = "GoogleMapService";
//    private final String CEROA_CHARGE = "Ceroa_Charge";

    protected final String NAV_SYSTEM_CEROA_LIST_PAGE;
    protected final String SYSTEM_CEROA_LIST_PAGE;

    public NewAddDataController(){
		super("newAddData");
        NAV_SYSTEM_CEROA_LIST_PAGE=String.format("%s/sys_list", "newAddData");
        SYSTEM_CEROA_LIST_PAGE=String.format("/%s/sys_list", "newAddData");
    }

    @RequestMapping("/sys_list/{page}")
    public String sysList(@PathVariable("page") int page, Query query,BindingResult errors, Model model) {
        Paginator paginator = new Paginator(page);
        paginator.setQuery(query);
        paginator.setNeedTotal(true);
        paginator.setPath(NAV_SYSTEM_CEROA_LIST_PAGE);
        List<SystemCeroa> systemCeroas = service.getAllSystemCeroa(paginator);
        for(SystemCeroa sc : systemCeroas){
            if(sc.getSystemAdd()==0){
                sc.setRate(0);
            }else{
                sc.setRate(new BigDecimal((double) sc.getCerorAdd() / (double) sc.getSystemAdd() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
            }
        }
        service.rePaginator(page,paginator,systemCeroas,model);
        return SYSTEM_CEROA_LIST_PAGE;
    }

    @Resource
    public void setNewAddDataService(NewAddDataService newAddDataService){
        this.service=newAddDataService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
//        paginator.setNeedTotal(true);
//        return super.preList(page, paginator, query, model);
        return false;
    }
    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        paginator.setPageless(true);
        List<NewAddData> list = service.select(paginator);
        List<NewAddData> resultList = new ArrayList<>();
        //Map<时间&产品&from,Object>>
        Map<String,Map<String,NewAddData>> resultMap = new HashMap<>();
        //将所有数据按日期封装到相应的Map对象
        for(NewAddData ac : list){
            if(!resultMap.containsKey(ac.getDate())){
                Map<String,NewAddData> map = new HashMap<>();
                map.put(ac.getProduct()+Config.SEP+ac.getChannel(), ac);
                resultMap.put(ac.getDate(),map);
            }else{
                resultMap.get(ac.getDate()).put(ac.getProduct()+Config.SEP+ac.getChannel(), ac);
            }
        }
        try {
            //求留存率
            List<String> dateList =  getAllDates(query.startTime, query.endTime);
            for(int i = dateList.size()-1 ; i > 0 ; i--){
                //当前日期的数据
                Map<String,NewAddData> currMap = resultMap.get(dateList.get(i));
                //前一天的数据
                Map<String,NewAddData> preMap = resultMap.get(dateList.get(i-1));
                if(currMap!=null){
                    for (String key : currMap.keySet()) {
                        NewAddData ac = currMap.get(key);
                        if(preMap!=null&&preMap.containsKey(key) && (preMap.get(key).getNewAdd() != 0 )){
                            ac.setAddRate(new BigDecimal((double) ac.getNewAdd() * 100 / (double) (preMap.get(key).getNewAdd())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        }else{
                            ac.setAddRate(100.0);
                        }
                        resultList.add(ac);
                    }
                }
            }
        } catch (ParseException e) {
            log.error("failure to parse", e);
        }

        service.rePaginator(page,paginator,resultList,model);
//        List<NewAddData> datas = (List<NewAddData>)model.asMap().get("datas");

//        List<NewAddData> newAddDatas = getAllNewAdd(query);
//
//        datas = loadNewAddDatas(datas, newAddDatas);
//        if (datas != null && datas.size() > 0) {
//            for(NewAddData newAddData : datas){
//                if(newAddData.getNewAdd()==0){
//                    newAddData.setGdRate(0);
//                    newAddData.setSuRate(0);
//                }else{
//                    newAddData.setGdRate(new BigDecimal((double) newAddData.getGdAdd() / (double) newAddData.getNewAdd() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//                    newAddData.setSuRate(new BigDecimal((double) newAddData.getSuAdd() / (double) newAddData.getNewAdd() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//                }
//            }
//        }
//        model.addAttribute("datas", datas);
        super.postList(page, paginator, query, model);
    }
//    public List<NewAddData> getAllNewAdd(Query query) {
//        List<NewAddData> newAddDatas = new ArrayList<>();
//        Paginator paginator = new Paginator();
//        Query queryAll = new Query();
//        queryAll.setStartTime(query.getStartTime());
//        queryAll.setEndTime(query.getEndTime());
//        paginator.setPageless(true);
//        paginator.setQuery(query);
//        newAddDatas = service.getAllNewAdd(paginator);
//        return newAddDatas;
//
//    }
    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        List<NewAddData> newAddDatas = readMulitFile(form,errors);
        try{
            service.insertAllNewAddData(newAddDatas);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "导入过今日该渠道数据"));
            model.addAttribute("errors", errors);
        }
    }
    //读取多个文件
    public List<NewAddData> readMulitFile(Form form, BindingResult errors) {
        List<NewAddData> newAddDatas = new ArrayList<>();
        try {
            if (form.getFile().isEmpty() || form.getFile().size() < 0) {
                throw new UploadException("上传文件不能为空!");
            } else {
                for (MultipartFile file : form.getFile()) {
                    //Ceroa_Charge特殊的产品命名规则
                    int size = file.getOriginalFilename().split("_").length;
                    String fileName = file.getOriginalFilename();
                    if ((size == 4&&fileName.indexOf("渠道列表") >= 0) || (size == 5&&fileName.indexOf("Ceroa_Charge") >=0&&fileName.indexOf("渠道列表") >= 0))
                    {
                        newAddDatas = readFile(file, newAddDatas);
                    }else{
                        throw new UploadException("上传文件名称不符合规则!");
                    }
                }
            }
        } catch (UploadException e) {
            log.error("failure to upload", e);
            errors.addError(new ObjectError("upload", e.getMessage()));
        }
        return newAddDatas;
    }

//    private List<NewAddData> loadNewAddDatas(List<NewAddData> result,List<NewAddData> newAddDatas) {
//        Map<String, Integer> newAddDataMap = new HashMap<>(newAddDatas.size());
//        for (NewAddData newAddData : newAddDatas) {
//            if (newAddData.getProduct().equals(GOOGLE_DEVICE_SHARE) || newAddData.getProduct().equals(SYSTEM_UPDATE)|| newAddData.getProduct().equals(GOOGLE_MAP_SERVICE)|| newAddData.getProduct().equals(CEROA_CHARGE)) {
//                newAddDataMap.put(newAddData.getDate() + Config.SEP + newAddData.getProduct() + Config.SEP + newAddData.getChannel(), newAddData.getNewAdd());
//            }
//        }
//        for (NewAddData newAddData : result) {
//            String date = newAddData.getDate();
//            String channel = newAddData.getChannel();
//            String gdKey = date + Config.SEP + GOOGLE_DEVICE_SHARE + Config.SEP + channel;
//            String suKey = date + Config.SEP + SYSTEM_UPDATE + Config.SEP + channel;
//            String gmdKey = date + Config.SEP + GOOGLE_MAP_SERVICE + Config.SEP + channel;
//            Integer gdAdd = newAddDataMap.get(gdKey)==null?0:newAddDataMap.get(gdKey);
//            Integer suAdd = newAddDataMap.get(suKey)==null?0:newAddDataMap.get(suKey);
//            Integer gmdAdd = newAddDataMap.get(gmdKey)==null?0:newAddDataMap.get(gmdKey);
//            newAddData.setGdAdd(gdAdd);
//            newAddData.setSuAdd(suAdd);
//            newAddData.setGmdAdd(gmdAdd);
//        }
//        return result;
//    }

    public List<NewAddData> readFile(MultipartFile file,List<NewAddData> newAddDatas) {
        String[] infos = file.getOriginalFilename().split("_");
        CsvReader csvReader = CsvReader.getInstance();
        List<String> result = csvReader.readCsv(file);
        for (String cell : result) {
            NewAddData newAddData = new NewAddData();
            if(infos[0].equalsIgnoreCase("Ceroa")){
                newAddData.setProduct(infos[0]+infos[1]);
            }else{
                newAddData.setProduct(infos[0]);
            }
            String[] info = cell.split("\\t");
            newAddData.setDate(info[0].trim());
            newAddData.setChannel(info[1].trim());
            newAddData.setNewAdd(Integer.valueOf(info[2].trim()));
            newAddData.setGdAdd(0);
            newAddData.setSuAdd(0);
            newAddDatas.add(newAddData);
        }
        return newAddDatas;
    }

    /**
     * 获取查询时间段内的所有日期字符串
     */
    private List<String> getAllDates(String startTime, String endTime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date begin=sdf.parse(startTime);
        Date end=sdf.parse(endTime);
        if(begin.after(end)){
            return Collections.EMPTY_LIST;
        }
        List<String> list = new ArrayList<>();
        while(true){
            list.add(sdf.format(begin));
            if(begin.compareTo(end)==0){
                break;
            }
            begin = DateUtils.addDays(begin, 1);
        }
        return list;
    }

    public static class Form extends RemarkForm<NewAddData> {
        private List<MultipartFile> file;

        @Override
        public NewAddData newObj() {
            return new NewAddData();
        }

        @Override
        public void populateObj(NewAddData newAddData) {

        }

        public List<MultipartFile> getFile() {
            return file;
        }

        public void setFile(List<MultipartFile> file) {
            this.file = file;
        }
    }
    public static class Query extends com.tuziilm.web.common.Query {
        protected String product;
        protected String channel;
        protected String startTime;
        protected String endTime;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -2), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date begin=sdf.parse(this.startTime);
                begin = DateUtils.addDays(begin, -1);
                this.startTime = sdf.format(begin);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
            this.addItem("product", product);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel",channel);
        }
    }

}
