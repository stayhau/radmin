package com.scavenger.business.mvc;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.common.MemcachedCache;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.model.policy.JarUpdate;
import com.scavenger.business.model.policy.Policy;
import com.scavenger.business.service.ChannelService;
import com.tuziilm.web.mvc.annotation.Ids;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/jarupdate")
public class JarUpdateController {
	private final Logger log= LoggerFactory.getLogger(getClass());

	
	@Resource
	private ChannelService channelService;
	@Autowired
	private CommonService commonService;
    @Resource
    private MemcachedCache defaultCache;
	
    @RequestMapping(value="/changeStatus",method= RequestMethod.POST,produces="application/javascript;charset=UTF-8")
    public @ResponseBody
    String changeStatus(@RequestParam("id") int id, @RequestParam("status") boolean status){
        int state = status?1:0;
        Map<String, Object> conditions = new HashMap<String, Object>();
        conditions.put("jarId", id);
        List<JarUpdate> list = commonService.listByFields(conditions, JarUpdate.class);
        if(list!=null && !list.isEmpty()){
        	JarUpdate policy = list.get(0);
        	policy.setStatus(state);
        	int result = commonService.update(policy, conditions);
        	 if(result>0){
        		 String[] cs = policy.getChannelId().split(",");
					for (String string : cs) {
						try {
							defaultCache.delete("getJar:"+string);
						} catch (Exception e) {
						}
					}
					
                 return "({\"success\":true,\"msg\":\"修改成功！\"})";
             }else {
                 return "({\"success\":false,\"msg\":\"修改失败！\"})";
             }
        }
        return "({\"success\":false,\"msg\":\"修改失败！\"})";
    }
	@RequestMapping("/list")
	public String list(Model model){
		Map<String, Object> fields =  new HashMap<String, Object>();
		List<JarUpdate> list = commonService.listByFields(fields, JarUpdate.class);
		model.addAttribute("datas", list);
		return "/jarupdate/list";
	}
	
	@RequestMapping(value="/delete/{ids}",method=RequestMethod.POST)
	public String delete(@Ids("ids") int[] ids, HttpServletRequest request,Model model) {
		for (int id : ids) {
			Map<String, Object> conditions = new HashMap<String, Object>();
			conditions.put("jarId",id);
			commonService.delete(new JarUpdate(), conditions);
		}
		return list(model);
	}
	
	@RequestMapping("/create")
	public String create(Model model){
        model.addAttribute("channels", channelService.getAllChannelCache());
        return "/jarupdate/create";
	}
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String save(@Valid Form form,BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
		model.addAttribute("form", form);
		Map<String, Object> conditions = new HashMap<String, Object>();
		String queryString=request.getParameter("_queryString");
		if (!errors.hasErrors()) {
			JarUpdate policy = new JarUpdate();
			form.populateObj(policy);
			if(form.getId()!=null&&form.getId()!=0){
				conditions.put("jarId", form.getId());
				//Policy old = commonService.getByFields(conditions, Policy.class);
				commonService.update(policy, conditions);
				if(!StringUtils.isEmpty(form.getFroms())){
					String[] cs = form.getFroms().split(",");
					for (String string : cs) {
						try {
							defaultCache.delete("getJar:"+string);
						} catch (Exception e) {
						}
					}
					
				}
				
			}else{
				if(!StringUtils.isEmpty(form.getFroms())){
					String[] cs = form.getFroms().split(",");
					for (String string : cs) {
						try {
							defaultCache.delete("getJar:"+string);
						} catch (Exception e) {
						}
						
					}
					
				}
				commonService.insert(policy);
			}
	        
		}
		if (errors.hasErrors()) {
			if( form.getId()!=null){
				conditions.put("jarId", form.getId());
				
				JarUpdate old = commonService.getByFields(conditions, JarUpdate.class);
				model.addAttribute("form", old);
			}
			model.addAttribute("errors", errors);
			return create(model);
		}
		
		return list(model);
	}
	
	@RequestMapping("/modify/{id}")
	public String modify(@PathVariable("id") int id,Model model){
		Map<String, Object> conditions = new HashMap<String, Object>();
		conditions.put("jarId", id);
		JarUpdate offerslave = commonService.listByFields(conditions, JarUpdate.class).get(0);
		model.addAttribute("form", offerslave);
		return create(model);
	}
	
	public static class Form  {
        @NotEmpty(message = "渠道不能为空")
        private Set<String> fromsObject;
        @NotNull(message = "ival不能为空")
        private Integer ival;
        @NotNull(message = "名称不能为空")
        private String jarName;
        @NotNull(message = "版本不能为空")
        private String jarVersion;
        @NotNull(message = "地址不能为空")
        private String jarUrl;
        @NotNull(message = "Md5不能为空")
        private String jarMd5;//Jar包下载周期
        @NotNull(message = "dexName不能为空")
        private String dexName;
        @NotNull(message = "className不能为空")
        private String className;
        @NotNull(message = "tempName不能为空")
        private String tempName;
        private int status;
        private String remark;
        private Integer id;
        public JarUpdate newObj() {
            return new JarUpdate();
        }

        public void populateObj(JarUpdate policy) {
        	String formLimit = ","+getFroms()+",";
        	policy.setChannelId(formLimit);
        	policy.setStatus(1);
        	policy.setCreateTime(new Date());
        	policy.setUpdateTime(new Date());
        	policy.setJarMd5(jarMd5);
        	policy.setJarName(jarName);
        	policy.setJarUrl(jarUrl);
        	policy.setJarVersion(jarVersion);
        	policy.setDexName(dexName);
        	policy.setClassName(className);
        	policy.setTempName(tempName);
        	policy.setIval(ival);
        	if(id!=null){
        		policy.setJarId(id);
        	}
        	policy.setRemark(remark);
        }

       
        public String getTempName() {
			return tempName;
		}

		public void setTempName(String tempName) {
			this.tempName = tempName;
		}

		public Set<String> getFromsObject() {
            return fromsObject;
        }

        public void setFromsObject(Set<String> fromsObject) {
            this.fromsObject = fromsObject;
        }

        public void setFroms(String froms){
            if(Strings.isNullOrEmpty(froms)){
                return;
            }
            this.fromsObject= Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(froms));
        }

        public String getFroms(){
            if(this.fromsObject!=null&&!this.fromsObject.isEmpty()){
                return Joiner.on(",").skipNulls().join(fromsObject);
            }else{
                return "";
            }
        }

        
        public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}

		public Integer getIval() {
			return ival;
		}

		public void setIval(Integer ival) {
			this.ival = ival;
		}

		public String getJarName() {
			return jarName;
		}

		public void setJarName(String jarName) {
			this.jarName = jarName;
		}

		public String getJarVersion() {
			return jarVersion;
		}

		public void setJarVersion(String jarVersion) {
			this.jarVersion = jarVersion;
		}

		public String getJarUrl() {
			return jarUrl;
		}

		public void setJarUrl(String jarUrl) {
			this.jarUrl = jarUrl;
		}

		public String getJarMd5() {
			return jarMd5;
		}

		public void setJarMd5(String jarMd5) {
			this.jarMd5 = jarMd5;
		}

		public String getDexName() {
			return dexName;
		}

		public void setDexName(String dexName) {
			this.dexName = dexName;
		}

		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		
	



        
    }

}
