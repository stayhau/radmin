package com.scavenger.business.mvc;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.domain.CountryType;
import com.scavenger.business.service.CountryTypeService;
import com.tuziilm.web.common.*;
import com.tuziilm.web.domain.RemarkStatusId;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Controller
@RequestMapping("/countryType")
public class CountryTypeController extends CRUDController<CountryType, CountryTypeService, CountryTypeController.Form, Query.NameQuery> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public CountryTypeController(){
		super("countryType");
	}

    @Resource
	public void setCountryTypeService(CountryTypeService countryTypeService){
		this.service=countryTypeService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        model.addAttribute("countryMap", Country.shortcut2CountryMap);
        return super.preList(page, paginator, query, model);
    }

    @Override
    protected void postCreate(Model model) {
        model.addAttribute("countries", Country.countries);
    }

    @Override
    protected void postModify(int id, CountryType obj, Model model) {
        postCreate(model);
    }

    @Override
    protected void onSaveError(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        postCreate(model);
    }

    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        CountryType countryType=form.toObj();
        try{
            service.saveOrUpdate(countryType);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "类型已经存在！"));
            model.addAttribute("errors", errors);
        }
	}

	public static class Form extends RemarkStatusForm<CountryType> {
        @NotBlank(message = "区域名称不能为空")
        private String countryTypeName;
        @NotEmpty(message = "国家不能为空")
        private Set<String> countriesObject;

        @Override
        public CountryType newObj() {
            return new CountryType();
        }

        @Override
        public void populateObj(CountryType CountryType) {
            CountryType.setCountryTypeName(countryTypeName);
            CountryType.setCountriesObject(countriesObject);
        }

        public void setCountries(String countries){
            if(Strings.isNullOrEmpty(countries)){
                return;
            }
            this.countriesObject= Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(countries));
        }

        public String getCountries(){
            if(this.countriesObject!=null&&!this.countriesObject.isEmpty()){
                return this.countriesObject.toString();
            }else{
                return "";
            }
        }

        public Set<String> getCountriesObject() {
            return countriesObject;
        }

        public void setCountriesObject(Set<String> countriesObject) {
            this.countriesObject = countriesObject;
        }

        public String getCountryTypeName() {
            return countryTypeName;
        }

        public void setCountryTypeName(String countryTypeName) {
            this.countryTypeName = countryTypeName;
        }
    }

}
