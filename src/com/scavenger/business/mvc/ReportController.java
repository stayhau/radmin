package com.scavenger.business.mvc;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.scavenger.business.common.DateUtil;
import com.scavenger.business.common.LoginContext;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.model.Channelstatistics;
import com.scavenger.business.model.Offer;
import com.scavenger.business.model.ReadReport;
import com.scavenger.business.model.ReadReportChannelData;
import com.scavenger.business.mvc.AdownerStatisticsController.Query;
import com.tuziilm.web.common.DateUtils;

@Controller
@RequestMapping("/report")
public class ReportController {
	
	@Autowired
	private CommonService commonService;
	
	 @RequestMapping("/export")
	    private void exportSalesDataXls(Query query,HttpServletResponse response){
		 try {
			    Map<String, Object> fields =  new HashMap<String, Object>();
			    response.setContentType("text/csv");
		        response.setCharacterEncoding("GBK");
		        response.setHeader("Content-Disposition","attachment;filename=\"read_statistics.csv\"");
		        response.getWriter().write("广告主,url,阅读数,目标阅读数,完成百分比,单价,金额\n");
				String ex=" ";
				List<Map<String, Object>> resultList =  new ArrayList<Map<String, Object>>();
				List<ReadReport> list = commonService.listByFields(fields, ReadReport.class,ex);
				for (ReadReport readReport : list) {
					Map<String, Object> map =  new HashMap<String, Object>();
					 fields.clear();
					 fields.put("offerId", readReport.getOfferId());
					 if(query!=null){
							if(!StringUtils.isEmpty(query.getAdChannel())){
								fields.put("adChannel", query.getAdChannel());
							}
							if(!StringUtils.isEmpty(query.getStartTime())){
								ex=ex+" and createTime>='"+query.getStartTime().replaceAll("/", "-")+"'";
							}
							if(!StringUtils.isEmpty(query.getEndTime())){
								ex=ex+" and createTime<='"+query.getEndTime().replaceAll("/", "-")+"'";
							}
							if(query.getStatus()!=0){
								fields.put("status", query.getStatus());
							}
							
						}
					 try {
						 DecimalFormat df=new DecimalFormat(".##");
						 Offer o = commonService.getByFields(fields,Offer.class);
						 double bfb = Double.parseDouble(readReport.getReadNum()+"")/Double.parseDouble(o.getReadNum()+"")*100;
						 double tm = Double.parseDouble(readReport.getReadNum()+"")/1000*Double.parseDouble(o.getPayout()+"");
						 if(o!=null){
							 response.getWriter()
		                        .append(String.valueOf(o.getAdChannel())).append(",")
		                        .append(String.valueOf(o.getUrl())).append(",")
		                        .append(String.valueOf(readReport.getReadNum())).append(",")
		                        .append(String.valueOf(o.getReadNum())).append(",")
		                        .append(String.valueOf(df.format(bfb)+"%")).append(",")
		                        .append(String.valueOf(o.getPayout())).append(",")
		                        .append(String.valueOf((df.format(tm)))).append("\n");
						 }
					} catch (Exception e) {
					}
				}
		        
		} catch (Exception e) {
			// TODO: handle exception
		}
		   
	 }
	
	@RequestMapping("/list")
	public String list(Model model,Query query){
		boolean isadmin = LoginContext.isAdmin();
		String userName = LoginContext.getUsername();
		Map<String, Object> fields =  new HashMap<String, Object>();
		List<String> adList = new ArrayList<>();
		List<Offer> offerList = commonService.listByFields(fields, Offer.class," and adChannel is not null group by adChannel");
		for (Offer offer : offerList) {
			if(!StringUtils.isEmpty(offer.getAdChannel())){
				adList.add(offer.getAdChannel());
			}
			
		}
		String ex=" ";
		List<Map<String, Object>> resultList =  new ArrayList<Map<String, Object>>();
		List<ReadReport> list = commonService.listByFields(fields, ReadReport.class,ex);
		for (ReadReport readReport : list) {
			Map<String, Object> map =  new HashMap<String, Object>();
			 fields.clear();
			 fields.put("offerId", readReport.getOfferId());
			 if(query!=null){
					if(!StringUtils.isEmpty(query.getAdChannel())){
						fields.put("adChannel", query.getAdChannel());
					}
					if(!StringUtils.isEmpty(query.getStartTime())){
						ex=ex+" and createTime>='"+query.getStartTime().replaceAll("/", "-")+"'";
					}
					if(!StringUtils.isEmpty(query.getEndTime())){
						ex=ex+" and createTime<='"+query.getEndTime().replaceAll("/", "-")+"'";
					}
					if(query.getStatus()!=0){
						fields.put("status", query.getStatus());
					}
					
				}
			 if(!isadmin){
				 fields.put("auther", userName);
			 }
			 try {
				 Offer o = commonService.getByFields(fields,Offer.class);
				 if(o!=null){
					 map.put("offerId",readReport.getOfferId());
					 map.put("readNum", readReport.getReadNum());
					 map.put("readNumx", o.getReadNum());
					 map.put("updateTime", readReport.getCreateTime());
					 map.put("url", o.getUrl());
					 map.put("payout", o.getPayout());
					 map.put("remark", o.getRemark());
					 map.put("adChannel", o.getAdChannel());
					 map.put("readedNum", o.getReadedNum());
					 resultList.add(map);
				 }
			} catch (Exception e) {
			}
			

		}
		int type = LoginContext.getSystemUserType();
		if(type==3){
			return Channellist(model);
		}
		model.addAttribute("datas", resultList);
		model.addAttribute("adcs", adList);
		model.addAttribute("param", query);
		return "/report/list";
	}
	
	public static class Query  {
		private int status;
		private String startTime;
		private String endTime;
		private String adChannel;
		
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getStartTime() {
			return startTime;
		}
		public void setStartTime(String startTime) {
			this.startTime = startTime;
		}
		public String getEndTime() {
			return endTime;
		}
		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}
		public String getAdChannel() {
			return adChannel;
		}
		public void setAdChannel(String adChannel) {
			this.adChannel = adChannel;
		}
		
		
	}
	
	@RequestMapping("/user-active")
	public String Userlist(Model model){
		Map<String, Object> fields =  new HashMap<String, Object>();
		List<Channelstatistics> list = commonService.listByFields(fields, Channelstatistics.class);
		model.addAttribute("datas", list);
		return "/report/channelstatistics";
		
	}
	
	
	
	@RequestMapping("/channel-list")
	public String Channellist(Model model){
		Map<String, Object> fields =  new HashMap<String, Object>();
		
		String d = DateUtil.formatDateYMD(new Date());
		fields.put("status", 1);
		if(!LoginContext.isAdmin()){
			fields.put("channelId", LoginContext.getUsername());
		}
		List<Map<String, Object>> resultList =  new ArrayList<Map<String, Object>>();
		List< Channelstatistics> list = commonService.listByFields(fields,  Channelstatistics.class);
		//获取今天完成的Offer 
	
		
		
		model.addAttribute("datas", list);
		return "/report/channel-list";
	}
}
