package com.scavenger.business.mvc;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.domain.App;
import com.scavenger.business.domain.LogChannel;
import com.scavenger.business.service.AppService;
import com.scavenger.business.service.Channel2SalesmanService;
import com.scavenger.business.service.EventLogStatisticsService;
import com.scavenger.business.service.LogChannelService;
import com.tuziilm.web.common.IdForm;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/logchannel")
public class LogChannelController extends CRUDController<LogChannel, LogChannelService, LogChannelController.Form, Query.NameQuery> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    @Resource
    private AppService appService;
    @Resource
    private EventLogStatisticsService eventLogStatisticsService;
    public LogChannelController(){
		super("logchannel");
	}

    @Resource
	public void setLogChannelService(LogChannelService logChannelService){
		this.service=logChannelService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        model.addAttribute("appMaps", appService.getAllAppMapCache());
        return super.preList(page, paginator, query, model);
    }

    @Override
    protected void postCreate(Model model) {
        List<App> apps = appService.getAllAppCache();
        model.addAttribute("apps", apps);
        Map<String, List<String>> channel2App = new HashMap<>();
        for (App app :apps){
            List<String> channels = eventLogStatisticsService.getChannelsByApp(app.getPkgName());
            for(int i =0;i<channels.size();i++){
                if(Strings.isNullOrEmpty(channels.get(i))) {
                    channels.set(i, "unknown");
                }
            }
            channel2App.put(app.getPkgName(), channels);
        }
        model.addAttribute("appChannelsMap", channel2App);
    }

    @Override
    protected void postModify(int id, LogChannel obj, Model model) {
        postCreate(model);
    }

    @Override
    protected void onSaveError(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        postCreate(model);
    }

    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        LogChannel channel=form.toObj();
        try{
            service.saveOrUpdate(channel);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "类型已经存在！"));
            model.addAttribute("errors", errors);
        }
	}

	public static class Form extends IdForm<LogChannel> {
        private Set<String> fromsObject;
        @Pattern(regexp = "[^0]*",message = "请选择应用")
        private String pkgName;

        @Override
        public LogChannel newObj() {
            return new LogChannel();
        }

        @Override
        public void populateObj(LogChannel LogChannel) {
            LogChannel.setFromsObject(fromsObject);
            LogChannel.setPkgName(pkgName);
        }

        public String getPkgName() {
            return pkgName;
        }

        public void setPkgName(String pkgName) {
            this.pkgName = pkgName;
        }

        public void setFroms(String froms){
            if(Strings.isNullOrEmpty(froms)){
                return;
            }
            this.fromsObject= Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(froms));
        }

        public String getFroms(){
            if(this.fromsObject!=null&&!this.fromsObject.isEmpty()){
                return Joiner.on(",").skipNulls().join(fromsObject);
            }else{
                return "";
            }
        }
        public Set<String> getFromsObject() {
            return fromsObject;
        }

        public void setFromsObject(Set<String> fromsObject) {
            this.fromsObject = fromsObject;
        }


    }

}
