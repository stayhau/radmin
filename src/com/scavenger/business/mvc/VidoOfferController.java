package com.scavenger.business.mvc;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.scavenger.business.common.DateUtil;
import com.scavenger.business.common.LoginContext;
import com.scavenger.business.common.PageHolder;
import com.scavenger.business.common.service.VidoCommonService;
import com.scavenger.business.model.AdChannel;
import com.scavenger.business.model.OfferHistory;
import com.scavenger.business.model.vido.Offer;
import com.scavenger.business.service.ChannelService;
import com.tuziilm.web.common.Query.Item;
import com.tuziilm.web.mvc.annotation.Ids;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/vidooffer")
public class VidoOfferController {
	private final Logger log= LoggerFactory.getLogger(getClass());

	
	@Resource
	private ChannelService channelService;
	@Autowired
	private VidoCommonService vidoCommonService;
	
	
	public static class Query  {
		private LinkedHashMap<String, Item> items=new LinkedHashMap<String, Item>();
		private int status;
		private String startTime;
		private String endTime;
		private String adChannel;
		
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getStartTime() {
			return startTime;
		}
		public void setStartTime(String startTime) {
			this.startTime = startTime;
		}
		public String getEndTime() {
			return endTime;
		}
		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}
		public String getAdChannel() {
			return adChannel;
		}
		public void setAdChannel(String adChannel) {
			this.adChannel = adChannel;
		}
		public LinkedHashMap<String, Item> getItems() {
			return items;
		}
		public void setItems(LinkedHashMap<String, Item> items) {
			this.items = items;
		}
		
		
	}

	@RequestMapping("/list")
	public String list(Model model,Query sear){
		Map<String, Object> fields =  new HashMap<String, Object>();
		List<AdChannel> adChannelList = vidoCommonService.listByFields(fields, AdChannel.class);
		//默认时间
		Calendar ca = Calendar.getInstance();
		String endTime = DateUtil.formatDateYMD(ca.getTime());
		if(!StringUtils.isEmpty(sear.getEndTime())){
			endTime = sear.getEndTime();
		}
		ca.add(Calendar.DAY_OF_MONTH, -1);
		String startTime = DateUtil.formatDateYMD(ca.getTime());
		if(!StringUtils.isEmpty(sear.getStartTime())){
			startTime = sear.getStartTime();
		}
		if(sear.getStatus()!=0){
			fields.put("status", sear.getStatus());
		}
		if(!StringUtils.isEmpty(sear.getAdChannel())){
			fields.put("adChannel", sear.getAdChannel());
		}
		
		String ext = "    and createTime<='"+endTime+" 23:59' and createTime >='"+startTime+"'";
		List<Offer> list = vidoCommonService.listByFields(fields, Offer.class,ext);
		PageHolder<Map<String, Object>> pageHolder = new PageHolder<>(20);
		vidoCommonService.page(fields, Offer.class, pageHolder);
		model.addAttribute("datas", list);
		sear.setStartTime(startTime);
		sear.setEndTime(endTime);
		model.addAttribute("sear", sear);
		model.addAttribute("adChannel", adChannelList);
		model.addAttribute("pageHolder", pageHolder);
		return "/vidooffer/list";
	}
	
	@RequestMapping(value="/delete/{ids}",method=RequestMethod.POST)
	public String delete(@Ids("ids") int[] ids, HttpServletRequest request,Model model) {
		for (int id : ids) {
			Map<String, Object> conditions = new HashMap<String, Object>();
			conditions.put("offerId",id);
			Offer offer = vidoCommonService.listByFields(conditions, Offer.class).get(0);
			int row=vidoCommonService.delete(new Offer(), conditions);
			
		}
		return list(model,new Query());
	}
	
	@RequestMapping(value="/verify/{ids}",method=RequestMethod.POST)
	public String verify(@Ids("ids") int[] ids, HttpServletRequest request,Model model) {
		for (int id : ids) {
			Map<String, Object> conditions = new HashMap<String, Object>();
			conditions.put("offerId",id);
			Offer offer = vidoCommonService.listByFields(conditions, Offer.class).get(0);
			offer.setStatus(1);
			int row=vidoCommonService.update(offer, conditions);
			
		}
		return list(model,new Query());
	}
	
	@RequestMapping("/create")
	public String create(Model model, HttpServletRequest request){
        model.addAttribute("channels", channelService.getAllChannelCache());
        String _queryString=request.getParameter("_queryString");
        model.addAttribute("_queryString", _queryString);
        return "/vidooffer/create";
	}
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String save(@Valid Form form,BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
		model.addAttribute("form", form);
		Map<String, Object> conditions = new HashMap<String, Object>();
		String queryString=request.getParameter("_queryString");
		if (!errors.hasErrors()&&StringUtils.isEmpty(queryString)) {
			Map<String, Object> fields =  new HashMap<String, Object>();
			fields.clear();
            fields.put("url", form.getUrl());
            fields.put("status", 1);
             int c = vidoCommonService.countByFields(fields, Offer.class);
            if(c>=1){
            	String  error=form.getUrl()+"  已录入数据库，请重新输入url!";
            	model.addAttribute("errors", errors);
            	return create(model,request);
            }
			
			
			Offer policy = new Offer();
			OfferHistory offerHistory = new OfferHistory();
			form.populateObj(policy);
			if(form.getOfferId()!=null&&form.getOfferId()!=0){
				conditions.put("offerId", form.getOfferId());
				//Policy old = vidoCommonService.getByFields(conditions, Policy.class);
				int row=vidoCommonService.update(policy, conditions);
				if(row>=1){
					offerHistory.setActionType("update");
					offerHistory.setAuther(LoginContext.getUsername());
					offerHistory.setActionTime(new Date());
					vidoCommonService.insert(offerHistory);
				}
			}else{
				int row=vidoCommonService.insert(policy);
			}
	        
		}else if(!errors.hasErrors()){
			//更新操作
			Offer offer = new Offer();
			form.populateObj(offer);
			conditions.clear();
			conditions.put("offerId",form.getOfferId() );
			vidoCommonService.update(offer, conditions);
		}
		if (errors.hasErrors()) {
			if( form.getOfferId()!=null){
				conditions.put("offerId", form.getOfferId());
				Offer old = vidoCommonService.getByFields(conditions, Offer.class);
				model.addAttribute("form", old);
			}
			model.addAttribute("errors", errors);
			return create(model,request);
		}
		return list(model,new Query());
	}
	
	@RequestMapping("/modify/{id}")
	public String modify(@PathVariable("id") int id,Model model, HttpServletRequest request){
		Map<String, Object> conditions = new HashMap<String, Object>();
		conditions.put("offerId", id);
		Offer offerslave = vidoCommonService.listByFields(conditions, Offer.class).get(0);
		model.addAttribute("form", offerslave);
		return create(model,request);
	}
	

	
	public static class Form  {
      
		@NotEmpty(message = "阅读地址不能为空")
     	private	String	url;
       // @NotEmpty(message = "单价不能为空")
     	private	Double	payout;
        //@NotEmpty(message = "阅读数不能为空")
     	private	Integer	readNum;
     	private	Integer	readedNum;
     	private	Integer	great;
     	private	Integer	reply;
    	private	String	auther;
    	private	Date	creaetTime;
    	private	Date	updateTime;
    	@NotEmpty(message = "开始时间不能为空")
    	private	String	startTime;
    	@NotEmpty(message = "结束时间不能为空")
    	private	String	endTime;
    	private Integer weight;
        private String adChannel;
        private Integer status;
        private String remark;
        private Integer offerId;
        public Offer newObj() {
            return new Offer();
        }

        public void populateObj(Offer policy) {
        	
        	policy.setCreateTime(new Date());
        	policy.setUpdateTime(new Date());
        	policy.setEndTime(endTime);
        	policy.setStartTime(startTime);
        	policy.setPayout(payout);
        	policy.setAuther(auther);
        	policy.setReadNum(readNum);
        	policy.setReadedNum(readedNum);
        	policy.setUrl(url);
        	policy.setWeight(weight);
        	policy.setAdChannel(adChannel);
        	if(status!=null){
        		policy.setStatus(status);
        	}else{
        		policy.setStatus(-2);
        	}
        	
        	if(offerId!=null){
        		policy.setOfferId(offerId);
        	}
        }

       



        
        public String getAdChannel() {
			return adChannel;
		}

		public void setAdChannel(String adChannel) {
			this.adChannel = adChannel;
		}

		public Integer getReadedNum() {
			return readedNum;
		}

		public void setReadedNum(Integer readedNum) {
			this.readedNum = readedNum;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}


		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	
		public Double getPayout() {
			return payout;
		}

		public void setPayout(Double payout) {
			this.payout = payout;
		}

		public Integer getReadNum() {
			return readNum;
		}

		public void setReadNum(Integer readNum) {
			this.readNum = readNum;
		}

		public Integer getGreat() {
			return great;
		}

		public void setGreat(Integer great) {
			this.great = great;
		}

		public Integer getReply() {
			return reply;
		}

		public void setReply(Integer reply) {
			this.reply = reply;
		}

		public String getAuther() {
			return auther;
		}

		public void setAuther(String auther) {
			this.auther = auther;
		}

		public Date getCreaetTime() {
			return creaetTime;
		}

		public void setCreaetTime(Date creaetTime) {
			this.creaetTime = creaetTime;
		}

		public Date getUpdateTime() {
			return updateTime;
		}

		public void setUpdateTime(Date updateTime) {
			this.updateTime = updateTime;
		}

	

		public String getStartTime() {
			return startTime;
		}

		public void setStartTime(String startTime) {
			this.startTime = startTime;
		}

		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}

		public Integer getWeight() {
			return weight;
		}

		public void setWeight(Integer weight) {
			this.weight = weight;
		}

		public Integer getOfferId() {
			return offerId;
		}

		public void setOfferId(Integer offerId) {
			this.offerId = offerId;
		}

	



        
    }

}
