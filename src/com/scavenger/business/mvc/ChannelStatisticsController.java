package com.scavenger.business.mvc;

import com.scavenger.business.domain.ChannelStatistics;
import com.scavenger.business.service.ChannelStatisticsService;
import com.scavenger.business.service.NewAddDataService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/** 
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/1 17:54
 */
@Controller
@RequestMapping(value="/gpData/channel_statistics")
public class ChannelStatisticsController extends ListController<ChannelStatistics,ChannelStatisticsService,ChannelStatisticsController.Query> {

    private final Logger log = LoggerFactory.getLogger(getClass());
    public ChannelStatisticsController() {
        super("gpData/channel_statistics");
    }

    @Resource
    private NewAddDataService newAddDataService;

    @Resource
    public void setChannelStatisticsService(ChannelStatisticsService channelStatisticsService){
        this.service=channelStatisticsService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        return false;
    }

    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        //查询注册、活跃、次留 相对前一日数据上下浮动5%预警
        List<ChannelStatistics> list = service.select(paginator);
        List<ChannelStatistics> resultList = new ArrayList<>();
        //Map<时间,Map<from,Object>>
        Map<String,Map<String,ChannelStatistics>> resultMap = new HashMap<>();
        //将所有数据按日期封装到相应的Map对象
        for(ChannelStatistics ac : list){
            if(!resultMap.containsKey(ac.getDate())){
                Map<String,ChannelStatistics> map = new HashMap<>();
                map.put(ac.getChannel(), ac);
                resultMap.put(ac.getDate(),map);
            }else{
                resultMap.get(ac.getDate()).put(ac.getChannel(), ac);
            }
        }
        try {
            //判断是否警告
            List<String> dateList =  getAllDates(query.startTime, query.endTime);
            for(int i = dateList.size()-1 ; i > 0 ; i--){
                //当前日期的数据
                Map<String,ChannelStatistics> currMap = resultMap.get(dateList.get(i));
                //前一天的数据
                Map<String,ChannelStatistics> preMap = resultMap.get(dateList.get(i-1));
                if(currMap!=null){
                    for (String key : currMap.keySet()) {
                        ChannelStatistics cs = currMap.get(key);
                        if(preMap!=null&&preMap.containsKey(key)) {
                            ChannelStatistics preCs = preMap.get(key);
                            if (preCs.getReg() > cs.getReg() * 1.1 || preCs.getReg() * 1.1 < cs.getReg()
                                    || preCs.getActive() > cs.getActive() * 1.1 || preCs.getActive() * 1.1 < cs.getActive()
                                    || preCs.getRemain() > cs.getReg() * 1.1 || preCs.getRemain() * 1.1 < cs.getRemain()) {
                                cs.setWarning(true);
                            }else{
                                cs.setWarning(false);
                            }
                        }else{
                            cs.setWarning(false);
                        }
                        resultList.add(cs);
                    }
                }
            }
        } catch (ParseException e) {
            log.error("failure to parse", e);
        }
        newAddDataService.rePaginator(page,paginator,resultList,model);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date begin=sdf.parse(query.getStartTime());
            begin = DateUtils.addDays(begin, 2);
            query.setStartTime(sdf.format(begin));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ChannelStatistics channelStatistics = service.getSum(paginator);
        model.addAttribute("sum", channelStatistics);
        super.postList(page, paginator, query, model);
    }
    /**
     * 获取查询时间段内的所有日期字符串
     */
    private List<String> getAllDates(String startTime, String endTime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date begin=sdf.parse(startTime);
        Date end=sdf.parse(endTime);
        if(begin.after(end)){
            return Collections.EMPTY_LIST;
        }
        List<String> list = new ArrayList<>();
        while(true){
            list.add(sdf.format(begin));
            if(begin.compareTo(end)==0){
                break;
            }
            begin = DateUtils.addDays(begin, 1);
        }
        return list;
    }

    @RequestMapping("/export")
    private void exportSalesDataXls(Query query,HttpServletResponse response){
        response.setContentType("text/csv");
        response.setCharacterEncoding("GBK");
        response.setHeader("Content-Disposition","attachment;filename=\"channel_statistics.csv\"");
        Paginator paginator = new Paginator(1, 300000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date begin=sdf.parse(query.getStartTime());
            begin = DateUtils.addDays(begin, 2);
            query.setStartTime(sdf.format(begin));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        paginator.setQuery(query);
        List<ChannelStatistics> list = service.list(paginator);

        try {
            response.getWriter().write("日期,渠道,注册,活跃,次日留存,金额,拦截金额\n");
            for(ChannelStatistics ta : list ){
                response.getWriter()
                        .append(String.valueOf(ta.getDate())).append(",")
                        .append(String.valueOf(ta.getChannel())).append(",")
                        .append(String.valueOf(ta.getReg())).append(",")
                        .append(String.valueOf(ta.getActive())).append(",")
                        .append(String.valueOf(ta.getRemain())).append(",")
                        .append(String.valueOf(ta.getPayout())).append(",")
                        .append(String.valueOf(ta.getInterpayout())).append("\n");
            }
        } catch (IOException e) {
           log.error("export failure",e);
        }
    }


    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;
        private String channel;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -2), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            startTime = startTime.replaceAll("/", "-");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date begin=sdf.parse(startTime);
                begin = DateUtils.addDays(begin, -1);
                this.startTime = sdf.format(begin);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.addItem("startTime", this.startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel", channel);
        }
    }

}
