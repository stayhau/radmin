package com.scavenger.business.mvc;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.domain.CountryType;
import com.scavenger.business.domain.PriceArea;
import com.scavenger.business.service.CountryTypeService;
import com.scavenger.business.service.PriceAreaService;
import com.tuziilm.web.common.Country;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.common.RemarkStatusForm;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Controller
@RequestMapping("/priceArea")
public class PriceAreaController extends CRUDController<PriceArea, PriceAreaService, PriceAreaController.Form, Query.NameQuery> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public PriceAreaController(){
		super("priceArea");
	}
    @Resource
    private CountryTypeService countryTypeService;
    @Resource
	public void setPriceAreaService(PriceAreaService priceAreaService){
		this.service=priceAreaService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        model.addAttribute("countryMap", Country.shortcut2CountryMap);
        return super.preList(page, paginator, query, model);
    }

    @Override
    protected void postCreate(Model model) {
        model.addAttribute("countries", Country.countries);
        model.addAttribute("countryTypes", countryTypeService.listAll());
    }

    @Override
    protected void postModify(int id, PriceArea obj, Model model) {
        postCreate(model);
    }

    @Override
    protected void onSaveError(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        postCreate(model);
    }

    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        PriceArea priceArea=form.toObj();
        try{
            service.saveOrUpdate(priceArea);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "类型已经存在！"));
            model.addAttribute("errors", errors);
        }

	}

	public static class Form extends RemarkStatusForm<PriceArea> {
        @NotEmpty(message = "国家不能为空")
        private Set<String> countriesObject;
        private Integer type;
        @NotNull(message = "价格不能为空")
        private double price;
        @NotBlank
        private String typeName;

        @Override
        public PriceArea newObj() {
            return new PriceArea();
        }

        @Override
        public void populateObj(PriceArea priceArea) {
            priceArea.setCountriesObject(countriesObject);
            priceArea.setPrice((int)(price*100));
            priceArea.setType(type);
            priceArea.setTypeName(typeName);
        }

        public void setCountries(String countries){
            if(Strings.isNullOrEmpty(countries)){
                return;
            }
            this.countriesObject= Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(countries));
        }

        public String getCountries(){
            if(this.countriesObject!=null&&!this.countriesObject.isEmpty()){
                return this.countriesObject.toString();
            }else{
                return "";
            }
        }

        public Set<String> getCountriesObject() {
            return countriesObject;
        }

        public void setCountriesObject(Set<String> countriesObject) {
            this.countriesObject = countriesObject;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }
    }

}
