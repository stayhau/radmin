package com.scavenger.business.mvc;

import com.scavenger.business.common.LoginContext;
import com.scavenger.business.domain.SdkChannelData;
import com.scavenger.business.service.SdkChannelDataService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/23
 * Time: 9:29
 */
@Controller
@RequestMapping(value="/sdkChannel")
public class SdkChannelDataController extends ListController<SdkChannelData,SdkChannelDataService,SdkChannelDataController.Query> {
    private final Logger log= LoggerFactory.getLogger(getClass());

    public SdkChannelDataController() {
        super("sdkChannel");
    }
    @Resource
    public void setSdkChannelDataService(SdkChannelDataService sdkChannelDataService){
        this.service=sdkChannelDataService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        return super.preList(page, paginator, query, model);
    }

    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;
        private String channel;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            if (LoginContext.isAdmin()) {
                this.channel = null;
            } else {
                this.channel = LoginContext.getUsername();
            }
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel", channel);
        }
    }
}
