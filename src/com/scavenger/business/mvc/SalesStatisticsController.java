package com.scavenger.business.mvc;

import com.scavenger.business.common.LoginContext;
import com.scavenger.business.domain.SalesStatistics;
import com.scavenger.business.service.SalesStatisticsService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/salestatistics")
public class SalesStatisticsController extends ListController<SalesStatistics, SalesStatisticsService, SalesStatisticsController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public SalesStatisticsController(){
		super("salestatistics");
	}

    @Resource
	public void setSalesStatisticsService(SalesStatisticsService channel2SalesmanService){
		this.service=channel2SalesmanService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        return super.preList(page, paginator, query, model);
    }

    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;
        private String salesman;
        private String channel;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyyMMdd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyyMMdd");
            if (LoginContext.isAdmin()) {
                this.channel = null;
            } else if(LoginContext.isOperator()) {
                this.channel = null;
                this.salesman = LoginContext.getUsername();
            } else {
                this.salesman = null;
                this.channel = LoginContext.getUsername();
            }
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "");
            this.addItem("endTime", endTime);
        }

        public String getSalesman() {
            return salesman;
        }

        public void setSalesman(String salesman) {
            this.salesman = salesman;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }
    }


}
