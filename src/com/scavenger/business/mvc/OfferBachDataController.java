package com.scavenger.business.mvc;

import com.scavenger.business.common.Config;
import com.scavenger.business.common.ReadUtil;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.domain.IncomeData;
import com.scavenger.business.domain.ReadView;
import com.scavenger.business.model.Offer;
import com.tuziilm.web.common.CsvReader;
import com.tuziilm.web.common.RemarkForm;
import com.tuziilm.web.exception.UploadException;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/offerbach")
public class OfferBachDataController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private CommonService commonService;

    public static void main(String[] args) throws ParseException {
        String x = "1000.00";
        System.out.println(Double.valueOf(x).intValue());


        double payout = 9;
        System.out.println(Math.round(payout * 0.8));
        System.out.println(new Double(Math.ceil(payout * 0.8)).intValue());
        System.out.println(new Double(Math.floor(payout * 0.8)).intValue());
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String create() {
        return "/offer/bach-insert";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(Form form, BindingResult errors, Model model,
                       HttpServletRequest request, HttpServletResponse response) {
        String rootPath = request.getSession().getServletContext().getRealPath(File.separator);

        Map<String, Object> map = readMulitFile(form, errors, rootPath);
        if (map != null && !map.isEmpty()) {
            String error = map.get("error").toString();
            if (!error.equals("no")) {
                errors.addError(new ObjectError("database", error));
                model.addAttribute("errors", errors);
                return "/offer/bach-insert";
            }
            List<Offer> offers = (List<Offer>) map.get("offers");
            try {
                for (int i = 0; i < offers.size(); i++) {
                    Offer offer = offers.get(i);
                    offer.setAuther(i % 2 == 0 ? "root" : "admin");
                    ReadView dataNew = ReadUtil.getDataNew(offer.getUrl());
                    int readedNum = 0;
                    if (dataNew != null) {
                        readedNum = dataNew.getViewCount();
                    }
                    offer.setReadedNum(readedNum);
                    commonService.insert(offer);
                }
            } catch (DuplicateKeyException e) {
                errors.addError(new ObjectError("database", "导入过今日该渠道数据"));
                model.addAttribute("errors", errors);
            }
        }
        Map<String, Object> fields = new HashMap<String, Object>();
        List<Offer> list = commonService.listByFields(fields, Offer.class);
        model.addAttribute("datas", list);
        String notice = (String) map.get("notice");
        if (notice.contains(",")) {
            model.addAttribute("notice", map.get("notice"));
        }
        return "/offer/list";
    }

    //读取多个文件
    public Map<String, Object> readMulitFile(Form form, BindingResult errors, String rootPath) {
        List<Offer> offers = new ArrayList<>();
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            if (form.getFile().isEmpty() || form.getFile().size() < 0) {
                throw new UploadException("上传文件不能为空!");
            } else {
                for (MultipartFile file : form.getFile()) {
                    String fileName = file.getOriginalFilename();
                    String ym = new SimpleDateFormat("yyyy").format(new Date());
                    String filePath = "uploadFile/" + ym + fileName;
                    File filex = new File(rootPath + filePath);
                    InputStream is = file.getInputStream();
                    result = readFilex(is);
                }
            }
        } catch (Exception e) {
            log.error("failure to upload", e);
            errors.addError(new ObjectError("upload", e.getMessage()));
        }

        return result;
    }


    public Map<String, Object> readFilex(InputStream is) {
        List<Offer> offers = new ArrayList<Offer>();
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, Object> fields = new HashMap<String, Object>();
        List<String> urls = new ArrayList<String>();
        String error = "no";
        StringBuilder sb = new StringBuilder();
        sb.append("有重复的单子：\n");
        HSSFWorkbook hWorkbook = null;
        try {
            hWorkbook = new HSSFWorkbook(is);
            HSSFSheet hSheet = hWorkbook.getSheetAt(0);
            DateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
            if (null != hSheet) {
                for (int i = 1; i < hSheet.getPhysicalNumberOfRows(); i++) {
                    Offer offer = new Offer();
                    HSSFRow hRow = hSheet.getRow(i);
                    if (hRow == null || hRow.getCell(0) == null || StringUtils.isEmpty(hRow.getCell(0).toString())) {
                        break;
                    }
                    String url = hRow.getCell(0).toString().trim();

//                    int readedNum = Double.valueOf(hRow.getCell(1).toString().trim()).intValue();
                    int readNum = 0;
                    int weight = 5;
                    String adChannel = "";
                    Date StartDate = hRow.getCell(3).getDateCellValue();
                    Date endDate = hRow.getCell(4).getDateCellValue();
                    if (hRow.getCell(6) != null) {
                        String t = hRow.getCell(6).toString().trim();
                        weight = Double.valueOf(hRow.getCell(6).toString().trim()).intValue();
                    }
                    if (hRow.getCell(7) != null) {
                        adChannel = hRow.getCell(7).toString().trim();
                    }
                    if (hRow.getCell(2) != null) {
                        String t = hRow.getCell(2).toString().trim();
                        readNum = Double.valueOf(hRow.getCell(2).toString().trim()).intValue();
                    }

                    if (endDate.before(StartDate)) {
                        error = url + "  开始时间大于结束时间，请修改后重新导入!";
                        break;
                    }
                    //检查是否有url 一样
                    if (urls.contains(url)) {
                        error = url + "  有重复记录，请删除重复记录后,重新导入!";
                        break;
                    }
                    fields.clear();
                    fields.put("url", url);
//                    fields.put("status", 1);
                    int c = commonService.countByFields(fields, Offer.class);
                    if (c >= 1) {
//                        error = url + "  已录入数据库，请删除后，重新导入!";
//                        break;
                        sb.append(url).append(",\n");
                    }
                    urls.add(url);
                    String startTime = formater.format(StartDate);
                    String endTime = formater.format(endDate);
                    int payout = Double.valueOf(hRow.getCell(5).toString().trim()).intValue();
                    offer.setUrl(url);
//                    offer.setReadedNum(readedNum);
                    offer.setReadNum(readNum);
                    offer.setStartTime(startTime);
                    offer.setEndTime(endTime);
                    double rate = 0.8;
                    try {
                        rate = Double.parseDouble(Config.getParam("rate"));
                    } catch (NumberFormatException ignore) {
                    }
                    int payoutNew;
                    // 9 ~ 12
                    if (payout > 15 && payout < 20) {
                        rate = 0.6;
                    } else if (payout >= 20 && payout < 25) {
                        // 10 ~ 12
                        rate = 0.5;
                    } else if (payout >= 25 && payout < 35) {
                        // 10 ~ 14
                        rate = 0.4;
                    } else if (payout >= 35 && payout < 45) {
                        // 10 ~ 13
                        rate = 0.3;
                    } else if (payout >= 45 && payout < 60) {
                        // 11 ~ 15
                        rate = 0.25;
                    }
                    payoutNew = new Double(Math.floor(payout * rate)).intValue();
                    if (payout >= 60) {
                        payoutNew = 15;
                    }

                    offer.setPayout(payoutNew);
                    offer.setPayout2(payout);
                    //默认导入就直接'开始'状态
                    offer.setStatus(1);
                    offer.setCreateTime(new Date());
                    offer.setUpdateTime(new Date());
                    offer.setWeight(weight);
                    offer.setAdChannel(adChannel);
                    offers.add(offer);
                }
            }
        } catch (Exception e) {
            error = "Import data error!";
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        result.put("error", error);
        result.put("offers", offers);
        result.put("notice", sb.toString());
        return result;
    }

    public Offer readFile(MultipartFile file) throws Exception {
        Offer offer = new Offer();
        CsvReader csvReader = CsvReader.getInstance();
        List<String> result = csvReader.readCsv(file);
        for (String cell : result) {
            String[] info = cell.split("\\t");
            String url = info[0].trim();
            int readedNum = Integer.parseInt(info[1].trim());
            int readNum = Integer.parseInt(info[2].trim());
            String startTime = info[3].trim();
            String endTime = info[4].trim();
            int payout = Integer.parseInt(info[5].trim());
            offer.setUrl(url);
            offer.setReadedNum(readedNum);
            offer.setReadNum(readNum);
            offer.setStartTime(startTime);
            offer.setEndTime(endTime);
            offer.setPayout(payout);
            offer.setStatus(-2);
            offer.setCreateTime(new Date());
            offer.setUpdateTime(new Date());
            offer.setWeight(5);
        }
        return offer;
    }


    public static class Form extends RemarkForm<IncomeData> {
        private List<MultipartFile> file;

        @Override
        public IncomeData newObj() {
            return new IncomeData();
        }

        @Override
        public void populateObj(IncomeData incomeData) {

        }

        public List<MultipartFile> getFile() {
            return file;
        }

        public void setFile(List<MultipartFile> file) {
            this.file = file;
        }
    }

}
