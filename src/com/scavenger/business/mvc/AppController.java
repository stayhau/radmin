package com.scavenger.business.mvc;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.scavenger.business.common.MemcachedCache;
import com.scavenger.business.common.RedisValues;
import com.scavenger.business.domain.App;
import com.scavenger.business.domain.Event;
import com.scavenger.business.domain.InnerEvent;
import com.scavenger.business.service.AppService;
import com.scavenger.business.service.EventLogStatisticsService;
import com.tuziilm.web.common.IdForm;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/24
 * Time: 14:31
 */
@Controller
@RequestMapping(value="/app")
public class AppController extends CRUDController<App,AppService,AppController.Form,Query.NameQuery>{
    private final Logger log = LoggerFactory.getLogger(getClass());
    public AppController() {
        super("app");
    }

    @Resource
    private EventLogStatisticsService eventLogStatisticsService;
    @Resource
    private MemcachedCache defaultCache;
    @Resource
    public void setAppService(AppService appService){
        this.service=appService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }

    @Override
    public void innerSave(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        App app = form.toObj();
//        if(!form.isModified()) {
//            List<String> channels = eventLogStatisticsService.getChannelsByApp(app.getPkgName());
//            String channelsString = null;
//            if(channels==null || channels.isEmpty()){
//                channelsString="";
//            }else{
//                for(int i =0;i<channels.size();i++){
//                    if(Strings.isNullOrEmpty(channels.get(i))) {
//                        channels.set(i, "unknown");
//                    }
//                }
//                channelsString=Joiner.on(",").skipNulls().join(channels);
//            }
//            app.setFroms(channelsString);
//        }
        try{
            service.saveOrUpdate(app);
            defaultCache.delete(RedisValues.getAppKey(app.getPkgName()));
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "app已经存在！"));
            model.addAttribute("errors", errors);
        }
    }

    @Override
    protected void preDelete(int[] ids) {
        for(int id:ids) {
            App app = service.get(id);
            if(app!=null){
                defaultCache.delete(RedisValues.getAppKey(app.getPkgName()));
            }
        }
        super.preDelete(ids);
    }

    public static class Form extends IdForm<App> {
        @NotBlank(message = "app不能为空")
        private String app;
        @NotBlank(message = "包名不能为空")
        private String pkgName;

        @NotEmpty(message = "事件不能为空")
        private Event[] events;
        private InnerEvent[] innerEvents;

        @Override
        public App newObj() {
            return new App();
        }

        @Override
        public void populateObj(App application) {
            application.setApp(app);
            application.setPkgName(pkgName);
            application.setEventsObject(events);
        }

        public String getApp() {
            return app;
        }

        public void setApp(String app) {
            this.app = app;
        }

        public String getPkgName() {
            return pkgName;
        }

        public void setPkgName(String pkgName) {
            this.pkgName = pkgName;
        }

        public InnerEvent[] getInnerEventsObject() {
            return innerEvents;
        }
        public Event[] getEventsObject() {
            return events;
        }

        public void setEventsObject(Event[] events) {
            this.events = events;
        }

        public String getInnerEvents() {
            return InnerEvent.toJsonWithNoException(innerEvents);
        }

        public String getEvents() {
            return Event.toJsonWithNoException(events);
        }

        public void setEvents(String eventsJson) {
            this.events = Event.nullOnExceptionValueOf(eventsJson, Event[].class);
            if(this.events==null || this.events.length<1){
                return;
            }
            this.innerEvents = new InnerEvent[this.events.length];
            for(int i=0;i<events.length;i++){
                innerEvents[i]=new InnerEvent(events[i]);
            }
        }
    }
}
