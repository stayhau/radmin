package com.scavenger.business.mvc;

import com.scavenger.business.domain.AdownerStatistics;
import com.scavenger.business.domain.ChannelStatistics;
import com.scavenger.business.service.AdownerStatisticsService;
import com.scavenger.business.service.ChannelStatisticsService;
import com.tuziilm.web.common.Country;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/** 
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/3 17:54
 */
@Controller
@RequestMapping(value="/gpData/adowner_statistics")
public class AdownerStatisticsController extends ListController<AdownerStatistics,AdownerStatisticsService,AdownerStatisticsController.Query> {
    private final Logger log = LoggerFactory.getLogger(getClass());
    public AdownerStatisticsController() {
        super("gpData/adowner_statistics");
    }

    @Resource
    public void setAdownerStatisticsService(AdownerStatisticsService adownerStatisticsService){
        this.service=adownerStatisticsService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        model.addAttribute("countryMap", Country.shortcut2CountryMap);
        model.addAttribute("countries", Country.countries);
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        AdownerStatistics adownerStatistics = service.getSum(paginator);
        model.addAttribute("sum", adownerStatistics);
        return super.preList(page, paginator, query, model);
    }
    @RequestMapping("/export")
    private void exportSalesDataXls(Query query,HttpServletResponse response){
        response.setContentType("text/csv");
        response.setCharacterEncoding("GBK");
        response.setHeader("Content-Disposition","attachment;filename=\"adowner_statistics.csv\"");
        Paginator paginator = new Paginator(1, 300000);
        paginator.setQuery(query);
        List<AdownerStatistics> list = service.list(paginator);

        try {
            response.getWriter().write("日期,渠道,国家,请求数,点击数,安装数,转化数，刷金额,拦截金额\n");
            for(AdownerStatistics ta : list ){
                Country country = Country.shortcut2CountryMap.get(ta.getCountry().toLowerCase());
                response.getWriter()
                        .append(String.valueOf(ta.getDate())).append(",")
                        .append(String.valueOf(ta.getChannelId())).append(",")
                        .append(String.valueOf(country==null?ta.getCountry():country.getChineseName())).append(",")
                        .append(String.valueOf(ta.getRequest())).append(",")
                        .append(String.valueOf(ta.getClick())).append(",")
                        .append(String.valueOf(ta.getInstall())).append(",")
                        .append(String.valueOf(ta.getCallback())).append(",")
                        .append(String.valueOf(ta.getPayout())).append(",")
                        .append(String.valueOf(ta.getInterpayout())).append("\n");
            }
        } catch (IOException e) {
           log.error("export failure",e);
        }
    }


    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;
        protected String channelId;
        protected String country;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getChannelId() {
            return channelId;
        }

        public void setChannelId(String channelId) {
            this.channelId = channelId;
            this.addItem("channelId", channelId);
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
            this.addItem("country", country);
        }
    }

}
