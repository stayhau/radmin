package com.scavenger.business.mvc;

import com.google.common.base.Strings;
import com.scavenger.business.domain.ChannelConversion;
import com.scavenger.business.domain.DataStatistics;
import com.scavenger.business.domain.SdkChannelData;
import com.scavenger.business.service.ChannelConversionService;
import com.scavenger.business.service.DataStatisticsService;
import com.scavenger.business.service.NewAddDataService;
import com.scavenger.business.service.SdkChannelDataService;
import com.tuziilm.web.common.IdForm;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Tuple;
import com.tuziilm.web.mvc.CRUDController;
import com.tuziilm.web.mvc.annotation.Ids;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/statistics")
public class StatisticsDataController extends CRUDController<DataStatistics, DataStatisticsService, StatisticsDataController.Form, StatisticsDataController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());
    private final String SUM_PRODUCT;

    @Resource
    private ChannelConversionService conversionService;
    @Resource
    private NewAddDataService newAddDataService;
    @Resource
    private SdkChannelDataService sdkChannelDataService;

    public StatisticsDataController() {
        super("statistics");
        SUM_PRODUCT=String.format("/%s/sum_product", "statistics");
    }

    @Resource
    public void setDataStatisticsService(DataStatisticsService dataStatisticsService){
        this.service=dataStatisticsService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }
    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        List<String> products = service.getProducts();
        List<String> channels = service.getChannels();
        model.addAttribute("products", products);
        model.addAttribute("channels", channels);

        Map<String,ChannelConversion> conversionMap = conversionService.getAllChannelConversionMapCache();
        List<DataStatistics> preResult = (List<DataStatistics>)model.asMap().get("datas");
        Integer highConversion = 0;
        Integer middleConversion = 0;
        Integer normalConversion = 0;
        for(DataStatistics ds : preResult) {
            ChannelConversion cc = conversionMap.get(ds.getChannel());
            if(cc!=null) {
                highConversion = cc.getHighConversion();
                middleConversion = cc.getMiddleConversion();
                normalConversion = cc.getNormalConversion();
            }else{
                highConversion = 100;
                middleConversion = 100;
                normalConversion = 100;
            }
            ds.setHighConversion(highConversion);
            ds.setMiddleConversion(middleConversion);
            ds.setNormalConversion(normalConversion);
            ds.setNewHighPrice(ds.getHighPrice() * highConversion / 1000);
            ds.setNewMiddlePrice(ds.getMiddlePrice() * middleConversion / 1000);
            ds.setNewNormalPrice(ds.getNormalPrice() * normalConversion / 1000);
        }
        model.addAttribute("datas", preResult);
        super.postList(page, paginator, query, model);
    }

    @Override
    protected void postModify(int id, DataStatistics obj, Model model) {
        Map<String,ChannelConversion> conversionMap = conversionService.getAllChannelConversionMapCache();
        ChannelConversion cc = conversionMap.get(obj.getChannel());
        model.addAttribute("cc", cc);
    }

    @Override
    protected void onSaveError(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        Map<String,ChannelConversion> conversionMap = conversionService.getAllChannelConversionMapCache();
        ChannelConversion cc = conversionMap.get(form.getChannel());
        model.addAttribute("cc", cc);
    }

    @Override
    public void innerSave(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        ChannelConversion channelConversion = new ChannelConversion();
        channelConversion.setHighConversion((int) (form.getHighConversion() * 1000));
        channelConversion.setMiddleConversion((int) (form.getMiddleConversion() * 1000));
        channelConversion.setNormalConversion((int) (form.getNormalConversion() * 1000));
        channelConversion.setChannel(form.getChannel());
        conversionService.updateByChannel(channelConversion);
    }
    @RequestMapping(value = "/sumProduct/{page}")
    public String goSumProduct(@PathVariable("page") int page, Query query,BindingResult errors, Model model) {
        List<String> products = service.getProducts();
        model.addAttribute("products", products);
        Paginator paginator = new Paginator(page);
        paginator.setQuery(query);
        paginator.setPath("statistics/sumProduct");
        List<DataStatistics> results = service.getSumByProduct(paginator);
        newAddDataService.rePaginator(page, paginator, results, model);
        return SUM_PRODUCT;
    }

    @RequestMapping(value="/importData/{ids}",method= RequestMethod.POST)
    public String importData(@Ids("ids") int[] ids, HttpServletRequest request) {
        List<DataStatistics> dataStatisticses = service.getDataStatisticsByIds(ids);
        Tuple<?,?> tuple = getMatch(dataStatisticses);
        sdkChannelDataService.saveSdkChannelDataAndUpdateStatus(tuple);
        String queryString = request.getParameter("_queryString");
        return Strings.isNullOrEmpty(queryString) ? REDIRECT_LIST_PAGE : REDIRECT_LIST_PAGE + "?" + queryString;
    }

    public Tuple<?,?> getMatch(List<DataStatistics> dataStatisticses) {
        List<SdkChannelData> sdkChannelDatas = new ArrayList<>();
        for (DataStatistics ds : dataStatisticses) {
            ds.setStatus((byte)1);
            SdkChannelData sdkChannelData = getSalesDataValue(ds);
            sdkChannelDatas.add(sdkChannelData);
        }
        return Tuple.valueOf(sdkChannelDatas,dataStatisticses);
    }
    public SdkChannelData getSalesDataValue(DataStatistics ds) {
        SdkChannelData sdkChannelData = new SdkChannelData();
        Map<String, ChannelConversion> conversionMap = conversionService.getAllChannelConversionMapCache();
        Integer highConversion = 0;
        Integer middleConversion = 0;
        Integer normalConversion = 0;
        ChannelConversion cc = conversionMap.get(ds.getChannel());
        if (cc != null) {
            highConversion = cc.getHighConversion();
            middleConversion = cc.getMiddleConversion();
            normalConversion = cc.getNormalConversion();
        } else {
            highConversion = 100;
            middleConversion = 100;
            normalConversion = 100;
        }
        sdkChannelData.setHighPrice(ds.getHighPrice() * highConversion / 1000);
        sdkChannelData.setMiddlePrice(ds.getMiddlePrice() * middleConversion / 1000);
        sdkChannelData.setNormalPrice(ds.getNormalPrice() * normalConversion / 1000);
        sdkChannelData.setChannel(ds.getChannel());
        sdkChannelData.setProduct(ds.getProduct());
        sdkChannelData.setDate(ds.getDate());
        sdkChannelData.setTabletActive(ds.getTabletActive());
        sdkChannelData.setPhoneActive(ds.getPhoneActive());
        sdkChannelData.setTabletAdd(ds.getTabletAdd());
        sdkChannelData.setPhoneAdd(ds.getPhoneAdd());
        sdkChannelData.setTotalRegister(ds.getTotalRegister());
        return sdkChannelData;
    }

    public static class Form extends IdForm<DataStatistics> {
        @DecimalMax(value="1",message = "高价转化率必须小于1")
        @DecimalMin(value="0",message = "高价转化率必须大于0")
        private double highConversion;
        @DecimalMax(value="1",message = "中价转化率必须小于1")
        @DecimalMin(value="0",message = "中价转化率必须大于0")
        private double middleConversion;
        @DecimalMax(value="1",message = "低价转化率必须小于1")
        @DecimalMin(value="0",message = "低价转化率必须大于0")
        private double normalConversion;
        private String channel;

        @Override
        public DataStatistics newObj() {
            return new DataStatistics();
        }

        @Override
        public void populateObj(DataStatistics dataStatistics) {

        }

        public double getHighConversion() {
            return highConversion;
        }

        public void setHighConversion(double highConversion) {
            this.highConversion = highConversion;
        }

        public double getMiddleConversion() {
            return middleConversion;
        }

        public void setMiddleConversion(double middleConversion) {
            this.middleConversion = middleConversion;
        }

        public double getNormalConversion() {
            return normalConversion;
        }

        public void setNormalConversion(double normalConversion) {
            this.normalConversion = normalConversion;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }
    }

    public static class Query extends com.tuziilm.web.common.Query {
        protected String product;
        protected String channel;
        protected String startTime;
        protected String endTime;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyyMMdd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyyMMdd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "");
            this.addItem("endTime", endTime);
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
            this.addItem("product", product);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel",channel);
        }
    }

}
