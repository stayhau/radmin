package com.scavenger.business.mvc;

import com.google.common.collect.Lists;
import com.scavenger.business.common.LoginContext;
import com.scavenger.business.common.ReadUtil;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.model.Loss;
import com.scavenger.business.model.Offer;
import com.scavenger.business.model.ReadIncome;
import com.scavenger.business.service.ChannelService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/readincome")
public class ReadIncomeController {
    private final Logger log = LoggerFactory.getLogger(getClass());


    @Resource
    private ChannelService channelService;
    @Autowired
    private CommonService commonService;

    @RequestMapping("/list")
    public String list(Model model) {
        Map<String, Object> fields = new HashMap<String, Object>();
        List<ReadIncome> list = commonService.listByFields(fields, ReadIncome.class, "");
        List<ReadIncome> listNew = Lists.newArrayList();
        if (LoginContext.isSelf()) {
            for (ReadIncome r : list) {
                r.setIncome(r.getIncome2());
                r.setPayout(r.getPayout2());
                r.setReadNum(r.getReadNum2());
                listNew.add(r);
            }
        } else {
            for (ReadIncome r : list) {
                double payout = 0;
                if (r.getIncome() > 0) {
                    BigDecimal bg = new BigDecimal(r.getIncome() / (r.getReadNum() / 1000));
                    payout = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                }
                r.setPayout(payout);
                listNew.add(r);
            }
        }
        model.addAttribute("datas", listNew);
        return "/readincome/list";
    }


    @RequestMapping("/create")
    public String create(Model model) {
        model.addAttribute("channels", channelService.getAllChannelCache());
        return "/readincome/create";
    }

    protected void outputJosn(final HttpServletResponse response, final String msg) {
        try {
            response.setContentType("text/html");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().write(msg);
            response.getWriter().flush();
            response.getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/check-url")
    public void checkUrl(String url, HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(url)) {
            outputJosn(response, "url不能为空");
            return;
        }

        String lurl = ReadUtil.getShotUrl(url);
        if (StringUtils.isEmpty(lurl)) {
            outputJosn(response, "链接异常，请检查链接！");
            return;
        }
        outputJosn(response, "success");
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@Valid Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        model.addAttribute("form", form);
        Map<String, Object> conditions = new HashMap<String, Object>();
        if (!errors.hasErrors()) {
            List<Loss> list = commonService.listByFields(conditions, Loss.class);
            Loss lo = new Loss();
            if (list != null && !list.isEmpty()) {
                lo = list.get(0);
                lo.setLoss(form.getLoss());
                lo.setFloatValue(form.getFloatValue());
                lo.setAuto(form.getAuto());
                commonService.update(lo, conditions);
            }
        }
        return list(model);
    }

    @RequestMapping("/modify")
    public String modify(Model model) {
        Map<String, Object> conditions = new HashMap<String, Object>();
        Loss loss = commonService.listByFields(conditions, Loss.class).get(0);
        model.addAttribute("form", loss);
        return create(model);
    }


    public static class Form {

        private Double loss;
        private Double floatValue;
        private Integer auto;

        public Offer newObj() {
            return new Offer();
        }

        public void populateObj(Loss lossInfo) {
            lossInfo.setLoss(loss);
            lossInfo.setFloatValue(floatValue);
            lossInfo.setAuto(auto);
        }

        public Double getLoss() {
            return loss;
        }

        public void setLoss(Double loss) {
            this.loss = loss;
        }

        public Double getFloatValue() {
            return floatValue;
        }

        public void setFloatValue(Double floatValue) {
            this.floatValue = floatValue;
        }

        public Integer getAuto() {
            return auto;
        }

        public void setAuto(Integer auto) {
            this.auto = auto;
        }


    }

}
