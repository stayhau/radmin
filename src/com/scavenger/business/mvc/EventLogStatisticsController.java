package com.scavenger.business.mvc;

import com.google.common.base.Strings;
import com.scavenger.business.domain.App;
import com.scavenger.business.domain.EventLogStatistics;
import com.scavenger.business.service.AppService;
import com.scavenger.business.service.EventLogStatisticsService;
import com.scavenger.business.service.LogChannelService;
import com.tuziilm.web.common.Country;
import com.tuziilm.web.common.IdForm;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.CRUDController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/8/25
 * Time: 9:38
 */
@Controller
@RequestMapping(value="eventLogStatistics")
public class EventLogStatisticsController extends CRUDController<EventLogStatistics,EventLogStatisticsService,EventLogStatisticsController.Form,EventLogStatisticsController.Query> {

    private final Logger log = LoggerFactory.getLogger(getClass());
    public EventLogStatisticsController() {
        super("eventLogStatistics");
    }

    @Resource
    private AppService appService;
    @Resource
    private LogChannelService logChannelService;
    @Resource
    private EventLogStatisticsService eventLogStatisticsService;
    @Resource
    public void setEventLogStatisticsService(EventLogStatisticsService eventLogStatisticsService){
        this.service=eventLogStatisticsService;
    }

    @Override
    public void innerSave(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        List<App> apps = appService.getAllAppCache();
        model.addAttribute("apps", apps);
        Map<String, List<String>> channel2App = new HashMap<>();
        for (App app :apps){
            List<String> channels = eventLogStatisticsService.getChannelsByApp(app.getPkgName());
            for(int i =0;i<channels.size();i++){
                if(Strings.isNullOrEmpty(channels.get(i))) {
                    channels.set(i, "unknown");
                }
            }
            channel2App.put(app.getPkgName(), channels);
        }
        model.addAttribute("appChannelsMap", channel2App);
        model.addAttribute("countryMap", Country.shortcut2CountryMap);
        model.addAttribute("countries", Country.countries);
        model.addAttribute("logChannels", logChannelService.getAllChannel2SalesmanMapCache());


        return super.preList(page, paginator, query, model);
    }

    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        model.addAttribute("sum", service.getSum(paginator));
    }

    public static class Form extends IdForm<EventLogStatistics> {

        @Override
        public EventLogStatistics newObj() {
            return new EventLogStatistics();
        }

        @Override
        public void populateObj(EventLogStatistics eventLogStatistics) {

        }
    }
    public static class Query extends com.tuziilm.web.common.Query {
        protected String app;
        protected String eventName;
        protected String channel;
        protected String startTime;
        protected String endTime;
        protected String country;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.channel = "all";
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getApp() {
            return app;
        }

        public void setApp(String app) {
            this.app = app;
            this.addItem("app", app);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel",channel);
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
            this.addItem("eventName",eventName);
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
            this.addItem("country",country);
        }
    }
}
