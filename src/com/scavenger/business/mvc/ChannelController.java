package com.scavenger.business.mvc;

import com.scavenger.business.domain.Channel;
import com.scavenger.business.service.ChannelService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.common.RemarkForm;
import com.tuziilm.web.mvc.CRUDController;
import com.tuziilm.web.mvc.annotation.Ids;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/channel")
public class ChannelController extends CRUDController<Channel, ChannelService, ChannelController.Form, Query.NameQuery> {
    private final Logger log= LoggerFactory.getLogger(getClass());

    public ChannelController(){
        super("channel");
    }

    @Resource
    public void setChannelService(ChannelService channelService){
        this.service=channelService;
    }

    @Override
    public String delete(@Ids("ids") int[] ids, HttpServletRequest request) {
        String page = super.delete(ids, request);
        return page;
    }
    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }

    @Override
    public void innerSave(Form form, BindingResult errors, Model model,
                          HttpServletRequest request, HttpServletResponse response) {
        Channel channel=form.toObj();
        channel.setDay(0);
        try{
            service.saveOrUpdate(channel);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "该渠道已经存在！"));
            model.addAttribute("errors", errors);
        }

    }


    public static class Form extends RemarkForm<Channel> {
        @NotBlank(message = "名称不能为空")
        private String name;
        @NotBlank(message = "类型不能为空")
        private String from;
        private Integer day;
        @NotNull(message="转化率不能为空")
        private Double conversion;

        @Override
        public Channel newObj() {
            return new Channel();
        }

        @Override
        public void populateObj(Channel channel) {
            channel.setName(name);
            channel.setFrom(from);
            channel.setDay(0);
            channel.setConversion(conversion);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public Integer getDay() {
            return day;
        }

        public void setDay(Integer day) {
            this.day = day;
        }

		public Double getConversion() {
			return conversion;
		}

		public void setConversion(Double conversion) {
			this.conversion = conversion;
		}

		
        
        
    }
}
