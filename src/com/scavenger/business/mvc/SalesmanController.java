package com.scavenger.business.mvc;

import com.scavenger.business.domain.Salesman;
import com.scavenger.business.service.SalesmanService;
import com.tuziilm.web.common.IdForm;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/salesman")
public class SalesmanController extends CRUDController<Salesman, SalesmanService, SalesmanController.Form, Query.NameQuery> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public SalesmanController(){
		super("salesman");
	}

    @Resource
	public void setSalesmanService(SalesmanService salesmanService){
		this.service=salesmanService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }

    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        Salesman salesman=form.toObj();
        try{
            service.saveOrUpdate(salesman);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "业务员已经存在！"));
            model.addAttribute("errors", errors);
        }
	}

    @Override
    protected void onSaveError(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        postCreate(model);
    }

    public static class Form extends IdForm<Salesman> {
        @NotBlank(message = "业务员不能为空")
        private String salesman;

        @Override
        public Salesman newObj() {
            return new Salesman();
        }

        @Override
        public void populateObj(Salesman salesmen) {
            salesmen.setSalesman(salesman);
        }

        public String getSalesman() {
            return salesman;
        }

        public void setSalesman(String salesman) {
            this.salesman = salesman;
        }

    }

}
