package com.scavenger.business.mvc.callback;

import com.scavenger.business.common.MailSend;
import com.scavenger.business.domain.CdnChecker;
import com.scavenger.business.domain.ChannelStatistics;
import com.scavenger.business.mvc.ChannelStatisticsController;
import com.scavenger.business.service.CdnCheckerService;
import com.scavenger.business.service.ChannelStatisticsService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.callback.AbstractCallbackController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jackson.JsonNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/17
 * Time: 11:43
 */
@Controller("ChannelDataWarningController")
public class ChannelDataWarningController extends AbstractCallbackController {
    private final static String[] sendTo = {"huangyuling@scavengers.mobi"};
    @Resource
    private ChannelStatisticsService channelStatisticsService;

    @RequestMapping(value = "/sendWarning",method = RequestMethod.GET)
    public @ResponseBody void getWarningMail(ChannelStatisticsController.Query query){
        Paginator paginator = new Paginator(1, 300000);
        paginator.setQuery(query);
        List<ChannelStatistics> list = channelStatisticsService.select(paginator);
        List<ChannelStatistics> resultList = new ArrayList<>();
        //Map<时间,Map<from,Object>>
        Map<String,Map<String,ChannelStatistics>> resultMap = new HashMap<>();
        //将所有数据按日期封装到相应的Map对象
        for(ChannelStatistics ac : list){
            if(!resultMap.containsKey(ac.getDate())){
                Map<String,ChannelStatistics> map = new HashMap<>();
                map.put(ac.getChannel(), ac);
                resultMap.put(ac.getDate(),map);
            }else{
                resultMap.get(ac.getDate()).put(ac.getChannel(), ac);
            }
        }
        try {
            //判断是否警告
            List<String> dateList =  getAllDates(query.getStartTime(), query.getEndTime());
            for(int i = dateList.size()-1 ; i > 0 ; i--){
                //当前日期的数据
                Map<String,ChannelStatistics> currMap = resultMap.get(dateList.get(i));
                //前一天的数据
                Map<String,ChannelStatistics> preMap = resultMap.get(dateList.get(i-1));
                if(currMap!=null){
                    for (String key : currMap.keySet()) {
                        ChannelStatistics cs = currMap.get(key);
                        if(preMap!=null&&preMap.containsKey(key)) {
                            ChannelStatistics preCs = preMap.get(key);
                            if (preCs.getReg() > cs.getReg() * 1.1 || preCs.getReg() * 1.1 < cs.getReg()
                                    || preCs.getActive() > cs.getActive() * 1.1 || preCs.getActive() * 1.1 < cs.getActive()
                                    || preCs.getRemain() > cs.getReg() * 1.1 || preCs.getRemain() * 1.1 < cs.getRemain()) {
                                resultList.add(cs);
                                resultList.add(preCs);
                            }
                        }
                    }
                }
            }
        } catch (ParseException e) {
            log.error("failure to parse", e);
        }
        StringBuffer mailInfo = new StringBuffer();
        mailInfo.append("以下渠道数据变化幅度超过10%：").append("<br>");
        if(resultList.size()>0&&!resultList.isEmpty()){
            for(ChannelStatistics cs : resultList) {
                mailInfo.append(cs.toString()).append("<br>");
            }
            MailSend ms = new MailSend();
            try {
                ms.send(sendTo,mailInfo.toString(),"渠道数据预警");
            } catch (Exception e) {
                log.error("mail send fail",e);
            }
        }
    }
    /**
     * 获取查询时间段内的所有日期字符串
     */
    private List<String> getAllDates(String startTime, String endTime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date begin=sdf.parse(startTime);
        Date end=sdf.parse(endTime);
        if(begin.after(end)){
            return Collections.EMPTY_LIST;
        }
        List<String> list = new ArrayList<>();
        while(true){
            list.add(sdf.format(begin));
            if(begin.compareTo(end)==0){
                break;
            }
            begin = DateUtils.addDays(begin, 1);
        }
        return list;
    }
}
