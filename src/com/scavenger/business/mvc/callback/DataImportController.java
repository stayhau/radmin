package com.scavenger.business.mvc.callback;

import com.scavenger.business.onlinebusiness.domain.SalesData;
import com.scavenger.business.service.SalesDataService;
import com.tuziilm.web.mvc.callback.AbstractCallbackController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/10/10
 * Time: 11:43
 */
@Controller("DataImportController")
public class DataImportController extends AbstractCallbackController {
    @Resource
    private SalesDataService salesDataService;
    @RequestMapping(value = "/openApi/dataImport/{date}/{channel}/{high}/{middle}/{low}" ,method = RequestMethod.GET)
    public @ResponseBody
    void getResult(@PathVariable("date") String date,@PathVariable("channel") String channel,
                   @PathVariable("high") Integer high,@PathVariable("middle") Integer middle,@PathVariable("low") Integer low) {
        SalesData salesData = new SalesData();
        salesData.setDate(date.replaceAll("/", "-"));
        salesData.setChannel(channel);
        salesData.setNewHigh(high);
        salesData.setNewMiddle(middle);
        salesData.setNewNormal(low);
        salesData.setNewTotal(high + middle + low);
        salesData.setIsShow("true");
        List<SalesData> salesDatas = new ArrayList<>();
        salesDatas.add(salesData);
        salesDataService.saveSalesDataAndUpdateStatus(com.tuziilm.web.common.Tuple.valueOf(salesDatas, new ArrayList<>()));
    }

}
