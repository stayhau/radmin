package com.scavenger.business.mvc.callback;

import com.scavenger.business.common.MailSend;
import com.scavenger.business.domain.CdnChecker;
import com.scavenger.business.service.CdnCheckerService;
import com.tuziilm.web.mvc.callback.AbstractCallbackController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Layout;
import org.codehaus.jackson.JsonNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/10/10
 * Time: 11:43
 */
@Controller("CdnCheckController")
public class CdnCheckController  extends AbstractCallbackController {
    @Resource
    private CdnCheckerService cdnCheckerService;
    private final static String HOSTURL = "http://www.17ce.com/site/cdn";
    private final static String FRESHURL = "http://www.17ce.com/site/ajaxfresh";
    private final static String[] sendTo = {"huangyuling@scavengers.mobi","denglitao@scavengers.mobi"};

    //获取验证码
    public String doByJs(String str) throws Exception{
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        File file = new File(CdnCheckController.class.getResource("sha.js").toURI());
        FileReader reader = new FileReader(file);
        engine.eval(reader);
        String c = "";
        if(engine instanceof Invocable) {
            Invocable invoke = (Invocable)engine;
            c = invoke.invokeFunction("hex_sha1", str).toString();
        }
        reader.close();
        return c;
    }
    @RequestMapping(value = "/checkcdn/getResult/{speed}" ,method = RequestMethod.GET)
    public @ResponseBody
    void getResult(@PathVariable("speed") int speed,HttpServletRequest request, HttpServletResponse response){
        try{

            String url = "http://third.storage.yunvm.com/ScavengersPuzzle.apk";
            int rt = 1;//get 1 post 2
            String str ="CEyL@!R"+ url + rt + "k^%*j!zwZ";
            String verify = doByJs(str);
            String param = "&url="+url+"&curl=&rt="+rt+"&nocache=1&host=&referer=&cookie=&agent=&speed=&postfield=&verify="
                    +verify+"&pingcount=&pingsize=&area[]=2&area[]=3&&isp[]=0&isp[]=1&isp[]=2&isp[]=6&isp[]=7&isp[]=8&isp[]=4&";
            String result = sendPost(HOSTURL, param);
            if(result.isEmpty()||result==null){
                return;
            }
            JsonNode cdn_node = mapper.readTree(result);
            String tid = cdn_node.get("tid").asText();
            Integer num = 0;
            List<CdnChecker> list = new ArrayList<>();
            list = getCDNChecker(list, tid, num);
            cdnCheckerService.insertAllCdnChecker(list);
        }catch (Exception e){
            log.error("failure do quest",e);
        }

        //将速度小于500KB的发送邮件给运营
        List<CdnChecker> lastest = cdnCheckerService.getLastest();
        StringBuffer mailInfo = new StringBuffer();
        mailInfo.append("以下监测点速度较慢：").append("<br>");
        boolean flag = false;
        for(CdnChecker cdnChecker : lastest){

            if(cdnChecker.getSpeed().indexOf("KB")>0&&Double.parseDouble(cdnChecker.getSpeed().split("KB")[0])<speed) {
                flag = true;
                mailInfo.append(cdnChecker.toString()).append("<br>");
            }
        }
        if(flag){
            MailSend ms = new MailSend();
            try {
                ms.send(sendTo,mailInfo.toString(),"");
            } catch (Exception e) {
                log.error("mail send fail",e);
            }
        }
    }
    public List<CdnChecker> getCDNChecker(List<CdnChecker> result,String tid,Integer num) {
        Map<Integer, Integer> reqTimes = new HashMap<>();
        while (true) {
            //防止某些站点检测不到数据
            if(reqTimes.get(num)==null) {
                reqTimes.put(num, 0);
            }else if(reqTimes.get(num)>100) {
                return result;
            }else {
                reqTimes.put(num,reqTimes.get(num) + 1);
            }
            String ajaxResult = getData(tid, num);
            Map<String, Object> ajax_node = parseJSON2Map(ajaxResult);
            Integer freshNum = Integer.valueOf(ajax_node.get("num").toString());
            Integer freshType = Integer.valueOf(ajax_node.get("taskstatus").toString());
            String freshData = ajax_node.get("freshdata").toString();
//            System.out.println("freshData = " + freshData);
            if (freshNum > num && !"[]".equalsIgnoreCase(freshData)) {
                Map<String, Object> data = parseJSON2Map(freshData);
                for (Object value : data.values()) {
                    List<Map<String, Object>> list = (List<Map<String, Object>>) value;
                    for (Map<String, Object> map : list) {
                        CdnChecker checker = new CdnChecker();
                        checker.setSpeed(map.get("speed").toString());
                        checker.setFileSize(map.get("FileSize").toString());
                        checker.setTotalTime(map.get("TotalTime").toString());
                        checker.setConnectTime(map.get("ConnectTime").toString());
                        checker.setNsLookup(map.get("NsLookup").toString());
                        checker.setDowntime(map.get("downtime").toString());
                        checker.setNsLookup(map.get("NsLookup").toString());
                        checker.setIsp(map.get("isp").toString());
                        checker.setView(map.get("view").toString());
                        checker.setRealSize(map.get("realsize").toString());
                        checker.setName(map.get("name").toString());
                        Map<String, Object> srcIp = parseJSON2Map(map.get("SrcIP").toString());
                        checker.setIpFrom(srcIp.get("ipfrom").toString());
                        checker.setIp(srcIp.get("srcip").toString());
                        result.add(checker);
                    }
                }
                num = freshNum;
            } else if (freshType == 3) {
                break;
            }
        }
        return result;
    }

    public static Map<String, Object> parseJSON2Map(String jsonStr) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 最外层解析
        JSONObject json = JSONObject.fromObject(jsonStr);
        for (Object k : json.keySet()) {
            Object v = json.get(k);
            // 如果内层还是数组的话，继续解析
            if (v instanceof JSONArray) {
                List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                Iterator<JSONObject> it = ((JSONArray) v).iterator();
                while (it.hasNext()) {
                    try {
                        JSONObject json2 = it.next();
                        list.add(parseJSON2Map(json2.toString()));
                    } catch (Exception e) {
                        //异常处理
                        String x="{'"+k+"':'"+v.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "")+"'}";
                        JSONObject jsonx = JSONObject.fromObject(x);
                        list.add(parseJSON2Map(x));
                    }

                }
                map.put(k.toString(), list);
            } else {
                map.put(k.toString(), v);
            }
        }
        return map;
    }
    public String getData(String tid,Integer num) {
        String ajaxParam = "tid="+tid+"&&num="+num+"&&ajax_over=0";
        String ajaxResult = sendPost(FRESHURL, ajaxParam);
        return ajaxResult;
    }

    /**
     * 向指定 URL 发送POST方法的请求
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            conn.setRequestProperty("Content-Type",	"application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length","286");
            conn.setRequestProperty("Accept-Language","zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
            //conn.setRequestProperty("Accept-Encoding","gzip, deflate");
            conn.setRequestProperty("Referer","http://www.17ce.com/site/cdn.html");
            conn.setRequestProperty("Host","www.17ce.com");
            conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0");
            conn.setRequestProperty("Accept-Charset", "utf-8");
            conn.setRequestProperty("contentType", "utf-8");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(),"utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }
}
