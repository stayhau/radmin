package com.scavenger.business.mvc.callback;

import com.scavenger.business.common.MD5Utils;
import com.scavenger.business.domain.BasicData;
import com.scavenger.business.domain.RevisitData;
import com.scavenger.business.service.BasicDataService;
import com.tuziilm.web.common.DateUtils;
import com.tuziilm.web.common.JsonObject;
import com.tuziilm.web.common.JsonSupport;
import com.tuziilm.web.mvc.callback.AbstractCallbackController;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/9/20
 * Time: 16:41
 */
@Controller
@RequestMapping("/openApi")
public class OpenApiController extends AbstractCallbackController{
    private final Logger log= LoggerFactory.getLogger(getClass());
    private final static String HOST_URL = "http://openapi.lotuseed.com:6080";
    private final static String LOGIN = "/v2/comm/login?";
    private final static String APPS = "/v2/comm/apps.get?";
    private final static String APPCHANS = "/v2/comm/appchans.get?";
    private final static String BASICDATA = "/v2/comm/basicdata.list?";
    private final static String REVISIT = "/v2/comm/revisit.list?";
    private final static String ONEDIM = "/v2/comm/onedim.get?";
    private final static String USERNAME = "caoyaui@gmail.com";
    private final static String PASSWD = "caoyaui123";
    @Resource
    private BasicDataService basicDataService;

    /**
     * 用户认证
     * Date: 2016/9/20 17:04
     */
    @RequestMapping(value="/authentication")
    public void authentication(HttpServletResponse response){
        try {
            //登录用户获取token
            long time = System.currentTimeMillis();
            String login_param = "username=" + USERNAME + "&password=" + MD5Utils.encode(PASSWD, String.valueOf(time)) + "&time=" + time;
            String login_sign = getSign(login_param);
            String login_result = doGet(LOGIN,login_param,login_sign);
            JsonNode login_node = mapper.readTree(login_result);
            String token = login_node.findValue("token").asText();
            //获取可访问的产品列表
            String app_param = "token=" + token + "&time=" + time+"&os=1";
            String app_sign = getSign(app_param);
            String app_result = doGet(APPS, app_param, app_sign);
            JsonNode app_node = mapper.readTree(app_result);
            List<JsonNode> apps = app_node.findValues("data");
            List<BasicData> bds = new ArrayList<>();
            List<RevisitData> revds = new ArrayList<>();
            for(JsonNode jo : apps) {
                String appkey = jo.findValue("appkey").asText();
                String appName = jo.findValue("appname").asText();
                //获取指定产品基础数据明细
                String basic_param = "token=" + token + "&time=" + time + "&appkey=" + appkey+"&startdate="+ DateUtils.yesterdayString("yyyyMMdd")+"&enddate="+DateUtils.yesterdayString("yyyyMMdd")+"&version=*";
                String basic_sign = getSign(basic_param);
                String basic_result = doGet(BASICDATA, basic_param, basic_sign);
                Map<Integer,String> channels = getChannels(token, appkey, time);
                JsonNode basic_node = mapper.readTree(basic_result);
                Iterator<JsonNode> basic_data = basic_node.findPath("data").getElements();
                while(basic_data.hasNext()){
                    JsonNode data = basic_data.next();
                    BasicData bd = new BasicData();
                    bd.setActive(data.get("A").asInt());
                    bd.setCountUsers(data.get("C").asInt());
                    bd.setDate(data.get("D").asText());
                    bd.setProduct(appName);
                    bd.setNewAdd(data.get("N").asInt());
                    bd.setStartTimes(data.get("S").asInt());
                    bd.setCountStartTimes(data.get("T").asInt());
                    bd.setUpdateUsers(data.get("U").asInt());
                    bd.setChannel(channels.get(data.get("channel").asInt()));
                    bds.add(bd);
                }
                //获取留存率数据
                String revisit_param = "token="+token+"&appkey="+appkey+"&startdate="+ DateUtils.yesterdayString("yyyyMMdd")+"&enddate="+DateUtils.yesterdayString("yyyyMMdd");
                String revisit_sign = getSign(revisit_param);
                String revisit_result = doGet(REVISIT, revisit_param, revisit_sign);
                JsonNode revisit_node = mapper.readTree(revisit_result);
                Iterator<JsonNode> revisit_data = revisit_node.findPath("data").getElements();
                while(revisit_data.hasNext()){
                    JsonNode data = revisit_data.next();
                    RevisitData bd = new RevisitData();
                    bd.setOneDay(data.get("Q").asInt());
                    bd.setTwoDays(data.get("W").asInt());
                    bd.setThreeDays(data.get("E").asInt());
                    bd.setFourDays(data.get("R").asInt());
                    bd.setFiveDays(data.get("T").asInt());
                    bd.setSixDays(data.get("Y").asInt());
                    bd.setSevenDays(data.get("U").asInt());
                    bd.setEightDays(data.get("I").asInt());
                    bd.setNineDays(data.get("O").asInt());
                    bd.setTenDays(data.get("P").asInt());
                    bd.setFourteenDays(data.get("B").asInt());
                    bd.setThirtyDays(data.get("N").asInt());
                    bd.setNinetyDays(data.get("M").asInt());
                    bd.setDate(data.get("D").asText());
                    bd.setProduct(appName);
                    bd.setChannel(channels.get(data.get("C").asInt()));
                    revds.add(bd);
                }
                //获取产品单维度汇总信息
//                String onedim_param = "token=" + token + "&appkey=" + appkey + "&startdate=" + DateUtils.yesterdayString("yyyyMMdd") + "&enddate=" + DateUtils.yesterdayString("yyyyMMdd") + "&dim=C|A2|I|D|N|O|F" + "&time=" + time;
//                String onedim_sign = getSign(onedim_param);
//                String onedim_result = doGet(ONEDIM, onedim_param, onedim_sign);
//                System.out.println("onedim_result = " + onedim_result);
//                JsonNode onedim_node = mapper.readTree(onedim_result);
            }
            //save basic data into database--------------------------
            basicDataService.saveAllDatas(bds, revds);

        }catch (IOException e) {
            log.error("get request failure!",e);
        }
    }
    /**
     * 参数加密
     * Date: 2016/9/21 21:40
     */
    public String getSign(String param){
        try {
            return MD5Utils.encode(param + "Java@1.6*1234567890");
        } catch (UnsupportedEncodingException e) {
            log.error("md5 encode failure!",e);
            return null;
        }
    }
    /**
     * 获取产品渠道
     * Date: 2016/9/21 21:41
     */
    public Map<Integer,String> getChannels(String token,String appkey,long time) throws IOException {
        //有效渠道
        String channel_param = "token=" + token + "&appkey=" + appkey + "&time=" + time;
        //显示删除渠道
        String del_channel_param = "token=" + token + "&appkey=" + appkey + "&time=" + time+"&visible=1";
        String channel_sign = getSign(channel_param);
        String del_channel_sign = getSign(del_channel_param);
        String channel_result = doGet(APPCHANS, channel_param, channel_sign);
        String del_channel_result = doGet(APPCHANS, del_channel_param, del_channel_sign);
        JsonNode channel_node = mapper.readTree(channel_result);
        JsonNode del_channel_node = mapper.readTree(del_channel_result);
        Iterator<JsonNode> channels = channel_node.findPath("data").getElements();
        Iterator<JsonNode> del_channels = del_channel_node.findPath("data").getElements();
        Map<Integer, String> channelMap = new HashMap<>();
        while (channels.hasNext()) {
            JsonNode jo = channels.next();
            Integer id = jo.get("id").asInt();
            String channel = jo.get("code").asText();
            channelMap.put(id, channel);
        }
        while (del_channels.hasNext()) {
            JsonNode jo = del_channels.next();
            Integer id = jo.get("id").asInt();
            String channel = jo.get("code").asText();
            channelMap.put(id, channel);
        }
        return channelMap;
    }
    /**
     * http get 请求
     * @Title: doGet
     * @author tuziilm
     * @return String
     * @throws
     */
    public static String doGet(String requestUrl,String param,String sign) throws IOException {
        String url = HOST_URL + requestUrl + param + "&sign=" + sign;
        String body = "{}";
        DefaultHttpClient client = new DefaultHttpClient();
        try {
            HttpGet get = new HttpGet(url);
            HttpResponse response = client.execute(get);
            HttpEntity entity = response.getEntity();
            body = EntityUtils.toString(entity, Charset.forName("utf-8"));
        } finally {
            client.getConnectionManager().shutdown();
        }
        return body;
    }

}
