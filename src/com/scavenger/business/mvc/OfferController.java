package com.scavenger.business.mvc;

import com.google.common.collect.Lists;
import com.scavenger.business.common.CoolUtil;
import com.scavenger.business.common.DateUtil;
import com.scavenger.business.common.LoginContext;
import com.scavenger.business.common.PageHolder;
import com.scavenger.business.common.ReadUtil;
import com.scavenger.business.common.RedisKey;
import com.scavenger.business.common.RedisUtil;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.domain.ReadView;
import com.scavenger.business.model.AdChannel;
import com.scavenger.business.model.Offer;
import com.scavenger.business.model.OfferHistory;
import com.scavenger.business.model.ReadIncome;
import com.scavenger.business.service.ChannelService;
import com.tuziilm.web.common.Query.Item;
import com.tuziilm.web.mvc.annotation.Ids;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/offer")
public class OfferController {
    private final Logger log = LoggerFactory.getLogger(getClass());

//    @Autowired
//    private RedisUtil redisUtil;
    @Resource
    private ChannelService channelService;
    @Autowired
    private CommonService commonService;


    public static class Query {
        private LinkedHashMap<String, Item> items = new LinkedHashMap<String, Item>();
        private int status;
        private String startTime;
        private String endTime;
        private String adChannel;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getAdChannel() {
            return adChannel;
        }

        public void setAdChannel(String adChannel) {
            this.adChannel = adChannel;
        }

        public LinkedHashMap<String, Item> getItems() {
            return items;
        }

        public void setItems(LinkedHashMap<String, Item> items) {
            this.items = items;
        }
    }

    public static String get(String url) {
        try {
            HttpClient httpclient = new HttpClient();
            GetMethod method = new GetMethod(url);
            httpclient.executeMethod(method);
            return new String(method.getResponseBody(), "utf-8");
        } catch (Exception e) {
        }
        return "";
    }

    @RequestMapping("/getapi")
    public String getapiNum(Model model) {
        String rs = get("http://wxapi.51tools.info/wx/my.ashx?key=xx_275468013");
        model.addAttribute("api", rs);
        return "/offer/getapi";
    }

    @RequestMapping("/list")
    public String list(Model model, Query sear) {
        Map<String, Object> fields = new HashMap<String, Object>();
        List<AdChannel> adChannelList = commonService.listByFields(fields, AdChannel.class);
        //默认时间
        Calendar ca = Calendar.getInstance();
        String endTime = DateUtil.formatDateYMD(ca.getTime());
        if (!StringUtils.isEmpty(sear.getEndTime())) {
            endTime = sear.getEndTime();
        }
        ca.add(Calendar.DAY_OF_MONTH, -1);
        String startTime = DateUtil.formatDateYMD(ca.getTime());
        if (!StringUtils.isEmpty(sear.getStartTime())) {
            startTime = sear.getStartTime();
        }
        if (sear.getStatus() != 0) {
            fields.put("status", sear.getStatus());
        }
        if (!StringUtils.isEmpty(sear.getAdChannel())) {
            fields.put("adChannel", sear.getAdChannel());
        }
        String userNameString = LoginContext.getUsername();
        String ext = " and createTime<='" + endTime + " 23:59:59' and createTime >='" + startTime + "'";
        if ("root".equals(userNameString)) {
            ext = " and payout>0 and auther='root'";
        }

        List<Offer> list = commonService.listByFields(fields, Offer.class, ext);
        PageHolder<Map<String, Object>> pageHolder = new PageHolder<>(20);
        commonService.page(fields, Offer.class, pageHolder);
        List<Offer> listNew = Lists.newArrayList();
        if (LoginContext.isSelf()) {
            for (Offer r : list) {
                r.setPayout(r.getPayout2());
                listNew.add(r);
            }
            model.addAttribute("datas", listNew);
        } else {
            model.addAttribute("datas", list);
        }

        sear.setStartTime(startTime);
        sear.setEndTime(endTime);
        model.addAttribute("sear", sear);
        model.addAttribute("adChannel", adChannelList);
        model.addAttribute("pageHolder", pageHolder);
        return "/offer/list";
    }

    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.POST)
    public String delete(@Ids("ids") int[] ids, HttpServletRequest request, Model model) {
        for (int id : ids) {
            Map<String, Object> conditions = new HashMap<String, Object>();
            conditions.put("offerId", id);
            Offer offer = commonService.listByFields(conditions, Offer.class).get(0);
            int row = commonService.delete(new Offer(), conditions);
            if (row >= 1) {
                OfferHistory offerHistory = new OfferHistory(offer);
                offerHistory.setActionType("delete");
                offerHistory.setActionTime(new Date());
                offerHistory.setAuther(LoginContext.getUsername());
                commonService.insert(offerHistory);
            }

        }
        return list(model, new Query());
    }

    @RequestMapping(value = "/verify/{ids}", method = RequestMethod.POST)
    public String verify(@Ids("ids") int[] ids, HttpServletRequest request, Model model) {
        for (int id : ids) {
            Map<String, Object> conditions = new HashMap<String, Object>();
            conditions.put("offerId", id);
            Offer offer = commonService.listByFields(conditions, Offer.class).get(0);
            offer.setStatus(1);
            int row = commonService.update(offer, conditions);
            if (row >= 1) {
                OfferHistory offerHistory = new OfferHistory(offer);
                offerHistory.setActionType("verify");
                offerHistory.setActionTime(new Date());
                offerHistory.setAuther(LoginContext.getUsername());
                commonService.insert(offerHistory);
            }

        }
        return list(model, new Query());
    }

    @RequestMapping("/create")
    public String create(Model model) {
        model.addAttribute("channels", channelService.getAllChannelCache());
        return "/offer/create";
    }

    protected void outputJosn(final HttpServletResponse response, final String msg) {
        try {
            response.setContentType("text/html");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().write(msg);
            response.getWriter().flush();
            response.getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/check-url")
    public void checkUrl(String url, HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(url)) {
            outputJosn(response, "url不能为空");
            return;
        }

        String lurl = ReadUtil.getShotUrl(url);
        if (StringUtils.isEmpty(lurl)) {
            outputJosn(response, "链接异常，请检查链接！");
            return;
        }
        outputJosn(response, "success");
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@Valid Form form, BindingResult errors, Model model, HttpServletRequest request,
                       HttpServletResponse response) throws UnsupportedEncodingException {
        model.addAttribute("form", form);
        Map<String, Object> conditions = new HashMap<>();
        if (!errors.hasErrors()) {
            Map<String, Object> fields = new HashMap<>();
            fields.clear();
            String urlString = form.getUrl().trim();
            fields.put("url", urlString);
            fields.put("status", 1);
            int c = commonService.countByFields(fields, Offer.class);
            if (c >= 1 && (form.getOfferId() == null || form.getOfferId() == 0)) {
                String error = form.getUrl() + "  已录入数据库，请重新输入url!";
                model.addAttribute("errors", error);
                return create(model);
            }
            String lurl = ReadUtil.getShotUrl(form.getUrl());
            if (StringUtils.isEmpty(lurl)) {
                String error = form.getUrl() + "链接异常，请检查链接！";
                model.addAttribute("errors", error);
                return create(model);
            }

            //设置readedNum
            ReadView dataNew = ReadUtil.getDataNew(lurl);
            if (dataNew == null) {
                model.addAttribute("errors", "获取已阅读数失败！");
                return create(model);
            }
            form.setReadedNum(dataNew.getViewCount());
            Offer policy = new Offer();
            OfferHistory offerHistory = new OfferHistory();
            form.populateObj(policy);
            form.populateObj(offerHistory);
            if (form.getOfferId() != null && form.getOfferId() != 0) {
                conditions.put("offerId", form.getOfferId());
                Offer old = commonService.getByFields(conditions, Offer.class);
                urlString = form.getUrl().trim();
                old.setLurl(lurl);
                old.setUrl(urlString);
                old.setWeight(policy.getWeight());
                old.setReadNum(policy.getReadNum());
                old.setReadedNum(policy.getReadedNum());
                old.setPayout(policy.getPayout());
                old.setAdChannel(policy.getAdChannel());
                old.setRemark(policy.getRemark());
                commonService.update(old, conditions);

            } else {
                policy.setLurl(lurl);
                commonService.insert(policy);
            }

        }
        if (errors.hasErrors()) {
            if (form.getOfferId() != null) {
                conditions.put("id", form.getOfferId());
                Offer old = commonService.getByFields(conditions, Offer.class);
                model.addAttribute("form", old);
            }
            model.addAttribute("errors", errors);
            return create(model);
        }
        return list(model, new Query());
    }

    @RequestMapping("/modify/{id}")
    public String modify(@PathVariable("id") int id, Model model) {
        Map<String, Object> conditions = new HashMap<String, Object>();
        conditions.put("offerId", id);
        Offer offerslave = commonService.listByFields(conditions, Offer.class).get(0);
        model.addAttribute("form", offerslave);
        return create(model);
    }


    public static class Form {
        @NotEmpty(message = "阅读地址不能为空")
        private String url;
        // @NotEmpty(message = "单价不能为空")
        private Double payout;
        //@NotEmpty(message = "阅读数不能为空")
        private Integer readNum;
        private Integer readedNum;
        private Integer great;
        private Integer reply;
        private String auther;
        private Date creaetTime;
        private Date updateTime;
        //@NotEmpty(message = "开始时间不能为空")
        private String startTime;
        //@NotEmpty(message = "结束时间不能为空")
        private String endTime;
        private Integer weight;
        private String adChannel;
        private Integer status;
        private String remark;
        private Integer offerId;

        public Offer newObj() {
            return new Offer();
        }

        public void populateObj(Offer policy) {
            policy.setCreateTime(new Date());
            policy.setUpdateTime(new Date());
            policy.setEndTime(DateUtil.formatDateYMD(new Date()));
            policy.setStartTime(DateUtil.formatDateYMD(new Date()));
            policy.setPayout(payout);
            policy.setAuther(auther);
            policy.setReadNum(readNum);
            policy.setReadedNum(readedNum);
            String urlString = url.trim();
            policy.setUrl(urlString);
            policy.setWeight(weight);
            policy.setAdChannel(adChannel);
            policy.setStatus(1);
            if (offerId != null) {
                policy.setOfferId(offerId);
            }
            policy.setRemark(remark);
        }


        public String getAdChannel() {
            return adChannel;
        }

        public void setAdChannel(String adChannel) {
            this.adChannel = adChannel;
        }

        public Integer getReadedNum() {
            return readedNum;
        }

        public void setReadedNum(Integer readedNum) {
            this.readedNum = readedNum;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }


        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }


        public Double getPayout() {
            return payout;
        }

        public void setPayout(Double payout) {
            this.payout = payout;
        }

        public Integer getReadNum() {
            return readNum;
        }

        public void setReadNum(Integer readNum) {
            this.readNum = readNum;
        }

        public Integer getGreat() {
            return great;
        }

        public void setGreat(Integer great) {
            this.great = great;
        }

        public Integer getReply() {
            return reply;
        }

        public void setReply(Integer reply) {
            this.reply = reply;
        }

        public String getAuther() {
            return auther;
        }

        public void setAuther(String auther) {
            this.auther = auther;
        }

        public Date getCreaetTime() {
            return creaetTime;
        }

        public void setCreaetTime(Date creaetTime) {
            this.creaetTime = creaetTime;
        }

        public Date getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Date updateTime) {
            this.updateTime = updateTime;
        }


        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public Integer getWeight() {
            return weight;
        }

        public void setWeight(Integer weight) {
            this.weight = weight;
        }

        public Integer getOfferId() {
            return offerId;
        }

        public void setOfferId(Integer offerId) {
            this.offerId = offerId;
        }


    }

}
