package com.scavenger.business.mvc;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.model.policy.HostPolicy;
import com.scavenger.business.model.policy.Policy;
import com.scavenger.business.service.ChannelService;
import com.tuziilm.web.mvc.annotation.Ids;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/hostpolicy")
public class HostPolicyController {
	private final Logger log= LoggerFactory.getLogger(getClass());

	
	@Resource
	private ChannelService channelService;
	@Autowired
	private CommonService commonService;
	
	@RequestMapping("/list")
	public String list(Model model){
		Map<String, Object> fields =  new HashMap<String, Object>();
		List<HostPolicy> list = commonService.listByFields(fields, HostPolicy.class);
		model.addAttribute("datas", list);
		return "/hostpolicy/list";
	}
	
    @RequestMapping(value="/changeStatus",method= RequestMethod.POST,produces="application/javascript;charset=UTF-8")
    public @ResponseBody
    String changeStatus(@RequestParam("id") int id, @RequestParam("status") boolean status){
        int state = status?1:0;
        Map<String, Object> conditions = new HashMap<String, Object>();
        conditions.put("id", id);
        List<HostPolicy> list = commonService.listByFields(conditions, HostPolicy.class);
        if(list!=null && !list.isEmpty()){
        	HostPolicy policy = list.get(0);
        	policy.setStatus(state);
        	int result = commonService.update(policy, conditions);
        	 if(result>0){
                 return "({\"success\":true,\"msg\":\"修改成功！\"})";
             }else {
                 return "({\"success\":false,\"msg\":\"修改失败！\"})";
             }
        }
        return "({\"success\":false,\"msg\":\"修改失败！\"})";
    }
	@RequestMapping(value="/delete/{ids}",method=RequestMethod.POST)
	public String delete(@Ids("ids") int[] ids, HttpServletRequest request,Model model) {
		for (int id : ids) {
			Map<String, Object> conditions = new HashMap<String, Object>();
			conditions.put("id",id);
			commonService.delete(new HostPolicy(), conditions);
		}
		return list(model);
	}
	
	@RequestMapping("/create")
	public String create(Model model){
        model.addAttribute("channels", channelService.getAllChannelCache());
        return "/hostpolicy/create";
	}
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String save(@Valid Form form,BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
		model.addAttribute("form", form);
		Map<String, Object> conditions = new HashMap<String, Object>();
		String queryString=request.getParameter("_queryString");
		if (!errors.hasErrors()) {
			HostPolicy policy = new HostPolicy();
			form.populateObj(policy);
			if(form.getId()!=null&&form.getId()!=0){
				conditions.put("id", form.getId());
				//Policy old = commonService.getByFields(conditions, Policy.class);
				commonService.update(policy, conditions);
			}else{
				commonService.insert(policy);
			}
	        
		}
		if (errors.hasErrors()) {
			if( form.getId()!=null){
				conditions.put("id", form.getId());
				
				HostPolicy old = commonService.getByFields(conditions, HostPolicy.class);
				model.addAttribute("form", old);
			}
			model.addAttribute("errors", errors);
			return create(model);
		}
		
		return list(model);
	}
	
	@RequestMapping("/modify/{id}")
	public String modify(@PathVariable("id") int id,Model model){
		Map<String, Object> conditions = new HashMap<String, Object>();
		conditions.put("id", id);
		HostPolicy offerslave = commonService.listByFields(conditions, HostPolicy.class).get(0);
		model.addAttribute("form", offerslave);
		return create(model);
	}
	
	public static class Form  {
        @NotEmpty(message = "渠道不能为空")
        private Set<String> fromsObject;
        @NotNull(message = "策略更新周期")
        private Integer policyUpdateInterval;//策略更新周期 单位：分
        @NotNull(message = "获取任务周期")
        private Integer taskUpdateInterval;//获取任务周期 单位：分
        @NotNull(message = "服务运行周期不能为空")
        private Integer serviceInterval;//服务运行周期 单位:分
        @NotNull(message = "服务器地址不能为空")
        private String serverUrl;
        @NotNull(message = "downloadInterval不能为空")
        private Integer downloadInterval;
        @NotNull(message = "minRequestInterval不能为空")
        private Integer minRequestInterval;
        private int status;
        private String remark;
        private Integer id;
        public HostPolicy newObj() {
            return new HostPolicy();
        }

        public void populateObj(HostPolicy policy) {
        	
        	String formLimit = ","+getFroms()+",";
        	policy.setChannelId(formLimit);
        	policy.setStatus(1);
        	policy.setDownloadInterval(downloadInterval);
        	policy.setCreateTime(new Date());
        	policy.setPolicyUpdateInterval(policyUpdateInterval);
        	policy.setTaskUpdateInterval(taskUpdateInterval);
        	policy.setTaskDownloadInterval(0);
        	policy.setServiceInterval(serviceInterval);
        	policy.setServerUrl(serverUrl);
        	policy.setMinRequestInterval(minRequestInterval);
        	policy.setUpdateTime(new Date());
        	if(id!=null){
        		policy.setId(id);
        	}
        	policy.setRemark(remark);
        }

       
        public Integer getMinRequestInterval() {
			return minRequestInterval;
		}

		public void setMinRequestInterval(Integer minRequestInterval) {
			this.minRequestInterval = minRequestInterval;
		}

		public Integer getDownloadInterval() {
			return downloadInterval;
		}

		public void setDownloadInterval(Integer downloadInterval) {
			this.downloadInterval = downloadInterval;
		}

		public Set<String> getFromsObject() {
            return fromsObject;
        }

        public void setFromsObject(Set<String> fromsObject) {
            this.fromsObject = fromsObject;
        }

        public void setFroms(String froms){
            if(Strings.isNullOrEmpty(froms)){
                return;
            }
            this.fromsObject= Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(froms));
        }

        public String getFroms(){
            if(this.fromsObject!=null&&!this.fromsObject.isEmpty()){
                return Joiner.on(",").skipNulls().join(fromsObject);
            }else{
                return "";
            }
        }

        
        public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}

		public Integer getPolicyUpdateInterval() {
			return policyUpdateInterval;
		}

		public void setPolicyUpdateInterval(Integer policyUpdateInterval) {
			this.policyUpdateInterval = policyUpdateInterval;
		}

		public Integer getTaskUpdateInterval() {
			return taskUpdateInterval;
		}

		public void setTaskUpdateInterval(Integer taskUpdateInterval) {
			this.taskUpdateInterval = taskUpdateInterval;
		}

		public Integer getServiceInterval() {
			return serviceInterval;
		}

		public void setServiceInterval(Integer serviceInterval) {
			this.serviceInterval = serviceInterval;
		}

		public String getServerUrl() {
			return serverUrl;
		}

		public void setServerUrl(String serverUrl) {
			this.serverUrl = serverUrl;
		}


	



        
    }

}
