package com.scavenger.business.mvc.lucky;

import com.scavenger.business.domain.Channel;
import com.scavenger.business.model.UserReport;
import com.scavenger.business.service.UserReportService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.RemarkForm;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * 注册激活统计报表
 */
@Controller
@RequestMapping("/user-report")
public class UserReportController extends CRUDController<UserReport, UserReportService, UserReportController.Form,
        UserReportController.Query> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    public UserReportController() {
        super("user-report");
    }

    @Resource
    public void setUserReportService(UserReportService reportService) {
        this.service = reportService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        model.addAttribute("listTotal", this.service.listTotal());
        return super.preList(page, paginator, query, model);
    }

    @Override
    public void innerSave(Form form, BindingResult bindingResult, Model model, HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse) {
    }

    public static class Form extends RemarkForm<Channel> {
        @NotBlank(message = "名称不能为空")
        private String name;
        @NotBlank(message = "类型不能为空")
        private String from;
        private Integer day;
        @NotNull(message = "转化率不能为空")
        private Double conversion;

        @Override
        public Channel newObj() {
            return new Channel();
        }

        @Override
        public void populateObj(Channel channel) {
            channel.setName(name);
            channel.setFrom(from);
            channel.setDay(0);
            channel.setConversion(conversion);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public Integer getDay() {
            return day;
        }

        public void setDay(Integer day) {
            this.day = day;
        }

        public Double getConversion() {
            return conversion;
        }

        public void setConversion(Double conversion) {
            this.conversion = conversion;
        }
    }

    public static class Query extends com.tuziilm.web.common.Query {
        protected String startDate;
        protected String endDate;

        public Query() {
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
            this.addItem("startDate", startDate);
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
            this.addItem("endDate", endDate);
        }
    }
}
