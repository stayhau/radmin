package com.scavenger.business.mvc;

import com.google.common.base.Strings;
import com.scavenger.business.domain.ChannelConversion;
import com.scavenger.business.domain.PriceArea;
import com.scavenger.business.gp.domain.GpData;
import com.scavenger.business.onlinebusiness.domain.SalesData;
import com.scavenger.business.service.GpDataService;
import com.scavenger.business.service.NewAddDataService;
import com.tuziilm.web.common.Config;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Controller
@RequestMapping("/gpData")
public class GpDataController extends ListController<GpData, GpDataService, GpDataController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public GpDataController(){
		super("gpData");
	}
    @Resource
    private NewAddDataService newAddDataService;

    @Resource
	public void setGpDataService(GpDataService gpDataService){
		this.service=gpDataService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(false);
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        return super.preList(page, paginator, query, model);
    }

    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        List<GpData> datas = (List<GpData>)model.asMap().get("datas");
        Map<String, GpData> resultMap = new HashMap<>();
        for(GpData gpData:datas) {
            String key = gpData.getChannel() + Config.SEP + DateFormatUtils.format(gpData.getDate(), "yyyy-MM-dd");
            if (resultMap.containsKey(key)) {
                if (gpData.getIntercepted() == 1) {
                    resultMap.get(key).setInterceptPrice(new BigDecimal(gpData.getPrice() + resultMap.get(key).getInterceptPrice()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                } else {
                    resultMap.get(key).setPrice(new BigDecimal(gpData.getPrice() + resultMap.get(key).getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                }
            } else {
                if (gpData.getIntercepted() == 1) {
                    gpData.setInterceptPrice(gpData.getPrice());
                    gpData.setPrice(0);
                    resultMap.put(key, gpData);
                } else {
                    resultMap.put(key, gpData);
                }
            }
        }
        List<GpData> results = new ArrayList<>();
        for(String key : resultMap.keySet()) {
            results.add(resultMap.get(key));
        }
        Collections.sort(results, new Comparator<GpData>() {
            @Override
            public int compare(GpData o1, GpData o2) {
                if (o1.getDate().getTime() > o2.getDate().getTime()) {
                    return -1;
                } else if (o1.getDate().getTime() < o2.getDate().getTime()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        newAddDataService.rePaginator(page, paginator, results, model);

    }
    @RequestMapping("/export")
    private void exportSalesDataXls(Query query,HttpServletResponse response){
        response.setContentType("text/csv");
        response.setCharacterEncoding("GBK");
        response.setHeader("Content-Disposition","attachment;filename=\"gp_data.csv\"");
        Paginator paginator = new Paginator(1, 300000);
        paginator.setQuery(query);
        List<GpData> list = service.list(paginator);
        Map<String, GpData> resultMap = new HashMap<>();
        for(GpData gpData:list) {
            String key = gpData.getChannel() + Config.SEP + DateFormatUtils.format(gpData.getDate(), "yyyy-MM-dd");
            if (resultMap.containsKey(key)) {
                if (gpData.getIntercepted() == 1) {
                    resultMap.get(key).setInterceptPrice(new BigDecimal(gpData.getPrice() + resultMap.get(key).getInterceptPrice()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                } else {
                    resultMap.get(key).setPrice(new BigDecimal(gpData.getPrice() + resultMap.get(key).getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                }
            } else {
                if (gpData.getIntercepted() == 1) {
                    gpData.setInterceptPrice(gpData.getPrice());
                    gpData.setPrice(0);
                    resultMap.put(key, gpData);
                } else {
                    resultMap.put(key, gpData);
                }
            }
        }
        List<GpData> results = new ArrayList<>();
        for(String key : resultMap.keySet()) {
            results.add(resultMap.get(key));
        }
        Collections.sort(results, new Comparator<GpData>() {
            @Override
            public int compare(GpData o1, GpData o2) {
                if (o1.getDate().getTime() > o2.getDate().getTime()) {
                    return -1;
                } else if (o1.getDate().getTime() < o2.getDate().getTime()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        try {
            response.getWriter().write("日期,渠道,金额,拦截金额\n");
            for(GpData ta : results ){
                response.getWriter()
                        .append(DateFormatUtils.format(ta.getDate(), "yyyy-MM-dd")).append(",")
                        .append(String.valueOf(ta.getChannel())).append(",")
                        .append(String.valueOf(ta.getPrice())).append(",")
                        .append(String.valueOf(ta.getInterceptPrice())).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;
        private String channel;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel", channel);
        }
    }


}
