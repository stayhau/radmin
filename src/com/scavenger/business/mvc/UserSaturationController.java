package com.scavenger.business.mvc;

import com.scavenger.business.domain.UserSaturation;
import com.scavenger.business.service.UserSaturationService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/** 
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/1 17:54
 */
@Controller
@RequestMapping(value="/user_saturation")
public class UserSaturationController extends ListController<UserSaturation,UserSaturationService,UserSaturationController.Query> {

    private final Logger log = LoggerFactory.getLogger(getClass());
    public UserSaturationController() {
        super("user_saturation");
    }

    @Resource
    public void setUserSaturationService(UserSaturationService userSaturationService){
        this.service=userSaturationService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        model.addAttribute("channels", service.getChannels());
        return true;
    }


    @RequestMapping("/export")
    private void exportSalesDataXls(Query query,HttpServletResponse response){
        response.setContentType("text/csv");
        response.setCharacterEncoding("GBK");
        response.setHeader("Content-Disposition","attachment;filename=\"user_saturation.csv\"");
        Paginator paginator = new Paginator(1, 300000);
        paginator.setQuery(query);
        List<UserSaturation> list = service.list(paginator);
        try {
            response.getWriter().write("日期,渠道,活跃用户数,不开展业务数,不饱和数,饱和数,开展业务数\n");
            for(UserSaturation ta : list ){
                response.getWriter()
                        .append(String.valueOf(ta.getDate())).append(",")
                        .append(String.valueOf(ta.getChannel())).append(",")
                        .append(String.valueOf(ta.getActive())).append(",")
                        .append(String.valueOf(ta.getSat0())).append(",")
                        .append(String.valueOf(ta.getSat1())).append(",")
                        .append(String.valueOf(ta.getSat2())).append(",")
                        .append(String.valueOf(ta.getCount())).append(",")
                        .append("\n");
            }
        } catch (IOException e) {
           log.error("export failure",e);
        }
    }


    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;
        private String channel;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel", channel);
        }
    }

}
