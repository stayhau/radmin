package com.scavenger.business.mvc;

import com.scavenger.business.domain.OsNewAdd;
import com.scavenger.business.service.OsNewAddService;
import com.tuziilm.web.common.CsvReader;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.RemarkForm;
import com.tuziilm.web.exception.UploadException;
import com.tuziilm.web.mvc.CRUDController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

@Controller
@RequestMapping("/osNewAdd")
public class OsNewAddController extends CRUDController<OsNewAdd, OsNewAddService, OsNewAddController.Form, OsNewAddController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public OsNewAddController(){
		super("osNewAdd");
    }

    @Resource
    public void setOsNewAddService(OsNewAddService osNewAddService){
        this.service=osNewAddService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }
    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        List<String> products = service.getProducts();
        List<String> channels = service.getChannels();
        model.addAttribute("products", products);
        model.addAttribute("channels", channels);
        int sum = service.sumByQueryItem(paginator);
        List<OsNewAdd> datas = (List<OsNewAdd>)model.asMap().get("datas");
        if(!datas.isEmpty()&&datas.size()>0){
            for(OsNewAdd os : datas){
                if(sum==0){
                   os.setRate(0);
                }else{
                    os.setRate(new BigDecimal((double) os.getNewAdd() / (double) sum * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                }
            }
        }
        model.addAttribute("datas", datas);
        super.postList(page, paginator, query, model);
    }
    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        List<OsNewAdd> osNewAdds = readMulitFile(form,errors);
        try{
            service.insertAllOsNewAdd(osNewAdds);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "导入过今日该渠道数据"));
            model.addAttribute("errors", errors);
        }
    }
    //读取多个文件
    public List<OsNewAdd> readMulitFile(Form form, BindingResult errors) {
        List<OsNewAdd> osNewAdds = new ArrayList<>();
        try {
            if (form.getFile().isEmpty() || form.getFile().size() < 0) {
                throw new UploadException("上传文件不能为空!");
            } else {
                for (MultipartFile file : form.getFile()) {
                    if (file.getOriginalFilename().split("_").length != 5||file.getOriginalFilename().indexOf("操作系统")<0) {
                        throw new UploadException("上传文件名称不符合规则!");
                    } else {
                        osNewAdds = readFile(file, osNewAdds);
                    }
                }
            }
        } catch (UploadException e) {
            log.error("failure to upload", e);
            errors.addError(new ObjectError("upload", e.getMessage()));
        }
        return osNewAdds;
    }

    public List<OsNewAdd> readFile(MultipartFile file,List<OsNewAdd> osNewAdds) {
        String[] infos = file.getOriginalFilename().split("_");
        CsvReader csvReader = CsvReader.getInstance();
        List<String> result = csvReader.readCsv(file);
        for (String cell : result) {
            OsNewAdd osNewAdd = new OsNewAdd();
            osNewAdd.setProduct(infos[0]);
            osNewAdd.setChannel(infos[2].substring(2));
            osNewAdd.setDate(infos[3]);
            String[] info = cell.split("\\t");
            osNewAdd.setOs(info[0].trim());
            osNewAdd.setNewAdd(Integer.valueOf(info[1].trim()));
            osNewAdds.add(osNewAdd);
        }
        return osNewAdds;
    }

    public static class Form extends RemarkForm<OsNewAdd> {
        private List<MultipartFile> file;

        @Override
        public OsNewAdd newObj() {
            return new OsNewAdd();
        }

        @Override
        public void populateObj(OsNewAdd newAddData) {

        }

        public List<MultipartFile> getFile() {
            return file;
        }

        public void setFile(List<MultipartFile> file) {
            this.file = file;
        }
    }
    public static class Query extends com.tuziilm.web.common.Query {
        protected String product;
        protected String channel;
        protected String startTime;
        protected String endTime;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyyMMdd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyyMMdd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "");
            this.addItem("endTime", endTime);
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
            this.addItem("product", product);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel",channel);
        }
    }

}
