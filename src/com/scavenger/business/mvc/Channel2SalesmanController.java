package com.scavenger.business.mvc;

import com.scavenger.business.domain.Channel2Salesman;
import com.scavenger.business.service.Channel2SalesmanService;
import com.scavenger.business.service.SalesmanService;
import com.tuziilm.web.common.IdForm;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;

@Controller
@RequestMapping("/channel2salesman")
public class Channel2SalesmanController extends CRUDController<Channel2Salesman, Channel2SalesmanService, Channel2SalesmanController.Form, Query.NameQuery> {
	private final Logger log= LoggerFactory.getLogger(getClass());
    @Resource
    private SalesmanService salesmanService;
    public Channel2SalesmanController(){
		super("channel2salesman");
	}

    @Resource
	public void setChannel2SalesmanService(Channel2SalesmanService channel2SalesmanService){
		this.service=channel2SalesmanService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        model.addAttribute("salesmanMap", salesmanService.getAllSalesmanMapCache());
        return super.preList(page, paginator, query, model);
    }

    @Override
    protected void postCreate(Model model) {
        model.addAttribute("salesmen", salesmanService.getAllSalesmanCache());
    }

    @Override
    protected void postModify(int id, Channel2Salesman obj, Model model) {
        postCreate(model);
    }


    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        Channel2Salesman salesman=form.toObj();
        try{
            service.saveOrUpdate(salesman);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "渠道已经存在！"));
            model.addAttribute("errors", errors);
        }
	}

    @Override
    protected void onSaveError(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        postCreate(model);
    }

    public static class Form extends IdForm<Channel2Salesman> {
        @NotBlank(message = "渠道不能为空")
        private String channel;
        @Min(value= 0 ,message= "请选择业务员" )
        private Integer salesmanId;

        @Override
        public Channel2Salesman newObj() {
            return new Channel2Salesman();
        }

        @Override
        public void populateObj(Channel2Salesman channel2Salesman) {
            channel2Salesman.setChannel(channel);
            channel2Salesman.setSalesmanId(salesmanId);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public Integer getSalesmanId() {
            return salesmanId;
        }

        public void setSalesmanId(Integer salesmanId) {
            this.salesmanId = salesmanId;
        }

    }

}
