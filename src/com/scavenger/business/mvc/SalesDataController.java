package com.scavenger.business.mvc;

import com.google.common.base.Strings;
import com.scavenger.business.common.LoginContext;
import com.scavenger.business.domain.ChannelConversion;
import com.scavenger.business.domain.PriceArea;
import com.scavenger.business.domain.SalesStatistics;
import com.scavenger.business.onlinebusiness.domain.SalesData;
import com.scavenger.business.service.ChannelConversionService;
import com.scavenger.business.service.PriceAreaService;
import com.scavenger.business.service.SalesDataService;
import com.scavenger.business.service.SalesStatisticsService;
import com.tuziilm.web.common.Config;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

@Controller
@RequestMapping("/salesData")
public class SalesDataController extends ListController<SalesData, SalesDataService, SalesDataController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public SalesDataController(){
		super("salesData");
	}
    @Resource
    private PriceAreaService priceAreaService;
    @Resource
    private ChannelConversionService conversionService;
    @Resource
	public void setSalesDataService(SalesDataService salesDataService){
		this.service=salesDataService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        model.addAttribute("conversionMap", conversionService.getAllChannelConversionMapCache());
        model.addAttribute("priceAreaNames", conversionService.getAllPriceAreaName());
        model.addAttribute("dividedWays", conversionService.getAllDividedWay());
        List<String> channelsQuery = new ArrayList<>();
        channelsQuery.addAll(conversionService.getChannelsByDividedWay(paginator));
        channelsQuery.retainAll(conversionService.getChannelsByPriceAreaName(paginator));
        query.setChannels(channelsQuery);
        return super.preList(page, paginator, query, model);
    }

    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        List<SalesData> list = (List<SalesData>)model.asMap().get("datas");
        Map<String,ChannelConversion> conversionMap = conversionService.getAllChannelConversionMapCache();
        Map<String, PriceArea> priceAreaMap = priceAreaService.getAllPriceAreaMapCache();
        for(SalesData ta : list ) {
            String priceAreaName = conversionMap.get(ta.getChannel()) == null ? null : conversionMap.get(ta.getChannel()).getPriceAreaName();
            int hprice = 0;
            int mprice = 0;
            int nprice = 0;
            if (priceAreaMap != null && !priceAreaMap.isEmpty() && !Strings.isNullOrEmpty(priceAreaName)) {
                hprice = priceAreaMap.get(priceAreaName + Config.SEP + 1) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 1).getPrice();
                mprice = priceAreaMap.get(priceAreaName + Config.SEP + 2) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 2).getPrice();
                nprice = priceAreaMap.get(priceAreaName + Config.SEP + 3) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 3).getPrice();
            }
            int price = ta.getNewHigh() * hprice
                    + ta.getNewMiddle() * mprice
                    + ta.getNewNormal() * nprice;
            ta.setPrice(new BigDecimal((double) price / 100.0).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
        }
    }

    @RequestMapping("/export")
    private void exportSalesDataXls(Query query,HttpServletResponse response){
        response.setContentType("text/csv");
        response.setCharacterEncoding("GBK");
        response.setHeader("Content-Disposition","attachment;filename=\"sales_data.csv\"");
        Map<String,ChannelConversion> conversionMap = conversionService.getAllChannelConversionMapCache();
        Map<String, PriceArea> priceAreaMap = priceAreaService.getAllPriceAreaMapCache();
        try {
            Paginator paginator = new Paginator(1, 300000);
            paginator.setQuery(query);
            List<String> channelsQuery = new ArrayList<>();
            channelsQuery.addAll(conversionService.getChannelsByDividedWay(paginator));
            channelsQuery.retainAll(conversionService.getChannelsByPriceAreaName(paginator));
            query.setChannels(channelsQuery);
            List<SalesData> list = service.list(paginator);
            response.getWriter().write("日期,渠道,推送,分成,总量,高价,中价,普通,价格\n");
            for(SalesData ta : list ){
                String priceAreaName = conversionMap.get(ta.getChannel()) == null ? null : conversionMap.get(ta.getChannel()).getPriceAreaName();
                int hprice = 0;
                int mprice = 0;
                int nprice = 0;
                if(priceAreaMap!=null&&!priceAreaMap.isEmpty()&&!Strings.isNullOrEmpty(priceAreaName)) {
                    hprice = priceAreaMap.get(priceAreaName + Config.SEP + 1) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 1).getPrice();
                    mprice = priceAreaMap.get(priceAreaName + Config.SEP + 2) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 2).getPrice();
                    nprice = priceAreaMap.get(priceAreaName + Config.SEP + 3) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 3).getPrice();
                }
                int price = ta.getNewHigh()*hprice
                        +ta.getNewMiddle()*mprice
                        +ta.getNewNormal()*nprice;
                response.getWriter()
                        .append(ta.getDate()).append(",")
                        .append(String.valueOf(ta.getChannel())).append(",")
                        .append(String.valueOf(conversionMap.get(ta.getChannel()).getPriceAreaName())).append(",")
                        .append(String.valueOf(conversionMap.get(ta.getChannel()).getDividedWay())).append(",")
                        .append(String.valueOf(ta.getNewTotal())).append(",")
                        .append(String.valueOf(ta.getNewHigh())).append(",")
                        .append(String.valueOf(ta.getNewMiddle())).append(",")
                        .append(String.valueOf(ta.getNewNormal())).append(",")
                        .append(String.valueOf(new BigDecimal((double) price / 100.0).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())).append("\n");
            }
        } catch (Exception e) {
            log.error("failure to exportSalesDataXls",e);
        }
    }
    @RequestMapping("/export2")
    private void exportSalesData2Xls(Query query,HttpServletResponse response){
        response.setContentType("text/csv");
        response.setCharacterEncoding("GBK");
        response.setHeader("Content-Disposition","attachment;filename=\"sales_data_tj.csv\"");
        Map<String,ChannelConversion> conversionMap = conversionService.getAllChannelConversionMapCache();
        Map<String, PriceArea> priceAreaMap = priceAreaService.getAllPriceAreaMapCache();
        try {
            Paginator paginator = new Paginator(1, 300000);
            paginator.setQuery(query);
            List<String> channelsQuery = new ArrayList<>();
            channelsQuery.addAll(conversionService.getChannelsByDividedWay(paginator));
            channelsQuery.retainAll(conversionService.getChannelsByPriceAreaName(paginator));
            query.setChannels(channelsQuery);
            List<SalesData> list = service.list(paginator);
            List<String> channelList = new ArrayList<>();
            //标题
            StringBuffer title = new StringBuffer();
            title.append("日期,分成,");
            for(SalesData salesData :list) {
                if(channelList.contains(salesData.getChannel())){
                    continue;
                }else{
                    channelList.add(salesData.getChannel());
                }
            }
            for(String str : channelList){
                title.append(str).append(",");
            }
            title.append("总价\n");
            response.getWriter().write(title.toString());
            //将sqlData转成需要展示的数据
            Map<String, Map<String, Double>> result = new HashMap<String, Map<String, Double>>();
            for(SalesData ta :list){
                String priceAreaName = conversionMap.get(ta.getChannel()) == null ? null : conversionMap.get(ta.getChannel()).getPriceAreaName();
                int hprice = 0;
                int mprice = 0;
                int nprice = 0;
                if(priceAreaMap!=null&&!priceAreaMap.isEmpty()&&!Strings.isNullOrEmpty(priceAreaName)) {
                    hprice = priceAreaMap.get(priceAreaName + Config.SEP + 1) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 1).getPrice();
                    mprice = priceAreaMap.get(priceAreaName + Config.SEP + 2) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 2).getPrice();
                    nprice = priceAreaMap.get(priceAreaName + Config.SEP + 3) == null ? 0 : priceAreaMap.get(priceAreaName + Config.SEP + 3).getPrice();
                }
                int price = ta.getNewHigh()*hprice
                        +ta.getNewMiddle()*mprice
                        +ta.getNewNormal()*nprice;
                if(result.get(ta.getDate())==null) {
                    Map<String, Double> priceMap = new HashMap<>();
                    priceMap.put(ta.getChannel(), new BigDecimal((double) price / 100.0).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                    result.put(ta.getDate(), priceMap);
                }else {
                    result.get(ta.getDate()).put(ta.getChannel(), new BigDecimal((double) price / 100.0).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                }
            }
            //展示数据
            for(Map.Entry<String, Map<String,Double>> entry : result.entrySet()) {
                StringBuffer sb = new StringBuffer();
                sb.append(entry.getKey()).append(",").append(Strings.isNullOrEmpty(query.getDividedWay())?"全部":query.getDividedWay()).append(",");
                double totalPrice = 0;
                for(String channel :channelList) {
                    double price = entry.getValue().get(channel) == null ? 0 : entry.getValue().get(channel);
                    totalPrice += price;
                    sb.append(price).append(",");
                }
                sb.append(totalPrice).append("\n");
                response.getWriter().write(sb.toString());
            }
        } catch (Exception e) {
            log.error("failure to exportSalesDataPriceXls",e);
        }
    }

    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;
        private String salesman;
        private String channel;
        private String dividedWay;
        private String priceAreaName;
        private List<String> channels;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            if (LoginContext.isAdmin()) {
                this.channel = null;
            } else if(LoginContext.isOperator()) {
                this.channel = null;
                this.salesman = LoginContext.getUsername();
            } else {
                this.salesman = null;
                this.channel = LoginContext.getUsername();
            }
            this.dividedWay = "";
            this.priceAreaName = "";
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getSalesman() {
            return salesman;
        }

        public void setSalesman(String salesman) {
            this.salesman = salesman;
            this.addItem("salesman", salesman);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel", channel);
        }

        public String getDividedWay() {
            return dividedWay;
        }

        public void setDividedWay(String dividedWay) {
            this.dividedWay = dividedWay;
            this.addItem("dividedWay",dividedWay);
        }

        public String getPriceAreaName() {
            return priceAreaName;
        }

        public void setPriceAreaName(String priceAreaName) {
            this.priceAreaName = priceAreaName;
            this.addItem("priceAreaName",priceAreaName);
        }

        public List<String> getChannels() {
            return channels;
        }

        public void setChannels(List<String> channels) {
            this.channels = channels;
            this.addItem("channels",channels);
        }
    }


}
