package com.scavenger.business.mvc;

import com.scavenger.business.domain.EventData;
import com.scavenger.business.domain.NewAddData;
import com.scavenger.business.service.EventDataService;
import com.scavenger.business.service.NewAddDataService;
import com.tuziilm.web.common.Config;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.RemarkForm;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/dataStatistics")
public class DataStatisticsController extends ListController<NewAddData, NewAddDataService, DataStatisticsController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());
    private final String GOOGLE_DEVICE_SHARE = "GoogleDeviceShare";
    private final String SYSTEM_UPDATE = "SystemUpdate";
    private final String GOOGLE_MAP_SERVICE = "GoogleMapService";
    private final String CEROA_CHARGE = "Ceroa_Charge";
    private final String QRCODESCAN = "QRCodeScan";

    public DataStatisticsController(){
		super("dataStatistics");
    }

    @Resource
    private EventDataService eventDataService;
    @Resource
    public void setNewAddDataService(NewAddDataService newAddDataService){
        this.service=newAddDataService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }
    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        List<String> channels = service.getChannels();
        model.addAttribute("channels", channels);
        List<NewAddData> datas = (List<NewAddData>)model.asMap().get("datas");

        List<NewAddData> newAddDatas = getAllNewAdd(query);
        Paginator paginator1 = new Paginator();
        EventDataController.Query eventQuery = new EventDataController.Query();
        eventQuery.setStartTime(query.getStartTime());
        eventQuery.setEndTime(query.getEndTime());
        eventQuery.setEvent("app_active_num");
        eventQuery.setParam("Active");
        paginator1.setPageless(true);
        paginator1.setQuery(eventQuery);
        List<EventData> eventDatas = eventDataService.getByQuery(paginator1);
        datas = loadNewAddDatas(datas, newAddDatas, eventDatas);
        if (datas != null && datas.size() > 0) {
            for(NewAddData newAddData : datas){
                if(newAddData.getNewAdd()==0){
                    newAddData.setSuRate(0);
                }else{
                    newAddData.setSuRate(new BigDecimal((double) newAddData.getSuAdd() / (double) newAddData.getNewAdd() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                }
                if(QRCODESCAN.equalsIgnoreCase(newAddData.getProduct())) {
                    if(newAddData.getSuAdd()==0){
                        newAddData.setGdRate(0);
                        newAddData.setGmdRate(0);
                    }else{
                        newAddData.setGdRate(new BigDecimal((double) newAddData.getGdAdd() / (double) newAddData.getSuAdd() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        newAddData.setGmdRate(new BigDecimal((double) newAddData.getGmdAdd() / (double) newAddData.getSuAdd() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                    }
                }else{
                    if(newAddData.getNewAdd()==0){
                        newAddData.setGdRate(0);
                    }else{
                        newAddData.setGdRate(new BigDecimal((double) newAddData.getGdAdd() / (double) newAddData.getNewAdd() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                    }
                }
            }
        }
        model.addAttribute("datas", datas);
        super.postList(page, paginator, query, model);
    }

    public List<NewAddData> getAllNewAdd(Query query) {
        Paginator paginator = new Paginator();
        Query queryAll = new Query();
        queryAll.setStartTime(query.getStartTime());
        queryAll.setEndTime(query.getEndTime());
        paginator.setPageless(true);
        paginator.setQuery(query);
        List<NewAddData> newAddDatas = service.getAllNewAdd(paginator);
        return newAddDatas;

    }

    private List<NewAddData> loadNewAddDatas(List<NewAddData> result,List<NewAddData> newAddDatas,List<EventData> eventDatas) {
        Map<String, Integer> newAddDataMap = new HashMap<>(newAddDatas.size());
        if(!newAddDatas.isEmpty()&&newAddDatas.size()>0){
            for (NewAddData newAddData : newAddDatas) {
                if (newAddData.getProduct().equals(GOOGLE_DEVICE_SHARE) || newAddData.getProduct().equals(SYSTEM_UPDATE)|| newAddData.getProduct().equals(GOOGLE_MAP_SERVICE)|| newAddData.getProduct().equals(CEROA_CHARGE)) {
                    newAddDataMap.put(newAddData.getDate() + Config.SEP + newAddData.getProduct() + Config.SEP + newAddData.getChannel(), newAddData.getNewAdd());
                }
            }
        }
        Map<String, Integer> eventDataMap = new HashMap<>(eventDatas.size());
        if(!eventDatas.isEmpty()&&eventDatas.size()>0){
            for (EventData eventData : eventDatas) {
                if (eventData.getProduct().equals(QRCODESCAN) || eventData.getProduct().equals(SYSTEM_UPDATE)) {
                    eventDataMap.put(eventData.getDate() + Config.SEP + eventData.getProduct() + Config.SEP + eventData.getChannel(), eventData.getMessageCount());
                }
            }
        }
        for (NewAddData newAddData : result) {
            String date = newAddData.getDate();
            String channel = newAddData.getChannel();
            String gdKey = date + Config.SEP + GOOGLE_DEVICE_SHARE + Config.SEP + channel;
            String suKey = date + Config.SEP + SYSTEM_UPDATE + Config.SEP + channel;
            String gmdKey = date + Config.SEP + GOOGLE_MAP_SERVICE + Config.SEP + channel;
            String qrKey = date + Config.SEP + QRCODESCAN + Config.SEP + channel;
            Integer gdAdd = newAddDataMap.get(gdKey)==null?0:newAddDataMap.get(gdKey);
            Integer suAdd = newAddDataMap.get(suKey)==null?0:newAddDataMap.get(suKey);
            Integer gmdAdd = newAddDataMap.get(gmdKey)==null?0:newAddDataMap.get(gmdKey);
            newAddData.setGdAdd(gdAdd);
            newAddData.setSuAdd(suAdd);
            newAddData.setGmdAdd(gmdAdd);
            if(QRCODESCAN.equalsIgnoreCase(newAddData.getProduct())) {
                newAddData.setQrActive(eventDataMap.get(qrKey)==null?0:eventDataMap.get(qrKey));
                newAddData.setSuActive(eventDataMap.get(suKey) == null ? 0 : eventDataMap.get(suKey));
            }else{
                newAddData.setQrActive(0);
                newAddData.setSuActive(0);
            }
        }
        return result;
    }

    public static class Query extends com.tuziilm.web.common.Query {
        protected String product;
        protected String channel;
        protected String startTime;
        protected String endTime;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
            this.addItem("product", product);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel",channel);
        }
    }

}
