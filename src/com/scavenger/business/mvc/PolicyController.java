package com.scavenger.business.mvc;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.model.policy.Policy;
import com.scavenger.business.service.ChannelService;
import com.tuziilm.web.mvc.annotation.Ids;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/policy")
public class PolicyController {
    private final Logger log = LoggerFactory.getLogger(getClass());
    @Resource
    private ChannelService channelService;
    @Autowired
    private CommonService commonService;

    @RequestMapping(value = "/changeStatus", method = RequestMethod.POST, produces = "application/javascript;" +
			"charset=UTF-8")
    public @ResponseBody
    String changeStatus(@RequestParam("id") int id, @RequestParam("status") boolean status) {
        int state = status ? 1 : 0;
        Map<String, Object> conditions = new HashMap<String, Object>();
        conditions.put("id", id);
        List<Policy> list = commonService.listByFields(conditions, Policy.class);
        if (list != null && !list.isEmpty()) {
            Policy policy = list.get(0);
            policy.setStatus(state);
            int result = commonService.update(policy, conditions);
            if (result > 0) {
                return "({\"success\":true,\"msg\":\"修改成功！\"})";
            } else {
                return "({\"success\":false,\"msg\":\"修改失败！\"})";
            }
        }
        return "({\"success\":false,\"msg\":\"修改失败！\"})";
    }

    @RequestMapping("/list")
    public String list(Model model) {
        Map<String, Object> fields = new HashMap<String, Object>();
        List<Policy> list = commonService.listByFields(fields, Policy.class);
        model.addAttribute("datas", list);
        return "/policy/list";
    }

    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.POST)
    public String delete(@Ids("ids") int[] ids, HttpServletRequest request, Model model) {
        for (int id : ids) {
            Map<String, Object> conditions = new HashMap<String, Object>();
            conditions.put("id", id);
            commonService.delete(new Policy(), conditions);
        }
        return list(model);
    }

    @RequestMapping("/create")
    public String create(Model model) {
        model.addAttribute("channels", channelService.getAllChannelCache());
        return "/policy/create";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@Valid Form form, BindingResult errors, Model model, HttpServletRequest request,
					   HttpServletResponse response) throws UnsupportedEncodingException {
        model.addAttribute("form", form);
        Map<String, Object> conditions = new HashMap<String, Object>();
        String queryString = request.getParameter("_queryString");
        if (!errors.hasErrors()) {
            Policy policy = new Policy();
            form.populateObj(policy);
            if (form.getId() != null && form.getId() != 0) {
                conditions.put("id", form.getId());
                //Policy old = commonService.getByFields(conditions, Policy.class);
                commonService.update(policy, conditions);
            } else {
                commonService.insert(policy);
            }
        }
        if (errors.hasErrors()) {
            if (form.getId() != null) {
                conditions.put("id", form.getId());

                Policy old = commonService.getByFields(conditions, Policy.class);
                model.addAttribute("form", old);
            }
            model.addAttribute("errors", errors);
            return create(model);
        }

        return list(model);
    }

    @RequestMapping("/modify/{id}")
    public String modify(@PathVariable("id") int id, Model model) {
        Map<String, Object> conditions = new HashMap<String, Object>();
        conditions.put("id", id);
        Policy offerslave = commonService.listByFields(conditions, Policy.class).get(0);
        String clist = offerslave.getWorkCondition();
        String[] c = clist.split(",");
        model.addAttribute("workCondition", c);
        model.addAttribute("form", offerslave);
        return create(model);
    }


    public static class Form {
        @NotEmpty(message = "渠道不能为空")
        private Set<String> fromsObject;
        @NotNull(message = "间隔时间不能为空")
        private Integer workInterval;
        @NotNull(message = "工作次数不能为空")
        private Integer workCount;
        @NotNull(message = "阅读数量不能为空")
        private Integer readCount;
        @NotNull(message = "请选择工作条件")
        private String workCondition;
        @NotNull(message = "策略更新时间不能为空")
        private Integer configUpdateInterval;
        private int status;
        private String remark;
        private Integer id;
        private Integer startTime;
        private Integer endTime;
        private Integer delayTime;
        @NotNull(message = "白天开始时间不能为空")
        private Integer dtStartHour;
        @NotNull(message = "白天结束时间不能为空")
        private Integer dtEndHour;
        @NotNull(message = "白天最大次数不能为空")
        private Integer dtMaxCount;

        public Policy newObj() {
            return new Policy();
        }

        public void populateObj(Policy policy) {
            String formLimit = "," + getFroms() + ",";
            policy.setChannelId(formLimit);
            policy.setConfigUpdateInterval(configUpdateInterval);
            policy.setCreateTime(new Date());
            policy.setReadCount(readCount);
            policy.setStatus(1);
            policy.setDelayTime(delayTime);
            policy.setStartTime(startTime);
            policy.setEndTime(endTime);
            policy.setWorkCondition(workCondition);
            policy.setWorkCount(workCount);
            policy.setWorkInterval(workInterval);
            if (id != null) {
                policy.setId(id);
            }
            policy.setRemark(remark);
            policy.setDtStartHour(dtStartHour);
            policy.setDtEndHour(dtEndHour);
            policy.setDtMaxCount(dtMaxCount);
        }

        public Integer getDtStartHour() {
            return dtStartHour;
        }

        public void setDtStartHour(Integer dtStartHour) {
            this.dtStartHour = dtStartHour;
        }

        public Integer getDtEndHour() {
            return dtEndHour;
        }

        public void setDtEndHour(Integer dtEndHour) {
            this.dtEndHour = dtEndHour;
        }

        public Integer getDtMaxCount() {
            return dtMaxCount;
        }

        public void setDtMaxCount(Integer dtMaxCount) {
            this.dtMaxCount = dtMaxCount;
        }

        public Integer getStartTime() {
            return startTime;
        }

        public void setStartTime(Integer startTime) {
            this.startTime = startTime;
        }

        public Integer getEndTime() {
            return endTime;
        }

        public void setEndTime(Integer endTime) {
            this.endTime = endTime;
        }

        public Integer getDelayTime() {
            return delayTime;
        }

        public void setDelayTime(Integer delayTime) {
            this.delayTime = delayTime;
        }

        public Set<String> getFromsObject() {
            return fromsObject;
        }

        public void setFromsObject(Set<String> fromsObject) {
            this.fromsObject = fromsObject;
        }

        public void setFroms(String froms) {
            if (Strings.isNullOrEmpty(froms)) {
                return;
            }
            this.fromsObject = Sets.newHashSet(Splitter.on(",").omitEmptyStrings().trimResults().split(froms));
        }

        public String getFroms() {
            if (this.fromsObject != null && !this.fromsObject.isEmpty()) {
                return Joiner.on(",").skipNulls().join(fromsObject);
            } else {
                return "";
            }
        }


        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }


        public Integer getWorkInterval() {
            return workInterval;
        }

        public void setWorkInterval(Integer workInterval) {
            this.workInterval = workInterval;
        }

        public Integer getWorkCount() {
            return workCount;
        }

        public void setWorkCount(Integer workCount) {
            this.workCount = workCount;
        }

        public Integer getReadCount() {
            return readCount;
        }

        public void setReadCount(Integer readCount) {
            this.readCount = readCount;
        }

        public String getWorkCondition() {
            return workCondition;
        }

        public void setWorkCondition(String workCondition) {
            this.workCondition = workCondition;
        }

        public Integer getConfigUpdateInterval() {
            return configUpdateInterval;
        }

        public void setConfigUpdateInterval(Integer configUpdateInterval) {
            this.configUpdateInterval = configUpdateInterval;
        }


    }

}
