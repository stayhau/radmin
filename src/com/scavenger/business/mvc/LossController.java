package com.scavenger.business.mvc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.scavenger.business.common.LoginContext;
import com.scavenger.business.common.ReadUtil;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.model.Loss;
import com.scavenger.business.model.Offer;
import com.scavenger.business.service.ChannelService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/staticloss")
public class LossController {
	private final Logger log= LoggerFactory.getLogger(getClass());

	@Autowired
	private StringRedisTemplate stringRedisTemplate_206_6380;
	@Autowired
	private StringRedisTemplate stringRedisTemplate_206;
	

	
	@Resource
	private ChannelService channelService;
	@Autowired
	private CommonService commonService;
	@RequestMapping("/list")
	public String list(Model model, HttpServletRequest request){
		LoginContext.checkLogin(request.getSession());
		Map<String, Object> fields =  new HashMap<String, Object>();
		List<Loss> list = commonService.listByFields(fields, Loss.class,"");
		model.addAttribute("datas", list);
		return "/staticloss/list";
	}
	
	
	@RequestMapping("/create")
	public String create(Model model,HttpServletRequest request){
		LoginContext.checkLogin(request.getSession());
        model.addAttribute("channels", channelService.getAllChannelCache());
        return "/staticloss/create";
	}
	
	public static void main(String[] args) {
		System.out.println("=="+" dddd dd ".trim()+"==");
	}
	
	protected void outputJosn(final HttpServletResponse response, final String msg){
		try {
			response.setContentType("text/html");
			response.setCharacterEncoding("utf-8");
			response.setHeader("Pragma", "No-cache");
			response.setDateHeader("Expires", 0);
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(msg);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@RequestMapping(value="/check-url")
	public void checkUrl(String url, HttpServletRequest request, HttpServletResponse response){
		if(StringUtils.isEmpty(url)){
			outputJosn(response, "url不能为空");
			return;
		}
		
		String lurl = ReadUtil.getShotUrl(url);
		if(StringUtils.isEmpty(lurl)){
			outputJosn(response, "链接异常，请检查链接！");
			return;
		}
		outputJosn(response,"success");
	}
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String save(@Valid Form form,BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
		model.addAttribute("form", form);
		Map<String, Object> conditions = new HashMap<String, Object>();
		if (!errors.hasErrors()) {
			List<Loss> list = commonService.listByFields(conditions , Loss.class);
			Loss lo = new Loss();
			if(list!=null && !list.isEmpty()){
				lo = list.get(0);
				lo.setLoss(form.getLoss());
				lo.setFloatValue(form.getFloatValue());
				lo.setAuto(form.getAuto());
				commonService.update(lo, conditions);
			}
		}
		return list(model,request);
	}
	
	@RequestMapping("/modify")
	public String modify(Model model,HttpServletRequest request){
		LoginContext.checkLogin(request.getSession());
		Map<String, Object> conditions = new HashMap<String, Object>();
		Loss loss = commonService.listByFields(conditions, Loss.class).get(0);
		model.addAttribute("form", loss);
		return create(model,request);
	}
	
	@RequestMapping("/list-pre")
	public void getkey(String key,String tag,String port,String value,String key1,String action,HttpServletResponse response){
		try {
				HashOperations<String, Object, Object> op = stringRedisTemplate_206.opsForHash();
				StringRedisTemplate redis = null;
				if(port.equals("79")){
					redis = stringRedisTemplate_206;
				}else{
					redis = stringRedisTemplate_206_6380;
				}
				Object va = null;
				if(StringUtils.equals(action, "set")){
					switch (tag) {
					case "v":
						redis.opsForValue().set(key, value);
						break;
					case "h":
						redis.opsForHash().put(key, key1, value);
						break;
					case "l":
						redis.opsForList().leftPush(key, value);
						break;
					case "s":
						redis.opsForSet().add(key, value);
						break;
					case "zs":
						redis.opsForZSet().add(key, value, 1);
						break;
				  }
				}else if(StringUtils.equals(action, "get")) {
					switch (tag) {
					case "v":
						 va = redis.opsForValue().get(key);
						break;
					case "h":
						va = redis.opsForHash().get(key, key1);
						break;
					case "l":
						va = redis.opsForList().range(key, 0, -1);
						break;
					case "s":
						va = redis.opsForSet().members(key);
						break;
					case "zs":
						va = redis.opsForZSet().range(key, 0, -1);
						break;
				  }
				}else if(StringUtils.equals(action, "del")) {
					redis.delete(key);
				}
				outputJosn(response, va.toString());
		if(StringUtils.isEmpty(key)){
			return;
		}
		} catch (Exception e) {
		}
}
	
	public static class Form  {
      
     	private	Double	loss;
     	private	Double	floatValue;
     	private	Integer	auto;
        public Offer newObj() {
            return new Offer();
        }
        public void populateObj(Loss lossInfo) {
        	lossInfo.setLoss(loss);
        	lossInfo.setFloatValue(floatValue);
        	lossInfo.setAuto(auto);
        }
		public Double getLoss() {
			return loss;
		}
		public void setLoss(Double loss) {
			this.loss = loss;
		}
		public Double getFloatValue() {
			return floatValue;
		}
		public void setFloatValue(Double floatValue) {
			this.floatValue = floatValue;
		}
		public Integer getAuto() {
			return auto;
		}
		public void setAuto(Integer auto) {
			this.auto = auto;
		}


        
    }

}
