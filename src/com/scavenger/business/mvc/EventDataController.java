package com.scavenger.business.mvc;

import com.scavenger.business.domain.EventData;
import com.scavenger.business.service.EventDataService;
import com.tuziilm.web.common.CsvReader;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.RemarkForm;
import com.tuziilm.web.exception.UploadException;
import com.tuziilm.web.mvc.CRUDController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/eventData")
public class EventDataController extends CRUDController<EventData, EventDataService, EventDataController.Form, EventDataController.Query> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public EventDataController(){
		super("eventData");
    }

    @Resource
    public void setEventDataService(EventDataService eventDataService){
        this.service=eventDataService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }
    @Override
    protected void postList(int page, Paginator paginator, Query query, Model model) {
        List<String> products = service.getProducts();
        List<String> channels = service.getChannels();
        List<String> events = service.getEvents();
        List<String> parameters = service.getParams();
        model.addAttribute("products", products);
        model.addAttribute("channels", channels);
        model.addAttribute("events", events);
        model.addAttribute("parameters", parameters);
        super.postList(page, paginator, query, model);
    }
    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {

        List<EventData> eventDatas = readMulitFile(form,errors);

        try{
            service.insertAllEventData(eventDatas);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "导入过今日该渠道数据"));
            model.addAttribute("errors", errors);
        }
    }
    //读取多个文件
    public List<EventData> readMulitFile(Form form, BindingResult errors) {
        List<EventData> eventDatas = new ArrayList<>();
        try {
            if (form.getFile().isEmpty() || form.getFile().size() < 0) {
                throw new UploadException("上传文件不能为空!");
            } else {
                for (MultipartFile file : form.getFile()) {
                    String fileName = file.getOriginalFilename();
                    if(fileName.indexOf("事件")>=0 && fileName.indexOf("参数")>=0 && fileName.indexOf("消息数量")>=0) {
                        eventDatas = readFile(file, eventDatas);
                    }else{
                        throw new UploadException("上传文件名称不符合规则!");
                    }
                }
            }
        } catch (UploadException e) {
            log.error("failure to upload", e);
            errors.addError(new ObjectError("upload", e.getMessage()));
        }
        return eventDatas;
    }

    public List<EventData> readFile(MultipartFile file,List<EventData> eventDatas) {
        CsvReader csvReader = CsvReader.getInstance();
        List<String> result = csvReader.readCsv(file);
        for (String cell : result) {
            EventData eventData = new EventData();
            String fileName = file.getOriginalFilename();
            String[] infos = fileName.split("_");
            eventData.setProduct(infos[0]);
            Pattern pattern = Pattern.compile("[\\(\\)]");
            String[] strs = pattern.split(fileName);
            eventData.setEvent(strs[1].trim());
            eventData.setParam(strs[3].trim());
            String[] info = cell.split("\\t");
            eventData.setMessageCount(Integer.valueOf(info[2].trim()));
            eventData.setChannel(info[1]);
            eventData.setDate(info[0]);
            eventDatas.add(eventData);
        }
        return eventDatas;
    }

    public static class Form extends RemarkForm<EventData> {
        private List<MultipartFile> file;

        @Override
        public EventData newObj() {
            return new EventData();
        }

        @Override
        public void populateObj(EventData eventData) {

        }

        public List<MultipartFile> getFile() {
            return file;
        }

        public void setFile(List<MultipartFile> file) {
            this.file = file;
        }
    }
    public static class Query extends com.tuziilm.web.common.Query {
        protected String product;
        protected String channel;
        protected String event;
        protected String param;
        protected String startTime;
        protected String endTime;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
            this.addItem("product", product);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
            this.addItem("channel",channel);
        }

        public String getEvent() {
            return event;
        }

        public void setEvent(String event) {
            this.event = event;
            this.addItem("event",event);
        }

        public String getParam() {
            return param;
        }

        public void setParam(String param) {
            this.param = param;
            this.addItem("param",param);
        }
    }

}
