package com.scavenger.business.mvc;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.common.DateUtil;
import com.scavenger.business.common.LoginContext;
import com.scavenger.business.common.PageHolder;
import com.scavenger.business.common.ReadUtil;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.domain.Channel;
import com.scavenger.business.model.AdChannel;
import com.scavenger.business.model.Channelstatistics;
import com.scavenger.business.model.Offer;
import com.scavenger.business.model.OfferHistory;
import com.scavenger.business.model.ReadIncome;
import com.scavenger.business.model.Readnumstatistics;
import com.scavenger.business.mvc.OfferController.Query;
import com.scavenger.business.service.ChannelService;
import com.tuziilm.web.common.Query.Item;
import com.tuziilm.web.mvc.annotation.Ids;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/readnumstatistics")
public class ReadNumStatisticController {
	private final Logger log= LoggerFactory.getLogger(getClass());

	
	@Resource
	private ChannelService channelService;
	@Autowired
	private CommonService commonService;
	
	public static class Query  {
		private String createDate;

		public String getCreateDate() {
			return createDate;
		}

		public void setCreateDate(String createDate) {
			this.createDate = createDate;
		}
		
	}
	@RequestMapping("/list")
	public String list(Model model,Query sear){
		Map<String, Object> fields =  new HashMap<String, Object>();
		if(!StringUtils.isEmpty(sear.getCreateDate())){
			fields.put("createDate", sear.getCreateDate());
		}else{
			Calendar ca = Calendar.getInstance();
			ca.add(ca.DAY_OF_MONTH, -1);
			fields.put("createDate", DateUtil.formatDateYMD(ca.getTime()));
		}
		
		List<Readnumstatistics> list = commonService.listByFields(fields, Readnumstatistics.class," order by readrc ");
		fields.put("channelId", "tiger");
		List<Channelstatistics> cl = commonService.listByFields(fields,Channelstatistics.class);
		if(!cl.isEmpty()){
			model.addAttribute("active", cl.get(0).getActive());
		}
		model.addAttribute("datas", list);
		return "/readnumstatistics/list";
	}
	
	
	@RequestMapping("/create")
	public String create(Model model){
        model.addAttribute("channels", channelService.getAllChannelCache());
        return "/readnumstatistics/create";
	}
	
	public static void main(String[] args) {
		System.out.println("=="+" dddd dd ".trim()+"==");
	}
	
	protected void outputJosn(final HttpServletResponse response, final String msg){
		try {
			response.setContentType("text/html");
			response.setCharacterEncoding("utf-8");
			response.setHeader("Pragma", "No-cache");
			response.setDateHeader("Expires", 0);
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(msg);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@RequestMapping(value="/check-url")
	public void checkUrl(String url, HttpServletRequest request, HttpServletResponse response){
		if(StringUtils.isEmpty(url)){
			outputJosn(response, "url不能为空");
			return;
		}
		
		String lurl = ReadUtil.getShotUrl(url);
		if(StringUtils.isEmpty(lurl)){
			outputJosn(response, "链接异常，请检查链接！");
			return;
		}
		outputJosn(response,"success");
	}
	
	
	
	

	

}
