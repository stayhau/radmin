package com.scavenger.business.mvc;

import com.scavenger.business.domain.ChannelConversion;
import com.scavenger.business.service.ChannelConversionService;
import com.scavenger.business.service.PriceAreaService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.common.RemarkStatusForm;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Controller
@RequestMapping("/conversion")
public class ChannelConversionController extends CRUDController<ChannelConversion, ChannelConversionService, ChannelConversionController.Form, Query.NameQuery> {
	private final Logger log= LoggerFactory.getLogger(getClass());
    @Resource
    private PriceAreaService priceAreaService;

    public ChannelConversionController(){
		super("conversion");
	}

    @Resource
	public void setChannelConversionService(ChannelConversionService channelConversionService){
		this.service=channelConversionService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }

    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        ChannelConversion conversion=form.toObj();
        try{
            service.saveOrUpdate(conversion);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "类型已经存在！"));
            model.addAttribute("errors", errors);
        }

	}

    @Override
    protected void postCreate(Model model) {
        model.addAttribute("priceAreas", priceAreaService.getAllPriceAreaName());
    }

    @Override
    protected void postModify(int id, ChannelConversion obj, Model model) {
        postCreate(model);
    }

    @Override
    protected void onSaveError(Form form, BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response) {
        postCreate(model);
        form.setConversion((int)(form.getConversion() * 1000));
        form.setHighConversion((int)(form.getHighConversion() * 1000));
        form.setMiddleConversion((int)(form.getMiddleConversion() * 1000));
        form.setNormalConversion((int)(form.getNormalConversion() * 1000));
        model.addAttribute("form", form);
    }

    public static class Form extends RemarkStatusForm<ChannelConversion> {
        @Pattern(regexp = "[^0]*",message = "请选择价格区域配置")
        private String priceAreaName;
        @NotBlank(message = "渠道不能为空")
        private String channel;
        @NotNull(message = "转化率不能为空")
        private double conversion;
        @NotNull(message = "高价转化率不能为空")
        private double highConversion;
        @NotNull(message = "中价转化率不能为空")
        private double middleConversion;
        @NotNull(message = "低价转化率不能为空")
        private double normalConversion;
        @NotBlank(message = "分成方式")
        private String dividedWay;

        @Override
        public ChannelConversion newObj() {
            return new ChannelConversion();
        }

        @Override
        public void populateObj(ChannelConversion channelConversion) {
            channelConversion.setPriceAreaName(priceAreaName);
            channelConversion.setChannel(channel);
            channelConversion.setConversion((int) (conversion * 1000));
            channelConversion.setHighConversion((int) (highConversion * 1000));
            channelConversion.setMiddleConversion((int) (middleConversion * 1000));
            channelConversion.setNormalConversion((int) (normalConversion * 1000));
            channelConversion.setDividedWay(dividedWay);
        }

        public double getConversion() {
            return conversion;
        }

        public void setConversion(double conversion) {
            this.conversion = conversion;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public double getHighConversion() {
            return highConversion;
        }

        public void setHighConversion(double highConversion) {
            this.highConversion = highConversion;
        }

        public double getMiddleConversion() {
            return middleConversion;
        }

        public void setMiddleConversion(double middleConversion) {
            this.middleConversion = middleConversion;
        }

        public double getNormalConversion() {
            return normalConversion;
        }

        public void setNormalConversion(double normalConversion) {
            this.normalConversion = normalConversion;
        }

        public String getPriceAreaName() {
            return priceAreaName;
        }

        public void setPriceAreaName(String priceAreaName) {
            this.priceAreaName = priceAreaName;
        }

        public String getDividedWay() {
            return dividedWay;
        }

        public void setDividedWay(String dividedWay) {
            this.dividedWay = dividedWay;
        }
    }

}
