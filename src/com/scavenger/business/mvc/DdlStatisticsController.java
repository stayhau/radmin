package com.scavenger.business.mvc;

import com.scavenger.business.domain.DdlStatistics;
import com.scavenger.business.service.DdlStatisticsService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.mvc.ListController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/** 
 * Author: <a href="tuziilm@163.com">Tuziilm</a>
 * Date: 2016/11/5 17:54
 */
@Controller
@RequestMapping(value="/gpData/ddl_statistics")
public class DdlStatisticsController extends ListController<DdlStatistics,DdlStatisticsService,DdlStatisticsController.Query> {
    private final Logger log = LoggerFactory.getLogger(getClass());
    public DdlStatisticsController() {
        super("gpData/ddl_statistics");
    }

    @Resource
    public void setDdlStatisticsService(DdlStatisticsService ddlStatisticsService){
        this.service=ddlStatisticsService;
    }

    @Override
    protected boolean preList(int page, Paginator paginator, Query query, Model model) {
        paginator.setNeedTotal(true);
        DdlStatistics ddlStatistics = service.getSum(paginator);
        model.addAttribute("sum", ddlStatistics);
        return super.preList(page, paginator, query, model);
    }
    @RequestMapping("/export")
    private void exportSalesDataXls(Query query,HttpServletResponse response){
        response.setContentType("text/csv");
        response.setCharacterEncoding("GBK");
        response.setHeader("Content-Disposition","attachment;filename=\"ddl_statistics.csv\"");
        Paginator paginator = new Paginator(1, 300000);
        paginator.setQuery(query);
        List<DdlStatistics> list = service.list(paginator);

        try {
            response.getWriter().write("日期,请求数,安装数\n");
            for(DdlStatistics ta : list ){
                response.getWriter()
                        .append(String.valueOf(ta.getDate())).append(",")
                        .append(String.valueOf(ta.getRequest())).append(",")
                        .append(String.valueOf(ta.getInstall())).append("\n");
            }
        } catch (IOException e) {
           log.error("export failure",e);
        }
    }


    public static class Query extends com.tuziilm.web.common.Query {
        protected String startTime;
        protected String endTime;

        public Query() {
            this.startTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
            this.endTime = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime.replaceAll("/", "-");
            this.addItem("startTime", startTime);
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime.replaceAll("/", "-");
            this.addItem("endTime", endTime);
        }

    }

}
