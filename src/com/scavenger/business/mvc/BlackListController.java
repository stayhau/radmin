package com.scavenger.business.mvc;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.scavenger.business.common.service.CommonService;
import com.scavenger.business.domain.Salesman;
import com.scavenger.business.model.policy.BlackList;
import com.scavenger.business.model.policy.Policy;
import com.scavenger.business.mvc.SalesmanController.Form;
import com.scavenger.business.service.ChannelService;
import com.scavenger.business.service.SalesmanService;
import com.tuziilm.web.common.Country;
import com.tuziilm.web.common.IdForm;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.mvc.CRUDController;
import com.tuziilm.web.mvc.annotation.Ids;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/blacklist")
public class BlackListController {
	private final Logger log= LoggerFactory.getLogger(getClass());
	@Autowired
	private CommonService commonService;
	
	@RequestMapping("/get-black-list")
	public String getBlackList(Model model){
		Map<String, Object> fields =  new HashMap<String, Object>();
		fields.put("1", 1);
		 List<BlackList> b = commonService.listByFields(fields, BlackList.class);
		 if(b!=null &&!b.isEmpty()){
			 model.addAttribute("datas", b.get(0));
		 }
		return "/blacklist/blacklist";
	}
	
	@RequestMapping("/save-blacklist")
	public String saveBlackList(@Valid Form form,BindingResult errors, Model model, HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> conditions =  new HashMap<String, Object>();
		if(form.getId()!=null&&form.getId()!=0){
			conditions.put("id", form.getId());
			//Policy old = commonService.getByFields(conditions, Policy.class);
			BlackList b = new BlackList();
			b.setId(form.getId());
			b.setPkgName(form.getPkgName());
			b.setUpdateTime(new Date());
			commonService.update(b, conditions);
		}
		return getBlackList(model);
	}
	

	

	
	public static class Form  {
		@NotEmpty(message = "包名不能为空")
        private String pkgName;
        private Date updateTime;
        private Integer id;
        public Policy newObj() {
            return new Policy();
        }

        public void populateObj(BlackList policy) {
        	
        	policy.setUpdateTime(new Date());
        	policy.setPkgName(pkgName);
        	if(id!=null){
        		policy.setId(id);
        	}
        }

		public String getPkgName() {
			return pkgName;
		}

		public void setPkgName(String pkgName) {
			this.pkgName = pkgName;
		}

		public Date getUpdateTime() {
			return updateTime;
		}

		public void setUpdateTime(Date updateTime) {
			this.updateTime = updateTime;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

       


        
    }

}
