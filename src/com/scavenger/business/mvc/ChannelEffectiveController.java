package com.scavenger.business.mvc;

import com.scavenger.business.domain.ChannelEffective;
import com.scavenger.business.service.ChannelEffectiveService;
import com.tuziilm.web.common.Paginator;
import com.tuziilm.web.common.Query;
import com.tuziilm.web.common.RemarkStatusForm;
import com.tuziilm.web.mvc.CRUDController;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/effective")
public class ChannelEffectiveController extends CRUDController<ChannelEffective, ChannelEffectiveService, ChannelEffectiveController.Form, Query.NameQuery> {
	private final Logger log= LoggerFactory.getLogger(getClass());

    public ChannelEffectiveController(){
		super("effective");
	}

    @Resource
	public void setChannelEffectiveService(ChannelEffectiveService channelEffectiveService){
		this.service=channelEffectiveService;
	}

    @Override
    protected boolean preList(int page, Paginator paginator, Query.NameQuery query, Model model) {
        paginator.setNeedTotal(true);
        return super.preList(page, paginator, query, model);
    }

    @Override
	public void innerSave(Form form, BindingResult errors, Model model,
			HttpServletRequest request, HttpServletResponse response) {
        ChannelEffective effective=form.toObj();
        try{
            service.saveOrUpdate(effective);
        }catch(DuplicateKeyException e){
            errors.addError(new ObjectError("database", "类型已经存在！"));
            model.addAttribute("errors", errors);
        }
	}

	public static class Form extends RemarkStatusForm<ChannelEffective> {
        @NotBlank(message = "渠道不能为空")
        private String channel;
        @NotNull(message = "时间不能为空")
        private String date;

        @Override
        public ChannelEffective newObj() {
            return new ChannelEffective();
        }

        @Override
        public void populateObj(ChannelEffective channelEffective) {
            channelEffective.setChannel(channel);
            channelEffective.setDate(date.replaceAll("/", ""));
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

}
