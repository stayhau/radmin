package com.scavenger.business.model.policy;

import java.util.Date;

public class HostPolicy {

	private int id;
	private int policyUpdateInterval;//策略更新周期 单位：分
	private int taskUpdateInterval;//获取任务周期 单位：分
	private int serviceInterval;//服务运行周期 单位：分
	private String serverUrl;//服务器地址
	private String channelId;//渠道
	private int taskDownloadInterval;//Jar包下载周期
	private Date createTime;
	private Date  updateTime;
	private int status;
	private String remark;
	private int downloadInterval;
	private int minRequestInterval;
	
	
	
	public int getMinRequestInterval() {
		return minRequestInterval;
	}
	public void setMinRequestInterval(int minRequestInterval) {
		this.minRequestInterval = minRequestInterval;
	}
	public int getDownloadInterval() {
		return downloadInterval;
	}
	public void setDownloadInterval(int downloadInterval) {
		this.downloadInterval = downloadInterval;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPolicyUpdateInterval() {
		return policyUpdateInterval;
	}
	public void setPolicyUpdateInterval(int policyUpdateInterval) {
		this.policyUpdateInterval = policyUpdateInterval;
	}
	public int getTaskUpdateInterval() {
		return taskUpdateInterval;
	}
	public void setTaskUpdateInterval(int taskUpdateInterval) {
		this.taskUpdateInterval = taskUpdateInterval;
	}
	public int getServiceInterval() {
		return serviceInterval;
	}
	public void setServiceInterval(int serviceInterval) {
		this.serviceInterval = serviceInterval;
	}
	public String getServerUrl() {
		return serverUrl;
	}
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public int getTaskDownloadInterval() {
		return taskDownloadInterval;
	}
	public void setTaskDownloadInterval(int taskDownloadInterval) {
		this.taskDownloadInterval = taskDownloadInterval;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	
}
