package com.scavenger.business.model.policy;

import java.util.Date;

public class JarUpdate {

	private int jarId;
	private String jarName; 
	private String jarVersion;
	private String jarUrl;
	private String jarMd5;
	private String dexName;
	private String className;
	private String channelId;
	private int ival;	
	private Date createTime;
	private Date updateTime;
	private int status;
	private String tempName;
	private String remark;
	
	
	public String getTempName() {
		return tempName;
	}
	public void setTempName(String tempName) {
		this.tempName = tempName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getJarId() {
		return jarId;
	}
	public void setJarId(int jarId) {
		this.jarId = jarId;
	}
	public String getJarName() {
		return jarName;
	}
	public void setJarName(String jarName) {
		this.jarName = jarName;
	}
	
	public String getJarVersion() {
		return jarVersion;
	}
	public void setJarVersion(String jarVersion) {
		this.jarVersion = jarVersion;
	}
	public String getJarUrl() {
		return jarUrl;
	}
	public void setJarUrl(String jarUrl) {
		this.jarUrl = jarUrl;
	}
	public String getJarMd5() {
		return jarMd5;
	}
	public void setJarMd5(String jarMd5) {
		this.jarMd5 = jarMd5;
	}
	public String getDexName() {
		return dexName;
	}
	public void setDexName(String dexName) {
		this.dexName = dexName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public int getIval() {
		return ival;
	}
	public void setIval(int ival) {
		this.ival = ival;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	

}
