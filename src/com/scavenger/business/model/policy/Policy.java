package com.scavenger.business.model.policy;

import java.io.Serializable;
import java.util.Date;

public class Policy  implements Serializable {

	 private int  id;
	 private int  workInterval;
	 private int  workCount;
	 private int  readCount;
	 private String	workCondition;
	 private int  configUpdateInterval;
	 private int  status;
	 private String  channelId;
	 private Date createTime;
	 private String remark;
	 private Integer startTime;
     private Integer endTime;
     private Integer delayTime;
	private Integer dtStartHour;
	private Integer dtEndHour;
	private Integer dtMaxCount;

	public Integer getDtStartHour() {
		return dtStartHour;
	}

	public void setDtStartHour(Integer dtStartHour) {
		this.dtStartHour = dtStartHour;
	}

	public Integer getDtEndHour() {
		return dtEndHour;
	}

	public void setDtEndHour(Integer dtEndHour) {
		this.dtEndHour = dtEndHour;
	}

	public Integer getDtMaxCount() {
		return dtMaxCount;
	}

	public void setDtMaxCount(Integer dtMaxCount) {
		this.dtMaxCount = dtMaxCount;
	}

	public Integer getStartTime() {
		return startTime;
	}
	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}
	public Integer getEndTime() {
		return endTime;
	}
	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}
	public Integer getDelayTime() {
		return delayTime;
	}
	public void setDelayTime(Integer delayTime) {
		this.delayTime = delayTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getWorkInterval() {
		return workInterval;
	}
	public void setWorkInterval(int workInterval) {
		this.workInterval = workInterval;
	}
	public int getWorkCount() {
		return workCount;
	}
	public void setWorkCount(int workCount) {
		this.workCount = workCount;
	}
	public int getReadCount() {
		return readCount;
	}
	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}
	public String getWorkCondition() {
		return workCondition;
	}
	public void setWorkCondition(String workCondition) {
		this.workCondition = workCondition;
	}
	public int getConfigUpdateInterval() {
		return configUpdateInterval;
	}
	public void setConfigUpdateInterval(int configUpdateInterval) {
		this.configUpdateInterval = configUpdateInterval;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	 
	 
}
