package com.scavenger.business.model.vido;

import java.util.Date;

public class Policy {

	
	private int  id;
	private  int  workInterval;
	private  int workCount;
	private  int readCount;
	private String  workCondition;
	private int configUpdateInterval;
	private int status;
	private String  channelId ;
	private Date createTime;
	private String remark;
	private int startTime;
	private int  endTime;
	private int battery;
	private int delayTime;
	private int readTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getWorkInterval() {
		return workInterval;
	}
	public void setWorkInterval(int workInterval) {
		this.workInterval = workInterval;
	}
	public int getWorkCount() {
		return workCount;
	}
	public void setWorkCount(int workCount) {
		this.workCount = workCount;
	}
	public int getReadCount() {
		return readCount;
	}
	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}
	public String getWorkCondition() {
		return workCondition;
	}
	public void setWorkCondition(String workCondition) {
		this.workCondition = workCondition;
	}
	public int getConfigUpdateInterval() {
		return configUpdateInterval;
	}
	public void setConfigUpdateInterval(int configUpdateInterval) {
		this.configUpdateInterval = configUpdateInterval;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getStartTime() {
		return startTime;
	}
	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}
	public int getEndTime() {
		return endTime;
	}
	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}
	public int getBattery() {
		return battery;
	}
	public void setBattery(int battery) {
		this.battery = battery;
	}
	public int getDelayTime() {
		return delayTime;
	}
	public void setDelayTime(int delayTime) {
		this.delayTime = delayTime;
	}
	public int getReadTime() {
		return readTime;
	}
	public void setReadTime(int readTime) {
		this.readTime = readTime;
	}
	
	
}
