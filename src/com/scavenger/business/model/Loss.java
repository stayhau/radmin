package com.scavenger.business.model;

public class Loss {

	private double loss;
	private int sucLoss;
	private int auto;
	private double floatValue;
	
	
	public double getLoss() {
		return loss;
	}
	public void setLoss(double loss) {
		this.loss = loss;
	}
	public int getSucLoss() {
		return sucLoss;
	}
	public void setSucLoss(int sucLoss) {
		this.sucLoss = sucLoss;
	}
	public int getAuto() {
		return auto;
	}
	public void setAuto(int auto) {
		this.auto = auto;
	}
	public double getFloatValue() {
		return floatValue;
	}
	public void setFloatValue(double floatValue) {
		this.floatValue = floatValue;
	}
	
	
}
