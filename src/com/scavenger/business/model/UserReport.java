package com.scavenger.business.model;

import com.tuziilm.web.domain.RemarkStatusId;

/**
 * @author Lorry
 * @since 2019-09-21 11:14
 **/
public class UserReport extends RemarkStatusId {
    private String date;
    private String channel;
    private String salesman;
    private Integer register;
    private Integer nRegister;
    private Integer active;
    private Integer DAU;
    private Integer bizRegister;
    private Integer bizDAU;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public Integer getRegister() {
        return register;
    }

    public void setRegister(Integer register) {
        this.register = register;
    }

    public Integer getnRegister() {
        return nRegister;
    }

    public void setnRegister(Integer nRegister) {
        this.nRegister = nRegister;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getDAU() {
        return DAU;
    }

    public void setDAU(Integer DAU) {
        this.DAU = DAU;
    }

    public Integer getBizRegister() {
        return bizRegister;
    }

    public void setBizRegister(Integer bizRegister) {
        this.bizRegister = bizRegister;
    }

    public Integer getBizDAU() {
        return bizDAU;
    }

    public void setBizDAU(Integer bizDAU) {
        this.bizDAU = bizDAU;
    }
}
