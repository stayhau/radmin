package com.scavenger.business.model;

import java.util.Date;

public class Offer {
    private int offerId;
    private String url;
    private double payout;
    private double payout2;
    private int readNum;
    private int readedNum;
    private int great;
    private int reply;
    private String auther;
    private int status;
    private Date createTime;
    private Date updateTime;
    private String remark;
    private String startTime;
    private Date successTime;
    private String endTime;
    private int weight;
    private String adChannel;
    private String lurl;
    private int pulls;
    private int reportCnt;
    private int fadeViewCount;
    private double conversionRate;

    public int getFadeViewCount() {
        return fadeViewCount;
    }

    public void setFadeViewCount(int fadeViewCount) {
        this.fadeViewCount = fadeViewCount;
    }

    public double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(double conversionRate) {
        this.conversionRate = conversionRate;
    }

    public int getReportCnt() {
        return reportCnt;
    }

    public void setReportCnt(int reportCnt) {
        this.reportCnt = reportCnt;
    }

    public Date getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(Date successTime) {
        this.successTime = successTime;
    }

    public int getPulls() {
        return pulls;
    }

    public void setPulls(int pulls) {
        this.pulls = pulls;
    }

    public String getLurl() {
        return lurl;
    }

    public void setLurl(String lurl) {
        this.lurl = lurl;
    }

    public String getAdChannel() {
        return adChannel;
    }

    public void setAdChannel(String adChannel) {
        this.adChannel = adChannel;
    }

    public int getReadedNum() {
        return readedNum;
    }

    public void setReadedNum(int readedNum) {
        this.readedNum = readedNum;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getPayout() {
        return payout;
    }

    public void setPayout(double payout) {
        this.payout = payout;
    }

    public int getReadNum() {
        return readNum;
    }

    public void setReadNum(int readNum) {
        this.readNum = readNum;
    }

    public int getGreat() {
        return great;
    }

    public void setGreat(int great) {
        this.great = great;
    }

    public int getReply() {
        return reply;
    }

    public void setReply(int reply) {
        this.reply = reply;
    }

    public String getAuther() {
        return auther;
    }

    public void setAuther(String auther) {
        this.auther = auther;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public double getPayout2() {
        return payout2;
    }

    public void setPayout2(double payout2) {
        this.payout2 = payout2;
    }
}
