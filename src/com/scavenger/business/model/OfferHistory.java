package com.scavenger.business.model;

import java.util.Date;

public class OfferHistory extends Offer {

	private	int	offerId;
 	private	String	url;
 	private	double	payout;
 	private	int	readNum;
 	private	int	readedNum;
 	private	int	great;
 	private	int	reply;
	private	String	auther;
	private	int	status;
	private	Date	createTime;
	private	Date	updateTime;
	private	String	remark;
	private	String	startTime;
	private	String	endTime;
	private int weight;
	private String actionType;
	private Date actionTime;
	public OfferHistory(){}
	
	public OfferHistory(Offer offer){
		this.offerId=offer.getOfferId();
		this.url=offer.getUrl();
		this.payout=offer.getPayout();
		this.readNum=offer.getReadNum();
		this.readedNum=offer.getReadedNum();
		this.great=offer.getGreat();
		this.reply=offer.getReply();
		this.auther=offer.getAuther();
		this.status=offer.getStatus();
		this.createTime=offer.getCreateTime();
		this.updateTime=offer.getUpdateTime();
		this.remark=offer.getRemark();
		this.startTime=offer.getStartTime();
		this.endTime=offer.getEndTime();
		this.weight=offer.getWeight();
	}
	
	
	
	public Date getActionTime() {
		return actionTime;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public int getReadedNum() {
		return readedNum;
	}
	public void setReadedNum(int readedNum) {
		this.readedNum = readedNum;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getOfferId() {
		return offerId;
	}
	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public double getPayout() {
		return payout;
	}

	public void setPayout(double payout) {
		this.payout = payout;
	}

	public int getReadNum() {
		return readNum;
	}
	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}
	public int getGreat() {
		return great;
	}
	public void setGreat(int great) {
		this.great = great;
	}
	public int getReply() {
		return reply;
	}
	public void setReply(int reply) {
		this.reply = reply;
	}
	public String getAuther() {
		return auther;
	}
	public void setAuther(String auther) {
		this.auther = auther;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}
