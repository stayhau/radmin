package com.scavenger.business.model;

import java.util.Date;

public class Readnumstatistics {

	 private int id;
	 private int readNum ;
	 private int readrc;
	 private String createDate;
	 private Date createTime;
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getReadNum() {
		return readNum;
	}
	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}
	public int getReadrc() {
		return readrc;
	}
	public void setReadrc(int readrc) {
		this.readrc = readrc;
	}
	
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	 
	 
	
}
