package com.scavenger.business.model;

import java.util.Date;

public class ReadIncome {

	private int id;
	private int readNum;
	private int readNum2;
	private int income;
	private int income2;
	private String createDate;
	private double payout;
	private double payout2;

	public int getReadNum2() {
		return readNum2;
	}

	public void setReadNum2(int readNum2) {
		this.readNum2 = readNum2;
	}

	public int getIncome2() {
		return income2;
	}

	public void setIncome2(int income2) {
		this.income2 = income2;
	}

	public double getPayout2() {
		return payout2;
	}

	public void setPayout2(double payout2) {
		this.payout2 = payout2;
	}

	public double getPayout() {
		return payout;
	}

	public void setPayout(double payout) {
		this.payout = payout;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getReadNum() {
		return readNum;
	}
	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}
	public int getIncome() {
		return income;
	}
	public void setIncome(int income) {
		this.income = income;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
}
