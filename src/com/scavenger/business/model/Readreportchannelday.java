package com.scavenger.business.model;

import java.util.Date;

public class Readreportchannelday {
	private int  offerId;
	private String channelId;
	private int readNum	;
	private int greatNum;
	private int replayNum;
	private Date createDate;
	private Date createTime;
	private Date updateTime;
	public int getOfferId() {
		return offerId;
	}
	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public int getReadNum() {
		return readNum;
	}
	public void setReadNum(int readNum) {
		this.readNum = readNum;
	}
	public int getGreatNum() {
		return greatNum;
	}
	public void setGreatNum(int greatNum) {
		this.greatNum = greatNum;
	}
	public int getReplayNum() {
		return replayNum;
	}
	public void setReplayNum(int replayNum) {
		this.replayNum = replayNum;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	
}
