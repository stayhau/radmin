<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="损耗管理" scope="request"/>
<c:set var="_lossManager" value="active" scope="request"/>
<c:set var="_policyManager" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
<div class="page-header"><h1>损耗管理</h1></div>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${empty datas}">
						<tr>
							<td>没有任务记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>下发倍数</th>
							<th>浮动值</th>
							<th>自动/手动</th>
							<th>操作</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td><fmt:formatNumber value="${data.loss}" pattern="#0.00"/> </td>
								<td>${fn:escapeXml(data.floatValue)}</td>
								<td >${data.auto==1?"自动":"手动"}</td>
								<td class="operation operand1">
								<input type="button" class="btn btn-primary" onclick="javascript:doWithSingleItem('/staticloss/modify')" name="_action_modify" value="修改">
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
	<form id="toolbar_form" method="post" style="display: none">
	</form>
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>

$( "#startTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
	},dateFormat: "yy-mm-dd"});
$( "#endTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
	},dateFormat: "yy-mm-dd"} );
</script>
<script>
var toolbar_form=document.getElementById("toolbar_form");
function doWithSingleItem(path){
    toolbar_form.action=path;
    toolbar_form.submit();
}

	$(function() {
		$("[name='my-checkbox']").on('switchChange.bootstrapSwitch', function(event, state) {
			alert("dd");
			var id = this.getAttribute("id");
			var path = "${basePath}wgz/changeStatus";
			changeStatusWithItemAjax(id, path, "确认修改？",state);
		});
	});
</script>