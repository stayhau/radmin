<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建转化率" scope="request"/>
<c:set var="_underConversion" value="active" scope="request"/>
<c:set var="_basicInfo" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
		<div class="page-header"><h1>创建/修改转化率</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}conversion/save" method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${form.id}">
				<input name="_queryString" type="hidden" value="${param.queryString}">

				<div class="control-group required-field">
					<label class="control-label">渠道:</label>
					<div class="controls">
						<input name="channel" value="${fn:escapeXml(form.channel)}" type="text" class="input-large">
						<span class="remark"></span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">推送方式:</label>
					<div class="controls">
						<select id="sel_typeName" name="priceAreaName" class="input-small">
							<option value="0">请选择</option>
							<c:forEach items="${priceAreas}" var="priceArea">
								<option value="${priceArea}">${priceArea}</option>
							</c:forEach>
						</select>
						<script>document.getElementById("sel_typeName").value="${empty form.priceAreaName?0:form.priceAreaName}"</script>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">分成方式:</label>
					<div class="controls">
						<input name="dividedWay" value="${fn:escapeXml(empty form.dividedWay?"cps":form.dividedWay)}" type="text" class="input-large">
						<span class="remark"></span>
					</div>
				</div>
                <div class="control-group required-field">
                    <label class="control-label">转化率:</label>
                    <div class="controls">
                        <input name="conversion" value="${fn:escapeXml(form.conversion/1000)}" type="text" class="input-large">
                        <span class="remark">填小数值，如转化率25%，则填0.25</span>
                    </div>
                </div>
				<div class="control-group required-field">
					<label class="control-label">高价转化率:</label>
					<div class="controls">
						<input name="highConversion" value="${fn:escapeXml(form.highConversion/1000)}" type="text" class="input-large">
						<span class="remark">填小数值，如转化率25%，则填0.25</span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">中价转化率:</label>
					<div class="controls">
						<input name="middleConversion" value="${fn:escapeXml(form.middleConversion/1000)}" type="text" class="input-large">
						<span class="remark">填小数值，如转化率25%，则填0.25</span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">低价转化率:</label>
					<div class="controls">
						<input name="normalConversion" value="${fn:escapeXml(form.normalConversion/1000)}" type="text" class="input-large">
						<span class="remark">填小数值，如转化率25%，则填0.25</span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">是有有效:</label>
					<div class="controls">
						<select id="sel_type" name="status" class="input-small">
							<option value="1">是</option>
							<option value="0">否</option>
						</select>
						<script>document.getElementById("sel_type").value="${empty form.status?'1':form.status}"</script>
					</div>
				</div>
				<div class="control-group required-field">
				  <label class="control-label">备注:</label>
				  <div class="controls">
				    <input name="remark" value="${fn:escapeXml(form.remark)}" type="text" class="input-large">
				  </div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>

