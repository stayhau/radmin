<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建转化率" scope="request"/>
<c:set var="_underConversion" value="active" scope="request"/>
<c:set var="_basicInfo" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
<div class="page-header"><h1>创建/修改转化率</h1></div>

        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有任务记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th></th>
							<th>编号</th>
							<th>渠道</th>
							<th>推送方式</th>
							<th>分成方式</th>
							<th>转化率</th>
							<th>高价转化率</th>
							<th>中价转化率</th>
							<th>低价转化率</th>
							<th>操作</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td class="checkbox_td">
									<input type="checkbox" name="ids" value="${data.id}"/>
								</td>
								<td>${fn:escapeXml(data.id)}</td>
                                <td>${fn:escapeXml(data.channel)}</td>
								<td>${fn:escapeXml(data.priceAreaName)}</td>
								<td>${fn:escapeXml(data.dividedWay)}</td>
								<td>${fn:escapeXml(data.conversion/10)}%</td>
                                <td>${fn:escapeXml(data.highConversion/10)}%</td>
                                <td>${fn:escapeXml(data.middleConversion/10)}%</td>
                                <td>${fn:escapeXml(data.normalConversion/10)}%</td>
								<td class="operation operand1">
									<a class="btn btn-small btn-info" onclick="javascript:showDetail(${it.count},this);return false;"> 详情 </a>
								</td>
							</tr>
							<tr id="detail_${it.count }" style="display: none">
								<td></td>
								<td colspan="5">
									<ul>
                                        <li><strong>创建时间：</strong><fmt:formatDate value="${data.gmtCreate}" pattern="yyyy/MM/dd HH:mm:ss" var="gmtCreate"/>${gmtCreate}</li>
                                        <li><strong>更新时间：</strong><fmt:formatDate value="${data.gmtModified}" pattern="yyyy/MM/dd HH:mm:ss" var="gmtModified"/>${gmtModified}</li>
										<li><strong>备注：</strong>${fn:escapeXml(data.remark )}</li>
									</ul>
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<c:import url="../theme/${_theme}/toolbar.jsp">
					<c:param name="create">${basePath}conversion/create</c:param>
                    <c:param name="delete">${basePath}conversion/delete</c:param>
					<c:param name="modify">${basePath}conversion/modify</c:param>
				</c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>