<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="注册统计" scope="request"/>
<c:set var="_underSdkChannel" value="active" scope="request"/>
<c:set var="_newStatistics" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>注册统计</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}sdkChannel/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>日期</th>
							<th>渠道</th>
							<th>总注册量</th>
							<th>高价</th>
							<th>中价</th>
							<th>低价</th>
							<th>平板活跃</th>
							<th>手机活跃</th>
							<th>平板新增</th>
							<th>手机新增</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.channel)}</td>
								<td>${fn:escapeXml(data.totalRegister)}</td>
								<td>${fn:escapeXml(data.highPrice)}</td>
								<td>${fn:escapeXml(data.middlePrice)}</td>
								<td>${fn:escapeXml(data.normalPrice)}</td>
								<td>${fn:escapeXml(data.tabletActive)}</td>
								<td>${fn:escapeXml(data.phoneActive)}</td>
								<td>${fn:escapeXml(data.tabletAdd)}</td>
								<td>${fn:escapeXml(data.phoneAdd)}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">

			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}sdkChannel/list";
		document.getElementById("search_form").submit();
	}
</script>