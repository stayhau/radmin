<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="注册激活统计" scope="request"/>
<c:set var="_unUserReport" value="active" scope="request"/>
<c:set var="_report" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"/>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<script type="text/javascript" src="${basePath}static/theme/${_theme}/global.js"></script>
<!-- main content -->
<div class="page-header"><h1>注册激活统计</h1></div>
<%--<c:import url="../theme/default/name_search.jsp">
	<c:param name="action" value="${basePath}user-report/list"/>
</c:import>--%>
<div id="search">
	<form id="search_form" action="${basePath}user-report/list" method="post" class="form-inline pull-left">
		<label>开始时间:</label><input type="text" name="startDate" id="startDate" value="${sear.startDate}" >
		<label>结束时间:</label><input type="text" name="endDate" id="endDate" value="${sear.endDate}" >
		<input type="submit" class="btn btn-success" value="查询"/>
	</form>
</div>
<br/><br/>
<div id="list">
	<table class="table table-bordered table-striped table-hover">
		<c:choose>
			<c:when test="${not hasDatas}">
				<tr>
					<td>没有任务记录!</td>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<th>日期</th>
					<th>渠道</th>
					<th>注册数</th>
					<th>前N台注册数</th>
					<th>激活数</th>
					<th>活跃数</th>
					<th>业务包注册数</th>
					<th>业务包活跃数</th>
				</tr>
				<c:forEach var="data" items="${datas}" varStatus="it">
					<tr>
						<td>${fn:escapeXml(data.date)}</td>
						<td>${fn:escapeXml(data.channel)}</td>
						<td>${fn:escapeXml(data.register)}</td>
						<td>${fn:escapeXml(data.nRegister)}</td>
						<td>${fn:escapeXml(data.active)}</td>
						<td>${fn:escapeXml(data.DAU)}</td>
						<td>${fn:escapeXml(data.bizRegister)}</td>
						<td>${fn:escapeXml(data.bizDAU)}</td>
					</tr>
				</c:forEach>
				<tr>
					<th colspan="2">总和</th>
					<th>${listTotal.register}</th>
					<th colspan="3">${listTotal.nRegister}</th>
					<th colspan="2">${listTotal.bizRegister}</th>
				</tr>
			</c:otherwise>
		</c:choose>
	</table>
</div>
<div class="row-fluid">
	<div class="span8 paginator">
		<c:import url="../theme/${_theme}/paginator.jsp"/>
	</div>
</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"/>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startDate" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endDate" ).datepicker( "option", "minDate", selectedDate );
		},dateFormat: "yy-mm-dd"});
	$( "#endDate" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
		},dateFormat: "yy-mm-dd"} );
</script>