<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="损耗修改" scope="request"/>
<c:set var="_offerManager" value="active" scope="request"/>
<c:set var="_offerManager1" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<script>
function test(){
    x="";
	$("[class='ccxxxx']").each(function(index,element){
	    if(element.checked){
	    	x=x+element.value+","
	    }
	  });
	if(x.length>0){
		$("#xworkCondition").val(x);
	}
	url=$("#url").val();
	if(url==""){
		alert("url 不能为空!");
		return false;
	}
	$.get("/offer/check-url?url="+url,function(data){
		if(data!="success"){
			alert(data);
			return false;
		}
	});
	
	return true;
}

</script>
<!-- main content -->
		<div class="page-header"><h1>损耗修改</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			    <form action="${basePath}loss/save" onsubmit="return test()"  method="post" class="form-horizontal" enctype="multipart/form-data">
				<div class="control-group required-field">
					<label class="control-label">损耗:</label>
					<div class="controls">
						<input name="loss" id="loss" value="${fn:escapeXml(form.loss)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">浮动值:</label>
					<div class="controls">
						<input name="floatValue" value="${fn:escapeXml(form.floatValue)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">同步策略:</label>
					<div class="controls">
						<select name="auto" id="auto" >
						<option value="0">手动</option>
						<option value="1">自动</option>
						</select>
						<script>document.getElementById("auto").value="${empty form.auto?0:form.auto}"</script>
					</div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
$( "#startTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
	}});
$( "#endTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
	}} );

    function searchSelected(kwElemId, resultDivId){
        var kw = $("#"+kwElemId).val();
        $("#"+resultDivId+" .selLabel").each(function(){
            if(($("span",this).html()+"").indexOf(kw)==-1){
                $(this).hide();
            }else{
                $(this).show();
            }
        });
    }
    function showSelected(resultDivId){
        $("#"+resultDivId+" .selLabel").each(function(){
            if($("input[type=checkbox]",this)[0].checked){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
    }
    function toggleAllSelected(resultDivId, elem){
        $("#"+resultDivId+" .selLabel").each(function(){
            if($(this).is(":visible")){
                $($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
            }
        });
    }
    function searchAndSelect(kwElemId, resultDivId){
        var kw = $("#"+kwElemId).val();
        var shortcuts = kw.split(/[^a-zA-Z]+/);
        $("#"+resultDivId+" .selLabel").each(function(){
            if(shortcuts.length>0){
                for(var i=0;i<shortcuts.length;i++){
                    if(shortcuts[i].length!=2){
                        continue;
                    }
                    if(($("span",this).html()+"").indexOf(shortcuts[i].toUpperCase())==-1){
                        $(this).hide();
                    }else{
                        $(this).show();
                        $("input[type=checkbox]",this).attr("checked","checked");
                        break;
                    }
                }
            }else{
                $(this).hide();
            }
        });
    }
</script>
