<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="录入数据" scope="request"/>
<c:set var="_offerbach" value="active" scope="request"/>
<c:set var="_offerManager1" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
		<div class="page-header"><h1>批量录入单子</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}offerbach/save" method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${form.id}">
				<input name="_queryString" type="hidden" value="${param.queryString}">
				<div id="_cg_img" class="control-group required-field">
				  <label class="control-label">导入文件:</label>
				  <div class="controls">
				    <input name="file" type="file" multiple="multiple"/>
				  </div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="导入">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>