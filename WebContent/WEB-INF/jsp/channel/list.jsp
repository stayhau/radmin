<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="渠道管理" scope="request"/>
<c:set var="_underChannel" value="active" scope="request"/>
<c:set var="_channel" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<script type="text/javascript" src="${basePath}static/theme/${_theme}/global.js"></script>
<!-- main content -->
<div class="page-header"><h1>渠道管理</h1></div>
<c:import url="../theme/default/name_search.jsp">
	<c:param name="action" value="${basePath}channel/list"/>
</c:import>
<br/><br/>
<div id="list">
	<table class="table table-bordered table-striped table-hover">
		<c:choose>
			<c:when test="${not hasDatas}">
				<tr>
					<td>没有任务记录!</td>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<th></th>
					<th>编号</th>
					<th>名称</th>
					<th>渠道号</th>
					<c:if test="${root:isSelf()}"><th>转化率</th></c:if>
					<th>操作</th>
				</tr>
				<c:forEach var="data" items="${datas}" varStatus="it">
					<tr>
						<td class="checkbox_td">
							<input type="checkbox" name="ids" value="${data.id}"/>
						</td>
						<td>${fn:escapeXml(data.id)}</td>
						<td>${fn:escapeXml(data.name)}</td>
						<td>${fn:escapeXml(data.from)}</td>
						<c:if test="${root:isSelf()}"><td>${fn:escapeXml(data.conversion)}</td></c:if>
						<td class="operation operand1">
							<a class="btn btn-small btn-info" onclick="javascript:showDetail(${it.count},this);return false;"> 详情 </a>
						</td>
					</tr>
					<tr id="detail_${it.count }" style="display: none">
						<td></td>
						<td colspan="5">
							<ul>
								<li><strong>创建时间：</strong><fmt:formatDate value="${data.gmtCreate}" pattern="yyyy/MM/dd HH:mm:ss" var="gmtCreate"/>${gmtCreate}</li>
								<li><strong>更新时间：</strong><fmt:formatDate value="${data.gmtModified}" pattern="yyyy/MM/dd HH:mm:ss" var="gmtModified"/>${gmtModified}</li>
								<li><strong>备注：</strong>${fn:escapeXml(data.remark )}</li>
							</ul>
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</table>
</div>
<div class="row-fluid">
	<div class="span4 toolbar">
		<c:import url="../theme/${_theme}/toolbar.jsp">
			<c:param name="create">${basePath}channel/create</c:param>
			<c:param name="delete">${basePath}channel/delete</c:param>
			<c:param name="modify">${basePath}channel/modify</c:param>
		</c:import>
	</div>
	<div class="span8 paginator">
		<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
	</div>
</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>