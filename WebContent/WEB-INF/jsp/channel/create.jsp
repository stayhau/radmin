<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建渠道" scope="request"/>
<c:set var="_underChannel" value="active" scope="request"/>
<c:set var="_channel" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
<div class="page-header"><h1>创建/修改渠道</h1></div>
<div id="pageContent">
	<c:import url="../theme/${_theme}/errors.jsp"></c:import>
	<form action="${basePath}channel/save" method="post" class="form-horizontal" enctype="multipart/form-data">
		<input name="id" type="hidden" value="${form.id}">
		<input name="_queryString" type="hidden" value="${param.queryString}">

		<div class="control-group required-field">
			<label class="control-label">名称:</label>
			<div class="controls">
				<input name="name" value="${fn:escapeXml(form.name)}" type="text" class="input-large">
				<span class="remark">渠道名称</span>
			</div>
		</div>
		<div class="control-group required-field">
			<label class="control-label">渠道号:</label>
			<div class="controls">
				<input name="from" value="${fn:escapeXml(form.from)}" type="text" class="input-large">
				<span class="remark"></span>
			</div>
		</div>
		<c:if test="${root:isSelf()}">
			<div class="control-group required-field">
				<label class="control-label">转化率:</label>
				<div class="controls">
					<input name="conversion" value="${fn:escapeXml(form.conversion)}" type="text" class="input-large">
					<span class="remark"></span>
				</div>
			</div>
		</c:if>
		<div class="control-group required-field">
			<label class="control-label">备注:</label>
			<div class="controls">
				<input name="remark" value="${fn:escapeXml(form.remark)}" type="text" class="input-large">
			</div>
		</div>
		<div class="form-actions">
			<input class="btn btn-primary" type="submit" value="保存">
			<button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
		</div>
	</form>
</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>

