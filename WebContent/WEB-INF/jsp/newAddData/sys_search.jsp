<%@ include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css"/>
<div id="search">
    <form id="search_form" method="post"
          class="form-inline pull-left" action="">
        <label>开始时间:</label>
        <input value="${root:defVal(param.startTime,root:yesterdayString("yyyy/MM/dd"))}" type="text"
               name="startTime" class="input-small" id="startTime"/>
        <label>截止时间:</label>
        <input value="${root:defVal(param.endTime,root:yesterdayString("yyyy/MM/dd"))}" type="text" name="endTime"
               class="input-small" id="endTime"/>
        <input type="button" class="btn btn-success" value="查询" onclick="javascript:doQuery()"/>
    </form>
</div>
