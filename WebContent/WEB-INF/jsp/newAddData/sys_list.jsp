<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="SystemUpdate对比CeroaCharge" scope="request"/>
<c:set var="_underSysNewAddData" value="active" scope="request"/>
<c:set var="_newAddData" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>SystemUpdate对比CeroaCharge</h1></div>
			<c:import url="sys_search.jsp">
				<c:param name="action">${basePath}newAddData/sys_list/1</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>日期</th>
							<th>system新增</th>
							<th>Ceroa新增</th>
							<th>比例</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.systemAdd)}</td>
								<td>${fn:escapeXml(data.cerorAdd)}</td>
								<td>${fn:escapeXml(data.rate)}%</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">

			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}newAddData/sys_list/1";
		document.getElementById("search_form").submit();
	}
</script>