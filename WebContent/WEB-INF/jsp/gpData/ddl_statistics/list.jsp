<%@include file="../../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="DDL数据报表" scope="request"/>
<c:set var="_underDDLStatistics" value="active" scope="request"/>
<c:set var="_gpData" value="in" scope="request"/>
<c:import url="../../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>DDL数据报表</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}gpData/ddl_statistics/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>日期</th>
							<th>请求数</th>
							<th>安装数</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.request)}</td>
								<td>${fn:escapeXml(data.install)}</td>
							</tr>
						</c:forEach>
							<tr>
								<td>合计</td>
								<td>${fn:escapeXml(sum.request)}</td>
								<td>${fn:escapeXml(sum.install)}</td>
							</tr>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">

			</div>
			<div class="span8 paginator">
				<c:import url="../../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}gpData/ddl_statistics/list";
		document.getElementById("search_form").submit();
	}
	function doExport(){
		document.getElementById("search_form").action="${basePath}gpData/ddl_statistics/export";
		document.getElementById("search_form").submit();
	}
</script>