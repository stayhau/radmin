<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="数据分析" scope="request"/>
<c:set var="_underDataStatistics" value="active" scope="request"/>
<c:set var="_newAddData" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<style type="text/css">
	#warn_info td{
		background-color: #ffa016;
	}
	#error_info td{
		background-color: red;
	}
</style>
<div class="page-header"><h1>数据分析</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}dataStatistics/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th></th>
							<th>编号</th>
							<th>日期</th>
							<th>渠道</th>
							<th>产品</th>
							<th>新增</th>
							<th>system新增</th>
							<th>system转化率</th>
							<th>gd新增</th>
							<th>gd转化率</th>
							<th>gm新增</th>
							<th>gm转化率</th>
							<th>QR激活</th>
							<th>su激活</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td class="checkbox_td">
									<input type="checkbox" name="ids" id="ids" value="${data.id}"/>
								</td>
								<td>${fn:escapeXml(data.id)}</td>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.channel)}</td>
								<td>${fn:escapeXml(data.product)}</td>
								<td>${fn:escapeXml(data.newAdd)}</td>
								<td>${fn:escapeXml(data.suAdd)}</td>
								<td>${fn:escapeXml(data.suRate)}%</td>
								<td>${fn:escapeXml(data.gdAdd)}</td>
								<td>${fn:escapeXml(data.gdRate)}%</td>
								<td>${fn:escapeXml(data.gmdAdd)}</td>
								<td>${fn:escapeXml(data.gmdRate)}%</td>
								<td>${fn:escapeXml(data.qrActive)}</td>
								<td>${fn:escapeXml(data.suActive)}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<c:import url="../theme/${_theme}/toolbar.jsp">
				</c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}dataStatistics/list";
		document.getElementById("search_form").submit();
	}
</script>