<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建应用名" scope="request"/>
<c:set var="_underApp" value="active" scope="request"/>
<c:set var="_event" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
		<div class="page-header"><h1>创建应用名</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}app/save"  onsubmit="javascript:onSubmitAppRuleForm()" method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${form.id}">
				<input name="events" type="hidden" id="events">
				<input name="_queryString" type="hidden" value="${param.queryString}">

				<div class="control-group required-field">
					<label class="control-label">名称:</label>
					<div class="controls">
						<input name="app" value="${fn:escapeXml(form.app)}" type="text" class="input-large">
						<span class="remark"></span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">appId:</label>
					<div class="controls">
						<input name="pkgName" value="${fn:escapeXml(form.pkgName)}" type="text" class="input-large">
						<span class="remark"></span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">事件:</label>
					<div class="controls">
						<div id="_div_events">
							<c:forEach var="event" items="${form.eventsObject}" varStatus="vs">
								<div class="page-rules">
									<div class="alert alert-success" style="display: inline-block;min-width: 300px;">
										<span class="close" data-dismiss="alert" onclick="javascript:deleteEvent(${vs.index})" ><i class="icon-trash"></i></span>
										<span class="close" onclick="javascript:editEvent(${vs.index})" ><i class="icon-edit" style="margin-right: 10px;"></i></span>
										<ul>
											<li><span>事件名称：</span><span id="_pr_eventName_${vs.index}">${event.eventName}</span></li>
											<li><span>事件ID：</span><span id="_pr_ename_${vs.index}">${event.ename}</span></li>
											<li><span>事件类型：</span><span id="_pr_eventType_${vs.index}">${event.eventType==0?"归档":"统计"}</span></li>
										</ul>
									</div>
								</div>
							</c:forEach>
						</div>
						<a href="#_div_event_modal" role="button" class="btn" data-toggle="modal" onclick="javascript:addEvent()">新增事件</a>
						<div id="_div_event_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<span class="remark">新增/修改事件</span>
							</div>
							<div class="modal-body">
								<div class="control-group required-field">
									<label class="control-label">事件名称:</label>
									<div class="controls">
										<input id="_pr_eventName" name="eventName" type="text" class="input-medium">
									</div>
								</div>
								<div class="control-group required-field">
									<label class="control-label">事件ID:</label>
									<div class="controls">
										<input id="_pr_ename" name="ename" type="text" class="input-medium">
									</div>
								</div>
								<div class="control-group required-field">
									<label class="control-label">事件类型:</label>
									<div class="controls">
										<select id="_pr_eventType" name="eventType" class="input-small">
											<option value="1">统计</option>
											<option value="0">归档</option>
										</select>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
								<button class="btn btn-primary" type="button" onclick="javascript:onSaveEvent()">确定</button>
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script>
	function searchSelected(kwElemId, resultDivId){
		var kw = $("#"+kwElemId).val();
		$("#"+resultDivId+" .selLabel").each(function(){
			if(($("span",this).html()+"").indexOf(kw)==-1){
				$(this).hide();
			}else{
				$(this).show();
			}
		});
	}
	function showSelected(resultDivId){
		$("#"+resultDivId+" .selLabel").each(function(){
			if($("input[type=checkbox]",this)[0].checked){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
	}
	function toggleAllSelected(resultDivId, elem){
		$("#"+resultDivId+" .selLabel").each(function(){
			if($(this).is(":visible")){
				$($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
			}
		});
	}
	function searchAndSelect(kwElemId, resultDivId){
		var kw = $("#"+kwElemId).val();
		var shortcuts = kw.split(/[^a-zA-Z]+/);
		$("#"+resultDivId+" .selLabel").each(function(){
			if(shortcuts.length>0){
				for(var i=0;i<shortcuts.length;i++){
					if(shortcuts[i].length!=2){
						continue;
					}
					if(($("span",this).html()+"").indexOf(shortcuts[i].toUpperCase())==-1){
						$(this).hide();
					}else{
						$(this).show();
						$("input[type=checkbox]",this).attr("checked","checked");
						break;
					}
				}
			}else{
				$(this).hide();
			}
		});
	}
	showSelected("_channels");
</script>
<script>

	var editIdx=-1;//用于标志当前编辑的event
	var events=[];
	<c:forEach var="event" items="${form.innerEventsObject}" varStatus="vs">
	events.push(${event.jsonString});
	</c:forEach>

	function onSaveEvent(){
		var eventName=$("#_pr_eventName").val();
		var ename=$("#_pr_ename").val();
		var eventType = $("#_pr_eventType").val();
		var eventTypeName = eventType == 0 ? "归档" : "统计";
		if(/^\s*$/.test(eventName)){
			alert("事件名称不能为空");
			return;
		}
		if(/^\s*$/.test(ename)){
			alert("事件ID不能为空");
			return;
		}
		var idx=editIdx==-1?events.length:editIdx;
		var html='<div class="page-rules">'+
				'<div class="alert alert-success" style="display: inline-block;min-width: 300px;">'+
				'<span class="close" data-dismiss="alert" onclick="javascript:deleteEvent('+idx+')" ><i class="icon-trash"></i></span>'+
				'<span class="close" onclick="javascript:editEvent('+idx+')" ><i class="icon-edit" style="margin-right: 10px;"></i></span>'+
				'<ul>'+
				'<li><span>事件名称：</span><span id="_pr_eventName_'+idx+'">'+eventName+'</span></li>'+
				'<li><span>事件ID：</span><span id="_pr_ename_'+idx+'">'+ename+'</span></li>'+
				'<li><span>事件类型：</span><span id="_pr_eventType_'+idx+'">'+eventTypeName+'</span></li>';
		html = html+
		'</ul>'+
		'</div>'+
		'</div>';
		var event= {"eventName":eventName,"ename":ename,"eventType":eventType};
		if(editIdx==-1){
			events.push(event);
			$("#_div_events").append($(html));
		}else{
			events[editIdx]=event;
			$("#_pr_eventName_"+editIdx).html(eventName);
			$("#_pr_ename_"+editIdx).html(ename);
			$("#_pr_eventType_"+editIdx).html(eventTypeName);
		}
		$('#_div_event_modal').modal('hide');
	}
	function deleteEvent(idx){
		events[idx]=null;
	}
	function editEvent(idx){
		editIdx = idx;
		var pr=events[idx];
		$("#_pr_eventName").val(pr.eventName);
		$("#_pr_ename").val(pr.ename);
		$("#_pr_eventType").val(pr.eventType);

		$('#_div_event_modal').modal('show');
	}
	function addEvent(){
		editIdx = -1;
		$("#_pr_eventName").val("");
		$("#_pr_ename").val("");
		$("#_pr_evnetType").val("0");

	}
	function onSubmitAppRuleForm(){
		for(var i=0;i<events.length;i++){
			if(events[i]==null){
				events.splice(i,1);
			}
		}
		$("#events").val(JSON.stringify(events));
	}

</script>


