<%@ include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css"/>
<div id="search">
    <form id="search_form" method="post"
          class="form-inline pull-left" action="">
         <label>产品:</label>
        <select name="product" id="product_sel" class="input-small">
        <option value="">全部</option>
            <c:forEach items="${products}" var="product">
                <option value="${product}">${product}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("product_sel").value = '${root:defVal(param.product,"")}';
        </script>
        <label>渠道:</label>
        <select name="channel" id="channel_sel" class="input-small">
            <option value="">全部</option>
            <c:forEach items="${channels}" var="channel">
                <option value="${channel}">${channel}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("channel_sel").value = '${root:defVal(param.channel,"")}';
        </script>
        <label>事件:</label>
        <select name="event" id="event_sel" class="input-small">
            <option value="">全部</option>
            <c:forEach items="${events}" var="event">
                <option value="${event}">${event}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("event_sel").value = '${root:defVal(param.event,"")}';
        </script>
        <label>参数:</label>
        <select name="param" id="param_sel" class="input-small">
            <option value="">全部</option>
            <c:forEach items="${parameters}" var="parameter">
                <option value="${parameter}">${parameter}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("param_sel").value = '${root:defVal(param.param,"")}';
        </script>
        <label>开始时间:</label>
        <input value="${root:defVal(param.startTime,root:yesterdayString("yyyy/MM/dd"))}" type="text"
               name="startTime" class="input-small" id="startTime"/>
        <label>截止时间:</label>
        <input value="${root:defVal(param.endTime,root:yesterdayString("yyyy/MM/dd"))}" type="text" name="endTime"
               class="input-small" id="endTime"/>
        <input type="button" class="btn btn-success" value="查询" onclick="javascript:doQuery()"/>
    </form>
</div>
