<%@ include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css"/>
<div id="search">
    <form id="search_form" method="post"
          class="form-inline pull-left" action="">
<c:if test="${root:isAdmin()}">
        <label>渠道:</label>
        <select name="channel" id="channel_sel" class="input-small">
            <option value="">全部</option>
            <c:forEach items="${channels}" var="channel">
                <option value="${channel}">${channel}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("channel_sel").value = '${root:defVal(param.channel,"")}';
        </script>
</c:if>
        <label>开始时间:</label>
        <input value="${root:defVal(param.startTime,root:yesterdayString("yyyy/MM/dd"))}" type="text"
               name="startTime" class="input-small" id="startTime"/>
        <label>截止时间:</label>
        <input value="${root:defVal(param.endTime,root:yesterdayString("yyyy/MM/dd"))}" type="text" name="endTime"
               class="input-small" id="endTime"/>
        <input type="button" class="btn btn-success" value="查询" onclick="javascript:doQuery()"/>
        <input onclick="javascript:doExport()" type="button" class="btn btn-success" value="导出">
    </form>
</div>
