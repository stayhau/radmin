<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="用户耗损报表" scope="request"/>
<c:set var="_underUserLoss" value="active" scope="request"/>
<c:set var="_userSaturation" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>用户质量报表</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}user_loss/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>日期</th>
							<th>渠道</th>
							<th>莲子新增</th>
							<th>链子日活</th>
							<th>实际注册</th>
							<th>可运营新增</th>
							<th>可运营日活</th>
							<th>活跃度</th>
							<th>IMEI空</th>
							<th>imei重复</th>
							<th>mac重复</th>
							<th>GP收益</th>
							<th>无效区域</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.channel==""?"unknown":data.channel)}</td>
								<td>${fn:escapeXml(data.lotuseedNewAdd)}</td>
								<td>${fn:escapeXml(data.lotuseedActive)}</td>
								<td>${fn:escapeXml(data.register)}</td>
								<td>${fn:escapeXml(data.operateNewAdd)}</td>
								<td>${fn:escapeXml(data.operateActive)}</td>
								<!--
								<td>${fn:escapeXml(data.activeRate*100)}%</td>
								-->
								<td><fmt:formatNumber type="number" value="${data.activeRate*100} " maxFractionDigits="1"/>%</td>
								<td>${fn:escapeXml(data.noimei)}</td>
								<td>${fn:escapeXml(data.imeiRepeat)}</td>
								<td>${fn:escapeXml(data.macRepeat)}</td>
								<td>${fn:escapeXml(data.income)}</td>
								<td>${fn:escapeXml(data.invaildArea)}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<c:import url="../theme/${_theme}/toolbar.jsp">
				</c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}user_loss/list";
		document.getElementById("search_form").submit();
	}
	function doExport(){
		document.getElementById("search_form").action="${basePath}user_loss/export";
		document.getElementById("search_form").submit();
	}
</script>