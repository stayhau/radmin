<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="策略管理" scope="request"/>
<c:set var="_offerManager" value="active" scope="request"/>
<c:set var="_offerManager1" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
<div class="page-header"><h1>业务员管理</h1></div>
<div id="search">
    <form id="search_form" method="post"
          class="form-inline pull-left"  action="${basePath}ppwe/list">
          <label>开始时间:</label><input type="text" name="startTime" id="startTime" value="${sear.startTime}" >   
           <label>结束时间:</label><input type="text" name="endTime" id="endTime" value="${sear.endTime}" >   
         <label>状态:</label>
        <select name="status" id="product_sel" class="input-small">
        <option value="0">全部</option>
        <option value="5">已完成</option>
        <option value="1">排队中</option>
        <option value="6">检查中</option>
        
        </select>
        <label>广告主渠道:</label>
        <select name="adChannel" id="channel_sel" class="input-small" style="width: 200px;">
            <option value="">全部</option>
            <c:forEach items="${adChannel}" var="channel">
                <option value="${channel.channel}">${channel.channel}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("channel_sel").value = '${root:defVal(sear.adChannel,"")}';
            document.getElementById("product_sel").value = '${root:defVal(sear.status,"0")}';
        </script>
        <!-- 
        <label>开始时间:</label>
        <input value="${root:defVal(param.startTime,root:yesterdayString("yyyy/MM/dd"))}" type="text"
               name="startTime" class="input-small" id="startTime"/>
        <label>截止时间:</label>
        <input value="${root:defVal(param.endTime,root:yesterdayString("yyyy/MM/dd"))}" type="text" name="endTime"
               class="input-small" id="endTime"/> -->
        <input type="submit" id="btn" class="btn btn-success" value="查询" />
        <!-- <input type="button" id="exl"  class="btn btn-success" value="导出" /> -->
    </form>
</div>
        <br/><br/>
        <div class="row-fluid">
			<div class="span4 toolbar">
				<c:import url="../theme/${_theme}/toolbar.jsp">
					<c:param name="create">${basePath}ppwe/create</c:param>
                    <c:param name="delete">${basePath}ppwe/delete</c:param>
					<c:param name="modify">${basePath}ppwe/modify</c:param>
				</c:import>
			<c:if test="${(root:isAdmin())}">
				<input type="button" class="btn btn-primary" onclick="javascript:doWithMultiItem('/ppwe/verify','确定要批量审核吗?')" name="_action_modify" value="批量审核">
				</c:if>
			</div>
		</div>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${empty datas}">
						<tr>
							<td>没有任务记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th></th>
							<th>ID</th>
							<th>地址</th>
							<th>单价</th>
							<th>阅读数</th>
							<th>当前阅读数</th>
							<th>开始时间</th>
							<th>优先级</th>
							<th>广告主</th>
							<th>状态</th>
							<th>操作</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td class="checkbox_td">
									<input type="checkbox" name="ids" value="${data.offerId}"/>
								</td>
								<td>${fn:escapeXml(data.offerId)}</td>
								<td>${fn:substring(data.url,0,50)}</td>
								<td>${fn:escapeXml(data.payout)}/1000</td>
								<td >${data.readNum}</td>
								<td >${data.readedNum}</td>
								<td >${data.startTime}</td>
								<td >${data.weight}</td>
								<td >${data.adChannel}</td>
								
								<td style="background-color: ${data.status==1?"green":data.status==-2?"yellow":data.status==-1?"gray":"red"}">${root:OffereStatusStr(data.status)}</td>
									<td class="operation operand1">
									<a class="btn btn-small btn-info" onclick="javascript:showDetail(${data.offerId},this);return false;"> 详情 </a>
								</td>
							</tr>
							<tr id="detail_${data.offerId}" style="display: none">
								<td></td>
								<td colspan="7">
									<ul>
                                        <li><strong>创建时间：</strong><fmt:formatDate value="${data.createTime}" pattern="yyyy/MM/dd HH:mm:ss" var="gmtCreate"/>${gmtCreate}</li>
                                        <li>${data.remark}</li>
                                        <li>单子录入人:${data.auther}</li>
                                        <li>结束时间:${data.endTime}</li>
                                        <li>url:${data.url}</li>
									</ul>
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<c:import url="../theme/${_theme}/toolbar.jsp">
					<c:param name="create">${basePath}ppwe/create</c:param>
                    <c:param name="delete">${basePath}ppwe/delete</c:param>
					<c:param name="modify">${basePath}ppwe/modify</c:param>
				</c:import>
			<c:if test="${(root:isAdmin())}">
				<input type="button" class="btn btn-primary" onclick="javascript:doWithMultiItem('/ppwe/verify','确定要批量审核吗?')" name="_action_modify" value="批量审核">
				</c:if>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/pageholder-default.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>

$( "#startTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
	},dateFormat: "yy-mm-dd"});
$( "#endTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
	},dateFormat: "yy-mm-dd"} );
</script>
<script>
	$(function() {
		$("[name='my-checkbox']").on('switchChange.bootstrapSwitch', function(event, state) {
			alert("dd");
			var id = this.getAttribute("id");
			var path = "${basePath}wgz/changeStatus";
			changeStatusWithItemAjax(id, path, "确认修改？",state);
		});
	});
</script>