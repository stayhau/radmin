<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="单子录入" scope="request"/>
<c:set var="_offerManager" value="active" scope="request"/>
<c:set var="_offerManager1" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<script>
function test(){
    x="";
	$("[class='ccxxxx']").each(function(index,element){
	    if(element.checked){
	    	x=x+element.value+","
	    }
	  });
	if(x.length>0){
		$("#xworkCondition").val(x);
	}
	
	return true;
}

</script>
<!-- main content -->
		<div class="page-header"><h1>单子新增</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}ppwe/save" onsubmit="return test()"  method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="offerId" type="hidden" value="${form.offerId}">
				<input name="auther" type="hidden" value="${root:username()}">
				<input name="_queryString" type="hidden" value="${param.queryString}">
				<div class="control-group required-field">
					<label class="control-label">地址:</label>
					<div class="controls">
						<input name="url" value="${fn:escapeXml(form.url)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">单价:</label>
					<div class="controls">
						<input name="payout" value="${fn:escapeXml(form.payout)}" type="text" class="input-large">/1000
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">阅读数量:</label>
					<div class="controls">
						<input name="readNum" value="${fn:escapeXml(form.readNum)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">当前阅读数:</label>
					<div class="controls">
					  <input name="readedNum" value="${fn:escapeXml(form.readedNum)}" type="text" class="input-large">(可不填)
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">广告主:</label>
					<div class="controls">
					  <input name="adChannel" value="${fn:escapeXml(form.adChannel)}" type="text" class="input-large">(可不填,用于分类)
				</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">优先级:</label>
					<div class="controls">
						<select name="weight" id="weight" >
						<option value="0">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						</select>
						<script>document.getElementById("weight").value="${empty form.weight?5:form.weight}"</script>
					</div>
				</div>
				<c:if test="${root:isAdmin()}">
				</c:if>
                	<div class="control-group required-field">
					<label class="control-label">备注:</label>
					<div class="controls">
						<input name="remark" value="${fn:escapeXml(form.remark)}" type="text" class="input-large">
					</div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
$( "#startTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
	}});
$( "#endTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
	}} );

    function searchSelected(kwElemId, resultDivId){
        var kw = $("#"+kwElemId).val();
        $("#"+resultDivId+" .selLabel").each(function(){
            if(($("span",this).html()+"").indexOf(kw)==-1){
                $(this).hide();
            }else{
                $(this).show();
            }
        });
    }
    function showSelected(resultDivId){
        $("#"+resultDivId+" .selLabel").each(function(){
            if($("input[type=checkbox]",this)[0].checked){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
    }
    function toggleAllSelected(resultDivId, elem){
        $("#"+resultDivId+" .selLabel").each(function(){
            if($(this).is(":visible")){
                $($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
            }
        });
    }
    function searchAndSelect(kwElemId, resultDivId){
        var kw = $("#"+kwElemId).val();
        var shortcuts = kw.split(/[^a-zA-Z]+/);
        $("#"+resultDivId+" .selLabel").each(function(){
            if(shortcuts.length>0){
                for(var i=0;i<shortcuts.length;i++){
                    if(shortcuts[i].length!=2){
                        continue;
                    }
                    if(($("span",this).html()+"").indexOf(shortcuts[i].toUpperCase())==-1){
                        $(this).hide();
                    }else{
                        $(this).show();
                        $("input[type=checkbox]",this).attr("checked","checked");
                        break;
                    }
                }
            }else{
                $(this).hide();
            }
        });
    }
</script>
