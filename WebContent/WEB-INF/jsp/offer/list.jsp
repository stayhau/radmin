<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="Offer管理" scope="request"/>
<c:set var="_offerManager" value="active" scope="request"/>
<c:set var="_offerManager1" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"/>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css"/>
<!-- main content -->
<div class="page-header"><h1>Offer管理</h1></div>
<div id="search">
    <form id="search_form" method="post"
          class="form-inline pull-left" action="${basePath}offer/list">
        <label>开始时间:</label><input type="text" name="startTime" id="startTime" value="${sear.startTime}">
        <label>结束时间:</label><input type="text" name="endTime" id="endTime" value="${sear.endTime}">
        <label>状态:</label>
        <select name="status" id="product_sel" class="input-small">
            <option value="0">全部</option>
            <option value="1">未完成</option>
            <option value="2">异常</option>
            <option value="5">已完成</option>
            <option value="6">待检查</option>
            <option value="10">转化异常</option>
        </select>
        <label>广告主渠道:</label>
        <select name="adChannel" id="channel_sel" class="input-small" style="width: 200px;">
            <option value="">全部</option>
            <c:forEach items="${adChannel}" var="channel">
                <option value="${channel.channel}">${channel.channel}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("channel_sel").value = '${root:defVal(sear.adChannel,"")}';
            document.getElementById("product_sel").value = '${root:defVal(sear.status,"0")}';
        </script>
        <input type="submit" id="btn" class="btn btn-success" value="查询"/>
        <!-- <input type="button" id="exl"  class="btn btn-success" value="导出" /> -->
    </form>
</div>
<div class="row-fluid">
    <div class="span4 toolbar">
        <c:if test="${(root:isSelf())}">
            <c:import url="../theme/${_theme}/toolbar.jsp">
                <c:param name="create">${basePath}offer/create</c:param>
                <c:param name="delete">${basePath}offer/delete</c:param>
                <c:param name="modify">${basePath}offer/modify</c:param>
            </c:import>
            <input type="button" class="btn btn-primary" name="_action_modify" value="批量审核"
                   onclick="javascript:doWithMultiItem('/offer/verify','确定要批量审核吗?')">
        </c:if>
    </div>
</div>
<c:if test="${notice!=null}">
    <ul id="notice">
        <li>${notice}</li>
    </ul>
</c:if>
<br/><br/>
<div id="list">
    <table class="table table-bordered table-striped table-hover">
        <c:choose>
            <c:when test="${empty datas}">
                <tr>
                    <td>没有任务记录!</td>
                </tr>
            </c:when>
            <c:otherwise>
                <tr>
                    <c:if test="${(root:isSelf())}">
                        <th></th>
                        <th>ID</th>
                    </c:if>
                    <th>地址</th>
                    <th>单价/k</th>
                    <th>阅读数</th>
                    <th>当前阅读数</th>
                    <th>下发数</th>
                        <%--<th>上报数</th>--%>
                    <th>转化率</th>
                    <th>开始时间</th>
                    <th>成功时间</th>
                    <th>状态</th>
                        <%-- <th>操作</th>--%>
                </tr>
                <c:forEach var="data" items="${datas}" varStatus="it">
                    <tr>
                        <c:if test="${(root:isSelf())}">
                            <td class="checkbox_td">
                                <input type="checkbox" name="ids" value="${data.offerId}"/>
                            </td>
                            <td>${fn:escapeXml(data.offerId)}</td>
                        </c:if>
                        <td>${fn:substring(data.url,0,50)}</td>
                        <td>${fn:escapeXml(data.payout)}</td>
                        <td>${data.readNum}</td>
                        <td>${data.readedNum}</td>
                        <td>${data.pulls}</td>
                            <%--<td>${data.reportCnt}</td>--%>
                        <td>${data.conversionRate}</td>
                        <td><fmt:formatDate value="${data.createTime}" pattern="MM-dd HH:mm"
                                            var="gmtCreate"/>${gmtCreate}</td>
                        <td><fmt:formatDate value="${data.successTime}" pattern="MM-dd HH:mm"
                                            var="successTime"/>${successTime}</td>
                        <td style="color:white;background-color: ${data.status==1?"green":data.status==-2?"#f0ad4e":data.status==-1?"gray":data.status==10?"#886600":"#db623d"}">${root:OffereStatusStr(data.status)}</td>
                            <%--<td class="operation operand1">
                                <a class="btn btn-small btn-info"
                                   onclick="javascript:showDetail(${data.offerId},this);return false;"> 详情 </a>
                            </td>--%>
                    </tr>
                    <%--<tr id="detail_${data.offerId}" style="display: none">
                        <td></td>
                        <td colspan="10">
                            <ul>
                                <li><strong>接单时间：</strong>
                                    <fmt:formatDate value="${data.createTime}" pattern="yyyy/MM/dd HH:mm:ss"
                                                    var="gmtCreate"/>${gmtCreate}</li>
                                <li><strong>广告主：</strong>${data.adChannel}</li>
                                <li><strong>备注：</strong>${data.remark}</li>
                            </ul>
                        </td>
                    </tr>--%>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </table>
</div>
<div class="row-fluid">
    <div class="span4 toolbar">
        <c:if test="${(root:isSelf())}">
            <c:import url="../theme/${_theme}/toolbar.jsp">
                <c:param name="create">${basePath}offer/create</c:param>
                <c:param name="delete">${basePath}offer/delete</c:param>
                <c:param name="modify">${basePath}offer/modify</c:param>
            </c:import>
            <input type="button" class="btn btn-primary"
                   onclick="javascript:doWithMultiItem('/offer/verify','确定要批量审核吗?')" name="_action_modify" value="批量审核">
        </c:if>
    </div>
    <div class="span8 paginator">
        <c:import url="../theme/${_theme}/paginator.jsp"></c:import>
    </div>
    <div class="span8 paginator">
        <c:import url="../theme/${_theme}/pageholder-default.jsp"></c:import>
    </div>
</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>

    $("#startTime").datepicker({
        onClose: function (selectedDate) {
            $("#endTime").datepicker("option", "minDate", selectedDate);
        }, dateFormat: "yy-mm-dd"
    });
    $("#endTime").datepicker({
        onClose: function (selectedDate) {
            $("#startTime").datepicker("option", "maxDate", selectedDate);
        }, dateFormat: "yy-mm-dd"
    });
</script>
<script>
    $(function () {
        $("[name='my-checkbox']").on('switchChange.bootstrapSwitch', function (event, state) {
            alert("dd");
            var id = this.getAttribute("id");
            var path = "${basePath}wgz/changeStatus";
            changeStatusWithItemAjax(id, path, "确认修改？", state);
        });
    });
</script>