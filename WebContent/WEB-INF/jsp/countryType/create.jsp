<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建国家区域" scope="request"/>
<c:set var="_underCountryType" value="active" scope="request"/>
<c:set var="_basicInfo" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
		<div class="page-header"><h1>创建/修改国家区域</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}countryType/save" method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${form.id}">
				<input name="_queryString" type="hidden" value="${param.queryString}">

				<div class="control-group required-field">
					<label class="control-label">区域名称:</label>
					<div class="controls">
						<input name="countryTypeName" value="${fn:escapeXml(countryTypeName)}" type="text" class="input-large">
						<span class="remark"></span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">国家:</label>
					<div class="controls">
						<div class="control-search-bar">
							<input id="_country_kw" name="_country_kw" class="input-medium">
							<input class="btn" type="button" onclick="javascript:searchSelected('_country_kw','_countries');" value="搜索">
							<input class="btn" type="button" onclick="javascript:searchAndSelect('_country_kw','_countries');" value="搜索并选择">
							<input class="btn" type="button" onclick="javascript:showSelected('_countries');" value="选择项">
							<input type="checkbox" onchange="javascript:toggleAllSelected('_countries', this);">全选/全不选
						</div>
						<div id="_countries">
							<c:forEach var="country" items="${countries}">
								<span class="selLabel"><input ${root:contains(form.countriesObject, country.shortcut)?"checked=\"checked\"":""} type="checkbox" name="countriesObject" value="${country.shortcut}"><span>${country.chineseName}(${fn:toUpperCase(country.shortcut)})</span></span>
							</c:forEach>
						</div>
					</div>
				</div>

				<div class="control-group required-field">
				  <label class="control-label">备注:</label>
				  <div class="controls">
				    <input name="remark" value="${fn:escapeXml(form.remark)}" type="text" class="input-large">
				  </div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script>
	function searchSelected(kwElemId, resultDivId){
		var kw = $("#"+kwElemId).val();
		var country = kw.split(/[^\u4E00-\u9FA5]+/);
		$("#"+resultDivId+" .selLabel").each(function(){
			if(country.length>0){
				for(var i=0;i<country.length;i++){
					if(($("span",this).html()+"").indexOf(country[i])==-1){
						$(this).hide();
					}else{
						$(this).show();
						break;
					}
				}
			}else{
				$(this).hide();
			}
		});
	}
	function showSelected(resultDivId){
		$("#"+resultDivId+" .selLabel").each(function(){
			if($("input[type=checkbox]",this)[0].checked){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
	}
	function toggleAllSelected(resultDivId, elem){
		$("#"+resultDivId+" .selLabel").each(function(){
			if($(this).is(":visible")){
				$($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
			}
		});
	}
	showSelected("_countries");

	function searchAndSelect(kwElemId, resultDivId){
		var kw = $("#"+kwElemId).val();
		var shortcuts = kw.split(/[^a-zA-Z]+/);
		$("#"+resultDivId+" .selLabel").each(function(){
			if(shortcuts.length>0){
				for(var i=0;i<shortcuts.length;i++){
					if(shortcuts[i].length!=2){
						continue;
					}
					if(($("span",this).html()+"").indexOf(shortcuts[i].toUpperCase())==-1){
						$(this).hide();
					}else{
						$(this).show();
						$("input[type=checkbox]",this).attr("checked","checked");
						break;
					}
				}
			}else{
				$(this).hide();
			}
		});
	}

</script>

