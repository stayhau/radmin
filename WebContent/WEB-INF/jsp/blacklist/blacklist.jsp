<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建业务员转化率" scope="request"/>
<c:set var="_underblacklist" value="active" scope="request"/>
<c:set var="_policyManager" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
		<div class="page-header"><h1>策略新增</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}blacklist/save-blacklist" onsubmit="return test()"  method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${datas.id}">
				<input name="_queryString" type="hidden" value="${param.queryString}">
                </div>
                	<div class="control-group required-field">
					<label class="control-label">黑名单(","分隔):</label>
					<div class="controls">
					<textarea name="pkgName"  class="input-large" rows="10" cols="30">${fn:escapeXml(datas.pkgName)}</textarea>
					</div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
