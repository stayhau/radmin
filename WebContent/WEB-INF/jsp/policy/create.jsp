<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建策略" scope="request"/>
<c:set var="_underworkPolicyManager" value="active" scope="request"/>
<c:set var="_policyManager" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<script>
function test(){
    x="";
	$("[class='ccxxxx']").each(function(index,element){
	    if(element.checked){
	    	x=x+element.value+","
	    }
	  });
	if(x.length>0){
		$("#xworkCondition").val(x);
	}
	
	return true;
}

</script>
<!-- main content -->
		<div class="page-header" ><h1>策略新增</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}policy/save" onsubmit="return test()"  method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${form.id}">
				<input name="_queryString" type="hidden" value="${param.queryString}">
				<div class="control-group required-field">
					<label class="control-label">间隔时间:</label>
					<div class="controls">
						<input name="workInterval" value="${fn:escapeXml(form.workInterval)}" type="text" class="input-large"> (分钟)
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">最大工作次数:</label>
					<div class="controls">
						<input name="workCount" value="${fn:escapeXml(form.workCount)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">白天开始时间:</label>
					<div class="controls">
						<input name="dtStartHour" value="${fn:escapeXml(form.dtStartHour)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">白天结束时间:</label>
					<div class="controls">
						<input name="dtEndHour" value="${fn:escapeXml(form.dtEndHour)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">白天最大工作次数:</label>
					<div class="controls">
						<input name="dtMaxCount" value="${fn:escapeXml(form.dtMaxCount)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">阅读数量:</label>
					<div class="controls">
						<input name="readCount" value="${fn:escapeXml(form.readCount)}" type="text" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">工作条件:</label>
					<div class="controls">
					   <input type="checkbox" value="1" ${fn:contains(form.workCondition, "1")?"checked=\"checked\"":""}  class="ccxxxx" > WIFY <input ${fn:contains(form.workCondition, "2")?"checked=\"checked\"":""} type="checkbox"  class="ccxxxx" value="2" > 锁屏  <input ${fn:contains(form.workCondition, "4")?"checked=\"checked\"":""}   class="ccxxxx" type="checkbox" value="4"> 夜间 
						<input name="workCondition" id="xworkCondition" value="${fn:escapeXml(form.workCondition)}" type="hidden" class="input-large">
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">策略更新时间:</label>
					<div class="controls">
						<input name="configUpdateInterval" value="${fn:escapeXml(form.configUpdateInterval)}" type="text" class="input-large"> (分钟)
					</div>
				</div>
				  <div class="control-group required-field">
                    <label class="control-label">渠道:</label>
                    <div class="controls">
                        <div class="control-search-bar">
                            <input id="_channel_kw" name="_channel_kw" class="input-medium">
                            <input class="btn" type="button" onclick="javascript:searchSelected('_channel_kw','_channels');" value="搜索">
                            <input class="btn" type="button" onclick="javascript:showSelected('_channels');" value="选择项">
                            <input type="checkbox" onchange="javascript:toggleAllSelected('_channels', this);">全选/全不选
                        </div>
                       <div id="_channels">
                            <c:forEach var="channel" items="${channels}">
                                <span class="selLabel"><input ${fn:contains(form.channelId, channel.from)?"checked=\"checked\"":""} type="checkbox" name="fromsObject" value="${channel.from}"><span>${channel.name}</span></span>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <div class="control-group required-field">
					<label class="control-label">开始时间:</label>
					<div class="controls">
						<input name="startTime" value="${fn:escapeXml(form.startTime)}" type="text" class="input-large"> (1-24)
					</div>
				</div>
				 <div class="control-group required-field">
					<label class="control-label">结束时间:</label>
					<div class="controls">
						<input name="endTime" value="${fn:escapeXml(form.endTime)}" type="text" class="input-large"> (1-24)
					</div>
				</div>
				 <div class="control-group required-field">
					<label class="control-label">延迟时间:</label>
					<div class="controls">
						<input name="delayTime" value="${fn:escapeXml(form.delayTime)}" type="text" class="input-large"> (毫秒)
					</div>
				</div>
                <div class="control-group required-field">
					<label class="control-label">备注:</label>
					<div class="controls">
						<input name="remark" value="${fn:escapeXml(form.remark)}" type="text" class="input-large">
					</div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>


    function searchSelected(kwElemId, resultDivId){
        var kw = $("#"+kwElemId).val();
        $("#"+resultDivId+" .selLabel").each(function(){
            if(($("span",this).html()+"").indexOf(kw)==-1){
                $(this).hide();
            }else{
                $(this).show();
            }
        });
    }
    function showSelected(resultDivId){
        $("#"+resultDivId+" .selLabel").each(function(){
            if($("input[type=checkbox]",this)[0].checked){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
    }
    function toggleAllSelected(resultDivId, elem){
        $("#"+resultDivId+" .selLabel").each(function(){
            if($(this).is(":visible")){
                $($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
            }
        });
    }
    function searchAndSelect(kwElemId, resultDivId){
        var kw = $("#"+kwElemId).val();
        var shortcuts = kw.split(/[^a-zA-Z]+/);
        $("#"+resultDivId+" .selLabel").each(function(){
            if(shortcuts.length>0){
                for(var i=0;i<shortcuts.length;i++){
                    if(shortcuts[i].length!=2){
                        continue;
                    }
                    if(($("span",this).html()+"").indexOf(shortcuts[i].toUpperCase())==-1){
                        $(this).hide();
                    }else{
                        $(this).show();
                        $("input[type=checkbox]",this).attr("checked","checked");
                        break;
                    }
                }
            }else{
                $(this).hide();
            }
        });
    }
</script>
