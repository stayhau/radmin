<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="创建隐藏渠道" scope="request"/>
<c:set var="_underLogChannel" value="active" scope="request"/>
<c:set var="_event" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
		<div class="page-header"><h1>创建/修改隐藏渠道</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}logchannel/save" method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${form.id}">
				<input name="_queryString" type="hidden" value="${param.queryString}">

				<div class="control-group required-field">
					<label class="control-label">应用名:</label>
					<div class="controls">
						<select id="sel_app" name="pkgName" class="input-small" onchange="javascript:changeEvent();">
							<option value="0">请选择</option>
							<c:forEach items="${apps}" var="app">
								<option value="${app.pkgName}">${app.app}</option>
							</c:forEach>
						</select>
						<script>document.getElementById("sel_app").value="${empty form.pkgName?0:form.pkgName}"</script>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">渠道:</label>
					<div class="controls">
						<div class="control-search-bar">
							<input id="_channel_kw" name="_channel_kw" class="input-medium">
							<input class="btn" type="button" onclick="javascript:searchSelected('_channel_kw','_channels');" value="搜索">
							<input class="btn" type="button" onclick="javascript:showSelected('_channels');" value="选择项">
							<input type="checkbox" onchange="javascript:toggleAllSelected('_channels', this);">全选/全不选
						</div>
						<div id="_channels">
						</div>
					</div>
				</div>
				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script>
	function changeEvent(){
		var appId = $("#sel_app").val();
		<c:forEach var="entry" items="${appChannelsMap}">
		if("${entry.key}"==appId){
			$("#_channels").empty();
			<c:forEach var="from" items="${entry.value}">
			$("#_channels").append("<span class='selLabel'><input ${root:contains(form.fromsObject, from)?'checked=\'checked\'':''} type='checkbox' name='fromsObject' value='${from}'><span>${from}</span></span>");
			</c:forEach>
		}
		</c:forEach>
	}
	window.onload=function(){
		changeEvent();
	}
	function searchSelected(kwElemId, resultDivId){
		var kw = $("#"+kwElemId).val();
		$("#"+resultDivId+" .selLabel").each(function(){
			if(($("span",this).html()+"").indexOf(kw)==-1){
				$(this).hide();
			}else{
				$(this).show();
			}
		});
	}
	function showSelected(resultDivId){
		$("#"+resultDivId+" .selLabel").each(function(){
			if($("input[type=checkbox]",this)[0].checked){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
	}
	function toggleAllSelected(resultDivId, elem){
		$("#"+resultDivId+" .selLabel").each(function(){
			if($(this).is(":visible")){
				$($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
			}
		});
	}
	function searchAndSelect(kwElemId, resultDivId){
		var kw = $("#"+kwElemId).val();
		var shortcuts = kw.split(/[^a-zA-Z]+/);
		$("#"+resultDivId+" .selLabel").each(function(){
			if(shortcuts.length>0){
				for(var i=0;i<shortcuts.length;i++){
					if(shortcuts[i].length!=2){
						continue;
					}
					if(($("span",this).html()+"").indexOf(shortcuts[i].toUpperCase())==-1){
						$(this).hide();
					}else{
						$(this).show();
						$("input[type=checkbox]",this).attr("checked","checked");
						break;
					}
				}
			}else{
				$(this).hide();
			}
		});
	}
	showSelected("_channels");
</script>


