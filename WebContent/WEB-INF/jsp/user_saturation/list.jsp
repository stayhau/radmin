<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="用户质量报表" scope="request"/>
<c:set var="_underUserSaturation" value="active" scope="request"/>
<c:set var="_userSaturation" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>用户质量报表</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}user_saturation/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>日期</th>
							<th>渠道</th>
							<th>活跃用户数</th>
							<th>不开展业务数</th>
							<th>不饱和</th>
							<th>饱和</th>
							<th>开展业务数</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.channel==""?"unknown":data.channel)}</td>
								<td>${fn:escapeXml(data.active)}</td>
								<td>${fn:escapeXml(data.sat0)}</td>
								<td>${fn:escapeXml(data.sat1)}</td>
								<td>${fn:escapeXml(data.sat2)}</td>
								<td>${fn:escapeXml(data.count)}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<c:import url="../theme/${_theme}/toolbar.jsp">
				</c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}user_saturation/list";
		document.getElementById("search_form").submit();
	}
	function doExport(){
		document.getElementById("search_form").action="${basePath}user_saturation/export";
		document.getElementById("search_form").submit();
	}
</script>