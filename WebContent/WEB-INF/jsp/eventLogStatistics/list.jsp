<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="日志上报统计" scope="request"/>
<c:set var="_underEventLogStatistics" value="active" scope="request"/>
<c:set var="_event" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>日志上报统计</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}eventLogStatistics/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>日期</th>
							<th>产品</th>
							<th>事件名称</th>
							<th>渠道</th>
							<th>国家</th>
							<th>新增</th>
							<th>日活</th>
							<th>独立IP</th>
							<th>手机</th>
							<th>平板</th>
							<th>排重新增</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.app==""?"unknown":data.app)}</td>
								<td>${fn:escapeXml(data.eventName==""?"unknown":data.eventName)}</td>
								<td>${fn:escapeXml(data.channel==""?"unknown":data.channel)}</td>
								<td>${fn:escapeXml(countryMap[data.geo]==null?"unknown":countryMap[data.geo].chineseName)}</td>
								<td>${fn:escapeXml(data.newAdd)}</td>
								<td>${fn:escapeXml(data.active)}</td>
								<td>${fn:escapeXml(data.ipCount)}</td>
								<td>${fn:escapeXml(data.phone)}</td>
								<td>${fn:escapeXml(data.tablet)}</td>
								<td>${fn:escapeXml(data.distinctAdd)}</td>
							</tr>
						</c:forEach>
						<tr>
							<td>合计</td>
							<td>合计</td>
							<td>合计</td>
							<td>合计</td>
							<td>合计</td>
							<td>${fn:escapeXml(sum.newAdd)}</td>
							<td>${fn:escapeXml(sum.active)}</td>
							<td>${fn:escapeXml(sum.ipCount)}</td>
							<td>${fn:escapeXml(sum.phone)}</td>
							<td>${fn:escapeXml(sum.tablet)}</td>
							<td>${fn:escapeXml(sum.distinctAdd)}</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<c:import url="../theme/${_theme}/toolbar.jsp">
				</c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}eventLogStatistics/list";
		document.getElementById("search_form").submit();
	}
</script>