<%@ include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css"/>
<div id="search">
    <form id="search_form" method="post"
          class="form-inline pull-left" action="">
         <label>产品:</label>
        <select name="app" id="product_sel" class="input-small" onclick="javascript:changeEvent();">
            <option value="">全部</option>
            <c:forEach items="${apps}" var="app">
                <option value="${app.pkgName}">${app.app}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("product_sel").value = '${root:defVal(param.app,"")}';
        </script>
        <label>事件:</label>
        <select name="eventName" id="eventName_sel" class="input-small">
            <option value="">全部</option>

        </select>

        <label>渠道:</label>
        <select name="channel" id="channel_sel" class="input-small">
            <option value="all">全部</option>

        </select>

        <label>国家:</label>
        <select name="country" id="country_sel" class="input-small">
            <option value="">全部</option>
            <c:forEach items="${countries}" var="c">
                <option value="${c.shortcut}">${c.firstLetterOfChineseName}-${c.chineseName}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("country_sel").value = '${root:defVal(param.country,"")}';
        </script>
        <label>开始时间:</label>
        <input value="${root:defVal(param.startTime,root:yesterdayString("yyyy/MM/dd"))}" type="text"
               name="startTime" class="input-small" id="startTime"/>
        <label>截止时间:</label>
        <input value="${root:defVal(param.endTime,root:yesterdayString("yyyy/MM/dd"))}" type="text" name="endTime"
               class="input-small" id="endTime"/>
        <input type="button" class="btn btn-success" value="查询" onclick="javascript:doQuery()"/>
    </form>
</div>
<script type="text/javascript">
    function changeEvent(){
        var appId = $("#product_sel").val();
        <c:forEach var="app" items="${apps}">
        if("${app.pkgName}"==appId){
            $("#eventName_sel").empty();
            $("#eventName_sel").append("<option value=''>全部</option>");

            <c:forEach var="event" items="${app.eventsObject}">
            $("#eventName_sel").append("<option value='${event.ename}'>${event.eventName}</option>");
            </c:forEach>

        }
        </c:forEach>
        <c:forEach var="entry" items="${appChannelsMap}">
        if("${entry.key}"==appId){
            $("#channel_sel").empty();
            $("#channel_sel").append("<option value='all'>全部</option>");
            <c:forEach var="from" items="${entry.value}">
            var channels=[];
            <c:forEach var="channel" items="${logChannels[entry.key].fromsObject}">
            channels.push("${channel}");
            </c:forEach>
            if(channels.indexOf("${from}")>=0){
                //hidden channel
            }else{
                if(${from=='unknown'}){
                    $("#channel_sel").append("<option value=''>${from}</option>");
                }else{
                    $("#channel_sel").append("<option value='${from}'>${from}</option>");
                }
            }
            </c:forEach>

        }
        </c:forEach>
    }

</script>
<script type="text/javascript">
    window.onload=function(){
        changeEvent();
        document.getElementById("eventName_sel").value = '${root:defVal(param.eventName,"")}';
        document.getElementById("channel_sel").value = '${root:defVal(param.channel,"all")}';
    }
</script>

