<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="录入数据" scope="request"/>
<c:set var="_underStatisticsData" value="active" scope="request"/>
<c:set var="_newStatistics" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>线上SDK注册统计管理</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}incomeData/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th></th>
							<th>编号</th>
							<th>日期</th>
							<th>产品</th>
							<th>渠道</th>
							<th>总注册量</th>
							<th>高价</th>
							<th>中价</th>
							<th>普通</th>
							<th>原高价</th>
							<th>高价转化率</th>
							<th>原中价</th>
							<th>中价转化率</th>
							<th>原普通</th>
							<th>普通转化率</th>
							<th>平板活跃</th>
							<th>手机活跃</th>
							<th>平板新增</th>
							<th>手机新增</th>
							<th>录入状态</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td class="checkbox_td">
									<input type="checkbox" name="ids" id="ids" value="${data.id}"/>
								</td>
								<td>${fn:escapeXml(data.id)}</td>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.product)}</td>
								<td>${fn:escapeXml(data.channel)}</td>
								<td>${fn:escapeXml(data.totalRegister)}</td>
								<td>${fn:escapeXml(data.newHighPrice)}</td>
								<td>${fn:escapeXml(data.newMiddlePrice)}</td>
								<td>${fn:escapeXml(data.newNormalPrice)}</td>
								<td>${fn:escapeXml(data.highPrice)}</td>
								<td>${fn:escapeXml(data.highConversion/10)}%</td>
								<td>${fn:escapeXml(data.middlePrice)}</td>
								<td>${fn:escapeXml(data.middleConversion/10)}%</td>
								<td>${fn:escapeXml(data.normalPrice)}</td>
								<td>${fn:escapeXml(data.normalConversion/10)}%</td>
								<td>${fn:escapeXml(data.tabletActive)}</td>
								<td>${fn:escapeXml(data.phoneActive)}</td>
								<td>${fn:escapeXml(data.tabletAdd)}</td>
								<td>${fn:escapeXml(data.phoneAdd)}</td>
								<td>${fn:escapeXml(data.status==0?"未录入":(data.status==1?"成功":"录入失败"))}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<input onclick="javascript:checkAllBoxes('checkbox_td',this);" type="checkbox" id="checkALL" name="_action_checkAll">全选
				<c:import url="../theme/${_theme}/toolbar.jsp">
					<c:param name="importData">${basePath}statistics/importData</c:param>
					<c:param name="modify">${basePath}statistics/modify</c:param>
				</c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}statistics/list";
		document.getElementById("search_form").submit();
	}
	function checkAllBoxes(result,elem){
		$(" ."+result).each(function(){
			if($(this).is(":visible")){
				$($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
			}
		});
	}
</script>