<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="修改统计数据" scope="request"/>
<c:set var="_underStatisticsData" value="active" scope="request"/>
<c:set var="_newStatistics" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
		<div class="page-header"><h1>修改统计数据</h1></div>
		<div id="pageContent">
			<c:import url="../theme/${_theme}/errors.jsp"></c:import>
			<form action="${basePath}statistics/save" method="post" class="form-horizontal" enctype="multipart/form-data">
				<input name="id" type="hidden" value="${form.id}">
				<input name="channel" type="hidden" value="${form.channel}">
				<input name="_queryString" type="hidden" value="${queryString}">

				<div class="control-group required-field">
					<label class="control-label">高价区转化率:</label>
					<div class="controls">
						<input name="highConversion" value="${fn:escapeXml(cc==null?0.1:cc.highConversion/1000)}" type="text" class="input-large">
						<span class="remark">填小数值，如转化率25%，则填0.25</span>
					</div>
				</div>

				<div class="control-group required-field">
					<label class="control-label">中价区转化率:</label>
					<div class="controls">
						<input name="middleConversion" value="${fn:escapeXml(cc==null?0.1:cc.middleConversion/1000)}" type="text" class="input-large">
						<span class="remark">填小数值，如转化率25%，则填0.25</span>
					</div>
				</div>
				<div class="control-group required-field">
					<label class="control-label">低价区转化率:</label>
					<div class="controls">
						<input name="normalConversion" value="${fn:escapeXml(cc==null?0.1:cc.normalConversion/1000)}" type="text" class="input-large">
						<span class="remark">填小数值，如转化率25%，则填0.25</span>
					</div>
				</div>

				<div class="form-actions">
				  <input class="btn btn-primary" type="submit" value="保存">
				  <button type="button" class="btn" onclick="javascript:history.go(-1)">取消</button>
				</div>
			</form>
        </div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>

