<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="阅读数据" scope="request"/>
<c:set var="_ungerreport" value="active" scope="request"/>
<c:set var="_report" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>

<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css" />
<!-- main content -->
<div class="page-header"><h1>阅读数据</h1></div>
<div id="search">
    <form id="search_form" method="post"
          class="form-inline pull-left"  action="${basePath}report/list">
         <label>状态:</label>
        <select name="status" id="product_sel" class="input-small">
        <option value="0">全部</option>
        <option value="5">已完成</option>
        <option value="1">生效</option>
        <option value="2">审核中</option>
        </select>
        <label>广告主渠道:</label>
        <select name="adChannel" id="channel_sel" class="input-small" style="width: 200px;">
            <option value="">全部</option>
            <c:forEach items="${adcs}" var="channel">
                <option value="${channel}">${channel}</option>
            </c:forEach>
        </select>
        <script type="text/javascript">
            document.getElementById("channel_sel").value = '${root:defVal(param.adChannel,"")}';
            document.getElementById("product_sel").value = '${root:defVal(param.status,"0")}';
        </script>
        <!-- 
        <label>开始时间:</label>
        <input value="${root:defVal(param.startTime,root:yesterdayString("yyyy/MM/dd"))}" type="text"
               name="startTime" class="input-small" id="startTime"/>
        <label>截止时间:</label>
        <input value="${root:defVal(param.endTime,root:yesterdayString("yyyy/MM/dd"))}" type="text" name="endTime"
               class="input-small" id="endTime"/> -->
        <input type="button" id="btn" class="btn btn-success" value="查询" />
        <input type="button" id="exl"  class="btn btn-success" value="导出" />
    </form>
</div>

        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${empty datas}">
						<tr>
							<td>没有任务记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>ID</th>
							<th>地址</th>
							<th>阅读数</th>
							<th>目标阅读量</th>
							<th>完成比率</th>
							<th>当前阅读量</th>
							<th>单价</th>
							<th>广告主</th>
							<th>备注</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr>
								<td>${data.offerId}</td>
								<td>${fn:escapeXml(data.url)}</td>
								<td>${fn:escapeXml(data.readNum)}</td>
								<td>${fn:escapeXml(data.readNumx)}</td>
								<td ><fmt:formatNumber type="number" value="${data.readNum/data.readNumx*100 }" pattern="0.00" maxFractionDigits="2"/>%</td>
								<td >${fn:escapeXml(data.readedNum)}</td>
								<td >${data.payout}</td>
								<td >${data.adChannel}</td>
								<td >${data.remark}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>

<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
$(function(){
	$("#exl").click(function(){
		$("#search_form").attr("action","${basePath}report/export");
		$("#search_form").submit();
	});
	
	$("#btn").click(function(){
		$("#search_form").attr("action","${basePath}report/list");
		$("#search_form").submit();
	});
	
})

</script>
<script>

$( "#startTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
	}});
$( "#endTime" ).datepicker( {
	onClose: function( selectedDate ) {
		$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
	}} );
</script>