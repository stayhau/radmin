<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="业务包管理" scope="request"/>
<c:set var="_underjarupdate" value="active" scope="request"/>
<c:set var="_policyManager" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<link rel="stylesheet" href="${basePath}static/jquery/jquery-ui.css"/>
<!-- main content -->
<div class="page-header"><h1>业务包管理</h1></div>

<br/><br/>
<div id="list">
    <table class="table table-bordered table-striped table-hover">
        <c:choose>
            <c:when test="${empty datas}">
                <tr>
                    <td>没有任务记录!</td>
                </tr>
            </c:when>
            <c:otherwise>
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>名称</th>
                    <th>版本</th>
                    <th>地址</th>
                    <th>dexName</th>
                    <th>className</th>
                    <th>tempName</th>
                    <th>ival</th>
                    <th>详情</th>
                    <th>操作</th>
                </tr>
                <c:forEach var="data" items="${datas}" varStatus="it">
                    <tr>
                        <td class="checkbox_td">
                            <input type="checkbox" name="ids" value="${data.jarId}"/>
                        </td>
                        <td>${data.jarId}</td>
                        <td>${fn:escapeXml(data.jarName)}</td>
                        <td>${fn:escapeXml(data.jarVersion)}</td>
                        <td>${data.jarUrl}</td>
                        <td>${data.dexName}</td>
                        <td>${data.className}</td>
                        <td>${data.tempName}</td>
                        <td>${data.ival}</td>
                        <td class="operation operand1">
                            <a class="btn btn-small btn-info"
                               onclick="javascript:showDetail(${data.jarId},this);return false;"> 详情 </a>
                        </td>
                        <td>
                            <input id="${data.jarId}" type="checkbox"
                                   name="my-checkbox" ${data.status==1?'checked':'unchecked'}>
                        </td>
                    </tr>
                    <tr id="detail_${data.jarId}" style="display: none">
                        <td></td>
                        <td colspan="10">
                            <ul>
                                <li><strong>创建时间：</strong><fmt:formatDate value="${data.createTime}"
                                                                          pattern="yyyy/MM/dd HH:mm:ss"
                                                                          var="gmtCreate"/>${gmtCreate}</li>
                                <li><strong>MD5：</strong>${data.jarMd5}</li>
                                <li><strong>渠道：</strong>${data.channelId}</li>
                                <li><strong>备注：</strong>${data.remark}</li>
                            </ul>
                        </td>
                    </tr>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </table>
</div>
<div class="row-fluid">
    <div class="span4 toolbar">
        <c:import url="../theme/${_theme}/toolbar.jsp">
            <c:param name="create">${basePath}jarupdate/create</c:param>
            <c:param name="delete">${basePath}jarupdate/delete</c:param>
            <c:param name="modify">${basePath}jarupdate/modify</c:param>
        </c:import>
    </div>
    <div class="span8 paginator">
        <c:import url="../theme/${_theme}/paginator.jsp"></c:import>
    </div>
</div>
<form id="paginator_form" method="post" style="display: none">
    <input type="hidden" name="debug" value=""/>

</form>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script>

    var paginator_form = document.getElementById("paginator_form");

    function gotoPage(page) {
        paginator_form.action = "/jarupdate/list";
        paginator_form.submit();
    }

    function changeStatusWithItemAjax(id, path, confirmMsg, state) {
        if (confirmMsg && confirm(confirmMsg)) {
            var request = $.ajax({
                type: "POST",
                url: path,
                data: {
                    id: id,
                    status: state
                }
            });
            request.done(function (msg) {
                var result = eval(msg);
                if (result.success) {
                    gotoPage($("#paginator .active a").html());
                    $("#" + id).bootstrapSwitch("state", state, true);
                } else {
                    alert(result.msg);
                    $("#" + id).bootstrapSwitch("state", !state, true);
                }
            });
            request.fail(function (msg) {
                $("#" + id).bootstrapSwitch("state", !state, true);
                alert("异常");
            });
        } else {
            $("#" + id).bootstrapSwitch("state", !state, true);
        }
    }

    $(function () {
        $("[name='my-checkbox']").on('switchChange.bootstrapSwitch', function (event, state) {
            var id = this.getAttribute("id");
            var path = "${basePath}jarupdate/changeStatus";
            changeStatusWithItemAjax(id, path, "确认修改？", state);
        });
    });
</script>