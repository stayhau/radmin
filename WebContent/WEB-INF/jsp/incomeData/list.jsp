<%@include file="../include/common.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="_pageTitle" value="录入数据" scope="request"/>
<c:set var="_underIncomeData" value="active" scope="request"/>
<c:set var="_incomeData" value="in" scope="request"/>
<c:import url="../theme/${_theme}/header.jsp"></c:import>
<!-- main content -->
<div class="page-header"><h1>录入推广数据</h1></div>
			<c:import url="search.jsp">
				<c:param name="action">${basePath}incomeData/list</c:param>
			</c:import>
        <br/><br/>
		<div id="list">
			<table class="table table-bordered table-striped table-hover">
				<c:choose>
					<c:when test="${not hasDatas}">
						<tr>
							<td>没有应用记录!</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th></th>
							<th>编号</th>
							<th>日期</th>
							<th>渠道</th>
							<th>产品</th>
							<th>总量</th>
							<th>高价</th>
							<th>中价</th>
							<th>普通</th>
							<th>实际转化率</th>
							<th>原总量</th>
							<th>原高价</th>
							<th>原中价</th>
							<th>原普通</th>
							<th>NA</th>
							<th>默认转化率</th>
							<th>价格</th>
							<th>录入状态</th>
						</tr>
						<c:forEach var="data" items="${datas}" varStatus="it">
							<tr style="${data.status==-1?'color:red':''}">
								<td class="checkbox_td">
									<input type="checkbox" name="ids" id="ids" value="${data.id}"/>
								</td>
								<td>${fn:escapeXml(data.id)}</td>
								<td>${fn:escapeXml(data.date)}</td>
								<td>${fn:escapeXml(data.channel)}</td>
								<td>${fn:escapeXml(data.product)}</td>
								<td>${fn:escapeXml(data.newTotal)}</td>
								<td>${fn:escapeXml(data.newHigh)}</td>
								<td>${fn:escapeXml(data.newMiddle)}</td>
								<td>${fn:escapeXml(data.newNormal)}</td>
								<td>${fn:escapeXml(data.actualConversion)}%</td>
								<td>${fn:escapeXml(data.oldTotal)}</td>
								<td>${fn:escapeXml(data.oldHigh)}</td>
								<td>${fn:escapeXml(data.oldMiddle)}</td>
								<td>${fn:escapeXml(data.oldNormal)}</td>
								<td>${fn:escapeXml(data.oldNA)}</td>
								<td>${fn:escapeXml(data.defaultConversion/10)}%</td>
								<td>${fn:escapeXml(data.income/100)}</td>
								<td>${fn:escapeXml(data.status==0?"未录入":(data.status==1?"成功":"录入失败"))}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span4 toolbar">
				<input onclick="javascript:checkAllBoxes('checkbox_td',this);" type="checkbox" id="checkALL" name="_action_checkAll">全选
				<c:import url="../theme/${_theme}/toolbar.jsp">
					<c:param name="importCsv">${basePath}incomeData/create</c:param>
					<c:param name="importData">${basePath}incomeData/importData</c:param>
					<c:param name="modify">${basePath}incomeData/edit</c:param>
					<c:param name="delete">${basePath}incomeData/delete</c:param>
				</c:import>
			</div>
			<div class="span8 paginator">
				<c:import url="../theme/${_theme}/paginator.jsp"></c:import>
			</div>
		</div>
<!-- end main content -->
<c:import url="../theme/${_theme}/footer.jsp"></c:import>
<script src="${basePath}static/jquery/jquery-ui.js"></script>
<script src="${basePath}static/jquery/jquery.ui.datepicker-zh-TW.js"></script>
<script>
	$( "#startTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#endTime" ).datepicker( "option", "minDate", selectedDate );
		}} );
	$( "#endTime" ).datepicker( {
		onClose: function( selectedDate ) {
			$( "#startTime" ).datepicker( "option", "maxDate", selectedDate );
		}} );
	function doQuery(){
		document.getElementById("search_form").action="${basePath}incomeData/list";
		document.getElementById("search_form").submit();
	}
	function checkAllBoxes(result,elem){
		$(" ."+result).each(function(){
			if($(this).is(":visible")){
				$($("input[type=checkbox]",this)[0]).attr("checked",elem.checked);
			}
		});
	}
</script>