<%@page import="com.scavenger.business.common.SystemUserType"%>
<%@page import="com.scavenger.business.common.LoginContext"%>
<%@ include file="../../include/common.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>${_pageTitle}</title>
    <link rel="icon" type="image/png" href="${basePath}static/common/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${basePath}static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${basePath}static/bootstrap/css/bootstrap-switch.css" rel="stylesheet">
    <link href="${basePath}static/theme/${_theme}/global.css?v=201310161618" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 55px;
        padding-bottom: 0px;
        color: #0d62e5;
      }
      .sidebar-nav {
        padding: 0px 0;
        background-color:#eeeeee
      }
      /*定义二级菜单样式*/
      .secondmenu a {
        font-size: 14px;
        line-height: 20px;
        color: #4A515B;
        text-align: center;
        background-color: #fff;
      }

    </style>
    <link href="${basePath}static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="${basePath}">运营管理后台</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              <a href="${basePath}logout" class="navbar-link">登出</a>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span2">
          <div class="well sidebar-nav">
              <c:import url="${jspPath}theme/default/func.jsp"/>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span10">
