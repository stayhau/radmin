<%@ include file="../../include/common.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="accordion" id="accordion2">
    <c:if test="${(root:isAdmin() ) or root:systemUserType()==1}">
        <div class="accordion-group">
            <div class="accordion-heading">
			<span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
				运营管理<i class="pull-right"></i>
			</span>
            </div>
            <div id="collapse1" class="accordion-body collapse ${_offerManager1}">
                <ul class="nav nav-list secondmenu">
                    <li class="${_offerManager}"><a href="${basePath}offer/list">Offer管理</a></li>
                    <c:if test="${root:isSelf()}">
                        <li class="${_offerbach}"><a href="${basePath}offerbach/create">Offer批量录入</a></li>
                    </c:if>
                </ul>
            </div>
        </div>

        <c:if test="${root:isAdmin()}">
            <div class="accordion-group">
                <div class="accordion-heading">
			<span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
				报表管理<i class="pull-right"></i>
			</span>
                </div>
                <div id="collapse4" class="accordion-body collapse ${_report}">
                    <c:if test="${root:systemUserType()!=3}">
                        <ul class="nav nav-list secondmenu">
                            <c:if test="${root:isAdmin()}">
                                <%--<li class="${_ungerchannelreport}"><a href="${basePath}report/channel-list">渠道数据</a></li>--%>
                                <%--<li class="${_uncl}"><a href="${basePath}report/user-active">渠道活跃注册数据</a></li>--%>
                                <li class="${_unUserReport}"><a href="${basePath}user-report/list">注册激活统计</a></li>
                                <%--<li class="${_readnumstatistics}"><a href="${basePath}readnumstatistics/list">阅读分布数据</a></li>--%>
                                <li class="${_readIncome}"><a href="${basePath}readincome/list">阅读收益</a></li>
                            </c:if>
                        </ul>
                    </c:if>
                    <c:if test="${root:systemUserType()==3}">
                        <ul class="nav nav-list secondmenu">
                            <li class="${_ungerchannelreport}"><a href="${basePath}report/channel-list">渠道数据</a></li>
                        </ul>
                    </c:if>
                </div>
            </div>
        </c:if>
        <%--<c:if test="${root:isAdmin()}">
        <div class="accordion-group">
            <div class="accordion-heading">
                <span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse21">
                    视频单子管理<i class="pull-right ${_slasemanManager==in?'icon-chevron-right':'icon-chevron-down'}"></i>
                </span>
            </div>
            <div id="collapse21" class="accordion-body collapse ${_vidoofferManager1}">
                <ul class="nav nav-list secondmenu">
                    <li class="${_vidoofferManager}"><a href="${basePath}vidooffer/list">单子管理</a></li>
                    <!-- <li class="${_vidoofferbach}"><a href="${basePath}offerbach/create">单子批量录入</a></li> -->
                </ul>
            </div>
        </div>
        </c:if>--%>

    </c:if>
    <c:if test="${(root:isAdmin())}">
        <c:if test="${root:isAdmin()}">
            <div class="accordion-group">
                <div class="accordion-heading">
			<span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
			策略配置<i class="pull-right"></i>
			</span>
                </div>
                <div id="collapse3" class="accordion-body collapse ${_policyManager}">
                    <ul class="nav nav-list secondmenu">
                        <li class="${_underhostpolicyManager}"><a href="${basePath}hostpolicy/list">更新包策略</a></li>
                            <li class="${_underworkPolicyManager}"><a href="${basePath}policy/list">业务包策略</a></li>
                            <li class="${_underjarupdate}"><a href="${basePath}jarupdate/list">业务包管理</a></li>
                           <%-- <li class="${_vidounderworkPolicyManager}"><a href="${basePath}vidopolicy/list">视频策略</a></li>--%>
                            <li class="${_underblacklist}"><a href="${basePath}blacklist/get-black-list">黑名单管理</a></li>
                            <li class="${_lossManager}"><a href="${basePath}staticloss/list">转化管理</a></li>
                    </ul>
                </div>
            </div>
        </c:if>
        <div class="accordion-group">
            <div class="accordion-heading">
			<span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
				渠道管理<i class="pull-right"></i>
			</span>
            </div>
            <div id="collapse2" class="accordion-body collapse ${_channel}">
                <ul class="nav nav-list secondmenu">
                    <li class="${_underChannel}"><a href="${basePath}channel/list">渠道管理</a></li>
                </ul>
            </div>
        </div>
    </c:if>
    <c:if test="${root:isAdmin()}">
        <div class="accordion-group">
            <div class="accordion-heading">
			<span class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
				系统管理<i class="pull-right"></i>
			</span>
            </div>
            <div id="collapseFive" class="accordion-body collapse ${_sysuser}">
                <ul class="nav nav-list secondmenu">
                    <c:if test="${root:isAdmin()}">
                        <li class="${_underSysUser}"><a href="${basePath}sysuser/list">系统管理</a></li>
                    </c:if>
                    <li class="${_underUserInfo}"><a
                            href="${basePath}sysuser/${isUnderUserInfo?'info_modify':'modify'}/${root:uid()}">用户管理</a>
                    </li>
                </ul>
            </div>
        </div>
    </c:if>


</div>