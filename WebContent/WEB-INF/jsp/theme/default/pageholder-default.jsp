<%@page import="com.scavenger.business.common.PageHolder"%>
<%@ page pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 该页面已经废弃，请勿再使用，可以使用page-holder-default.jsp -->
<%--
参数：
pageHolderName：PageHolder在requestScope里面的名字，默认为"pageHolder"。
--%>
<%
    if(request.getParameter("tip") != null){
    	request.setAttribute("tip", request.getParameter("tip"));
    }
	String pageHolderName = request.getParameter("pageHolderName");
	if(pageHolderName == null){
		pageHolderName = "pageHolder";
	}
	pageContext.setAttribute("pageHolderName", pageHolderName);
	pageContext.setAttribute("pageHolder", request.getAttribute(pageHolderName));
	PageHolder p = (PageHolder)request.getAttribute(pageHolderName);
	int pageS = (p.getPageIndex() / 10) * 10 + 1;
	int pageE = p.getPageIndex() / 10* 10 + 10;
	if(p.getPageIndex() % 10 == 0){
		pageE = p.getPageIndex();
		pageS = p.getPageIndex() - 10 + 1;
	}
	pageE = pageE > p.getPageCount() ? p.getPageCount() : pageE;
	request.setAttribute("pageS", pageS);
	request.setAttribute("pageE", pageE);
%>

<c:if test="${pageHolder.rowCount > 0 }">
<div class="pagination pagination-right">
	<input id="current_page" type="hidden" value="?${pageHolder.pageIndexKey }=${pageHolder.pageIndex }${pageHolder.params }" />
	<ul>
	<c:choose>
		<c:when test="${pageHolder.firstPage }"><a>上一页</a></c:when>
		<c:otherwise>
			<c:url var="firstPage" value="">
				<c:param name="${pageHolder.pageIndexKey }" value="1" />
			</c:url>
			<c:url var="previousPage" value="">
				<c:param name="${pageHolder.pageIndexKey }" value="${pageHolder.pageIndex - 1 }" />
			</c:url>
			<li><a href="${previousPage }${pageHolder.params }">上一页 </a></li> 
		</c:otherwise>
	</c:choose>
	<c:forEach begin="${pageS }" end="${pageE }" var="i" >
		<c:choose>
			<c:when test="${pageHolder.pageIndex == i }">
				<a href="?${pageHolder.pageIndexKey }=${i }${pageHolder.params }" class="this_page">${i }</a>
			</c:when>
			<c:otherwise>
				<a href="?${pageHolder.pageIndexKey }=${i }${pageHolder.params }">${i }</a>
			</c:otherwise>
      	</c:choose>
	</c:forEach>
	<c:if test="${pageE < pageHolder.pageCount}">
		<a href="?${pageHolder.pageIndexKey}=${pageE + 1}${pageHolder.params}" title="">&gt;&gt;</a>
	</c:if>
	<c:choose>
        <c:when test="${pageHolder.lastPage }"><a>下一页</a></c:when>
        <c:otherwise>
			<c:url var="nextPage" value="">
				<c:param name="${pageHolder.pageIndexKey }" value="${pageHolder.pageIndex + 1 }"/>
			</c:url>
			<c:url var="lastPage" value="">
				<c:param name="${pageHolder.pageIndexKey }" value="${pageHolder.pageCount }"/>
			</c:url>
			<a href="${nextPage }${pageHolder.params }">下一页</a>
		</c:otherwise>
	</c:choose>
	</ul>
	共${pageHolder.pageCount }页
	转到第&nbsp;<input id="target_page" type="text" style="width: 36px;" value="${pageHolder.pageIndex }" maxpage="${pageHolder.pageCount }">&nbsp;页
  	<a href="javascript:;"><button id="go" class="btn btn-success">GO</button></a>
</div>
</c:if>