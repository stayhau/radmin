<%@ include file="../../include/common.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
      </div>
      </div><!--/row-->

    </div><!--/.fluid-container-->
    <script type="text/javascript" src="${basePath}static/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="${basePath}static/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="${basePath}static/bootstrap/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="${basePath}static/theme/${_theme}/global.js?v=201310171041"></script>
    <script type="text/javascript">
    $(function() {
        $("[name='my-checkbox']").bootstrapSwitch({
            size:"mini",
            offColor:'danger',
            onText:'生效',
            offText:'失效'
        });
        $("div[id^='collapse']").collapse({
            toggle: false
        });
    });
    </script>
    
  </body>
</html>